<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$data = json_decode(base64_decode($_GET['status']), true);

// echo "difference = " . mktime(date('h, i, s, m, d, y')) - $data['created'];
// exit;

include_once 'includes/paths.php';

include(ABS_PATH . 'classes/curl.php');

session_start();

// include("general.php");
// adding one month to current date
//duration obtained from API
$duration = $_SESSION['duration'];
$date1 = date("Y-m-d");
$time = strtotime($date1);
$final = date("Y-m-d", strtotime("$duration day", $time));


//invoice details
if (isset($_SESSION['user']) && isset($_SESSION['new_plan'])) {
	$curl =  new curl();
	$user_data = [
		'action' => "updateUser",
		'email' => $_SESSION['user']['email'],
		'productId' => $data['product']['id'],
		'status' => "subscribed",
		'adminId' => ADMIN_ID,
		'expirationDate' => $final,
		'stripeEmail' => $_SESSION['stripe']['email'],
		'stripeCustomerId' => $_SESSION['stripe']['stripe_customer']
	];

	$output = $curl->curl_call($user_data);
	$output = json_decode($output, true);

	// create an invoice
	$in_data = [
		"action" => "createInvoice",
		"adminId" => ADMIN_ID,
		"userId" => $output['result']['userId'],
		"amount" => $data['product']['amount'],
		"currency" => $data['product']['currency'],
		"productId" => $data['product']['id'],
		"VATAmount" => $data['product']['vat'],
		"VATFactor" => $data['product']['vat_factor'],
		"paymentStatus" => "1",
		"paymentIntent" => $_SESSION['payment_intent'],
		"dateOfPurchase" => date('Y-m-d')
	];

	$invoice_data = json_decode($curl->curl_call($in_data), true);

	if ($_SESSION['user']['preferredLanguage'] === 'EN')
		$msg = "Please click OK to login again for affecting your changes.";
	else
		$msg = 	"Cliquez sur OK pour vous reconnecter en prenant en compte les modifications.";
	if ($output['status'] === 'success') {
		//header("location: subscription.php");
		echo '<script>alert("' . $msg . '");window.location.href="index.php";</script>';
	} else {
		echo "something went wrong!";
	}
} else {
	header("location: index.php");
}

// function for updating user
