<?php

error_reporting(0);

class curl
{

	public static function curl_call($data)
	{

		if (isset($data)) {
			if ($data != NULL) {
				$data_string = json_encode($data);
				$len = strlen($data_string);
			}

			$agents = array();
			$agents[] = "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0";
			$agents[] = "Mozilla/5.0 (Windows NT 6.1; rv:31.0) Gecko/20100101 Firefox/31.0";
			$agents[] = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36";
			$agents[] = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)";
			$agent = $agents[array_rand($agents)];

			$ch = curl_init();

			// curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 100);
			// curl_setopt($ch, CURLOPT_ENCODING, "");
			// curl_setopt($ch, CURLOPT_USERAGENT, $agent);
			// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			// curl_setopt($ch, CURLOPT_TIMEOUT, 400);



			curl_setopt($ch, CURLOPT_URL, "https://api.spella.com/1");
			// curl_setopt($ch, CURLOPT_URL, "https://restcountries.eu/rest/v2/all");

			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Connection: Keep-Alive',
				'Content-Length: ' . $len
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$server_output = curl_exec($ch);
			curl_close($ch);
			return $server_output;
		}
	}
}
