<?php
include("general.php");
require_once '3D-stripe/stripe-php/init.php';
require_once 'includes/stripe_sca_config.php';
require_once 'api/invoice.php';



if (empty($_SESSION['user']['user_id'])) {
	header("location: index.php");
	exit;
}

$invoice = get_invoice();
if ($invoice)
	$invoice = json_decode($invoice, true);

if ($_POST['request'] == 'getdata') {
	if (!empty($_SESSION['user'])) {
		echo json_encode($_SESSION['user']);
		exit;
	} else {
		return 0;
		exit;
	}
}


// print_r($invoice['result']);
// exit;
require_once('smarty-2.6.31/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->template_dir = 'templates';
$smarty->compile_dir = 'tmp';

// include('templates/loader.html');
include("home_header.php");
include("home_body_header.php");


$days_remaining = strtotime($_SESSION['user']['productExpiry']) - strtotime(Date('Y-m-d'));
$days_remaining = intval($days_remaining / 60 / 60 / 24); // converting to days

$smarty->assign('name', $_SESSION['user']['name']);
$smarty->assign('productid', $_SESSION['user']['productSubscribed']);
$smarty->assign('plan_expiry', $_SESSION['user']['productExpiry']);
$smarty->assign('autorenew', $_SESSION['user']['autorenew']);
$smarty->assign('userid', $_SESSION['user']['user_id']);
$smarty->assign('user_email', $_SESSION['user']['email']);
$smarty->assign('product_status', $_SESSION['user']['product_status']);
$smarty->assign('subscribed_date', $_SESSION['user']['subscribedDate']);
$smarty->assign('plan_expiry_days', $days_remaining);
$smarty->assign('invoices', $invoice['result']);

//$smarty->assign('plan_expiry', "20-11-2019");


$smarty->display('subscription.tpl');
