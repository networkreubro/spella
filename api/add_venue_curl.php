<?php
include('general_path.php');
include(ABS_PATH . 'classes/curl.php');
include(ABS_PATH . 'general.php');



$action = $_POST['action'];


switch ($action) {
	case 'searchWeb':
		$output = findpages($_POST);
		echo $output;
		break;

	case 'getServices':
		$output = getServices();
		echo $output;
		break;
	case 'getVenueTypes':
		$output = getVenueTypes($_POST);
		echo $output;

	case 'validatePage':
		$output = validatePage($_POST);
		echo $output;
		break;
	case 'createVenue':
		$output = createVenue($_POST);
		echo $output;
		break;
	case 'validateVenue':
		$output = validateVenue($_POST);
		echo $output;
		break;
	case 'getEditPage':
		$output = getVenueTypesForEditPage($_POST);
		echo $output;
		break;
	case 'getImages':
		$output = getImageOfWebsite($_POST);
		echo $output;
		break;

	default:
		echo "Nothing as of yet";
		break;
}


function getImageOfWebsite($data)
{

	$url = 'http://api.screenshotlayer.com/api/capture?access_key=2d0b9231d83bfee949a17fe92ec18ccc&url=' . urlencode($data['url']) . '&viewport=1440x900&width=320&format=JPG';
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$return = curl_exec($ch);
	curl_close($ch);

	return base64_encode($return);
}





function createVenue($data)
{


	$curl = new curl();
	$usable = $data;
	if (!empty($usable)) {
		$output = $curl->curl_call($usable);
		return $output;
		exit;
	} else {
		return false;
	}
}

function validateVenue($data)
{

	$curl = new curl();
	$usable = $data;
	if (!empty($data)) {
		$output = $curl->curl_call($usable);
		return $output;
		exit;
	} else {
		return false;
	}
}


function validatePage($data)
{

	$curl = new curl();
	$usable = $data;
	if (!empty($usable)) {
		$output = $curl->curl_call($usable);
		return $output;
		exit;
	} else {
		echo "error";
	}
}




function findpages($data)
{

	$data_use = $data;

	$curl = new curl();

	$output = $curl->curl_call($data_use);

	return $output;
}


function getServices()
{
	$output = file_get_contents("https://api.spella.com/services/services.json");

	// echo "12525645";exit;
	if ($output) {
		return $output;
	} else {
		echo "no data";
	}
}




function getVenueTypes()
{

	$str = file_get_contents("https://api.spella.com/services/venuetypes.json");


	$json = json_decode($str, true);


	$arr = array();
	$i = 0;

	foreach ($json as $key => $value) {
		foreach ($value as $key1 => $value1) {
			$arr[$i] = $value1['locales'][$_POST['lang']];
			$i++;
		}
	}
	$output = json_encode($arr);
	echo $output;
	exit;
}



function getVenueTypesForEditPage()
{

	$str = file_get_contents("https://api.spella.com/services/venuetypes.json");

	return $str;
	exit;
}
