<?php
include('general_path.php');
include(ABS_PATH . 'classes/curl.php');

$curl = new curl();


if ($_POST['userId']) {
	$data = $_POST;
	$output = $curl->curl_call($data);

	$venueList = json_decode($output, True);


	if ($venueList['result']['graph'] == NULL) {
		echo 0;
	} else {
		// $i=0;
		// foreach($venueList as $key=>$value)
		// 	{

		// 		foreach($value['venues'] as $key1=>$value1)
		// 		{
		// 			$return_array[$i]=$value1;
		// 			$i++;
		// 		}

		// 	}
		echo $output;
		//echo json_encode($return_array);
		exit;
	}
}

function getVenueCount($user_id)
{
	$curl = new curl();

	$params = [
		'action' => 'getVenues',
		'userId' => $user_id
	];

	$venues = json_decode($curl->curl_call($params), true);

	return count($venues['result']['graph']);
}
function getCompetitorCount($user_id)
{
	$curl = new curl();

	$params = [
		'action' => 'getVenues',
		'userId' => $user_id
	];

	$venues = json_decode($curl->curl_call($params), true);

	return ((count($venues['result']['venues']))-(count($venues['result']['graph'])));
}

function get_products()
{

	$data = [
		'action' => 'listProducts'
	];

	$url = 'https://api.spella.com/1/listProducts';
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$return = curl_exec($ch);
	curl_close($ch);

	return $return;
}
