<?php

if (isset($_POST['action'])) {
    require_once "../includes/paths.php";
}

include(ABS_PATH . 'classes/curl.php');

switch ($_POST['action']) {
    case 'listInvoices':
        echo get_invoice();
        exit;
        break;
    case 'getInvoiceDetails':
        echo get_invoice_details($_POST['invoice_id']);
        exit;
        break;
}


function get_invoice()
{
    if (!empty($_SESSION['user']['user_id'])) {
        $curl = new curl();

        $data['action'] = "listInvoices";
        $data['adminId'] = ADMIN_ID;
        $data['userId'] = $_SESSION['user']['user_id'];
        return $curl->curl_call($data);
    }
    return false;
}

function get_invoice_details($invoice_id)
{
    $curl = new curl();
    // echo json_encode(["bfehb" => "safsd"]);
    $data['action'] = 'readInvoice';
    $data['adminId'] = ADMIN_ID;
    $data['invoiceId'] = $invoice_id;
    return $curl->curl_call($data);
}
