<?php

session_start();
require_once '../3D-stripe/stripe-php/init.php';
require_once '../includes/stripe_sca_config.php';

$sessionid = "nil";
$session = "";
$customer_id = $_SESSION['stripe']['customer_id'];

if ($_POST["authscreate"] == "spSCSESS345xx") {


  \Stripe\Stripe::setApiKey($stripe['secret_key']);

  try {

    $session = \Stripe\Invoice::all(['limit' => 3,'customer' => $customer_id]);
  } catch (\Stripe\Error\InvalidRequest $e) {
    catch_errors($e, $dbObj);
    $body = $e->getJsonBody();
    $err  = $body['error'];
  } catch (\Stripe\Error\Authentication $e) {
    catch_errors($e, $dbObj);
    $body = $e->getJsonBody();
    $err  = $body['error'];
  } catch (\Stripe\Error\ApiConnection $e) {
    catch_errors($e, $dbObj);
    $body = $e->getJsonBody();
    $err  = $body['error'];
  } catch (Exception $ex) {
    $err = $ex->getMessage();
  }
}

if ($err) {
  echo $err;
  exit;
}

// $sessionid = $session->id;

// echo $sessionid;
exit;