<?php

session_start();
require_once '../3D-stripe/stripe-php/init.php';
require_once '../includes/stripe_sca_config.php';
require_once "getVenue_curl.php";

// if (!isset($_SESSION['user'])) {
//   // header("location: index.php");
//   // echo "hai";
// }

$sessionid = "nil";
$session_customer_id = "";
$session_customer_email = "";
$session = "";


if ($_POST["authscreate"] == "spSCSESS345xx") {

  $planid = trim($_POST["productId"]);
  // get product details
  $productlist = json_decode(get_products(), true)['result'];
  foreach ($productlist as $key => $val) {
    if ($planid == $val['id']) {
      $product = $val;
      $duration = $val['duration'];
    }
  }

  // echo json_encode($product);
  // exit;

  // check for currenncy of user
  $price = ($_POST['currency'] === 'EN') ? $product['price-USD'] : $product['price-EUR'];

  $_SESSION['new_plan'] = $product;
  $_SESSION['duration'] = $duration;

  \Stripe\Stripe::setApiKey($stripe['secret_key']);


  // check whether user exist or not

  if (empty($_SESSION['stripe']['stripe_customer'])) {
    // create stripe customer
    $customer = \Stripe\Customer::create([
      'email' => $_SESSION['user']['email'],
      'name' => $_SESSION['user']['name'],
      'metadata' => [
        'user_id' => $_SESSION['user']['user_id']
      ]
    ]);

    // Set customer details to session
    $_SESSION['stripe']['stripe_customer'] = $customer->id;
    $_SESSION['stripe']['email'] = $customer->email;
  }

  $price_total = $price;

  if ($_SESSION['user']['countryCode'] == "BE") {
    // calculating VAT for Belgium customers
    $vat = $price * (VAT / 100);

    $price_total = $price + $vat;
  }

  // for yearly billing

  // get number of months based on duration(days)

  $months = intval($duration / 30); //assuming each months is of 30 days
  $price_total *= $months;

  $update_user_data = [
    'created' => mktime(date('h, i, s, m, d, y')),
    "test_date" => date('h, i, s, m, d, y'),
    'product' => [
      'id' => $product['id'],
      'amount' => $price,
      'amount_total' => $price_total,
      'duration' => $product['duration'],
      'vat' => $vat,
      'vat_factor' => VAT,
      'currency' => ($_POST['currency'] == 'EN') ? 'USD' : 'EUR'
    ],
    "user" => [
      "stripe" => [
        "customer" => $customer->id,
        "email" => $customer->email
      ]
    ]
  ];

  $param = rtrim(
    base64_encode(
      json_encode($update_user_data)
    ),
    "="
  );

  $session_data = [
    'payment_method_types' => ['card'],
    'customer' => $_SESSION['stripe']['stripe_customer'],
    'line_items' => [[
      'name' => $product['name-en'],
      'amount' => $price_total * 100,
      'currency' => ($_POST['currency'] == 'EN') ? 'USD' : 'EUR',
      'quantity' => 1
    ]],
    'metadata' => [
      'userId' => $_SESSION['user']['user_id'],
      'user_email' => $_SESSION['user']['email']
    ],
    'success_url' =>  SITEURL . "pay_success.php?status=" . $param,
    'cancel_url' => SITEURL . "pay_fail.php",
  ];

  try {

    $session = \Stripe\Checkout\Session::create($session_data);

    $_SESSION['payment_intent'] = $session->payment_intent;
    $_SESSION['new_plan_price'] = $product['price-EUR'];
  } catch (\Stripe\Error\InvalidRequest $e) {
    catch_errors($e, $dbObj);
    $body = $e->getJsonBody();
    $err  = $body['error'];
  } catch (\Stripe\Error\Authentication $e) {
    catch_errors($e, $dbObj);
    $body = $e->getJsonBody();
    $err  = $body['error'];
  } catch (\Stripe\Error\ApiConnection $e) {
    catch_errors($e, $dbObj);
    $body = $e->getJsonBody();
    $err  = $body['error'];
  } catch (Exception $ex) {
    $err = $ex->getMessage();
  }
}

if ($err) {
  echo $err;
  exit;
}
echo $session->id;
exit;
