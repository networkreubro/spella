<?php
include('general_path.php');
include(ABS_PATH.'classes/curl.php');
include(ABS_PATH.'general.php');


if($_POST)
{
$action=$_POST['action'];


switch($action)
{
	case 'getDates' : 
							$date= date("Y-m-d");
							$fromDate = date("Y-m-d", strtotime("-5 months"));
							$dat=array("current"=>$date,"start"=>$fromDate);
							echo json_encode($dat);
							exit;
							break;

	case 'venueData' 	:  
							$output=venueData($_POST);
							echo $output;
							break;
	
	case 'getMonths'	: 	$output= getMonths($_POST);
							if($output)
							{
								echo json_encode($output);
								exit;
							}
							else{
								echo "No data,error";
								exit;
								}
							break;
	case 'getCustomMonths'	: $output=getCustomMonths($_POST);
								if($output)
							{
								echo json_encode($output);
								exit;
							}
							else{
								echo "No data,error";
								exit;
								}
							break;

	case 'venueDetails'		: $output=getVenueDetails($_POST);
							if ($output) {
								echo $output;
							}
							else{
								echo "No data";exit();
							}
							break;
	case 'getServices'		: echo $output = file_get_contents("https://api.spella.com/services/services.json");
							break;

	case 'default' 	  	: echo "Nothing as of yet";
							break;


}	
}

function getVenueDetails($data){
	$curl= new curl();
	$output=$curl->curl_call($data);
	return $output;
}

function venueData($data)
{

	$curl= new curl();
	$output=$curl->curl_call($data);

	return $output;
}



function getMonths($data)
{

$value= array();

$dates= json_decode($data["dates"],true);



$start    = new DateTime($dates['start']);
$start->modify('first day of this month');
$end      = new DateTime($dates['current']);
$end->modify('first day of next month');
$interval = DateInterval::createFromDateString('1 month');
$period   = new DatePeriod($start, $interval, $end);
$i=0;
foreach ($period as $dt) 
{
    $month[$i] =$dt->format("M") . PHP_EOL; 
    $i++;
}

return $month;
}


function getCustomMonths($data)
{

$value= array();

$dates= json_decode($data["dates"],true);

$start    = new DateTime($dates['startDate']);
$end      = new DateTime($dates['endDate']);
$interval = DateInterval::createFromDateString('month');
$period   = new DatePeriod($start, $interval, $end);
$i=0;
foreach ($period as $dt) 
{
    $month[$i] =$dt->format("M") . PHP_EOL; 
    $i++;
}


return $month;
}