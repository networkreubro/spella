<?php
include('general_path.php');
include(ABS_PATH . 'classes/curl.php');
include(ABS_PATH . 'general.php');


$curl = new curl();

if ($_POST) {
	$action = $_POST['action'];


	switch ($action) {
		case 'getSession':
			echo json_encode($_SESSION['user']);
			break;
		case 'save':
			$fields = array();
			$fields = $_POST;
			save($fields);
			break;
		case 'default':
			echo "Nothing as of yet";
			break;
	}
}





function save($fields)
{
	if (!empty($fields)) {
		//checking the existense of  all the optional fields
		if ($fields['action']) {
			$data['action'] = "updateUser";
			$data['userId'] = $_SESSION['user_id'];
			$data['role'] = "user";
		}
		if ($fields['email']) {
			$data['email'] = $fields['email'];
		}
		if ($fields['name']) {
			$data['name'] = $fields['name'];
		}
		if ($fields['password']) {
			$data['password'] = $fields['password'];
		}
		if ($fields['phone']) {
			$data['cellphone'] = $fields['phone'];
		}
		if ($fields['country']) {
			$data['countryCode'] = $fields['country'];
		}
		if ($fields['pushActive']) {
			$data['pushActive'] = $fields['pushActive'];
		}
		if ($fields['preferredLanguage']) {
			$data['preferredLanguage'] = $fields['preferredLanguage'];
		}
		if ($fields['reportThreshold']) {
			$data['reportThreshold'] = $fields['reportThreshold'];
		}
		if ($fields['reportDaily']) {
			$data['reportDaily'] = $fields['reportDaily'];
		}
		if ($fields['reportWeekly']) {
			$data['reportWeekly'] = $fields['reportWeekly'];
		}
		if ($fields['reportWeekDay']) {
			$data['reportWeekDay'] = $fields['reportWeekDay'];
		}
		if ($fields['reportRange']) {
			$data['reportRange'] = $fields['reportRange'];
		}
		$curl = new curl();
		$output = $curl->curl_call($data);
		$result = array();
		$result = json_decode($output, true);
		if ($result['status'] == 'success') {
			$_SESSION['user']['user_id']			= $result['result']['userId'];
			$_SESSION['user']['user_cellphone']		= $result['result']['cellphone'];
			$_SESSION['user']['name']				= $result['result']['name'];
			$_SESSION['user']['pushActive']			= $result['result']['pushActive'];
			$_SESSION['user']['countryCode']		= $result['result']['countryCode'];
			$_SESSION['user']['role']				= $result['result']['role'];
			$_SESSION['user']['email']				= $result['result']['email'];
			$_SESSION['user']['reportThreshold']		= $result['result']['reportThreshold'];
			$_SESSION['user']['reportDaily']			= $result['result']['reportDaily'];
			$_SESSION['user']['reportWeekly']			= $result['result']['reportWeekly'];
			$_SESSION['user']['reportWeekDay']			= $result['result']['reportWeekDay'];
			$_SESSION['user']['reportRange']			= $result['result']['reportRange'];
		}
		echo $output;
		exit;
	} else {
		echo "error";
	}
}
