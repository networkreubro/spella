<?php

error_reporting(0);

if (!isset($_POST['action'])) {
    echo "invalid request";
    exit;
}

switch ($_POST['action']) {
    case 'getSession':
        echo get_session();
        break;
}


function get_session()
{
    return json_encode(
        $_SESSION
    );
}
