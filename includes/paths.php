<?php
if (!defined('ABS_PATH')) {

	if ($_SERVER['HTTP_HOST'] == 'localhost') {
    	define('ABS_PATH', 'C:/wamp64/www/spella/');
	}
	else{
    	define("ABS_PATH", "/var/www/html/spella/");
	}
}

// admin ID for common use

define('ADMIN_ID', '101BD4D8-7E6D-4910-9E59-55B03283DB00');

define('VAT', 21);

if ($_SERVER['HTTP_HOST'] == '10.0.0.5') {
    define('SITEURL', 'http://10.0.0.5/spella/');
}

if ($_SERVER['HTTP_HOST'] == 'reubro.tk') {
    define('SITEURL', 'https://reubro.tk/spella/');
}
if ($_SERVER['HTTP_HOST'] == 'dashboard.spella.com') {
    define('SITEURL', 'http://dashboard.spella.com/');
}

if ($_SERVER['HTTP_HOST'] == 'localhost') {
    define('SITEURL', 'http://localhost/spella/');
}


if ($_SERVER['HTTP_HOST'] == 'spella.reubro.tk') {
    define('SITEURL', 'http://spella.reubro.tk/');
}
