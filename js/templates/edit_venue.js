$(function() {
  //adding venue types

  $("#loading").hide();
  var langu = localStorage.getItem("lang").toLowerCase();
  // console.log(langu);
  $.ajax({
    url: "api/add_venue_curl.php",
    type: "POST",
    dataType: "json",
    data: { action: "getEditPage", lang: "en" },
    success: function(data) {
      if (data) {
        data.types.sort(function(a, b) {
          return a.tag.localeCompare(b.tag);
        });
        $.each(data.types, function(index, element) {
          $("#venueTypes").append(
            `<option value=` +
              element.tag +
              `>` +
              element.locales[langu] +
              `</option>`
          );
        });
      } else {
        $("#venueTypes").append(
          `<option value=` + null + `>` + "No Types" + `</option>`
        );
      }
    }
  });
  $.urlParam = function(name) {
    var results = new RegExp("[?&]" + name + "=([^&#]*)").exec(
      window.location.href
    );
    if (results == null) {
      return null;
    } else {
      return decodeURI(results[1]) || 0;
    }
  };
  var venueid = $.urlParam("id");
  localStorage.setItem("UpdateUseId", venueid);
  var userdata = getuserData();
  userdata = $.parseJSON(userdata);

  // console.log("=========USER" + venueid)

  /*Getting VenueDetails*/
  var venueD;
  $.ajax({
    url: "api/venue_edit_curl.php",
    type: "POST",
    data: {
      action: "venueDetails",
      userId: userdata.user_id,
      venueId: venueid
    },
    success: function(venueDetails) {
      console.log(venueDetails);
      venueD = $.parseJSON(venueDetails);
      // console.clear();
       console.log(venueD);

      $("#venueName").val(venueD.result.venueName);
      $("#venueTypes").val(venueD.result.venueType);

      /*VenueServices Data*/
      $.ajax({
        url: "api/add_venue_curl.php",
        type: "POST",
        data: { action: "getServices" },
        success: function(data) {
          localStorage.removeItem("ServiceData");
          localStorage.setItem("ServiceData", data);
          var service_array = $.parseJSON(data);
           console.log(service_array);

          var serviceId;
          $.each(venueD.result.services, function(index, element) {
            $.each(service_array.services, function(index1, element1) {
              if (element.serviceName === element1.domainKey ) {
                serviceId = element1.id;
                var deletiid = Math.floor(1000 + Math.random() * 9000);
                $("#pageTable").append(
                  `<tr><td class="word-Breaker" data-value=` +
                    element.serviceVenueURL +
                    ` id=` +
                    serviceId +
                    `><img width="50"
         	  height="50" src="https://spella.com/services/logos-mini/` +
                    element1.icon +
                    `" alt="" /></td>
            <td class="word-Breaker "  id=` +
                    deletiid +
                    `>` +
                    element.serviceVenueURL +
                    `</td>
            <td class="delete-icon-addedit">
            <span class="red">
            <a onclick="deletion(` +
                    deletiid +
                    `);" >
            <i  class='fas fa-trash-alt'></i>
            </a></span>
            </td></tr>`
                );
              }
            });
          });
        },
        error: function() {
          alert("Not responding error, refresh page ");
        },
        async: false
      });
    }
  });

  $("#loading").fadeOut();
});

var insert = [];
function popupOpen() {
  var venuelen = $("#venueName").val().length;
  if (venuelen === 0) {
    $("#venueName")
      .attr("placeholder", "Name Compulsory")
      .css("text-color", "red");
  } else {
    if (insert.length === 0) {
      var serviceslist = localStorage.getItem("serviceslist");
      servicesListData = JSON.parse(serviceslist);
      console.log(servicesListData);
      var data = localStorage.getItem("ServiceData");
      obj = $.parseJSON(data);
      $.each(servicesListData, function(key, value) {
      $.each(obj.services, function(index, element) {
        if (element.id == value)  {
          // if(element.searchKey=="fr.hotels.com"){
          //   insert += `<li data-value=" ${element.searchKey}"  id="${element.searchKey}" onclick="next_pop('hotels','${element.id}','${element.icon}');"><a href="javascript:void(0);"><img width="100" height="100" src="https://spella.com/services/logos-mini/${element.icon}" alt="" /><p>${element.displayName}</p></a></li>`;
          //   }
          //  else if(element.searchKey=="google.com"){
          //     insert += `<li data-value=" ${element.searchKey}"  id="${element.searchKey}" onclick="next_pop('gplus','${element.id}','${element.icon}');"><a href="javascript:void(0);"><img width="100" height="100" src="https://spella.com/services/logos-mini/${element.icon}" alt="" /><p>${element.displayName}</p></a></li>`;
          //   }
          //   else{
			  
          insert += ` <li data-value="` + element.searchKey + `"  id="` + element.searchKey + `" onclick="next_pop(this.id,'` + element.id +
            `','` + element.icon + `');">
                         <a href="javascript:void(0);"><img width="100" height="100" src="https://spella.com/services/logos-mini/` +
            element.icon +
            `" alt="" /><p>` +
            element.displayName +
            `</p></a></li>
                          `;
        }
      // }
      });
    });
      $("#logos").html(insert);
      $("#Modal1").modal();
    } else {
      $("#logos").html(insert);
      $("#Modal1").modal();
    }
  }
}

function next_pop(id, element_id, icon) {
  localStorage.removeItem("serviceId");
  localStorage.setItem("serviceId", element_id);
  localStorage.removeItem("serviceIcon");
  localStorage.setItem("serviceIcon", icon);

  var userData = getuserData();
  var userObj = $.parseJSON(userData);

  var user_id = userObj.user_id;

  $(".pageList").html("");
  var val = id;
  $(".loader").removeClass("hidden");

  venue = $("#venueName").val();

  var venueType = $("#venueTypes").val();
  // val = val.replace("https://", "");
  // what = "site:"+val+" "+venue+" "+venueType;

  what = venue + " " + "site:" + val;

  $.ajax({
    url: "api/add_venue_curl.php",
    type: "POST",
    data: {
      action: "searchWeb",
      userId: user_id,
      what: what
    },
    success: function(data) {
      var obj = $.parseJSON(data);

      if (obj.status === "success") {
        if (Object.keys(obj.result).length === 0) {
          $(".pageList").html(
            "<li>Sorry no results , please try another Services</li>"
          );
        }

        $.each(obj.result, function(index, element) {
          $(".pageList").append(
            "<li id=" +
              element.link +
              " onclick='pageSelect(this.id);'><a href='javascript:void(0);'>" +
              element.title +
              "</a></li>"
          );
        });
      } else {
        $(".pageList").append(
          "<li>Error, please check your search keywords</li>"
        );
      }

      // console.log(obj);
    },
    error: function() {
      alert(
        "Please refresh the page, contact administrator if the problem still persists"
      );
    }
  });  
  $("#Modal6 #customPageURL").attr("data-service-id", val);
  $("#Modal6 #customPageTitle").html("Custom Page (" + val + ")");
  $("#Modal2").modal();
}

// Random id generator

function guidGenerator() {
  var S4 = function() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  };
  return (
    S4() +
    S4() +
    "-" +
    S4() +
    "-" +
    S4() +
    "-" +
    S4() +
    "-" +
    S4() +
    S4() +
    S4()
  );
}

function getuserData() {
  var userData;

  $.ajax({
    url: "home.php",
    type: "POST",
    data: { request: "getdata" },
    success: function(data) {
      userData = data;
    },
    error: function() {
      alert(
        "Erreur Lors du traitement de la commande, veuillez actualiser la page"
      );
    },
    async: false
  });

  return userData;
}

function modal2close() {
  $("#Modal1").modal("hide");
  $("#Modal2").modal("hide");
}

function modal3close() {
  $("#Modal1").modal("hide");
  $("#Modal3").modal("hide");
  $("#Modal2").modal("hide");
  $("#Modal4").modal("hide");
  window.stop();
}

function pageSelect(str) {
  var url = str;
  var data = { action: "getImages", url: url };
  // console.log(data);
  $("#urlInsert").val(url);
  $("#openUrl").attr("href", url);
  $.ajax({
    url: "api/add_venue_curl.php",
    type: "POST",
    data: { action: "getImages", url: url },
    success: function(data) {
      var basicUrl = "data:image/gif;base64," + data;
      $("#pageInsert").attr("src", basicUrl);
    },
    async: false
  });

  $("#Modal3").modal();
}

$("pagesBack").click(function() {
  $("#Modal2").modal("hide");
});

function pagesUtilize() {
  url = $("#urlInsert").val();
  var userData = getuserData();
  var userObj = $.parseJSON(userData);
  var icon = localStorage.getItem("serviceIcon");

  var user_id = userObj.user_id;

  var serviceId = localStorage.getItem("serviceId");
  $.ajax({
    url: "api/add_venue_curl.php",
    type: "POST",
    data: {
      action: "validatePage",
      userId: user_id,
      pageURL: url,
      venueName: venue
    },

    success: function(data) {
      localStorage.removeItem("serviceId");
      // console.log(data);
      var result = $.parseJSON(data);
      if (result.status === "success") {
        var deletiid = Math.floor(1000 + Math.random() * 9000);
        $("#pageTable").append(
          `<tr><td class="word-Breaker" data-value=` +
            url +
            ` id=` +
            serviceId +
            `><img width="50" height="50" 
            src="https://spella.com/services/logos-mini/` +
            icon +
            `" alt="" /></td>
            <td  id=` +
            deletiid +
            `>` +
            url +
            `</td>
            <td class="delete-icon-addedit">
            <span class="red">
            <a onclick="deletion(` +
            deletiid +
            `);" >
            <i  class='fas fa-trash-alt'></i>
            </a></span>
            </td></tr>`
        );
        $("#save_button").fadeIn();
        localStorage.removeItem("serviceId");
        modal3close();
      }
    }
  });
}

function deletion(event) {
  $("#pageTable")
    .find("td[id=" + event + "]")
    .parent()
    .remove();
}

// ===============================================SAVE BUTTON============================================//

$("#save_button").click(function() {
  $("#loading").show();

  var venueType = $("#venueTypes").val();
  var venue = $("#venueName").val();
  var userData = getuserData();
  var userObj = $.parseJSON(userData);
  var user_id = userObj.user_id;
  var venueId = localStorage.getItem("UpdateUseId");

  var i = 0;
  var services = [];
  $("#pageContent > tbody  > tr td:first-child").each(function() {
    var id = $(this).attr("id");
    var url = $(this).attr("data-value");
    var serviceVenueUUID = guidGenerator();
    services[i] = {
      serviceVenueUUID: serviceVenueUUID,
      serviceId: id,
      serviceVenueURL: url,
      serviceVenueStatus: "unknown",
      serviceEnabled: true
    };
    i++;
  });

  if (services.length < 1) {
    switch (localStorage.getItem("lang").toLowerCase()) {
      case "en":
        alert("Add One page atleast");
        break;
      case "fr":
        alert("ajouter au moins une page");
        break;
    }

    $("#loading").fadeOut();
    return false;
  }

  // console.log(
  //   user_id +
  //     "====" +
  //     venue +
  //     "====" +
  //     venueId +
  //     "=====" +
  //     venueType +
  //     "====" +
  //     services
  // );
  // return false;
  $.ajax({
    url: "api/venue_edit_curl.php",
    type: "POST",
    data: {
      action: "updateVenue",
      userId: user_id,
      venueName: venue,
      venueId: venueId,
      venueType: venueType,
      venueServices: services
    },
    success: function(data) {
      // console.log("=======venue Updation" + data);
      var result = $.parseJSON(data);
      if (result.status === "success") {
        $.ajax({
          url: "api/add_venue_curl.php",
          type: "POST",
          data: {
            action: "validateVenue",
            userId: user_id,
            venueId: venueId,
            services: services
          },
          success: function(data) {
            console.log(data);
            var obj_val = $.parseJSON(data);
            if (obj_val.status === "success") {
              // console.log("validation success");
              window.location.href = "home.php";
            }
          },
          error(data) {
            // console.log(data);
            var obj = $.parseJSON(data);
            alert(data.message);
          },
          async: false
        });
      }
    },
    error: function() {
      alert("VenueEdition Faliure,Refreshing page");
      loaction.reload();
    },
    async: false
  });
}); //save button operation ends

function openOthers() {
  $("#Modal4").modal();
  $("#Modal4pageInsert").hide();

  $("#validtorAlerter").html("");
}

function goValidator() {
  var url = $("#Modal4urlInsert").val();

  $("#Modal4pageInsert").prop("src", null);

  $("#Modal4pageInsert").show();

  $.ajax({
    url: "api/add_venue_curl.php",
    type: "POST",
    data: { action: "getImages", url: url },
    success: function(data) {
      var basicUrl = "data:image/gif;base64," + data;
      $("#Modal4pageInsert").attr("src", basicUrl);
    },
    async: false
  });

  var userData = getuserData();
  var userObj = $.parseJSON(userData);
  var icon = localStorage.getItem("serviceIcon");

  var user_id = userObj.user_id;

  var venue = $("#venueName").val();

  $.ajax({
    url: "api/add_venue_curl.php",
    type: "POST",
    data: {
      action: "validatePage",
      userId: user_id,
      pageURL: url,
      venueName: venue
    },

    success: function(data) {
      var lang = localStorage.getItem("lang").toLowerCase();
      var msg = "";
      var alerter = "";
      var misAlerter = "";
      switch (lang) {
        case "fr":
          msg = "Cette page n'est pas dans un format reconnu par Spella";
          alerter = "succès de validation";
          misAlerter = "validation échouée";
          break;

        case "en":
          msg = "This page is not in a format that Spella understands";
          alerter = "Validation Success";
          misAlerter = "validation Failed";
          break;
        default:
          msg = "This page is not in a format that Spella understands";
          break;
      }
      var obj = $.parseJSON(data);
      // console.log(obj);
      if (obj.status === "success") {
        // console.log(obj.result);
        $("#validtorAlerter").html(alerter);
        $("#Modal4Save").attr("disabled", false);
      } else {
        alert(msg);
        $("#validtorAlerter").html(misAlerter);
        $("#Modal4urlInsert").val("");
        $("#Modal4Save").attr("disabled", true);
      }
    },
    error: function(data) {
      console.log(data);
    }
  });
}

function Modal4pagesUtilize() {
  goValidator();
  var url = $("#Modal4urlInsert").val();
  var count = 0;
  var obj = $.parseJSON(localStorage.getItem("ServiceData"));

  // console.log(obj);

  $.each(obj.services, function(index, element) {
    if (
      url.indexOf(element.domainKey) > 1 ||
      url.indexOf(element.shortName) > 1
    ) {
      count++;
      var deletiid = Math.floor(1000 + Math.random() * 9000);
      $("#pageTable").append(
        `<tr><td data-value=` +
          url +
          ` id=` +
          element.id +
          `><img width="50" height="50" 
            src="https://spella.com/services/logos-mini/` +
          element.icon +
          `" alt="" /></td>
            <td  id=` +
          deletiid +
          `>` +
          url +
          `</td><td><span class="red">
            <a  onclick="deletion(` +
          deletiid +
          `);" >
            <i  class='fas fa-trash-alt'></i>
            </a></span></td></tr>`
      );
      $("#save_button").fadeIn();
      modal3close();
      localStorage.removeItem("serviceId");
      localStorage.setItem("serviceId", element.id);
    }
    $("#Modal4urlInsert").val("");
  });

  if (count == 0) {
    alert("Service Unavailable");
    modal3close();
  }
}

function openAddCustomSource() {
  // $("#Modal5").modal();
  $.ajax({
    url: "api/add_venue_curl.php",
    type: "POST",
    data: { action: "getServices" },
    success: function(data) {
      data = JSON.parse(data);
     
      var insertnew = "";
      $.each(data.services, function(index, element) {
        if (element.active) {
          insertnew +=
            ` <li data-value="` +
            element.searchKey +
            `"  id="` +
            element.domainKey +
            `> <a href="javascript:void(0);"><img width="100" height="100" src="https://spella.com/services/logos-mini/`+
            element.icon +
            `" alt="" /><p>` +
            element.displayName +
            `</p></a><div class="checkbox">
            <input type="checkbox" name="addsource" value="`+element.id+`" id="`+element.id+`">
            <label for="`+element.id+`" style="color: #111;"></label>
          </div></li>`; 
        }
        
      });
      
      $("#logos5").html(insertnew);
      $("#Modal1").modal("toggle");
      $("#Modal5").modal();
    }
  });
}

function addSource() {
	var selected = new Array();
	$("input:checkbox[name=addsource]:checked").each(function(){
	    selected.push($(this).val());
	});
	console.log(selected);
}

function showModal6() {
	$("#Modal6").modal();
}

function validateURL() {
	$("#em_error").hide();
	var err = false;
	var errMsg = "";
	var customURL = $("#customPageURL").val();	
	var lang = localStorage.getItem("lang").toLowerCase();
	if ($("#customPageURL").get(0).checkValidity() == true) {			
		hostDomain = (new URL(customURL)).hostname;
		serviceDomain = $("#customPageURL").attr('data-service-id');	
		console.log(hostDomain);
		console.log(serviceDomain);
		if (hostDomain.indexOf(serviceDomain) >= 0) {
			$("#em_error").hide();
			$("#Modal6").modal("toggle");
			pageSelect(customURL);
		} else {
			err = true;
			if (lang == "en") {
				errMsg = "Should match with the service domain";
			} else if (lang == "fr") {
				errMsg = "Doit correspondre au domaine de service";
			}				
		}
	} else {
		err = true;
		if (lang == "en") {
			errMsg = "Invalid URL";
		} else if (lang == "fr") {
			errMsg = "URL invalide";
		}
	}
	if (err) {		
		$("#em_error").html(errMsg);
		$("#em_error").show();
	}
}