
var transButton = '';
var revertButton = '';
    var ratingData= {
      count:0,
      totalRating:0,
      sentence:[]
    }
var langT = localStorage.getItem("lang").toLowerCase();
var searchPlaceHolder = '';
switch(langT)
{
  case 'en' : 
              transButton = "Translate";
              revertButton="Revert";
              ratingData.sentence[0]=' comments on '
              ratingData.sentence[1]= ' reviews ';
              searchPlaceHolder = 'Filter';
              break;

  case 'fr' : 
              ratingData.sentence[0]=' commentaires sur ';
              ratingData.sentence[1]=' avis ';
              transButton = "Traduction";
              revertButton = "Revenir";
              searchPlaceHolder = "Filtrer";
              break;

  default :   transButton = "Translate";
              ratingData.sentence[0]=' comments on '
              ratingData.sentence[1]= ' reviews ';
              revertButton="Revert";
              searchPlaceHolder = 'Filter';

               break;
}
$("#searching").attr("placeholder",searchPlaceHolder);

function Load_RData(message,venueDetailData) {
if(message==='nothing')
{
  return false
}
  
  console.log(message)

var ratings=0;
var starnumber=0;
var venueD=$.parseJSON(venueDetailData);
$.each(venueD.result.services,function(index,element){
  console.log(element.reviews.length)
});
var using = message.split(",");
var month="";

console.log(message)

var enMonths = {
  'Jan':01,
  'Feb':02,
  'Mar':03,
  'Apr':04,
  'May':05,
  'Jun':06,
  'Jul':07,
  'Aug':08,
  'Sep':09,
  'Oct':10,
  'Nov':11,
  'Dec':12
};


var frMonths = {
  'Janv.':01,
  'Févr.':02,
  'Mars':03,
  'Avr.':04,
  'Mai':05,
  'Juin':06,
  'Juil.':07,
  'Août':08,
  'Sept.':09,
  'Oct.':10,
  'Nov.':11,
  'Déc.':12
};

switch(langT)
{
  case 'en' : month= enMonths[using[0]];
              break;
  case 'fr' : month= frMonths[using[0]];
              break;
  default : month = enMonths[using[0]];
              break;
}

 console.log(month)

  var temp = [];
      $.each(venueD.result.services,function(index,element){
        var service= getName(element.serviceId);
          var serviceUrl=getServiceUrl(service.idService,venueid)
        $.each(element.reviews,function(index1,element1){
          element1.service = service;
          element1.serviceUrl = serviceUrl;
          temp.push(element1);
        })
      })
      venueD.result.services = temp;
      venueD.result.services.sort(function(a,b){
        var dateA=new Date(a.date), dateB=new Date(b.date)
        return dateB-dateA //sort by date ascending
      });

ratingData.count=0;
ratingData.totalRating=0;

$(".list").html("")

var revMonth=[];

    var arr=[];
if(using[1]==='positive' || using[1]==='positif')
{
  console.log('jere')
$.each(venueD.result.services,function(index1,element1){

      	
      	if(element1.note=="")
      	{
      		console.log("note not here")
      		return
      	}

      revMonth = element1.date.split("-");
      console.log(revMonth[1]);
      console.log(month)
          if(Number(element1.note)>=Number(3.5) && Number(revMonth[1].toString())==Number(month.toString()))
          {    
              ratings= parseInt(ratings+element1.note);
              starnumber=parseInt(starnumber+1);          
                var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var simpleDate = element1.date;
                var date = getFormattedDate(element1.date)
                ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
                if(sentence.length > 0)
                {
  $(".list").append(`
       <li class="venue-review ">
            <p style="display: none;" class="realDate">`+simpleDate+`</p>
            <div class="venue-logo">
              <a href="`+element1.serviceUrl+`" target="_blank"><img src="`+element1.service.idName+`" class="img-fluid" alt=""></a>
              <p style="display:none" class="service">`+element1.service.idServiceName+`</p> 
            </div>
             <div class="venue-title">
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
                if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }
                }
          



          }
      

  });


var monkeyList = new List('test-list', {
  valueNames: ['realDate','disdate','diftitle','title-en','full','full-en','service'],
  page: 10,
  pagination: true
});
  }
else if(using[1]==='negative' || using[1]==='négatif')
{
$.each(venueD.result.services,function(index1,element1){

      revMonth = element1.date.split("-");
          if(Number(element1.note)<Number(3.5) && Number(revMonth[1].toString())==Number(month.toString()))
          {
              ratings= parseInt(ratings+element1.note);
              starnumber=parseInt(starnumber+1);          
                var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var simpleDate = element1.date;
                var date = getFormattedDate(element1.date);
                ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
                if(sentence.length > 0)
                {
                    $(".list").append(`
     <li class="venue-review ">
            <p style="display: none;" class="realDate">`+simpleDate+`</p>
            <div class="venue-logo">
              <a href="`+element1.serviceUrl+`" target="_blank"><img src="`+element1.service.idName+`" class="img-fluid" alt=""></a>
              <p style="display:none" class="service">`+element1.service.idServiceName+`</p> 
            </div>
             <div class="venue-title">
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
                 if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }

                }
          

          }


  });



 var monkeyList = new List('test-list', {
  valueNames: ['realDate','disdate','diftitle','title-en','full','full-en','service'],
  page: 10,
  pagination: true
});
  }

        $('html, body').animate({
        scrollTop: $("#tableAnchor").offset().top
    }, 2000);
var head = ratingData.count+ratingData.sentence[0]+ratingData.totalRating+ ratingData.sentence[1];
$(".reviewsHeading").html(head)
var star = ratings/starnumber;
    star = Number(Math.round(star+'e2')+'e-2');
    console.log("staring here",star)
    $("#starOuterVal").html(star);
  const starPercentage = (star / 5) * 100;
  const starPercentageRounded = `${(Math.round(starPercentage))}%`;
  console.log(starPercentageRounded)
  
  document.querySelector(`.stars-inner`).style.width = starPercentageRounded;    



}


function Load_Star(value,venueDetailData)
{
$(".list").html("")

var ratings=0;
var starnumber=0;        
ratingData.count=0;
ratingData.totalRating=0;          
var venueD=$.parseJSON(venueDetailData);

  var temp = [];
      $.each(venueD.result.services,function(index,element){
        var service= getName(element.serviceId);
          var serviceUrl=getServiceUrl(service.idService,venueid)
        $.each(element.reviews,function(index1,element1){
          element1.service = service;
          element1.serviceUrl = serviceUrl;
          temp.push(element1);
        })
      })
      venueD.result.services = temp;
      venueD.result.services.sort(function(a,b){
        var dateA=new Date(a.date), dateB=new Date(b.date)
        return dateB-dateA //sort by date ascending
      });

var notN= value[0];


$.each(venueD.result.services,function(index1,element1){
  
      if(Number(element1.note).toString()==notN.toString())
      {
              ratings= parseInt(ratings+element1.note);
              starnumber=parseInt(starnumber+1);          
                var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var simpleDate = element1.date;
                var date = getFormattedDate(element1.date)
                ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
           if(sentence.length > 0)
                {
                   $(".list").append(`
 <li class="venue-review ">
     <p style="display: none;" class="realDate">`+simpleDate+`</p>
            <div class="venue-logo">
              <a href="`+element1.serviceUrl+`" target="_blank"><img src="`+element1.service.idName+`" class="img-fluid" alt=""></a>
              <p style="display:none" class="service">`+element1.service.idServiceName+`</p> 
            </div>
             <div class="venue-title">
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
          
           if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }

                }
        

      }          


  });



 var monkeyList = new List('test-list', {
  valueNames: ['realDate','disdate','diftitle','title-en','full','full-en','service'],
  page: 10,
  pagination: true
});

$('html, body').animate({
        scrollTop: $("#tableAnchor").offset().top
    }, 2000);

var head = ratingData.count+ratingData.sentence[0]+ratingData.totalRating+ ratingData.sentence[1];
  $(".reviewsHeading").html(head)
    var star = ratings/starnumber;
    star = Number(Math.round(star+'e2')+'e-2');
    $("#starOuterVal").html(star);

  const starPercentage = (star / 5) * 100;
  const starPercentageRounded = `${(Math.round(starPercentage))}%`;
  document.querySelector(`.stars-inner`).style.width = starPercentageRounded;  

}


function morrisHan(id,service,venueDetailData)
{

var ratings=0;
var starnumber=0;

ratingData.count=0;
ratingData.totalRating=0;
var venueD= $.parseJSON(venueDetailData);

var serviceId = service;

var temp = [];
      $.each(venueD.result.services,function(index,element){
        var service= getName(element.serviceId);
          var serviceUrl=getServiceUrl(service.idService,venueid)
        $.each(element.reviews,function(index1,element1){
          element1.serviceId = element.serviceId;
          element1.service = service;
          element1.serviceUrl = serviceUrl;
          temp.push(element1);
        })
      })
      venueD.result.services = temp;
      venueD.result.services.sort(function(a,b){
        var dateA=new Date(a.date), dateB=new Date(b.date)
        return dateB-dateA //sort by date ascending
      });


var serviceData=$.parseJSON(localStorage.getItem("servicesjson"));
$(".list").html("")

    $.each(venueD.result.services,function(index1,element1){

      if(element1.serviceId===serviceId)
      {
          
              ratings= parseInt(ratings+element1.note);
              starnumber=parseInt(starnumber+1);          
                var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var simpleDate = element1.date;
                var date = getFormattedDate(element1.date)

                ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
            if(sentence.length > 0)
                {
                 
       $(".list").append(`
 <li class="venue-review ">
    <p style="display: none;" class="realDate">`+simpleDate+`</p>
            <div class="venue-logo">
              <a href="`+element1.serviceUrl+`" target="_blank"><img src="`+element1.service.idName+`" class="img-fluid" alt=""></a>
              <p style="display:none" class="service">`+element1.service.idServiceName+`</p> 
            </div>
             <div class="venue-title">
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
                if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }
                }

         
          
    
      }

    });

 var monkeyList = new List('test-list', {
  valueNames: ['realDate','disdate','diftitle','title-en','full','full-en','service'],
  page: 10,
  pagination: true
});
       $('html, body').animate({
        scrollTop: $("#tableAnchor").offset().top
    }, 2000); 

var head = ratingData.count+ratingData.sentence[0]+ratingData.totalRating+ ratingData.sentence[1];
  $(".reviewsHeading").html(head)

    var star = ratings/starnumber;
    star = Number(Math.round(star+'e2')+'e-2');
    $("#starOuterVal").html(star);

    const starPercentage = (star / 5) * 100;
    const starPercentageRounded = `${(Math.round(starPercentage))}%`;
    document.querySelector(`.stars-inner`).style.width = starPercentageRounded;    



}

function MainTopicLoad(value,lang,venueDetailData)
{

  console.log(value)
  console.log(lang)
  // console.log(venueDetailData)

var ratings=0;
var starnumber=0;
              
ratingData.count=0;
ratingData.totalRating=0;

var langT = localStorage.getItem("lang").toLowerCase();


$(".list").html("");

  var DictionaryD=$.parseJSON(localStorage.getItem("Dictionary"));
  var venueD=$.parseJSON(venueDetailData);
  var topic=value;
  var temp = [];

      $.each(venueD.result.services,function(index,element){
        var service= getName(element.serviceId);
          var serviceUrl=getServiceUrl(service.idService,venueid)
        $.each(element.reviews,function(index1,element1){
          element1.serviceId = element.serviceId;
          element1.service = service;
          element1.serviceUrl = serviceUrl;
          temp.push(element1);
        })
      })
      venueD.result.services = temp;
      venueD.result.services.sort(function(a,b){
        var dateA=new Date(a.date), dateB=new Date(b.date)
        return dateB-dateA //sort by date ascending
      });


      $.each(venueD.result.services,function(index1,element1){


          var flag=0;
          var sentense = element1.title+" "+element1.full;
          var res=sentense.toLowerCase(); 
          console.log("checking review"+index1+"with")
          $.each(DictionaryD.topics,function(index3,element3){
          var result;
          var checkedNew;

                //creating a good checking variable
                if(element3.locales.hasOwnProperty(lang+'-short'))
                {
                  var splitting_var=lang+'-short';

                    var splitter=element3.locales[splitting_var].split(" ")
                    checkedNew = splitter[0].split('.').join("");
                }
                else{
                  checkedNew= element3.locales[lang];
                }
                checkedNew=checkedNew.toLowerCase();
                if(checkedNew===topic)
                {
                  $.each(element3.keys,function(index4,element4)
                  {
                  innerchecker= element4.locales[lang];
                  innerchecker=innerchecker.toLowerCase();
                
                  result=countInstancesHere(res,innerchecker);
                  console.log(result)
                  if(result!==0)
                  {
                    flag++;
              ratings= parseInt(ratings+element1.note);
              starnumber=parseInt(starnumber+1);          
                var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var simpleDate = element1.date;
                var date = getFormattedDate(element1.date)
                ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
               if(sentence.length > 0)
                {
                    $(".list").append(`
  <li class="venue-review ">
            <p style="display: none;" class="realDate">`+simpleDate+`</p>
            <div class="venue-logo">
              <a href="`+element1.serviceUrl+`" target="_blank"><img src="`+element1.service.idName+`" class="img-fluid" alt=""></a>
              <p style="display:none" class="service">`+element1.service.idServiceName+`</p> 
            </div>
             <div class="venue-title">
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)

              if(element1['full-en']=="")
              {
                var btn = "#transb"+index1;
                $(btn).hide();
              }

                }
          

                  }
                    if(flag!==0)
                      {        
                        return false; 
                      }
                  });
              }  
              if(flag!==0)
                {return false;}
          });
            
        });
    
var head = ratingData.count+ratingData.sentence[0]+ratingData.totalRating+ ratingData.sentence[1];
  $(".reviewsHeading").html(head)

       $('html, body').animate({
        scrollTop: $("#tableAnchor").offset().top
    }, 2000); 

 var monkeyList = new List('test-list', {
  valueNames: ['realDate','disdate','diftitle','title-en','full','full-en','service'],
  page: 10,
  pagination: true
});

    var star = ratings/starnumber;
    star = Number(Math.round(star+'e2')+'e-2');
    $("#starOuterVal").html(star);
  const starPercentage = (star / 5) * 100;
  const starPercentageRounded = `${(Math.round(starPercentage))}%`;
  document.querySelector(`.stars-inner`).style.width = starPercentageRounded;    

}
function countInstancesHere(string, word) {
   return string.split(word).length - 1;
}

function handleMainTopicImpression(message,lang,venueDetailData)
{
  console.log(message,lang)
  var ratings=0;
var starnumber=0;
ratingData.count=0;
ratingData.totalRating=0;

// console.log(message)

$(".list").html("");
  
if(message==='nothing')
{
  return false;
}
var venueD=$.parseJSON(venueDetailData);
var DictionaryD=$.parseJSON(localStorage.getItem("Dictionary"));
var using = message.split(",");



// console.log(using)
var topic= using[0];

var temp = [];

      $.each(venueD.result.services,function(index,element){
        var service= getName(element.serviceId);
          var serviceUrl=getServiceUrl(service.idService,venueid)
        $.each(element.reviews,function(index1,element1){
          element1.serviceId = element.serviceId;
          element1.service = service;
          element1.serviceUrl = serviceUrl;
          temp.push(element1);
        })
      })
      venueD.result.services = temp;
      venueD.result.services.sort(function(a,b){
        var dateA=new Date(a.date), dateB=new Date(b.date)
        return dateB-dateA //sort by date ascending
      });


if(using[1]==='positive' || using[1]==='positif')
{

    $.each(venueD.result.services,function(index1,element1){

            var flag=0;
          var sentense = element1.title+" "+element1.full;
          var res=sentense.toLowerCase();
              $.each(DictionaryD.topics,function(index3,element3){
              var result;
              var checkedNew;

                //creating a good checking variable
                if(element3.locales.hasOwnProperty(lang+'-short'))
                {
                  var splitting_var=lang+'-short';

                    var splitter=element3.locales[splitting_var].split(" ")
                    checkedNew = splitter[0].split('.').join("");
                }
                else{
                  checkedNew= element3.locales[lang];
                }
                checkedNew=checkedNew.toLowerCase();

                if(checkedNew===topic)
                {
                  $.each(element3.keys,function(index4,element4)
                  {
                  innerchecker= element4.locales[lang];
                  innerchecker=innerchecker.toLowerCase();
                
                  result=countInstancesHere(res,innerchecker);
                  console.log(result)
                  if(result!==0)
                  {
                    console.log(element1.note)
              if(Number(element1.note)>=Number(3.5))
              {
                    flag++;
              ratings= parseInt(ratings+element1.note);
              starnumber=parseInt(starnumber+1);          
                var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var simpleDate = element1.date;
                var date = getFormattedDate(element1.date)
                ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }

                if(sentence.length > 0)
                {
                   $(".list").append(`
 <li class="venue-review ">
            <p style="display: none;" class="realDate">`+simpleDate+`</p>
            <div class="venue-logo">
              <a href="`+element1.serviceUrl+`" target="_blank"><img src="`+element1.service.idName+`" class="img-fluid" alt=""></a>
              <p style="display:none" class="service">`+element1.service.idServiceName+`</p> 
            </div>
             <div class="venue-title">
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
                if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }

                }


                    }
                  }
                    if(flag!==0)
                      {        
                        return false; 
                      }
                  });
              }  
              if(flag!==0)
                {return false;}
          });

              

        });
    

}
else if(using[1]==='negative' || using[1] === 'négatif')
{
     $.each(venueD.result.services,function(index1,element1){
    
            var flag=0;
          var sentense = element1.title+" "+element1.full;
          var res=sentense.toLowerCase();
              $.each(DictionaryD.topics,function(index3,element3){
              var result;
              var checkedNew;

                //creating a good checking variable
                if(element3.locales.hasOwnProperty(lang+'-short'))
                {
                  var splitting_var=lang+'-short';

                    var splitter=element3.locales[splitting_var].split(" ")
                    checkedNew = splitter[0].split('.').join("");
                }
                else{
                  checkedNew= element3.locales[lang];
                }
                checkedNew=checkedNew.toLowerCase();

                if(checkedNew===topic)
                {
                  $.each(element3.keys,function(index4,element4)
                  {
                  innerchecker= element4.locales[lang];
                  innerchecker=innerchecker.toLowerCase();
                
                  result=countInstancesHere(res,innerchecker);
                  console.log(result)
                  if(result!==0)
                  {
                    console.log(element1.note)
              if(Number(element1.note)<Number(3.5))
              {
                    flag++;
              ratings= parseInt(ratings+element1.note);
              starnumber=parseInt(starnumber+1);          
                var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var simpleDate = element1.date;
                var date = getFormattedDate(element1.date)
                ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
                if(sentence.length > 0)
                {
                    $(".list").append(`
 <li class="venue-review ">
            <p style="display: none;" class="realDate">`+simpleDate+`</p>
            <div class="venue-logo">
              <a href="`+element1.serviceUrl+`" target="_blank"><img src="`+element1.service.idName+`" class="img-fluid" alt=""></a>
              <p style="display:none" class="service">`+element1.service.idServiceName+`</p> 
            </div>
             <div class="venue-title">
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
                if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }

                }
          
                    }
                  }
                    if(flag!==0)
                      {        
                        return false; 
                      }
                  });
              }  
              if(flag!==0)
                {return false;}
          });

              

        });
 


}

 var monkeyList = new List('test-list', {
  valueNames: ['realDate','disdate','diftitle','title-en','full','full-en','service'],
  page: 10,
  pagination: true
});

var head = ratingData.count+ratingData.sentence[0]+ratingData.totalRating+ ratingData.sentence[1];
$(".reviewsHeading").html(head)

$('html, body').animate({
        scrollTop: $("#tableAnchor").offset().top
    }, 2000);

var star = ratings/starnumber;
    star = Number(Math.round(star+'e2')+'e-2');
    $("#starOuterVal").html(star);
  const starPercentage = (star / 5) * 100;

  const starPercentageRounded = `${(Math.round(starPercentage))}%`;

  document.querySelector(`.stars-inner`).style.width = starPercentageRounded;


}

function WordImpressionGraphLoad(value,language,venueDetailData)
{

  var using=value.split(",");
console.log(using)
   var DictionaryD=$.parseJSON(localStorage.getItem("Dictionary"));
  var venueD=$.parseJSON(venueDetailData);

var temp = [];

      $.each(venueD.result.services,function(index,element){
        var service= getName(element.serviceId);
          var serviceUrl=getServiceUrl(service.idService,venueid)
        $.each(element.reviews,function(index1,element1){
          element1.serviceId = element.serviceId;
          element1.service = service;
          element1.serviceUrl = serviceUrl;
          temp.push(element1);
        })
      })
      venueD.result.services = temp;
      venueD.result.services.sort(function(a,b){
        var dateA=new Date(a.date), dateB=new Date(b.date)
        return dateB-dateA //sort by date ascending
      });


  var topic=using[0];
  var ratings=0;
var starnumber=0;
ratingData.count=0;
ratingData.totalRating=0;
$(".list").html("");


if(using[1]==='positive' || using[1]==='positif')
{
        $.each(venueD.result.services,function(index1,element1){
          
          var flag=0;
          var sentense = element1.title+" "+element1.full;
          var res=sentense.toLowerCase(); 
          console.log("checking review"+index1+"with")
          var result;
                  result=countInstancesHere(res,topic);
                  console.log(result)
                  if(result!==0)
                  {
                    flag++;
                    if(Number(element1.note)>=Number(3.5))
                    {
             ratings= parseInt(ratings+element1.note);
              starnumber=parseInt(starnumber+1);          
                var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var simpleDate = element1.date;
                var date = getFormattedDate(element1.date)
                ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
              if(sentence.length > 0)
                {
                  
    $(".list").append(`
 <li class="venue-review ">
            <p style="display: none;" class="realDate">`+simpleDate+`</p>
            <div class="venue-logo">
              <a href="`+element1.serviceUrl+`" target="_blank"><img src="`+element1.service.idName+`" class="img-fluid" alt=""></a>
              <p style="display:none" class="service">`+element1.service.idServiceName+`</p> 
            </div>
             <div class="venue-title">
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
                   if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }
                }
        


                      }
                  }

                  });
          
}
else if(using[1]==='negative' || using[1]==='négatif')
{      $.each(venueD.result.services,function(index1,element1){
          
          var flag=0;
          var sentense = element1.title+" "+element1.full;
          var res=sentense.toLowerCase(); 
          console.log("checking review"+index1+"with")
          var result;
                  result=countInstancesHere(res,topic);
                  console.log(result)
                  if(result!==0)
                  {
                    if(Number(element1.note)<Number(3.5))
                    {
                   ratings= parseInt(ratings+element1.note);
              starnumber=parseInt(starnumber+1);          
                var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var simpleDate = element1.date;
                var date = getFormattedDate(element1.date)
                ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
                if(sentence.length > 0)
                {
                 $(".list").append(`
  <li class="venue-review ">
            <p style="display: none;" class="realDate">`+simpleDate+`</p>
            <div class="venue-logo">
              <a href="`+element1.serviceUrl+`" target="_blank"><img src="`+element1.service.idName+`" class="img-fluid" alt=""></a>
              <p style="display:none" class="service">`+element1.service.idServiceName+`</p> 
            </div>
             <div class="venue-title">
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
                  if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }


                }
            

                      }
                  }

                  });
          
            
           }


 var monkeyList = new List('test-list', {
  valueNames: ['realDate','disdate','diftitle','title-en','full','full-en','service'],
  page: 10,
  pagination: true
});

var head = ratingData.count+ratingData.sentence[0]+ratingData.totalRating+ ratingData.sentence[1];
$(".reviewsHeading").html(head)

$('html, body').animate({
        scrollTop: $("#tableAnchor").offset().top
    }, 2000); 
var star = ratings/starnumber;
    star = Number(Math.round(star+'e2')+'e-2');
    $("#starOuterVal").html(star);
  const starPercentage = (star / 5) * 100;

  const starPercentageRounded = `${(Math.round(starPercentage))}%`;

  document.querySelector(`.stars-inner`).style.width = starPercentageRounded;


}
