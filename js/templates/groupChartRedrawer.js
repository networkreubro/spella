
function reDrawstarloader(venueDetailData) {
    
  if(venueDetailData.length > 0) {
  $("#chart_div1div").fadeIn();

  var venueNotes1Count=0;
  var venueNotes2Count=0;
  var venueNotes3Count=0;
  var venueNotes4Count=0;
  var venueNotes5Count=0;

  if(venueDetailData.length!==0)
  {
    var NoteCount = venueDetailData;
  }


  $.each(NoteCount,function(index,element){
   venueNotes1Count=parseInt(element.venueNotes1Count+venueNotes1Count);
   venueNotes2Count=parseInt(element.venueNotes1Count+venueNotes1Count);
   venueNotes3Count=parseInt(element.venueNotes3Count+venueNotes3Count);
   venueNotes4Count=parseInt(element.venueNotes4Count+venueNotes4Count);
   venueNotes5Count=parseInt(element.venueNotes5Count+venueNotes5Count);

  });

      if(!window.myChart_chart_div1) {

              var chartData = {
         type: 'horizontalBar',
         data: {
            labels: ["1(★)","2(★)","3(★)","4(★)","5(★)"],
          datasets: [
              {
                  label: "Number of Notes",
                  backgroundColor: "#6E388A",
                  data: [venueNotes1Count,venueNotes2Count,venueNotes3Count,venueNotes4Count,venueNotes5Count]
              }
             
          ]
         },
         options: {
              barValueSpacing: 20,
              scales: {
                  yAxes: [{
                      ticks: {
                          min: 0,
                      }
                  }]
              }
          }
      } 

      window.canvas_chart_div1 = document.getElementById('chart_div1');
      window.myChart_chart_div1 = new Chart(window.canvas_chart_div1, chartData);

        $("#chart_div1_error").html('');
        $("#chart_div1").show();


      } else {

          // window.myChart_chart_div1.data.datasets.data = [];
          var notes =  [venueNotes1Count,venueNotes2Count,venueNotes3Count,venueNotes4Count,venueNotes5Count];
            window.myChart_chart_div1.data.datasets.forEach((dataset) => {
              $.each(dataset.data,function(index, second_val) {
                dataset.data[index] = notes[index];
              })
          });

            window.myChart_chart_div1.update();

                $("#chart_div1_error").html('');
                  $("#chart_div1").show()

      }


  } else {
   console.log('no data man')
      if (window.myChart_chart_div1) {
      window.myChart_chart_div1.data.labels = [];
      window.myChart_chart_div1.data.datasets.forEach((dataset) => {
          $.each(dataset.data,function(index2,element2){
                 delete(dataset.data[index2]);
                })
      });
      window.myChart_chart_div1.update();
      }
    $("#chart_div1_error").html(dataMessage);
    $("#chart_div1").hide()

  }

 // console.log(stats)


}

function reDrawBigChart(data) {

  var negative=0;
  var positive=0;

    var htitle1 = htitle2 = xaxislabel = yaxislabel = "";
    switch(localStorage.getItem('lang').toLowerCase())
    {
      case 'en' : htitle1='positive'; htitle2 = 'negative'; xaxislabel= 'count'; yaxislabel='month';
            break;
      case 'fr' : htitle1='positif'; htitle2 = 'négatif'; xaxislabel= 'compter'; yaxislabel='mois';
            break;
    }

    let label = [];
    let posi = [];
    let nega =[];

    if(data.length > 0) {

      var using= data;
    $.each(using,function(index,element){
      //let pair = [];
      label.push(element.yearMonth);
      nega.push(element.venueNegativeNotesCount);
      posi.push(element.venuePositiveNotesCount);
      //chartValues.push(pair);

    });

      if(!window.myChart_main_chart) {
         var chartData = {
       type: 'horizontalBar',
       data: {
          labels: label,
        datasets: [
            {
                label: htitle1,
                backgroundColor: "blue",
                data: posi,
            },
            {
                label: htitle2,
                backgroundColor: "red",
                data: nega,
            }
        ]
       },
       options: {
            barValueSpacing: 20,
            scales: {
                xAxes: [{
                  barPercentage: 0.4,
                    ticks: {
                        min: 0,
                    },
                    scaleLabel: {
                   display: true,
                   labelString: xaxislabel
      }
                }],
                yAxes: [{
          scaleLabel: {
          display: true,
          labelString: yaxislabel
            }
             }]
            }
        }
    } 

window.canvas_main_chart = document.getElementById('main_chart');
window.myChart_main_chart = new Chart(window.canvas_main_chart, chartData);
$("#main_chartdiv").fadeIn();
$("#main_chart_error").html('');

      } else {
          console.log(' are we here')
          console.log(label)
          console.log(nega)
          console.log(posi)

        window.myChart_main_chart.data.labels = [];
          label.forEach(element => {
          window.myChart_main_chart.data.labels.push(element);
          })
        window.myChart_main_chart.data.datasets.forEach((dataset,index) => {

              if(index == 0) {
                  dataset.data = posi;
              } else if(index == 1){
                  dataset.data = nega;
              }
            });
          console.log('myChart_main_chart',window.myChart_main_chart);

        window.myChart_main_chart.update();
        $("#main_chartdiv").fadeIn();
        $("#main_chart_error").html('');
      }




    } else {
    // console.log('no data man')
        if (window.myChart_main_chart) {
        window.myChart_main_chart.data.labels = [];
        window.myChart_main_chart.data.datasets.forEach((dataset) => {
            $.each(dataset.data,function(index2,element2){
                   delete(dataset.data[index2]);
                  })
        });
        window.myChart_main_chart.update();
        }
      $("#main_chart_error").html(dataMessage);
      $("#main_chart").hide()

    }


}

function RedrawRepartition(reviewsF,data,lang) {


  var using=data;
  var language=lang;
  // var chartValues = [];
  
  var sortTemp =[];
    for(let key in using) {
      sortTemp.push({
        key: key,
        value: using[key]
      })
    } 

  sortTemp = sortTemp.sort(function(a,b){
    return b.value - a.value
  })


    let word = [];
    let occ = [];
 
 $.each(sortTemp,function(index,element) {
  word.push(element.key)
  occ.push(element.value)
})

 if(reviewsF.length > 0) {

    //incase the chart is non existent at the current scenario.
    if (!window.myChart_barchart_material) {
      console.log('smeghing wrong')
       var htitle = "";
    switch(localStorage.getItem('lang').toLowerCase())
    {
      case 'en' : htitle='Count'
            break;
      case 'fr' : htitle='compter';
            break;
    }

   var chartData = {
   type: 'horizontalBar',
   data: {
      labels: word,
    datasets: [
        {
            label: htitle,
            backgroundColor: "#6E388A",
            data: occ
        }
    ]
   },
   options: {
        barValueSpacing: 20,
        scales: {
            xAxes: [{
              barPercentage: 0.4,
                ticks: {
                    min: 0,
                }
            }]
        }
    }
} 

         window.canvas_barchart_material = document.getElementById('barchart_material');
         window.myChart_barchart_material = new Chart(window.canvas_barchart_material, chartData);

  window.canvas_barchart_material.onclick = function(evt) {
   var activePoint = myChart_barchart_material.getElementAtEvent(evt)[0];
   var data = activePoint._chart.data;
   var datasetIndex = activePoint._datasetIndex;
  var mainlabel=activePoint._model['label'];
   var label = data.datasets[datasetIndex].label;
   var value = data.datasets[datasetIndex].data[activePoint._index];
   // alert(mainlabel+' : '+label+' - '+value);
  // console.log(value)
   MainTopicLoadGroup(reviewsF,mainlabel,language);
  
   //console.log(mainlabel);
};


    } else {

        window.myChart_barchart_material.data.labels = [];
          word.forEach(element => {
          window.myChart_barchart_material.data.labels.push(element);
          })
          // window.myChart_barchart_material.data.datasets.data = [];
            window.myChart_barchart_material.data.datasets.forEach((dataset) => {
              $.each(dataset.data,function(index, second_val) {
                dataset.data[index] = occ[index];
              })
          });


  window.canvas_barchart_material.onclick = function(evt) {
   var activePoint = window.myChart_barchart_material.getElementAtEvent(evt)[0];
   var data = activePoint._chart.data;
   var datasetIndex = activePoint._datasetIndex;
   var mainlabel=activePoint._model['label'];
   var label = data.datasets[datasetIndex].label;
   var value = data.datasets[datasetIndex].data[activePoint._index];

   MainTopicLoadGroup(reviewsF,mainlabel,language);
  
};

    window.myChart_barchart_material.update();
    
    $("#barchart_material_error").html('');
    $("#barchart_material").show()
    }


 } else{
  // console.log('no data man')
      if (window.myChart_barchart_material) {
      window.myChart_barchart_material.data.labels = [];
      window.myChart_barchart_material.data.datasets.forEach((dataset) => {
          $.each(dataset.data,function(index2,element2){
                 delete(dataset.data[index2]);
                })
      });
      window.myChart_barchart_material.update();
      }
    $("#barchart_material_error").html(dataMessage);
    $("#barchart_material").hide()
}

} // end of redrawRepartition

// redraw Impression

function RedrawImpressionGraph(reviewsF,neg,pos,lang) {

    var negative = neg;
    var positive = pos;
    var language=lang;
    var chartValues = [];
    var htitle1 = htitle2 = "";
    switch(localStorage.getItem('lang').toLowerCase())
    {
      case 'en' : htitle1='positive'; htitle2 = 'negative';
            break;
      case 'fr' : htitle1='positif'; htitle2 = 'négatif';
            break;
    }

    let label = [];
    let posi = [];
    let nega =[];

 var negPosCount = getCountOfPosNeg(negative,positive);
    var sortedData = []; 
    if(negPosCount.pos_count > 0 && negPosCount.neg_count > 0) {
     sortedData = multiSortForImpression(negative,positive);

    } else {
       if(negPosCount.pos_count > 0 ) {
             sortedData = singularSort('pos',positive);
       } else {
            sortedData = singularSort('neg',negative)
       }
    }
    
    $.each(sortedData,function(index,element) {
    label.push(element.label);  
    posi.push(element.pos);
    nega.push(element.neg);
    })

        if(reviewsF.length > 0) {

            if(!window.myChart_new_chart) {

              var chartData = {
       type: 'horizontalBar',
       data: {
          labels: label,
        datasets: [
            {
                label: htitle1,
                backgroundColor: "#67BFB3",
                data: posi,
            },
            {
                label: htitle2,
                backgroundColor: "#C73729",
                data: nega,
            }
        ]
       },
       options: {
            barValueSpacing: 20,
            scales: {
                xAxes: [{
                  barPercentage: 0.4,
                    ticks: {
                        min: 0,
                    },
                    scaleLabel: {
                display: true,
             labelString: 'probability'
            }
                }]
            }
        }
    } 

     window.canvas_new_chart = document.getElementById('new_chart');
     window.myChart_new_chart = new Chart(window.canvas_new_chart, chartData);

    window.canvas_new_chart.onclick = function(evt) {
       var activePoint = window.myChart_new_chart.getElementAtEvent(evt)[0];
       var data = activePoint._chart.data;
       var datasetIndex = activePoint._datasetIndex;
      var mainlabel=activePoint._model['label'];
       var label = data.datasets[datasetIndex].label;
       var value = data.datasets[datasetIndex].data[activePoint._index];
       var message = [];
       message.push(mainlabel)
       message.push(label)
       // console.log(message)
        handleMainTopicImpressionGroup(reviewsF,message.toString(),language);

    };

        } else { // incase the graph is existent then updating

          window.myChart_new_chart.data.labels = [];
          label.forEach(element => {
          window.myChart_new_chart.data.labels.push(element);
          })
          // window.myChart_barchart_material.data.datasets.data = [];
          console.log('now checking')
          window.myChart_new_chart.data.datasets.forEach((dataset,index) => {

              if(index == 0) {
                  dataset.data = posi;
              } else if(index == 1){
                  dataset.data = nega;
              }
            });

            window.canvas_new_chart.onclick = function(evt) {
             var activePoint = window.myChart_new_chart.getElementAtEvent(evt)[0];
             var data = activePoint._chart.data;
             var datasetIndex = activePoint._datasetIndex;
            var mainlabel=activePoint._model['label'];
             var label = data.datasets[datasetIndex].label;
             var value = data.datasets[datasetIndex].data[activePoint._index];
             var message = [];
             message.push(mainlabel)
             message.push(label)
            handleMainTopicImpressionGroup(reviewsF,message.toString(),language);
            
          };

    window.myChart_new_chart.update();
  
    $("#myChart_new_chart_error").html('');
    $("#new_chart").show()

        }

    } else {

       // console.log('no data man')
      if (window.myChart_new_chart) {
      window.myChart_new_chart.data.labels = [];
      window.myChart_new_chart.data.datasets.forEach((dataset) => {
           $.each(dataset.data,function(index2,element2){
                 delete(dataset.data[index2]);
                })
      });
      window.myChart_new_chart.update();
      }
    $("#myChart_new_chart_error").html(dataMessage);
    $("#new_chart").hide()

    }


} // end of redrawImpression


function RedrawWordImpression(reviewsF,positive,negative,lang) {

  var language=lang;
    var chartValues = [];
    var htitle1 = htitle2 = "";
    switch(localStorage.getItem('lang').toLowerCase())
    {
      case 'en' : htitle1='positive'; htitle2 = 'negative';
            break;
      case 'fr' : htitle1='positif'; htitle2 = 'négatif';
            break;
    }

    let label = [];
    let pos = [];
    let neg =[];
      var negPosCount = getCountOfPosNeg(negative,positive);
      var sortedData = []; 
      if(negPosCount.pos_count > 0 && negPosCount.neg_count > 0) {
        sortedData = multiSort(negative,positive);

        } else {
          if(negPosCount.pos_count > 0 ) {
             sortedData = singularSort('pos',positive);
          } else {
              sortedData = singularSort('neg',negative)
            }
        }
    

    $.each(sortedData,function(index,element) {
    label.push(element.label);  
    pos.push(element.pos);
    neg.push(element.neg);
    })

    if(reviewsF.length > 0) {

            if(!window.myChart_WordImpressionGraph) {

              var chartData = {
       type: 'horizontalBar',
       data: {
          labels: label,
        datasets: [
            {
                label: htitle1,
                backgroundColor: "#67BFB3",
                data: pos,
            },
            {
                label: htitle2,
                backgroundColor: "#C73729",
                data: neg,
            }
        ]
       },
       options: {
 maintainAspectRatio: false,
            barValueSpacing: 20,
            scales: {
                xAxes: [{
                  barPercentage: 0.4,
                    ticks: {
                        min: 0,
                    },
                    scaleLabel: {
                display: true,
             labelString: 'probability'
            }
                }]
            }
        }
    } 
    


     window.canvas_WordImpressionGraph = document.getElementById('WordImpressionGraph');
     window.myChart_WordImpressionGraph = new Chart(window.canvas_WordImpressionGraph, chartData);

    window.canvas_WordImpressionGraph.onclick = function(evt) {
       var activePoint = window.myChart_WordImpressionGraph.getElementAtEvent(evt)[0];
       var data = activePoint._chart.data;
       var datasetIndex = activePoint._datasetIndex;
       var mainlabel=activePoint._model['label'];
       var label = data.datasets[datasetIndex].label;
       var value = data.datasets[datasetIndex].data[activePoint._index];
       var message = [];
       message.push(mainlabel)
       message.push(label)
       // console.log(message)
       WordImpressionGraphLoadGroup(reviewsF,message.toString(),lang);

    };

    $("#WordImpressionGraph_error").html('');
    $("#WordImpressionGraph").show()
    var height = label.length * 38;
  $("#WordImpressionGraph_container").css('height',height)

  window.addEventListener('resize', function () {
               window.myChart_WordImpressionGraph.resize()
            })

        } else { // incase the graph is existent then updating

  var height = label.length * 38;
  $("#WordImpressionGraph_container").css('height',height)

  window.addEventListener('resize', function () {
               window.myChart_WordImpressionGraph.resize()
            })

          window.myChart_WordImpressionGraph.data.labels = [];
          label.forEach(element => {
          window.myChart_WordImpressionGraph.data.labels.push(element);
          })

          window.myChart_WordImpressionGraph.data.datasets.forEach((dataset,index) => {
              if(index == 0) {
                  dataset.data = pos;
              } else if(index == 1){
                  dataset.data = neg;
              }
            });
          console.log(window.myChart_WordImpressionGraph)

            window.canvas_WordImpressionGraph.onclick = function(evt) {
             var activePoint = window.myChart_WordImpressionGraph.getElementAtEvent(evt)[0];
             var data = activePoint._chart.data;
             var datasetIndex = activePoint._datasetIndex;
             var mainlabel=activePoint._model['label'];
             var label = data.datasets[datasetIndex].label;
             var value = data.datasets[datasetIndex].data[activePoint._index];
             var message = [];
             message.push(mainlabel)
             message.push(label)
             WordImpressionGraphLoadGroup(reviewsF,message.toString(),lang);
            
          };
    window.myChart_WordImpressionGraph.update();
  
    $("#WordImpressionGraph_error").html('');
    $("#WordImpressionGraph").show()
        }

    } else {

      // console.log('no data man')
      if (window.myChart_WordImpressionGraph) {
      window.myChart_WordImpressionGraph.data.labels = [];
      window.myChart_WordImpressionGraph.data.datasets.forEach((dataset) => {
           $.each(dataset.data,function(index2,element2){
                 delete(dataset.data[index2]);
                })
      });
      window.myChart_WordImpressionGraph.update();
      }
        console.log('numma evidem ethum')
        console.log(window.myChart_WordImpressionGraph)

    $("#WordImpressionGraph_error").html(dataMessage);
   $("#WordImpressionGraph").hide()

    }


} // end of redrawImpression