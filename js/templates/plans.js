$(document).ready(function() {
  renderProducts(localStorage["lang"]);
});

$("#langs").change(function(e) {
  $("#loading").show();
  renderProducts($("#langs").val());
  switchLanguage();
});

function renderProducts(lang = "EN") {
  $.ajax({
    url: "api/products_curl.php",
    type: "POST",
    data: { action: "listProducts" },
    success: function(data) {
      var obj = $.parseJSON(data);
      //hi
      var productListData = "";

      let title,
        price,
        description,
        btnText = "";
      // static trial product
      if (lang === "EN") {
        title = "Trial";
        price = "free";
        description =
          "15 days of trial for your venue<br/>5 standard informations sources (Trip Advisor, hotels.com, Booking, Facebook, Google)";
        btnText = "Start Trial";
      } else {
        title = "Essai";
        price = "Gratuit";
        description =
          "15 jours d’essai pour votre établissement<br/>5 sources d’informations standard (Trip Advisor, hotels.com, Booking, Facebook, Google)";
        btnText = "Commencer l’essai";
      }
      let productTrial = `<div class="col-lg-3 col-md-6 col-sm-12 m-t-25">
          <div class="product-info">
            <div class="whitebox-title p-4 text-center">
              <div class="title">
                <h3>${title}</h3>
              </div>
              <div class="price-view lead mt-3">
                <span class="bold red">${price}</span>
              </div>
              <div class="btn mt-4">
                <a class="btn btn-app bg-purple" onclick="redirectPlans('trial')">${btnText}</a>
              </div>
              <div class="info">
                <p class="mb-0">${description}</p>
              </div>
            </div>
          </div>
        </div>`;

      // static elite product

      if (lang === "EN") {
        description =
          "You are a group, and/or you own several venues that you need to track separately and/or together, you are an administration in charge of an area…<br/>We have dedicated offers for you.<br/>Contact us of a customised offer.";
        btnText = "Contact Us";
      } else {
        description =
          "Vous êtes un groupe et/ou disposez de plusieurs établissements que vous souhaitez suivre indépendamment et/ou conjointement, vous êtes un institutionnel en charge d’un territoire…<br/>Nous vous offrons des conditions particulières.<br/>Contactez-nous pour une offre personnalisée.";
        btnText = "Nous contacter";
      }

      let productElite = `<div class="col-lg-3 col-md-6 col-sm-12 m-t-25">
          <div class="product-info">
            <div class="whitebox-title p-4 text-center">
              <div class="title">
                <h3>Platinum</h3>
              </div>
              <div class="btn mt-4">
                <a class="btn btn-app bg-purple" onclick="redirectPlans('elite')">${btnText}</a>
              </div>
              <div class="info">
                <p class="mb-0">${description}</p>
              </div>
            </div>
          </div>
        </div>`;
      obj.result.forEach(function(item) {
        if (item["active"]) {
          var description;
          var id = item["id"].trim();

          var name;
          var price;
          var buttonText = "";
          var productData = "";
          var amount = "";

          switch (lang) {
            case "EN":
              description = item["description-en"];
              name = item["name-en"];
              price = "$" + item["price-USD"];
              buttonText = "Subscribe";
              amount = item["price-USD"];

              break;
            case "FR":
              description = item["description-fr"];
              name = item["name-fr"];
              price = "€" + item["price-EUR"];
              buttonText = "S'abonner";
              amount = item["price-EUR"];

              break;
            default:
              description = item["description-en"];
              name = item["name-en"];
              price = "$" + item["price-USD"];
          }

          //   if (user_subscribed_product) {
          //     if (id == user_subscribed_product) {
          //       $("#plan_name").text(name);
          //     }
          //   }

          productData += '<div class="col-lg-3 col-md-6 col-sm-12 m-t-25">';
          productData += '<div class="product-info">';
          productData += '<div class="whitebox-title p-4 text-center">';
          productData += '<div class="title">';
          productData += "<h3>" + name + "</h3>";
          productData += "</div>";
          productData += '<div class="price-view lead mt-3">';
          productData += '<span class="bold red">' + price + "*</span>";
          productData += "</div>";
          productData += '<div class="btn mt-4">';
          productData +=
            `<a class="btn btn-app bg-purple"  onclick="purchase('` +
            id +
            `');" >` +
            buttonText +
            `</a>`;
          productData += "</div>";
          productData += '<div class="info">';
          productData += '<p class="mb-0">' + description + "</p>";
          productData += "</div>";
          productData += "</div>";
          productData += "</div>";
          productData += "</div>";
          productListData += productData;
        }
      });

      $("#productList").html(productTrial);
      $("#productList").append(productListData);
      $("#productList").append(productElite);

      $("#loading").fadeOut();
    }
  });
}

function purchase(Id) {
  $.ajax({
    type: "POST",
    url: "profile.php",
    data: {
      action: "getdata"
    },
    success: function(response) {
      if (response == "false") {
        window.location = "index.php?cont=cGxhbnMucGhw";
      }
    }
  });
  return false;

  $("#loading").show();
  $.ajax({
    url: "api/sca_session_creator.php",
    type: "POST",
    data: {
      productId: Id,
      authscreate: "spSCSESS345xx",
      currency: localStorage["lang"]
    },
    datatype: "JSON",
    success: function(data) {
      var session_id;
      if (data != "nil") {
        session_id = data.trim();
        const stripe = Stripe("pk_live_8v0kB9r83On7lEZCT07K5Yzs00JDTzC1ju");
        stripe
          .redirectToCheckout({
            // Make the id field from the Checkout Session creation API response
            // available to this file, so you can provide it as parameter here
            // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
            sessionId: session_id
          })
          .then(result => {
            // If `redirectToCheckout` fails due to a browser or network
            // error, display the localized error message to your customer
            // using `result.error.message`.
          });
      }
    }
  });
}

function redirectPlans(plan) {
  if (plan == "trial") {
    window.location = "newuser.php";
  } else {
    window.location = "https://spella.com/contact/";
  }
}
