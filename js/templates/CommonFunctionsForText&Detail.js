function getCountOfPosNeg(negative,positive) {
      var negCount = 0;
      var posCount = 0;
    for(let key in negative,positive) {
      if(negative[key] >0) {
        negCount++;
      }
      if(positive[key] >0){
        posCount++;
      }
    }
    return {
      pos_count : posCount,
      neg_count : negCount
    }
}


function singularSort(type,using) {
  var sortTemp =[];
  for(let key in using) {
    if(using[key] !== 0) {
    sortTemp.push({
      label: key,
      neg : type == 'neg' ? using[key] : 0,
      pos : type == 'pos' ? using[key] : 0
    })
    }

  } 

    sortTemp = sortTemp.sort(function(a,b){
        return (b.pos + b.neg) - (a.pos + a.neg)
      })

  return sortTemp;
}

function multiSortForImpression(negative,positive) {
  var tempSort = [];
  console.log(negative)
  console.log(positive)
  for(let key in negative,positive) {

    tempSort.push({
      label : key,
      neg : negative[key],
      pos : positive[key]
    })
   

  } 

  console.log(tempSort);

  tempSort = tempSort.sort(function(a,b){
    return (b.pos + b.neg) - (a.pos + a.neg)
  })

  return tempSort;

}



function multiSort(negative,positive) {
  var tempSort = [];
  console.log(negative)
  console.log(positive)
  for(let key in negative,positive) {
    if(negative[key] !== 0 || positive[key] !== 0)
   {
    tempSort.push({
      label : key,
      neg : negative[key],
      pos : positive[key]
    })
   }

  } 

  console.log(tempSort);

  tempSort = tempSort.sort(function(a,b){
    return (b.pos + b.neg) - (a.pos + a.neg)
  })

  return tempSort;

}


function getName(data)
        {   
            var id=data;
            var idName={};
                    $.ajax({
                    url:"api/add_venue_curl.php",
                    type:"POST",
                    data:{"action":"getServices"},
                    success:function(data){
                        obj= $.parseJSON(data);
                        $.each(obj.services, function(index, element) 
                        {
                            if(element.id===id)
                            {  
                             idName.idName="https://spella.com/services/logos-mini/"+element.icon
                             idName.idService=element.shortName;
                             idName.idServiceName=element.shortName;
                            }
                      });
                    },error:function(){
                        alert("Not responding error, refresh page ");
                    },async:false
                });
                   return idName;
            }

function getServiceUrl(serviceName,venueId)
{

  var venueDetails = JSON.parse(localStorage.getItem("venueDetails"+venueId))

  var url = ""
    $.each(venueDetails.result.services,function(index,element){
      
      if(element.serviceName === 'gplus' && serviceName === 'googleplus') {
        url = element.serviceVenueURL;
      }

      if(element.serviceName===serviceName)
      {
        url = element.serviceVenueURL;
      }

    })
return url;
}


function getStar(data)
            {
             var i;
             var str=0;
             for(i=0;i<data;i++)
             {
                str+=`<i class="fas fa-star"></i>`;
             }
             var s='';
             try{
              s= str.replace(/^0+/, '');              
             }
             catch(err){
              console.log(err)
              s= '';
             }

                return s;
            }

function trimByWord(sentence,index1) {
var result = sentence;
var resultArray = result.split(" ");
if(resultArray.length > 10){
resultArray = resultArray.slice(0, 50);
result = resultArray.join(" ") +" "+ `<span class="more" onclick="more(`+index1+`);">more...</span>`;
}
return result;
}

function transl_call(value,title)
{

    var shortid='#short'+value;
  var transb='#transb'+value;
  var transtitle='#transtitle'+value;
  $(transb).hide();
  var org= '#default'+value;
  var orgtitle='#defaulttitle'+value;
  var translate ='#translate'+value;
  var rever='#rever'+value;
  $(shortid).hide();
  $(orgtitle).hide();
  $(org).hide();
  $(translate).fadeIn('500');
  $(transtitle).fadeIn();
  $(rever).fadeIn();
  
 }
 function revert_call(value)
{
  var orgtitle='#defaulttitle'+value;
  var transtitle='#transtitle'+value;

  var transb='#transb'+value;
  var translate ='#translate'+value;
  var org= '#short'+value;
  var rever='#rever'+value;
  
  $(transtitle).hide();
  $(translate).hide();
  $(orgtitle).fadeIn();
  $(org).fadeIn();
  $(transb).fadeIn();
  $(rever).hide();
 }

 function more(index1)
{

fullid='#default'+index1;
shortid='#short'+index1;

$(shortid).hide();
$(fullid).fadeIn();

}

function less(index1)
{

fullid='#default'+index1;
shortid='#short'+index1;

$(fullid).hide();
$(shortid).fadeIn();

}

 function getDates()
{
  var dates;
  $.ajax({
    url:"api/venue_details_curl.php",
    type:"POST",
    data:{"action":"getDates"},
    success:function(data) {  
       dates=data;
       console.log(data);
        },error:function(data)
        {
          console.log(data);
        },
        async: false
       });
  return dates;
}


function getFormattedDate(date)
{

  langD = localStorage.getItem('lang').toLowerCase();
  var dateD = date.split("-")
  var mon = '';

var enMonths = {
 01: 'Jan',
 02: 'Feb',
 03: 'Mar',
 04:'Apr.',
 05: 'May',
 06: 'Jun',
 07: 'Jul',
 08: 'Aug',
 09: 'Sep',
 10: 'Oct',
 11: 'Nov',
 12: 'Dec'
};


var frMonths = {
  01:'Janv.',
  02:'Févr.',
  03:'Mars',
  04:'Avr.',
  05: 'Mai',
  06:'Juin',
  07:'Juil.',
  08:'Août',
  09:'Sept.',
  10:'Oct.',
  11:'Nov.',
  12:'Déc.'
};



switch(langD){

  case 'en' : mon = enMonths[Number(dateD[1])];
        break;
  case 'fr' : mon= frMonths[Number(dateD[1])];
        break;
  default : mon= enMonths[Number(dateD[1])];
        break;
}


return "on"+" "+mon+" "+dateD[2]+" "+dateD[0];


}