var transtent='';
var revertent='';
    var ratingData= {
      count:0,
      totalRating:0,
      sentence:[]
    }
switch(localStorage.getItem('lang').toLowerCase())
{
  case 'en' :  ratingData.sentence[0]=' comments on '
              ratingData.sentence[1]= ' reviews ';
               transtent = "Translate";
               revertent = "Revert";
              break;

  case 'fr' : ratingData.sentence[0]=' commentaires sur ';
              ratingData.sentence[1]=' avis ';
              transtent = "Traduction";
              revertent = "Revenir";
              break;
  default :   ratingData.sentence[0]=' comments on '
              ratingData.sentence[1]= ' reviews ';
               transtent = "Traduction";
               revertent = "Revert";
              break;
}



function Load_DuplicationRData(message,venueDetailData)
{
  // console.log(venueDetailData)
  ratingData.count=0;
  ratingData.totalRating=0;
  if(message==='nothing')
{
  return false;
}
$(".list").html("")
var transtent='';
var revertent = '';
var langT = localStorage.getItem("lang").toLowerCase();

var venueD=$.parseJSON(venueDetailData);
// console.log(localStorage.getItem("venueData"))
var temp = [];

      $.each(venueD.result.services,function(index,element){
        var service= getName(element.serviceId);
          var serviceUrl=getServiceUrl(service.idService,venueid)
        $.each(element.reviews,function(index1,element1){
          element1.serviceId = element.serviceId;
          element1.service = service;
          element1.serviceUrl = serviceUrl;
          temp.push(element1);
        })
      })
      venueD.result.services = temp;
      venueD.result.services.sort(function(a,b){
        var dateA=new Date(a.date), dateB=new Date(b.date)
        return dateB-dateA //sort by date ascending
      });

var using = message.split(",");
var month;

var enMonths = {
  'Jan':01,
  'Feb':02,
  'Mar':03,
  'Apr':04,
  'May':05,
  'Jun':06,
  'Jul':07,
  'Aug':08,
  'Sep':09,
  'Oct':10,
  'Nov':11,
  'Dec':12
};


var frMonths = {
  'Janv.':01,
  'Févr.':02,
  'Mars':03,
  'Avr.':04,
  'Mai':05,
  'Juin':06,
  'Juil.':07,
  'Août':08,
  'Sept.':09,
  'Oct.':10,
  'Nov.':11,
  'Déc.':12
};
var ratings=0;
var starnumber=0;
switch(localStorage.getItem('lang').toLowerCase())
{
  case 'en' : month= enMonths[using[0]];    
              break;
  case 'fr' : month= frMonths[using[0]];
              break;
}

var revMonth=[];

    var arr=[];
if(using[1]==='positive')
{
$.each(venueD.result.services,function(index1,element1){
 
      revMonth = element1.date.split("-");

          if(Number(element1.note)>=Number(3.5) && Number(revMonth[1].toString())==Number(month.toString()))
          {    
                var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var simpleDate = element1.date;
                var date = getFormattedDate(element1.date)
                ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
                if(sentence.length > 0)
                {
                  $(".list").append(`
 <li class="venue-review ">
            <div class="venue-logo">
              <p style="display: none;" class="realDate">`+simpleDate+`</p>
              <a href="`+element1.serviceUrl+`" target="_blank"><img src="`+element1.service.idName+`" class="img-fluid" alt=""></a>
              <p style="display:none" class="service">`+element1.service.idServiceName+`</p> 
            </div>
             <div class="venue-title">
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
                 if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }


                }
           
          }


  });

var monkeyList = new List('test-list', {
  valueNames: ['disdate','diftitle','title-en','full','full-en','service','realDate'],
  page: 10,
  pagination: true
});
  }
else if(using[1]==='negative')
{
$.each(venueD.result.services,function(index1,element1){
 
      revMonth = element1.date.split("-");
          if(Number(element1.note)<Number(3.5) && Number(revMonth[1].toString())==Number(month.toString()))
          {
                var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var simpleDate = element1.date;
                var date = getFormattedDate(element1.date)
                ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
                if(sentence.length > 0)
                {
                   $(".list").append(`
 <li class="venue-review ">
            <div class="venue-logo">
            <p style="display: none;" class="realDate">`+simpleDate+`</p>
              <a href="`+element1.serviceUrl+`" target="_blank"><img src="`+element1.service.idName+`" class="img-fluid" alt=""></a>
              <p style="display:none" class="service">`+element1.service.idServiceName+`</p> 
            </div>
             <div class="venue-title">
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
                if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }         
         

                }
           
          }


  });


var monkeyList = new List('test-list', {
  valueNames: ['disdate','diftitle','title-en','full','full-en','service','realDate'],
  page: 10,
  pagination: true
});
  }
var head = ratingData.count+ratingData.sentence[0]+ratingData.totalRating+ ratingData.sentence[1];
  $(".reviewsHeading").html(head)
        $('html, body').animate({
        scrollTop: $("#tableAnchor").offset().top
    }, 2000);


}


function Load_DuplicationStar(value,venueDetailData)
{
$(".list").html("")

ratingData.count=0;
ratingData.totalRating=0;

   var venueD=$.parseJSON(venueDetailData);

var temp = [];

      $.each(venueD.result.services,function(index,element){
        var service= getName(element.serviceId);
          var serviceUrl=getServiceUrl(service.idService,venueid)
        $.each(element.reviews,function(index1,element1){
          element1.serviceId = element.serviceId;
          element1.service = service;
          element1.serviceUrl = serviceUrl;
          temp.push(element1);
        })
      })
      venueD.result.services = temp;
      venueD.result.services.sort(function(a,b){
        var dateA=new Date(a.date), dateB=new Date(b.date)
        return dateB-dateA //sort by date ascending
      });


var notN= value[0];
console.log("==============elementNote")
$.each(venueD.result.services,function(index1,element1){
  
      console.log(notN.toString())
      console.log("==============elementNote")
      console.log(Number(element1.note).toString())

      if(Number(element1.note).toString()==notN.toString())
      {
             var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var simpleDate = element1.date;
                var date = getFormattedDate(element1.date)
                ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
                if(sentence.length > 0)
                {
                             $(".list").append(`
 <li class="venue-review ">
            <div class="venue-logo">
            <p style="display: none;" class="realDate">`+simpleDate+`</p>
              <a href="`+element1.serviceUrl+`" target="_blank"><img src="`+element1.service.idName+`" class="img-fluid" alt=""></a>
              <p style="display:none" class="service">`+element1.service.idServiceName+`</p> 
            </div>
             <div class="venue-title">
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
                if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }

                }     


      }          


  });


var monkeyList = new List('test-list', {
  valueNames: ['disdate','diftitle','title-en','full','full-en','service','realDate'],
  page: 10,
  pagination: true
});
var head = ratingData.count+ratingData.sentence[0]+ratingData.totalRating+ ratingData.sentence[1];
$(".reviewsHeading").html(head)

$('html, body').animate({
        scrollTop: $("#tableAnchor").offset().top
    }, 2000);


}


function morrisHanDuplication(id,service,venueDetailData)
{
$(".list").html("")
ratingData.count=0;
ratingData.totalRating=0;


var venueD= $.parseJSON(venueDetailData);
var temp = [];

      $.each(venueD.result.services,function(index,element){
        var service= getName(element.serviceId);
          var serviceUrl=getServiceUrl(service.idService,venueid)
        $.each(element.reviews,function(index1,element1){
          element1.serviceId = element.serviceId;
          element1.service = service;
          element1.serviceUrl = serviceUrl;
          temp.push(element1);
        })
      })
      venueD.result.services = temp;
      venueD.result.services.sort(function(a,b){
        var dateA=new Date(a.date), dateB=new Date(b.date)
        return dateB-dateA //sort by date ascending
      });

console.log("==================");
console.log(venueD);
var serviceId;
var serviceData=$.parseJSON(localStorage.getItem("servicesjson"));

$.each(serviceData.services,function(index,element)
{
  if(element.domainKey===service)
  {
    serviceId=element.id;
      }
});

    $.each(venueD.result.services,function(index,element){
      
      if(element1.serviceId===serviceId)
      {
          
                var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var simpleDate = element1.date;
                var date = getFormattedDate(element1.date)
                ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
                if(sentence.length > 0)
                {
                    $(".list").append(`
  <li class="venue-review ">
            <div class="venue-logo">
            <p style="display: none;" class="realDate">`+simpleDate+`</p>
              <a href="`+element1.serviceUrl+`" target="_blank"><img src="`+element1.service.idName+`" class="img-fluid" alt=""></a>
              <p style="display:none" class="service">`+element1.service.idServiceName+`</p> 
            </div>
             <div class="venue-title">
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
                  if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }

                }
                   
          
         
          
      }

    });
var monkeyList = new List('test-list', {
  valueNames: ['disdate','diftitle','title-en','full','full-en','service','realDate'],
  page: 10,
  pagination: true
});
var head = ratingData.count+ratingData.sentence[0]+ratingData.totalRating+ ratingData.sentence[1];
$(".reviewsHeading").html(head)

       $('html, body').animate({
        scrollTop: $("#tableAnchor").offset().top
    }, 2000); 

  
}