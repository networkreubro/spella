window.canvas_array_for_redraw = [];
window.chartData_array_for_redraw = [];
var can_counter_for_redraw = 0;


function RedrawDefaultBarchart(id,venueDetailData) {
	var htmlid = '#'+id;
	var htmlerror_id = '#'+id+'_error';
	var canvasData_forUpdate;
	var chartData_forUpdate;
	$.each(window.graphData,function(index,element) {
		if(element.id === id) {
			canvasData_forUpdate = element.canvas_data;
			chartData_forUpdate = element.chart_data;
		} 

	});

	var venueD = $.parseJSON(venueDetailData);


  var titles="";
  var htitle1 = htitle2 = "";
  var subtitles = "";
  switch(localStorage.getItem("lang").toLowerCase())
  {
    case "en" : titles = "Negatives Vs Positives"; htitle1='positive'; htitle2 = 'negative'; xaxislabel= 'count'; yaxislabel='month';
                // subtitles =" Number of negatives vs positives " 
                break;
    case "fr" : titles = "Négatifs vs Positifs"; htitle1='positif'; htitle2 = 'négatif'; xaxislabel= 'compter'; yaxislabel='mois';
                // subtitles = "nombre d'avis positifs et négatifs";
                break;
  }
    

  var NPdata= getNPdata(venueDetailData);


    let label = [];
    let posi = [];
    let nega =[];

    for (var key in NPdata){
    if(key!=0){
   //console.log(NPdata[key][0]);
    label.push(NPdata[key][0]);
    posi.push(NPdata[key][1]);
    nega.push(NPdata[key][2]);
    
    //chartValues.push(pair);
    }
    }


	if(venueD.result.services.length > 0) {
		
		if(!chartData_forUpdate) {

   var chartData = {
       type: 'bar',
       data: {
          labels: label,
        datasets: [
            {
                label: htitle1,
                backgroundColor: "#67BFB3",
                data: posi,
            },
            {
                label: htitle2,
                backgroundColor: "#C73729",
                data: nega,
            }
        ]
       },
       options: {
            barValueSpacing: 20,
            scales: {
                yAxes: [{
                  barPercentage: 0.4,
                    ticks: {
                        min: 0,
                    },
                    scaleLabel: {
                   display: true,
                   labelString: xaxislabel
      }
                }],
                xAxes: [{
          scaleLabel: {
          display: true,
          labelString: yaxislabel
            }
             }]
            }
        }
    } 

  window.canvas_array_for_redraw[can_counter_for_redraw] = document.getElementById(id);
  window.chartData_array_for_redraw[can_counter_for_redraw] = new Chart(  window.canvas_array_for_redraw[can_counter_for_redraw], chartData);

  window.canvas_array_for_redraw[can_counter_for_redraw].onclick = function(evt) {
    
    var activePoint = window.chartData_array_for_redraw[can_counter_for_redraw].getElementAtEvent(evt)[0];
     var data = activePoint._chart.data;
     var datasetIndex = activePoint._datasetIndex;
    var mainlabel=activePoint._model['label'];
     var label = data.datasets[datasetIndex].label;
     var value = data.datasets[datasetIndex].data[activePoint._index];
     var message = [];
     message.push(mainlabel)
     message.push(label)

     // handleMainTopicImpression(,language,venueDetailData)
     Load_RData(message.toString(),venueDetailData);

  };
  //saving data for updation and future actions

  window.graphData.push({
      id : id,
      canvas_data : window.canvas_array_for_redraw[can_counter_for_redraw],
      chart_data : window.chartData_array_for_redraw[can_counter_for_redraw]
    
  });
    $(htmlerror_id).html('');
    $(htmlid).show()
    can_counter_for_redraw ++;
		} else {

          chartData_forUpdate.data.labels = [];
          label.forEach(element => {
          chartData_forUpdate.data.labels.push(element);
          })
          // window.myChart_barchart_material.data.datasets.data = [];
          console.log('now checking')
          chartData_forUpdate.data.datasets.forEach((dataset,index) => {

              if(index == 0) {
                  dataset.data = posi;
              } else if(index == 1){
                  dataset.data = nega;
              }
            });

          // console.log(canvas)
            canvasData_forUpdate.onclick = function(evt) {
              var activePoint = chartData_forUpdate.getElementAtEvent(evt)[0];
             var data = activePoint._chart.data;
             var datasetIndex = activePoint._datasetIndex;
            var mainlabel=activePoint._model['label'];
             var label = data.datasets[datasetIndex].label;
             var value = data.datasets[datasetIndex].data[activePoint._index];
             var message = [];
             message.push(mainlabel)
             message.push(label)

             // handleMainTopicImpression(,language,venueDetailData)
             Load_RData(message.toString(),venueDetailData);
            
          };

        chartData_forUpdate.update();
  
  //updating the graphdata with newer data
  $.each(window.graphData,function(index,element) {

        if(element.id === id) {
          element.canvas_data = canvasData_forUpdate;
          element.chart_data  = chartData_forUpdate;
        } 

      })      


    $(htmlerror_id).html('');
    $(htmlid).show()


		}


	} else {
		  // console.log('no data man')
      if (chartData_forUpdate) {
      chartData_forUpdate.data.labels = [];
      chartData_forUpdate.data.datasets.forEach((dataset) => {
          $.each(dataset.data,function(index2,element2){
                 delete(dataset.data[index2]);
                })
      });
      chartData_forUpdate.update();
      }
    $(htmlerror_id).html(dataMessage);
    $(htmlid).hide()

	}


}


// code for redrawing the start table

function Redrawstarloader(id,venueDetailData) {

  var htmlid = '#'+id;
  var htmlerror_id = '#'+id+'_error';

  var canvasData_forUpdate;
  var chartData_forUpdate;
  $.each(window.graphData,function(index,element) {

    if(element.id === id) {
      canvasData_forUpdate = element.canvas_data;
      chartData_forUpdate = element.chart_data;
    } 

  })


  var usingVenueData = $.parseJSON(venueDetailData);
    var titles = "";
    var titles2 = "";
    var starts = "";
  switch(localStorage.getItem("lang").toLowerCase())
  {
    case 'en' : titles = "Number of Notes";
                titles2 = "Number of Notes for past five months";
                stars = "Stars";
                break;
    case 'fr' : titles =  "Nombre de notes";
                titles2 = "Nombre de notes pour les cinq derniers mois";
                stars = "étoiles";
                break;
  }


  if(usingVenueData.result.services.length > 0) {

  var venueNotes1Count = 0;
  var venueNotes2Count = 0;
  var venueNotes3Count = 0;
  var venueNotes4Count = 0;
  var venueNotes5Count = 0;

  $.each(usingVenueData.result.services,function(index,element){
    $.each(element.reviews,function(index2,element2) {
      switch(element2.note) {
        case 1 : venueNotes1Count++;
                  break;
        case 2 : venueNotes2Count++;
                  break;
        case 3 : venueNotes3Count++;
                  break;
        case 4 : venueNotes4Count++;
                  break;
        case 5 : venueNotes5Count++;
                  break;
      } 
    })
  })


var maxVal = Math.max(venueNotes1Count,venueNotes2Count,venueNotes3Count,venueNotes4Count,venueNotes5Count)

var adder = parseInt(0.15*maxVal);
var maxVal = adder+maxVal;




      if(!chartData_forUpdate) {

          //drawing chart if not already existent(just in case)


      var chartData = {
         type: 'horizontalBar',
         data: {
            labels: ["1(★)","2(★)","3(★)","4(★)","5(★)"],
          datasets: [
              {
                  label: titles,
                  backgroundColor: "#67BFB3",
                  data: [venueNotes1Count,venueNotes2Count,venueNotes3Count,venueNotes4Count,venueNotes5Count]
              }
             
          ]
         },
         options: {
              barValueSpacing: 20,
              scales: {
                  yAxes: [{
                      scaleLabel: {
                display: true,
                labelString: stars
                  },
                      ticks: {
                          min: 0,
                      }
                  }],
                  xAxes: [{
                ticks : {
                  beginAtZero: true,
                  max : maxVal
                  },
                scaleLabel: {
                display: true,
                labelString: titles2
                  }
                   }]
              }
          }
      } 

       window.canvas_array_for_redraw[can_counter_for_redraw] = document.getElementById(id);
       window.chartData_array_for_redraw[can_counter_for_redraw] = new Chart(window.canvas_array_for_redraw[can_counter_for_redraw], chartData);


      window.canvas_array_for_redraw[can_counter_for_redraw].onclick = function(evt) {
         var activePoint = window.chartData_array_for_redraw[can_counter_for_redraw].getElementAtEvent(evt)[0];
         var data = activePoint._chart.data;
         var datasetIndex = activePoint._datasetIndex;
          var mainlabel = data.labels[activePoint._index];
          var label = data.datasets[datasetIndex].label;
          var value = mainlabel.split('')[0];
            
          Load_Star(value,venueDetailData);

       
      };

      window.graphData.push({
          id : id,
          canvas_data : window.canvas_array_for_redraw[can_counter_for_redraw],
          chart_data : window.chartData_array_for_redraw[can_counter_for_redraw]
        
      });
       can_counter_for_redraw++;
          //end of   
    $(htmlerror_id).html('');
    $(htmlid).show()
      } else { // the else part of updating graph
      var maxVal = Math.max(venueNotes1Count,venueNotes2Count,venueNotes3Count,venueNotes4Count,venueNotes5Count)

      var adder = parseInt(0.15*maxVal);
      var maxVal = adder+maxVal;

      delete(chartData_forUpdate.config.options.scales.xAxes[0].ticks.max)
      chartData_forUpdate.config.options.scales.xAxes[0].ticks.max = maxVal;

      console.log('urgnt',chartData_forUpdate.config.options.scales.xAxes[0])

      chartData_forUpdate.data.datasets.forEach((dataset) => {
        $.each(dataset,function(index, second_val) {
          dataset.data = [venueNotes1Count,
                          venueNotes2Count,
                          venueNotes3Count,
                          venueNotes4Count,
                          venueNotes5Count];
                  })
    });

  canvasData_forUpdate.onclick = function(evt) {
         var activePoint = chartData_forUpdate.getElementAtEvent(evt)[0];
         var data = activePoint._chart.data;
         var datasetIndex = activePoint._datasetIndex;
          var mainlabel = data.labels[activePoint._index];
          var label = data.datasets[datasetIndex].label;
          var value = mainlabel.split('')[0];
            
          Load_Star(value,venueDetailData);
       
      };
    chartData_forUpdate.update();

      //updating the graphdata with newer data
  $.each(window.graphData,function(index,element) {

        if(element.id === id) {
          element.canvas_data = canvasData_forUpdate;
          element.chart_data  = chartData_forUpdate;
        } 

      })      


          $(htmlerror_id).html('');
            $(htmlid).show()


      } // the else part of updating graph

    } else {

    if (chartData_forUpdate) {

      // chartData_forUpdate.data.labels = [];
      chartData_forUpdate.data.datasets.forEach((dataset) => {
          $.each(dataset.data,function(index2,element2){
                 delete(dataset.data[index2]);
                })
      });

      chartData_forUpdate.update();
      }
    $(htmlerror_id).html(dataMessage);
    $(htmlid).hide()

  }

  }//function end


  function RedrawpositiveNegativeTendecy(id,curvedata) {

  var htmlid = '#'+id;
  var htmlerror_id = '#'+id+'_error';
  var canvasData_forUpdate;
  var chartData_forUpdate;
  $.each(window.graphData,function(index,element) {

    if(element.id === id) {
      canvasData_forUpdate = element.canvas_data;
      chartData_forUpdate = element.chart_data;
    } 

  })





    var months=[];
    var obj= $.parseJSON(curvedata);
    var i=0;
    var positive=[];
    var negative=[];


  var titles = "";
  var monu ="";
  var neg = "";
  var pos ="";
          let label = [];
          let posi = [];
          let nega =[];
 switch(localStorage.getItem('lang').toLowerCase())
 {
  case 'en' : months_overall = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 
                                 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
              titles="Percentage of Negative vs positive";
              monu = "Months";
              neg = "Negatives";
              pos = "Positives";
              break;

  case 'fr' : months_overall = ['Janv.','Févr.','Mars','Avr.','Mai','Juin',
                                'Juil.','Août','Sept.','Oct.','Nov.','Déc.'];
              titles = "Pourcentage de négatif vs positif";
              monu =  "Mois";
              neg = "Négatifs"
              pos = "Positifs"
              break;

  default :  months_overall = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
              titles="Percentage of Negative vs positive";
              break;

 }


    if(obj.result.services.length > 0) {

      $.each(obj.result.stats,function(index,element){
          months[i]=element.yearMonth;
          var total = parseInt(element.venuePositiveNotesCount+element.venueNegativeNotesCount)
          // console.log("================"+total)
          // console.log("==========="+element.venuePositiveNotesCount)
    positive[i]=((element.venuePositiveNotesCount/total)*100).toFixed(2);;
    negative[i]=((element.venueNegativeNotesCount/total)*100).toFixed(2);;
           i++;
          });

      var j=0;
      var new_months=[];
      $.each(months,function(index,element){
       new_months[j] = months_overall[+element.split('-')[1] - 1];
          j++;
      });




        for(var i=0;i<new_months.length;i++)
        {
            label.push(new_months[i]);
            posi.push(positive[i]);
            nega.push(negative[i])
        }

          if(!chartData_forUpdate) {

                        var data = {
              labels: label,
              datasets: [{
                  label: pos,
                  lineTension: 0.5,
                  backgroundColor: "#42BD92",
                  borderColor: "#42BD92",
                  borderCapStyle: 'butt',
                  borderDash: [],
                  borderDashOffset: 0.0,
                  borderJoinStyle: 'miter',
                  pointBorderColor: "white",
                  pointBackgroundColor: "black",
                  pointBorderWidth: 1,
                  pointHoverRadius: 6,
                  pointHoverBackgroundColor: "#42BD92",
                  pointHoverBorderColor: "black",
                  pointHoverBorderWidth: 1,
                  pointRadius: 4,
                  pointHitRadius: 5,
                  data: posi,
                  spanGaps: false,
                },
                {
                  label: neg,
                  lineTension: 0.5,
                  backgroundColor: "#BF3A31",
                  borderColor: "#BF3A31", // The main line color
                  borderCapStyle: 'butt',
                  borderDash: [], // try [5, 15] for instance
                  borderDashOffset: 0.0,
                  borderJoinStyle: 'miter',
                  pointBorderColor: "black",
                  pointBackgroundColor: "white",
                  pointBorderWidth: 1,
                  pointHoverRadius: 6,
                  pointHoverBackgroundColor: "red",
                  pointHoverBorderColor: "black",
                  pointHoverBorderWidth: 1,
                  pointRadius: 4,
                  pointHitRadius: 5,
                  data: nega,
                  spanGaps: true,
                }

              ]
            };

            // Notice the scaleLabel at the same level as Ticks
            var options = {
              scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            },
                            scaleLabel: {
                                 display: true,
                                 labelString: titles,
                                 
                              }
                        }],
                        xAxes: [{
                            scaleLabel: {
                                 display: true,
                                 labelString: monu,
                                 
                              },
                              gridLines: {
                                display : false
                              }
                        }]             
                    },
                    elements: {
                                    line: {
                                            fill: false,

                                    }
                                } 
            };

            // Chart declaration:
            window.canvas_array_for_redraw[can_counter_for_redraw] = document.getElementById(id);
            window.chartData_array_for_redraw[canvas_array_for_redraw] = new Chart(window.canvas_array_for_redraw[can_counter_for_redraw], {
              type: 'line',
              data: data,
              options: options
            });

            window.canvas_array_for_redraw[can_counter_for_redraw].onclick = function(evt) {
               var activePoint = window.chartData_array_for_redraw[canvas_array_for_redraw].getElementAtEvent(evt)[0];
               var data = activePoint._chart.data;
               var datasetIndex = activePoint._datasetIndex;
                var mainlabel = data.labels[activePoint._index];
                var label = data.datasets[datasetIndex].label;
               var value = data.datasets[datasetIndex].data[activePoint._index];
                
                let message = [];
                message.push(label.toLowerCase())
                message.push(mainlabel)
              positiveOpinionReviewLoader(message.toString(),curvedata);

             
            };

                
                window.graphData.push({
                id : id,
                canvas_data : window.canvas_array_for_redraw[can_counter_for_redraw],
                chart_data : window.chartData_array_for_redraw[canvas_array_for_redraw]
              
                 });
                can_counter_for_redraw++;
                    $(htmlerror_id).html('');
                    $(htmlid).show()

          } else { // the update else for chart starting 
            console.log('hi mone')
            console.log(chartData_forUpdate)

            chartData_forUpdate.data.labels = [];
              label.forEach(element => {
              chartData_forUpdate.data.labels.push(element);
              })
              // window.myChart_barchart_material.data.datasets.data = [];
              console.log('now checking')
              chartData_forUpdate.data.datasets.forEach((dataset,index) => {

                  if(index == 0) {
                      dataset.data = posi;
                  } else if(index == 1){
                      dataset.data = nega;
                  }
                });

canvasData_forUpdate.onclick = function(evt) {
   var activePoint = chartData_forUpdate.getElementAtEvent(evt)[0];
   var data = activePoint._chart.data;
   var datasetIndex = activePoint._datasetIndex;
    var mainlabel = data.labels[activePoint._index];
    var label = data.datasets[datasetIndex].label;
   var value = data.datasets[datasetIndex].data[activePoint._index];
    
    let message = [];
    message.push(label.toLowerCase())
    message.push(mainlabel)
  positiveOpinionReviewLoader(message.toString(),curvedata);

 
};

            chartData_forUpdate.update();
      
      //updating the graphdata with newer data
      $.each(window.graphData,function(index,element) {

            if(element.id === id) {
              element.canvas_data = canvasData_forUpdate;
              element.chart_data  = chartData_forUpdate;
            } 

          });      


        $(htmlerror_id).html('');
        $(htmlid).show()


          }// the update else for chart ending


    } else {// incase no data in the obj

    if (chartData_forUpdate) {
      chartData_forUpdate.data.labels = [];
      chartData_forUpdate.data.datasets.forEach((dataset) => {
          $.each(dataset.data,function(index2,element2){
                 delete(dataset.data[index2]);
                })
      });

      chartData_forUpdate.update();
      }
    $(htmlerror_id).html(dataMessage);
    $(htmlid).hide()

  } // incase no data in the obj

  }
