
var transButton = '';
var revertButton = '';
    var ratingData= {
      count:0,
      totalRating:0,
      sentence:[]
    }

switch(localStorage.getItem("lang").toLowerCase())
{
  case 'en' : 
              transButton = "Translate";
              revertButton="Revert";
              ratingData.sentence[0]=' comments on '
              ratingData.sentence[1]= ' reviews ';
              break;

  case 'fr' : 
              ratingData.sentence[0]=' commentaires sur ';
              ratingData.sentence[1]=' avis ';
              transButton = "Traduction";
              revertButton = "Revenir";
              break;

  default :   transButton = "Translate";
               ratingData.sentence[0]=' comments on '
              ratingData.sentence[1]= ' reviews ';
              revertButton="Revert";
               break;
}



function MainTopicLoadGroup(reviewsF,value,language)
{
  console.log(language)
  console.log(reviewsF)
  console.log(value)
var lang = language;
$(".list").html("")
ratingData.count = 0;
ratingData.totalRating=0;
var langT = lang.toLowerCase();

	var DictionaryD=$.parseJSON(localStorage.getItem("Dictionary"));
	var topic=value;
	console.log(topic)

reviewsF.sort(function(a,b){
      var dateA=new Date(a.date), dateB=new Date(b.date)
        return dateB-dateA //sort by date descending
    })



    $.each(reviewsF,function (index1,element1) 
    { 	
    	var flag=0;
          var sentense = element1.title+" "+element1.full;
          var res=sentense.toLowerCase(); 
    		
    	$.each(DictionaryD.topics,function(index3,element3){
          var result;
          var checkedNew;

                //creating a good checking variable
                if(element3.locales.hasOwnProperty(lang+'-short'))
                {
                  var splitting_var=lang+'-short';

                    var splitter=element3.locales[splitting_var].split(" ")
                    checkedNew = splitter[0].split('.').join("");
                }
                else{
                  checkedNew= element3.locales[lang];
                }
                checkedNew=checkedNew.toLowerCase();
                if(checkedNew===topic)
                {
                  $.each(element3.keys,function(index4,element4)
                  {
                  innerchecker= element4.locales[lang];
                  innerchecker=innerchecker.toLowerCase();
                
                  result=countInstancesHere(res,innerchecker);
                  if(result!==0)
                  {
                  flag++;
                 var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var date = getFormattedDate(element1.date)
                 ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
                 if(sentence.length > 0)
                {
                  console.log('are we here')
      $(".list").append(`
      <li class="venue-review ">
            <div class="venue-logo">
            </div>
             <div class="venue-title">
             <h3>`+element1.venueName+`</h3>
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
                if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }

                }
       

                  }
                    if(flag!==0)
                      {        
                        return false; 
                      }
                  });
              }  
              if(flag!==0)
                {return false;}
          });


    });

var monkeyList = new List('test-list', {
  valueNames: ['disdate','diftitle','title-en','full','full-en'],
  page: 10,
  pagination: true
});
var head = ratingData.count+ratingData.sentence[0]+ratingData.totalRating+ ratingData.sentence[1];
$(".reviewsHeading").html(head)


$('html, body').animate({
        scrollTop: $("#reviewdiv").offset().top
    }, 2000); 

}




function handleMainTopicImpressionGroup(reviewsF,message,language)
{
  

reviewsF.sort(function(a,b){
      var dateA=new Date(a.date), dateB=new Date(b.date)
        return dateB-dateA //sort by date descending
    })

$(".list").html("")
var lang= language;

ratingData.count=0;
ratingData.totalRating=0;
var langT = lang.toLowerCase();


if(message==='nothing')
{
  return false;
}
var DictionaryD=$.parseJSON(localStorage.getItem("Dictionary"));
var using = message.split(",");
console.log(using)
    $("#reviewTable").DataTable().destroy();
    $("#reviewdata").html(null);
var topic= using[0];
if(using[1]==='positive' || using[1] ==='positif')
{

    $.each(reviewsF,function(index1,element1){
          var flag=0;
          var sentense = element1.title+" "+element1.full;
          var res=sentense.toLowerCase();
              $.each(DictionaryD.topics,function(index3,element3){
              var result;
              var checkedNew;

                //creating a good checking variable
                if(element3.locales.hasOwnProperty(lang+'-short'))
                {
                  var splitting_var=lang+'-short';

                    var splitter=element3.locales[splitting_var].split(" ")
                    checkedNew = splitter[0].split('.').join("");
                }
                else{
                  checkedNew= element3.locales[lang];
                }
                checkedNew=checkedNew.toLowerCase();

                if(checkedNew===topic)
                {
                  $.each(element3.keys,function(index4,element4)
                  {
                  innerchecker= element4.locales[lang];
                  innerchecker=innerchecker.toLowerCase();
                
                  result=countInstancesHere(res,innerchecker);
                  console.log(result)
                  if(result!==0)
                  {
                    console.log(element1.note)
              if(Number(element1.note)>=Number(3.5))
              {
                    console.log(result)
                    flag++;
                  var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var date = getFormattedDate(element1.date)
                 ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
             if(sentence.length > 0)
                {
                   $(".list").append(`
       <li class="venue-review ">
            <div class="venue-logo">
            </div>
             <div class="venue-title">
             <h3>`+element1.venueName+`</h3>
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
                 if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }

                }
           

                    }
                  }
                    if(flag!==0)
                      {        
                        return false; 
                      }
                  });
              }  
              if(flag!==0)
                {return false;}
          });

        });

}
else if(using[1]==='negative' || using[1]==='négatif')
{

    $.each(reviewsF,function(index1,element1){
          var flag=0;
          var sentense = element1.title+" "+element1.full;
          var res=sentense.toLowerCase();
              $.each(DictionaryD.topics,function(index3,element3){
              var result;
              var checkedNew;

                //creating a good checking variable
                if(element3.locales.hasOwnProperty(lang+'-short'))
                {
                  var splitting_var=lang+'-short';

                    var splitter=element3.locales[splitting_var].split(" ")
                    checkedNew = splitter[0].split('.').join("");
                }
                else{
                  checkedNew= element3.locales[lang];
                }
                checkedNew=checkedNew.toLowerCase();

                if(checkedNew===topic)
                {
                  $.each(element3.keys,function(index4,element4)
                  {
                  innerchecker= element4.locales[lang];
                  innerchecker=innerchecker.toLowerCase();
                
                  result=countInstancesHere(res,innerchecker);
                  console.log(result)
                  if(result!==0)
                  {
                    console.log(element1.note)
              if(Number(element1.note)<Number(3.5))
              {
                    console.log(result)
                    flag++;
                 var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var date = getFormattedDate(element1.date)
                 ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
                if(sentence.length > 0)
                {
                 $(".list").append(`
      <li class="venue-review ">
            <div class="venue-logo">
            </div>
             <div class="venue-title">
             <h3>`+element1.venueName+`</h3>
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
                 if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }

                }
           



                    }
                  }
                    if(flag!==0)
                      {        
                        return false; 
                      }
                  });
              }  
              if(flag!==0)
                {return false;}
          });

        });



}

var monkeyList = new List('test-list', {
  valueNames: ['disdate','diftitle','title-en','full','full-en'],
  page: 10,
  pagination: true
});

var head = ratingData.count+ratingData.sentence[0]+ratingData.totalRating+ ratingData.sentence[1];
$(".reviewsHeading").html(head)

       $('html, body').animate({
        scrollTop: $("#reviewdiv").offset().top
    }, 2000); 

}


function WordImpressionGraphLoadGroup(reviewsF,value,language)
{

reviewsF.sort(function(a,b){
      var dateA=new Date(a.date), dateB=new Date(b.date)
        return dateB-dateA //sort by date descending
    })

$(".list").html("")
  var using=value.split(",");
   var DictionaryD=$.parseJSON(localStorage.getItem("Dictionary"));
  var topic=using[0];
var langT = language.toLowerCase();

ratingData.count=0;
ratingData.totalRating=0;


if(using[1]==='positive' || using[1]==='positif')
{
        $.each(reviewsF,function(index1,element1){
          var flag=0;
          var sentense = element1.title+" "+element1.full;
          var res=sentense.toLowerCase(); 
          var result;
                  result=countInstancesHere(res,topic);
                  console.log(result)
                  if(result!==0)
                  {
                    flag++;
                    if(Number(element1.note)>=Number(3.5))
                    {
                var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var date = getFormattedDate(element1.date)
                ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
              if(sentence.length > 0)
                {
                   $(".list").append(`
      <li class="venue-review ">
            <div class="venue-logo">
            </div>
             <div class="venue-title">
             <h3>`+element1.venueName+`</h3>
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
                if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }


                }
           

                      }
                  }

                  });
      
}
else if(using[1]==='negative' || using[1]==='négatif')
{        
        $.each(reviewsF,function(index1,element1){
          var flag=0;
          var sentense = element1.title+" "+element1.full;
          var res=sentense.toLowerCase(); 
          var result;
                  result=countInstancesHere(res,topic);
                  console.log(result)
                  if(result!==0)
                  {
                    flag++;
                    if(Number(element1.note)<Number(3.5))
                    {
                var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var date = getFormattedDate(element1.date)
                 ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
                if(sentence.length > 0)
                {
                     $(".list").append(`
      <li class="venue-review ">
            <div class="venue-logo">
            </div>
             <div class="venue-title">
             <h3>`+element1.venueName+`</h3>
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
                  if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }

                }
         


                      }
                  }

                  });
        
            
           }
var monkeyList = new List('test-list', {
  valueNames: ['disdate','diftitle','title-en','full','full-en'],
  page: 10,
  pagination: true
});
var head = ratingData.count+ratingData.sentence[0]+ratingData.totalRating+ ratingData.sentence[1];
$(".reviewsHeading").html(head)     


$('html, body').animate({
        scrollTop: $("#reviewdiv").offset().top
    }, 2000); 


}



function countInstancesHere(string, word) {
   return string.split(word).length - 1;
}


