$(document).ready(function() {
  renderProducts(localStorage["lang"]);
});

$("#langs").change(function(e) {
  $("#loading").show();
  renderProducts($("#langs").val());
  switchLanguage();
});

function renderProducts(lang = "EN") {
  $.ajax({
    url: "api/products_curl.php",
    type: "POST",
    data: { action: "listProducts" },
    success: function(data) {
      var obj = $.parseJSON(data);

      var productListData = "";

      obj.result.forEach(function(item) {
        if (item["active"]) {
          var description;
          var id = item["id"].trim();

          var name;
          var price;
          var productData = "";
          var buttonText = "";
          var amount = "";

          switch (lang) {
            case "EN":
              description = item["description-en"];
              name = item["name-en"];
              price = "$" + item["price-USD"];
              buttonText = "Subscribe";
              amount = item["price-USD"];

              break;
            case "FR":
              description = item["description-fr"];
              name = item["name-fr"];
              price = "€" + item["price-EUR"];
              buttonText = "Souscrire";
              amount = item["price-EUR"];

              break;
            default:
              description = item["description-en"];
              name = item["name-en"];
              price = "$" + item["price-USD"];
          }

          //   if (user_subscribed_product) {
          //     if (id == user_subscribed_product) {
          //       $("#plan_name").text(name);
          //     }
          //   }

          productData = '<div class="col-lg-3 col-md-3 col-sm-12 m-t-25">';
          productData += '<div class="product-info">';
          productData += '<div class="whitebox-title p-4 text-center">';
          productData += '<div class="title">';
          productData += "<h3>" + name + "</h3>";
          productData += "</div>";
          productData += '<div class="price-view lead mt-3">';
          productData += '<span class="bold red">' + price + "</span>";
          productData += "</div>";
          productData += '<div class="btn mt-4">';
          productData +=
            `<a class="btn btn-app bg-purple"  onclick="purchase('` +
            id +
            `');" >` +
            buttonText +
            `</a>`;
          productData += "</div>";
          productData += '<div class="info">';
          productData += '<p class="mb-0">' + description + "</p>";
          productData += "</div>";
          productData += "</div>";
          productData += "</div>";
          productData += "</div>";
          productListData += productData;
        }
      });

      $("#productList").html(productListData);

      $("#loading").fadeOut();
    }
  });
}

function purchase(Id) {
  $("#loading").show();

  $.ajax({
    url: "api/sca_session_creator.php",
    type: "POST",
    data: { productId: Id, authscreate: "spSCSESS345xx" },
    success: function(data) {
      var session_id;

      if (data != "nil") {
        session_id = data.trim();
        const stripe = Stripe("pk_test_dD4ZenJ3EIVYYH2Do2HiobFV00X6VVECib");
        stripe
          .redirectToCheckout({
            // Make the id field from the Checkout Session creation API response
            // available to this file, so you can provide it as parameter here
            // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
            sessionId: session_id
          })
          .then(result => {
            // If `redirectToCheckout` fails due to a browser or network
            // error, display the localized error message to your customer
            // using `result.error.message`.
          });
      }
    }
  });
}
