$(document).ready(function() {
  loadCountry();
});

function newuser() {
  $(".validation-info").html("");
  var name = $("#name").val();
  var email = $("#email").val();
  var password = $("#password").val();
  var language = $("#language").val();

  if (name != "" && email != "" && password != "" && language != "") {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      $.ajax({
        url: "api/index_curl.php",
        type: "POST",
        dataType: "json",
        data: {
          action: "createUser",
          email: email,
          password: password,
          name: name,
          preferredLanguage: language,
          countryCode: $("#country").val(),
          cellphone: "",
          reportDaily: "true",
          reportWeekly: "true",
          reportRange: "1",
          pushActive: "true",
          reportThreshold: "3.5"
        },
        success: function(data) {
          var res = $.parseJSON(JSON.stringify(data));
          console.log(res.status);
          if (res.status == "error") {
            $("#commonmessage").html(
              "<span style='color:red'>" + res.result.message + "</span>"
            );
          } else {
            $("#commonmessage").html(
              "<span style='color:green'>You've registered successfully!<br> Please check your email inbox.</span>"
            );
          }
        }
      });
    } else {
      $("#email_error").html(
        "<span style='color:red;'>Enter valid email address</span>"
      );
    }
  } else {
    if (name == "") {
      $("#name_error").html("<span style='color:red;'>Name is required</span>");
    }
    if (password.length < 6 && password != "") {
      $("#pass_error").html(
        "<span style='color:red;'>Password must be atleast 6 charachters long</span>"
      );
    }
    if (email == "") {
      $("#email_error").html(
        "<span style='color:red;'>Enter your email address</span>"
      );
    }
    if (password == "") {
      $("#pass_error").html(
        "<span style='color:red;'>Enter password for your account</span>"
      );
    }
  }
}

function loadCountry() {
  $.getJSON("Json/countries.json", function(data) {
    let html = "";
    let selected = "";
    $.each(data, function(indexInArray, valueOfElement) {
      // check if france
      selected = valueOfElement.code == "FR" ? "selected" : "";
      html += `<option value=${valueOfElement.code} ${selected}>${valueOfElement.name}</option>`;
    });
    $("#country").html(html);
  });
}
