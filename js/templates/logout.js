setInterval(function() {
  checkSession();
}, 60000);

function checkSession() {
  $.ajax({
    url: "profile.php",
    type: "POST",
    data: { action: "getdata" },
    success: function(data) {
      if (data == 0) {
        logout();
      } else {
        console.log("session here");
      }
    },
    error: function(data) {
      console.log(data);
    }
  });
}

function logout() {
  $("#loading").fadeIn();

  $.ajax({
    url: "logout.php",
    type: "post",
    data: { action: "logout" },
    success: function(data) {
      console.log(data);
      if (data == 1) {
        // window.localStorage.clear();
        window.location.href = "index.php";
      }
    }
  });
}

function redirect(page) {
  switch (page) {
    case "home":
      location.href = "home.php";
      break;
    case "profile":
      location.href = "profile.php";
      break;
    case "add_venue":
      location.href = "add_venue.php";
      break;
    case "groups":
      location.href = "groups.php";
      break;
    case "subscription":
      location.href = "subscription.php";
      break;
    case "reviews":
      $("html,body").animate(
        {
          scrollTop: $("#tableAnchor").offset().top
        },
        "slow"
      );
      break;
    case "Services":
      $("html,body").animate(
        {
          scrollTop: $("#donutAnchor").offset().top
        },
        "slow"
      );
      break;
    case "Negatives":
      $("html,body").animate(
        {
          scrollTop: $("#barAnchor").offset().top
        },
        "slow"
      );
      break;
    case "notes":
      $("html,body").animate(
        {
          scrollTop: $("#starAnchor").offset().top
        },
        "slow"
      );
  }
}
