var globalflag=0;
var dataMessage = "";
$(function(){

 $('.connectedSortable').sortable({
    placeholder         : 'sort-highlight',
    connectWith         : '.connectedSortable',
    handle              : '.box-header, .nav-tabs',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });
  $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');


$('#datepicker1').datepicker({
autoclose: true,
format: 'yyyy/mm/dd',
endDate: 'today'
});

$('#datepicker2').datepicker({
autoclose: true,
format: 'yyyy/mm/dd',
endDate: 'today'
});


var lang = localStorage.getItem("lang").toLowerCase();

switch(lang){

  case 'fr' : $("#datepicker1").attr("placeholder","date de début");
            $("#datepicker2").attr("placeholder","date de fin");
              dataMessage = "<b>Pas encore de données</b>";
              break; 
  
  case 'en' : $("#datepicker1").attr("placeholder","Start Date");
              $("#datepicker2").attr("placeholder","End Date");
              dataMessage = "<b>No Data Yet</b>";
              break;
  
  default:  $("#datepicker1").attr("placeholder","date de début");
            $("#datepicker2").attr("placeholder","date de fin");
            dataMessage = "</b>No Data Yet</b>";
            break;
}



// graph redraw after window resize for google.
$(window).resize(function() {
    if(this.resizeTO) clearTimeout(this.resizeTO);
    this.resizeTO = setTimeout(function() {
        $(this).trigger('resizeEnd');
    }, 500);
});



$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}
var venueid=$.urlParam('id');    

  
  var venueDetails= localStorage.getItem("venueDetails"+venueid);
      var temp = $.parseJSON(venueDetails);
  $("#venueName").html(temp.result.venueName);
  if(venueDetails.length!==0)
  {
    var venueType= temp.result.venueType;
    $.ajax({
      url: "api/text_analysis_curl.php",
      type: "POST",
      data :{"action":"venueType","type":venueType},
      success:function(data){

        localStorage.removeItem("Dictionary");
        localStorage.setItem("Dictionary",data);
        var lang=localStorage.getItem("lang").toLowerCase();
      //  console.log(lang)
        var venueDetailData = getVenueDetailData(venueDetails);
        text_analysis(data,lang,venueDetailData, updating = false);
      },error:function(data)
      {
        alert("No Data, Please Refresh the page, if the problem still persist contact administrator");
      },async:false
    });



  }
      $("#loading").fadeOut();

});


function getVenueDetailData(venueDetails)
{

var datesData = $.parseJSON(localStorage.getItem("venueDates"+venueid));
var venueDetailDatareturn = null;
       $.ajax({
        url: 'api/venue_details_curl.php',
        type: "POST",
        data:{
    "action": "venueData",
    "userId": user_id,
    "venueId": venueid,
    "venueVersion": venueDetails.venueVersion,
    "checkVersion": false,
    "startDate": datesData.start,
    "endDate": datesData.current,
}, 
        success: function(venueDetailData)
        {

      venueDetailDatareturn = venueDetailData;

        },error:function (data)
        {
        //  console.log(data)
        },async:false

      });

return venueDetailDatareturn;
}


function text_analysis(data,lang,venueDetailData, updating)
{

  var venueData= $.parseJSON(venueDetailData);

  var datesData=$.parseJSON(localStorage.getItem("venueDates"+venueid));
   var dateRange =  datesData.start+" "+"to"+" "+datesData.current;
  $("#DateRange").html(dateRange);

  var DictionaryD=$.parseJSON(data);
  var innerchecker;
  var counter=[];
  var i=0;
  var countOfInnerCheckerNeg=[];
  var countOfInnerCheckerPos=[];
  var countOfInnerChecker=[];
  var j=0;
  var ReviewArray=[];
  var negCount=[];
  var posCount=[];
  var k=0;

  $.each(DictionaryD.topics,function(index3,element3){
              var result;
              var checkedNew;

        //creating a good checking variable
        if(element3.locales.hasOwnProperty(lang+'-short'))
        {
          var splitting_var=lang+'-short';

            var splitter=element3.locales[splitting_var].split(" ")
            checkedNew = splitter[0].split('.').join("");
        }
        else{
          checkedNew= element3.locales[lang];
        }


      checkedNew=checkedNew.toLowerCase();
      counter[i,checkedNew]=0;
      negCount[i,checkedNew]=0;
      posCount[i,checkedNew]=0;
      $.each(element3.keys,function(index4,element4)
      {
          k=0;
          innerchecker= element4.locales[lang];
          innerchecker=innerchecker.toLowerCase();
          countOfInnerChecker[j,innerchecker]=0;
          countOfInnerCheckerNeg[j,innerchecker]=0;
          countOfInnerCheckerPos[j,innerchecker]=0;

        $.each(venueData.result.services,function(index,element){

        $.each(element.reviews,function(index2,element2){

          var sentense = element2.title+" "+element2.full;
          var res=sentense.toLowerCase();   


            result=countInstances(res,innerchecker);

            counter[i,checkedNew]=parseInt(counter[i,checkedNew]+result);


            countOfInnerChecker[j,innerchecker]=parseInt(countOfInnerChecker[j,innerchecker]+result);
            if(element2.note<3.5)
            {
            countOfInnerCheckerNeg[j,innerchecker]=parseInt(countOfInnerCheckerNeg[j,innerchecker]+result);
            negCount[i,checkedNew]=parseInt(negCount[i,checkedNew]+result);
            }
            else{
            posCount[i,checkedNew]=parseInt(posCount[i,checkedNew]+result);
            countOfInnerCheckerPos[j,innerchecker]=parseInt(countOfInnerCheckerPos[j,innerchecker]+result);
            }
                
      
        });
      });
      j++;
    
    });
      i++;
             
    }); //Main dictionary loop end

  if(updating) {
    RedrawRepartition(counter,lang,venueDetailData)
    RedrawImpressionGraph(negCount,posCount,lang,venueDetailData);
    RedrawWordImpression(countOfInnerCheckerPos,countOfInnerCheckerNeg,lang,venueDetailData);
    drawTable(venueDetailData);
  } else {

    // return false;
  Repartition(counter,lang,venueDetailData);
  ImpressionGraph(negCount,posCount,lang,venueDetailData);
  WordImpressionGraph(countOfInnerCheckerPos,countOfInnerCheckerNeg,lang,venueDetailData);
  drawTable(venueDetailData);

  }
      

  
  
  
} 

function countInstances(string, word) {
   return string.split(word).length - 1;
}

function Repartition(data,lang,venueDetailData)
{


// console.log(venueDetailData)
  var using=data; 
  var sortTemp =[];
  for(let key in using) {
    sortTemp.push({
      key: key,
      value: using[key]
    })
  } 

  sortTemp = sortTemp.sort(function(a,b){
    return b.value - a.value
  })


  var venueD = $.parseJSON(venueDetailData)
  var language=lang;
    var chartValues = [];

if(venueD.result.services.length>0)
{
    var htitle = "";
    switch(localStorage.getItem('lang').toLowerCase())
    {
      case 'en' : htitle='Count'
            break;
      case 'fr' : htitle='Nombre';
            break;
    }

let label = [];
let value = [];

// for (let key in using) {
//   label.push(key)
//   value.push(using[key])
// }
$.each(sortTemp,function(index,element) {
  label.push(element.key)
  value.push(element.value)
})


var chartData = {
   type: 'horizontalBar',
   data: {
      labels: label,
    datasets: [
        {
            label: htitle,
            backgroundColor: "#6E388A",
            data: value
        }
    ]
   },
   options: {
        barValueSpacing: 20,
        scales: {
            xAxes: [{
              barPercentage: 0.4,
                ticks: {
                    min: 0,
                }
            }]
        }
    }
} 

 window.canvas_barchart_material = document.getElementById('barchart_material');
 window.myChart_barchart_material = new Chart(window.canvas_barchart_material, chartData);

window.canvas_barchart_material.onclick = function(evt) {
   var activePoint = window.myChart_barchart_material.getElementAtEvent(evt)[0];
   var data = activePoint._chart.data;
   var datasetIndex = activePoint._datasetIndex;
  var mainlabel=activePoint._model['label'];
   var label = data.datasets[datasetIndex].label;
   var value = data.datasets[datasetIndex].data[activePoint._index];
   // alert(mainlabel+' : '+label+' - '+value);
  // console.log(value)
   MainTopicLoad(mainlabel,language,venueDetailData);
  
   //console.log(mainlabel);
};

}
else{
  $("#barchart_material_error").html(dataMessage);
  $("#barchart_material").css("height",10)
}

   
}

function ImpressionGraph(neg,pos,lang,venueDetailData)
{
  var negative=neg;
    // for(let key in negative) {
    //   negative[key] = 0;
    // }
  var positive=pos;
  var language=lang;
    var chartValues = [];
    var venueD = $.parseJSON(venueDetailData);
    if(venueD.result.services.length>0)
    {

    var htitle1 = htitle2 = "";
    switch(localStorage.getItem('lang').toLowerCase())
    {
      case 'en' : htitle1='positive'; htitle2 = 'negative';
            break;
      case 'fr' : htitle1='positif'; htitle2 = 'négatif';
            break;
    }

    let label = [];
    let pos = [];
    let neg =[];
      
      //finding if both negative and positive have values
    var negPosCount = getCountOfPosNeg(negative,positive);
    var sortedData = []; 
    if(negPosCount.pos_count > 0 && negPosCount.neg_count > 0) {
     sortedData = multiSortForImpression(negative,positive);

    } else {
       if(negPosCount.pos_count > 0 ) {
             sortedData = singularSort('pos',positive);
       } else {
            sortedData = singularSort('neg',negative)
       }
    }
    
    $.each(sortedData,function(index,element) {
    label.push(element.label);  
    pos.push(element.pos);
    neg.push(element.neg);
    })



   var chartData = {
   type: 'horizontalBar',
   data: {
      labels: label,
    datasets: [
        {
            label: htitle1,
            backgroundColor: "#67BFB3",
            data: pos,
        },
        {
            label: htitle2,
            backgroundColor: "#C73729",
            data: neg,
        }
    ]
   },
   options: {
        barValueSpacing: 20,
        scales: {
            xAxes: [{
              barPercentage: 0.4,
                ticks: {
                    min: 0,
                }
            }]
        }
    }
} 

 window.canvas_new_chart = document.getElementById('new_chart');
 window.myChart_new_chart = new Chart(window.canvas_new_chart, chartData);
 console.log('moving forward',window.myChart_new_chart)

window.canvas_new_chart.onclick = function(evt) {
   var activePoint = window.myChart_new_chart.getElementAtEvent(evt)[0];
   var data = activePoint._chart.data;
   var datasetIndex = activePoint._datasetIndex;
  var mainlabel=activePoint._model['label'];
   var label = data.datasets[datasetIndex].label;
   var value = data.datasets[datasetIndex].data[activePoint._index];
   var message = [];
   message.push(mainlabel)
   message.push(label)

   // console.log(message)
   handleMainTopicImpression(message.toString(),language,venueDetailData)

};


    }
    else{
      $("#myChart_new_chart_error").html(dataMessage)
      $("#myChart_new_chart_error").css("height",10)
    }
    

}

function WordImpressionGraph(pos,neg,lang,venueDetailData)
{   
  var venueD = $.parseJSON(venueDetailData);
  var negative=neg;
  var positive=pos;
  var chartValues=[];

  if(venueD.result.services.length>0)
  {
  
   var htitle1 = htitle2 = "";
    switch(localStorage.getItem('lang').toLowerCase())
    {
      case 'en' : htitle1='positive'; htitle2 = 'negative';
            break;
      case 'fr' : htitle1='positif'; htitle2 = 'négatif';
            break;
    }

//chartValues.push(['Word', 'negative','positive']);
    let label = [];
    let pos = [];
    let neg =[];
      //finding if both negative and positive have values
    var negPosCount = getCountOfPosNeg(negative,positive);
    var sortedData = []; 
    if(negPosCount.pos_count > 0 && negPosCount.neg_count > 0) {
     sortedData = multiSort(negative,positive);

    } else {
       if(negPosCount.pos_count > 0 ) {
             sortedData = singularSort('pos',positive);
       } else {
            sortedData = singularSort('neg',negative)
       }
    }
    

    $.each(sortedData,function(index,element) {
    label.push(element.label);  
    pos.push(element.pos);
    neg.push(element.neg);
    })


   var chartData = {
   type: 'horizontalBar',
   data: {
      labels: label,
    datasets: [
        {
            label: htitle1,
            backgroundColor: "#67BFB3",
            data: pos,
        },
        {
            label: htitle2,
            backgroundColor: "#C73729",
            data: neg,
        }
    ]
   },
   options: {
       maintainAspectRatio: false,
        barValueSpacing: 20,
        scales: {
            xAxes: [{
              barPercentage: 0.2,
                ticks: {
                    min: 0,
                }
            }]
        }
    }
} 

 window.canvas_WordImpressionGraph = document.getElementById('WordImpressionGraph');
 window.myChart_WordImpressionGraph = new Chart(window.canvas_WordImpressionGraph, chartData);

    window.canvas_WordImpressionGraph.onclick = function(evt) {
   var activePoint = window.myChart_WordImpressionGraph.getElementAtEvent(evt)[0];
   var data = activePoint._chart.data;
   var datasetIndex = activePoint._datasetIndex;
  var mainlabel=activePoint._model['label'];
   var label = data.datasets[datasetIndex].label;
   var value = data.datasets[datasetIndex].data[activePoint._index];
   var message = [];
   message.push(mainlabel)
   message.push(label)

   // console.log(message)
   WordImpressionGraphLoad(message.toString(),lang,venueDetailData)
};
  $("#WordImpressionGraph").show()
  var height = label.length * 38;
  $("#WordImpressionGraph_container").css("height",height);
    window.addEventListener('resize', function () {
               window.myChart_WordImpressionGraph.resize()
            })

  }else{

    $("#WordImpressionGraph_error").html(dataMessage)
    $("#WordImpressionGraph").hide()
    }
      
}
function drawTable(venueDetailData)
{
  var venueD= $.parseJSON(venueDetailData);
  var ratings=0;
var starnumber=0;

var transButton='';
var langT = localStorage.getItem("lang").toLowerCase();
    var ratingData= {
      count:0,
      totalRating:0,
      sentence:[]
    }

switch (langT)
{
  case 'fr' : transButton = "Traduction";
              revertButton = "Revenir";
              ratingData.sentence[0]=' commentaires sur ';
              ratingData.sentence[1]=' avis ';
               break;
  case  'en' : transButton = "Translate";
                revertButton = "Revert";
                ratingData.sentence[0]=' comments on ';
                ratingData.sentence[1]= ' reviews ';
                break;

  default : transButton = "Translate";
                revertButton = "Revert";
                ratingData.sentence[0]=' comments on ';
                ratingData.sentence[1]= ' reviews ';
        break;
}
  $(".list").html("");

var temp = [];
      $.each(venueD.result.services,function(index,element){
        var service= getName(element.serviceId);
          var serviceUrl=getServiceUrl(service.idService,venueid)
        $.each(element.reviews,function(index1,element1){
          element1.service = service;
          element1.serviceUrl = serviceUrl;
          temp.push(element1);
        })
      })
      venueD.result.services = temp;
      console.log(venueD.result.services)
      venueD.result.services.sort(function(a,b){
        var dateA=new Date(a.date), dateB=new Date(b.date)
        return dateB-dateA //sort by date ascending
      });








if(venueD.status==='success')
{
    $.each(venueD.result.services,function(index1,element1){
      ratings= parseInt(ratings+element1.note);
              starnumber=parseInt(starnumber+1);          
                var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var simpleDate = element1.date;
                var date = getFormattedDate(element1.date)
                ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
                if(sentence.length > 0){
            $(".list").append(`
      <li class="venue-review ">
      <p style="display: none;" class="realDate">`+simpleDate+`</p>
            <div class="venue-logo">
              <a href="`+element1.serviceUrl+`" target="_blank"><img src="`+element1.service.idName+`" class="img-fluid" alt=""></a>
              <p style="display:none" class="service">`+element1.service.idServiceName+`</p>  
            </div>
             <div class="venue-title">
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
                if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }
}
    });

           // applying styling to the table
}

var monkeyList = new List('test-list', {
  valueNames: ['disdate','diftitle','title-en','full','full-en','service'],
  page: 10,
  pagination: true
});

var head = ratingData.count+ratingData.sentence[0]+ratingData.totalRating+ ratingData.sentence[1];
$(".reviewsHeading").html(head)
         
var star = ratings/starnumber;
    star = Number(Math.round(star+'e2')+'e-2');
    $("#starOuterVal").html(star);
  const starPercentage = (star / 5) * 100;
  const starPercentageRounded = `${(Math.round(starPercentage))}%`;
  document.querySelector(`.stars-inner`).style.width = starPercentageRounded;    


          $("#loading").fadeOut();

}

//========================================================CUSTOM DATA ===========================

//daterange picker selecting custom data
$("#dateValidator").on('click',function(){

var start = $("#datepicker1").val();
var end = $("#datepicker2").val();


if(start!== "" && end!== "")
{
    var lang = localStorage.getItem("lang").toLowerCase();
  if(start > end )
  {
    switch(lang){
      case 'en' : alert("Start Date Greater than end date");
              break;
      case 'fr' : alert("de début Supérieure à la date de fin");
              break;
          }
      return false;
  }
  if( start === end)
  {
    switch(lang){
      case 'en' : alert("Dates cannot be equal");
              break;
      case 'fr' : alert("les dates ne peuvent pas être égales");
              break;
          }
      return false;

  }

globalflag++;//setting global flag to 1 to alert text analysis function to call custom table maker



$("#loading").fadeIn();

startDate = start.split('/')
startDate = startDate[0].trim()+'-'+startDate[1].trim()+'-'+startDate[2].trim()

endDate = end.split('/')
endDate = endDate[0].trim()+'-'+endDate[1].trim()+'-'+endDate[2].trim()
Cusdates={"startDate":startDate,"endDate":endDate};

          var datesData= {

                start: Cusdates.startDate,
                current: Cusdates.endDate
                    };

          localStorage.removeItem("venueDates"+venueid);
          localStorage.setItem("venueDates"+venueid,JSON.stringify(datesData)) 

LoadCustomDataHereStarting(Cusdates);

}
else{
  alert("Enter Dates")
}


});

/*=================================================Loading Custom Data=======================================*/

function LoadCustomDataHere(Cusdates)
{
     var dateRange =  Cusdates.startDate+" "+"to"+" "+Cusdates.endDate;
  $("#DateRange").html(dateRange);
  // alert("hi")
   $("#loading").fadeIn();
  $.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}

var venueid=$.urlParam('id');   
var user_id;
   $.ajax({
        url: 'api/profile_curl.php',
        type: "POST",
        data:{"action":"getSession"}, 
        success: function(userData)
        {
            var userData=$.parseJSON(userData);
         // console.log(userData)
           user_id = userData.user_id;
        },async:false
    }); 

//console.log(user_id)
$.ajax({
        url: 'api/venue_details_curl.php',
        type: "POST",
        data:{
    "action": "venueDetails",
    "userId": user_id,
    "venueId": venueid
    }, 
        success: function(venueDetails)
        {
          console.log(venueDetails);
          localStorage.removeItem("venueDetails"+venueid);
          localStorage.setItem("venueDetails"+venueid,venueDetails);
        },
        async:false
    });
var lang=localStorage.getItem("lang").toLowerCase();

var venueDetails = $.parseJSON(localStorage.getItem("venueDetails"+venueid));
     $.ajax({
        url: 'api/venue_details_curl.php',
        type: "POST",
        data:{
    "action": "venueData",
    "userId": user_id,
    "venueId": venueid,
    "venueVersion": venueDetails.venueVersion,
    "checkVersion": false,
    "startDate": Cusdates.startDate,
    "endDate": Cusdates.endDate,
}, 
        success: function(venueDetailData)
        {
          // console.log(venueData)
          //=========================================loding charts=======================     
          var dictionary=localStorage.getItem("Dictionary") ;  
          text_analysis(dictionary,lang,venueDetailData, updating = true);
        },
        async:false
    });
}



function BackToVenueDetail()
{

$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}
var venueid=$.urlParam('id');    

location.href="venue.php?id="+venueid;

}


function testfunctionText()
{
  globalflag++;
  var x=$('#DateCustomText').val();

 var mnths = { 
                Jan:"01", Feb:"02", Mar:"03", Apr:"04", May:"05", Jun:"06",
                Jul:"07", Aug:"08", Sep:"09", Oct:"10", Nov:"11", Dec:"12"
                 };

  if(x!=='NULL')
  {

      switch(x)
      {
      case  'crMonth' :   var date = new Date(), y = date.getFullYear(), m = date.getMonth();
                          var firstDay = new Date(y, m, 1).toString();
                          var lastDay = new Date().toString();
                         //  console.log(lastDay)
                         
                              var fdarr=[];
                                  fdarr=firstDay.split(" ");
                              var ldarr=[];
                                  ldarr=lastDay.split(" ");

                               firstDay = [ fdarr[3], mnths[fdarr[1]], fdarr[2] ].join("-");
                               lastDay = [ ldarr[3], mnths[ldarr[1]], ldarr[2] ].join("-");

                             //  console.log(lastDay)

                                Cusdates={"startDate":firstDay,"endDate":lastDay};
                                     var dateRange =  Cusdates.startDate+" "+"to"+" "+Cusdates.endDate;
                                     $("#DateRange").html(dateRange);
                                            var datesData= {
                                                      start: Cusdates.startDate,
                                                      current: Cusdates.endDate
                                                          };
                                            localStorage.removeItem("venueDates"+venueid);
                                            localStorage.setItem("venueDates"+venueid,JSON.stringify(datesData))                                
                                             LoadCustomDataHereStarting(Cusdates);
                          break;
      case  '3months' :   
                          var start;
                          var current;
                          $.ajax({

                            url: "api/dateFinder.php",
                            type:"POST",
                            data: {"action":"3months"},
                            success: function (dates) {
                                
                              var dates = $.parseJSON(dates);
                               start = dates.start.split("-"); 
                                  start[2]='01';  
                                  start=start.join('-');
                               current= dates.current;
                                Cusdates={"startDate":start,"endDate":current};
                                     var dateRange =  Cusdates.startDate+" "+"to"+" "+Cusdates.endDate;
                                      $("#DateRange").html(dateRange);
                                                var datesData= {
                                                      start: Cusdates.startDate,
                                                      current: Cusdates.endDate
                                                          };
                                            localStorage.removeItem("venueDates"+venueid);
                                            localStorage.setItem("venueDates"+venueid,JSON.stringify(datesData)) 
                                LoadCustomDataHereStarting(Cusdates)
                                  
                            },error:function (argument) {
                             // console.log(argument)
                            },async:false

                          });

                                        

                         break;
      case  '6months' : 
                          var start;
                          var current;
                          $.ajax({

                            url: "api/dateFinder.php",
                            type:"POST",
                            data: {"action":"6months"},
                            success: function (dates) {
                                
                              var dates = $.parseJSON(dates);
                               start = dates.start.split("-"); 
                                  start[2]='01';  
                                  start=start.join('-');
                               current= dates.current;
                                Cusdates={"startDate":start,"endDate":current};
                                     var dateRange =  Cusdates.startDate+" "+"to"+" "+Cusdates.endDate;
                                        $("#DateRange").html(dateRange);
                                                var datesData= {
                                                      start: Cusdates.startDate,
                                                      current: Cusdates.endDate
                                                          };
                                            localStorage.removeItem("venueDates"+venueid);
                                            localStorage.setItem("venueDates"+venueid,JSON.stringify(datesData)) 
                                LoadCustomDataHereStarting(Cusdates)
                                  
                            },error:function (argument) {
                              //console.log(argument)
                            },async:false

                          }); 
                         break;
      case  'crYear' :  
                           var start;
                          var current;
                          $.ajax({

                            url: "api/dateFinder.php",
                            type:"POST",
                            data: {"action":"crYear"},
                            success: function (dates) {
                              var dates = $.parseJSON(dates);
                               start = dates.start.split("-"); 
                                  start[2]='01';  
                                  start=start.join('-');
                               current= dates.current;
                                Cusdates={"startDate":start,"endDate":current};
                                     var dateRange =  Cusdates.startDate+" "+"to"+" "+Cusdates.endDate;
                                    $("#DateRange").html(dateRange);
                                                var datesData= {
                                                      start: Cusdates.startDate,
                                                      current: Cusdates.endDate
                                                          };
                                            localStorage.removeItem("venueDates"+venueid);
                                            localStorage.setItem("venueDates"+venueid,JSON.stringify(datesData)) 

                                LoadCustomDataHereStarting(Cusdates)
                                  
                            },error:function (argument) {
                              //console.log(argument)
                            },async:false

                          });   

                         break;
      case  '2years' :  
                        var start;
                          var current;
                          $.ajax({

                            url: "api/dateFinder.php",
                            type:"POST",
                            data: {"action":"2years"},
                            success: function (dates) {

                              var dates = $.parseJSON(dates);
                               start = dates.start.split("-"); 
                                  start[2]='01';  
                                  start=start.join('-');
                               current= dates.current;
                                Cusdates={"startDate":start,"endDate":current};
                                     var dateRange =  Cusdates.startDate+" "+"to"+" "+Cusdates.endDate;
                                      $("#DateRange").html(dateRange);
                                                var datesData= {
                                                      start: Cusdates.startDate,
                                                      current: Cusdates.endDate
                                                          };
                                            localStorage.removeItem("venueDates"+venueid);
                                            localStorage.setItem("venueDates"+venueid,JSON.stringify(datesData)) 
                                             LoadCustomDataHereStarting(Cusdates)
                                  
                            },error:function (argument) {
                              //console.log(argument)
                            },async:false

                          });   
                         break;
      case  '3years' :    
                          var start;
                          var current;
                          $.ajax({

                            url: "api/dateFinder.php",
                            type:"POST",
                            data: {"action":"3years"},
                            success: function (dates) {
                              //console.log(dates)

                              var dates = $.parseJSON(dates);
                               start = dates.start.split("-"); 
                                  start[2]='01';  
                                  start=start.join('-');
                               current= dates.current;
                                Cusdates={"startDate":start,"endDate":current};
                                     var dateRange =  Cusdates.startDate+" "+"to"+" "+Cusdates.endDate;
                                      $("#DateRange").html(dateRange);
                                                var datesData= {
                                                      start: Cusdates.startDate,
                                                      current: Cusdates.endDate
                                                          };
                                            localStorage.removeItem("venueDates"+venueid);
                                            localStorage.setItem("venueDates"+venueid,JSON.stringify(datesData))                               
                                          LoadCustomDataHereStarting(Cusdates)
                                  
                            },error:function (argument) {
                          //    console.log(argument)
                            },async:false

                          });   
                         break;






      }

  }
$('#DateCustomText').val('NULL');
}


function LoadCustomDataHereStarting(Cusdates)
{
  $("#loading").fadeIn();
    setTimeout(
    function() {
      LoadCustomDataHere(Cusdates);
      
    }, 400);
}

