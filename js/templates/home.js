//add venue button click

$("#addVenue").on("click", function() {

  var status = localStorage.getItem("product_status");
  if (localStorage["status"] == "trial") {
    alert("You can only add 1 venue in trial mode");
    return;
  } else if (cAddedVenues >= cTotalVenues) {
    let Msg =
      localStorage.getItem("lang") == "EN"
        ? "Your subscription doesn't allow you to add more venues. Please contact Spella to customise your account."
        : "Votre abonnement ne permet pas l'ajout d'un établissement supplémentaire. Contactez Spella pour personnaliser votre compte.";
    alert(Msg);
    return;
  }
  
  window.location.href = "add_venue.php";
  // console.log(status);
});

$(document).ready(function() {});
localStorage.setItem("addedcompet", numberofaddedcompet);
// globals
var langu = (localStorage.getItem("lang")
  ? localStorage.getItem("lang")
  : "EN"
).toLowerCase();

function pagesUtilize() {
  url = $("#urlInsert").val();
  var userData = getuserData();
  var userObj = $.parseJSON(userData);
  var icon = localStorage.getItem("serviceIcon");

  var user_id = userObj.user_id;

  var serviceId = localStorage.getItem("serviceId");
  $.ajax({
    url: "api/add_venue_curl.php",
    type: "POST",
    data: {
      action: "validatePage",
      userId: user_id,
      pageURL: url,
      venueName: venue
    },

    success: function(data) {
      localStorage.removeItem("serviceId");
      // console.log(data);
      var result = $.parseJSON(data);
      if (result.status === "success") {
        var deletiid = Math.floor(1000 + Math.random() * 9000);
        $("#pageTable").append(
          `<tr>
            <td data-value=` +
            url +
            ` id=` +
            serviceId +
            `><img width="50" height="50"
             src="https://api.spella.com/services/logos-mini/` +
            icon +
            `" alt="" /></td>
            <td style="word-break: break-all; padding:10px;" id=` +
            deletiid +
            `>` +
            url +
            `</td>
            <td><span class="red">
            <a onclick="deletion(` +
            deletiid +
            `);" >
            <i  class='fas fa-trash-alt'></i>
            </a></span></td>
            </tr>`
        );
        $("#save_button").fadeIn();
        localStorage.removeItem("serviceId");
        $("#servicelist").modal("hide");
        modal3close();
      }
    }
  });
}

function modal3close() {
  $("#Modal1").modal("hide");
  $("#Modal3").modal("hide");
  $("#Modal2").modal("hide");

  window.stop();
}

function pageSelect(str) {
  var url = str;
  var data = { action: "getImages", url: url };
  // console.log(data)
  $("#urlInsert").val(url);
  $("#loading").fadeIn();

  $("#openUrl").attr("href", url);
  setTimeout(function() {
    $.ajax({
      url: "api/add_venue_curl.php",
      type: "POST",
      data: { action: "getImages", url: url },
      success: function(data) {
        var basicUrl = "data:image/gif;base64," + data;
        $("#pageInsert").attr("src", basicUrl);
      },
      error: function() {
        alert("Error");
        $("#loading").fadeOut();
      },
      async: false
    });
  }, 400);
  $("#loading").fadeOut();

  $("#Modal3").modal();
}

function deletion(event) {
  // console.log("delete :"+event);
  $("#pageTable")
    .find("td[id=" + event + "]")
    .parent()
    .remove();
  saveBodyChecker();
}

function saveBodyChecker() {
  if ($("#pageContent > tbody > tr").length < 1) {
    $("#save_button").fadeOut();
  }
}

function accordion() {
  //the accordion code start
  var d = document,
    accordionToggles = d.querySelectorAll(".js-accordionTrigger"),
    setAria,
    setAccordionAria,
    switchAccordion,
    touchSupported = "ontouchstart" in window,
    pointerSupported = "pointerdown" in window;

  skipClickDelay = function(e) {
    e.preventDefault();
    e.target.click();
  };

  setAriaAttr = function(el, ariaType, newProperty) {
    el.setAttribute(ariaType, newProperty);
  };
  setAccordionAria = function(el1, el2, expanded) {
    switch (expanded) {
      case "true":
        setAriaAttr(el1, "aria-expanded", "true");
        setAriaAttr(el2, "aria-hidden", "false");
        break;
      case "false":
        setAriaAttr(el1, "aria-expanded", "false");
        setAriaAttr(el2, "aria-hidden", "true");
        break;
      default:
        break;
    }
  };

  //function
  switchAccordion = function(e) {
    // console.log("triggered");
    e.preventDefault();
    var thisAnswer = e.target.parentNode.nextElementSibling;
    var thisQuestion = e.target;
    if (thisAnswer.classList.contains("is-collapsed")) {
      setAccordionAria(thisQuestion, thisAnswer, "true");
      // console.log($(thisAnswer).attr("class"));
      // saveState();
      // console.log($(thisAnswer).attr("id"));
      saveState($(thisAnswer).attr("class"), $(thisAnswer).attr("id"));
    } else {
      setAccordionAria(thisQuestion, thisAnswer, "false");
      // console.log($(thisAnswer).attr("class"));
      saveState($(thisAnswer).attr("class"), $(thisAnswer).attr("id"));
      // console.log($(thisAnswer).attr("id"));
    }
    thisQuestion.classList.toggle("is-collapsed");
    thisQuestion.classList.toggle("is-expanded");
    thisAnswer.classList.toggle("is-collapsed");
    thisAnswer.classList.toggle("is-expanded");
    thisAnswer.classList.toggle("animateIn");
  };
  for (var i = 0, len = accordionToggles.length; i < len; i++) {
    if (touchSupported) {
      accordionToggles[i].addEventListener("touchstart", skipClickDelay, false);
    }
    if (pointerSupported) {
      accordionToggles[i].addEventListener(
        "pointerdown",
        skipClickDelay,
        false
      );
    }
    accordionToggles[i].addEventListener("click", switchAccordion, false);
  }
  //the accordion code end
}

function popupOpen() {
  var insert = [];

  var venuelen = $("#compvenueName").val().length;
  var venueTypeLen = $("#compvenueTypes").val().length;
  if (venuelen === 0) {
    $("#compvenueName")
      .attr("placeholder", "Name Compulsory")
      .css("text-color", "red");
  } else if (venueTypeLen === 0) {
    alert("Select Type");
  } else {
    $("#nameError").html("");
    $("#selectError").html("");
    if (insert.length === 0) {
      var serviceslist = localStorage.getItem("serviceslist");
      servicesListData = JSON.parse(serviceslist);
      $.each(servicesListData, function(key, value) {
        $.ajax({
          url: "api/add_venue_curl.php",
          type: "POST",
          data: { action: "getServices" },
          success: function(data) {
            localStorage.setItem("servicesData", data);
            obj = $.parseJSON(data);

            //console.log(data);
            $.each(obj.services, function(index, element) {
              if (element.id == value) {
                // insert += `<li data-value=${element.searchKey} id="${element.searchKey}" onClick="next_pop("${this.id}", "${element.id}", "${element.icon}");" <a href="javascript:void(0);"> <img width="100" height="100" src="https://api.spella.com/services/logos-mini/${element.icon}" alt="" /> <p> ${element.displayName} </p></a></li>`;
                // //console.log(insert);

                insert += `<li data-value=" ${element.searchKey}"  id="${element.searchKey}" onclick="next_pop('${element.searchKey}','${element.id}','${element.icon}');"><a href="javascript:void(0);"><img width="100" height="100" src="https://spella.com/services/logos-mini/${element.icon}" alt="" /><p>${element.displayName}</p></a></li>`;

                // insert +=
                //   ` <li data-value="` +
                //   element.id +
                //   `"  id="` +
                //   element.domainKey +
                //   `" onclick="next_pop(this.id,'` +
                //   element.id +
                //   `','` +
                //   element.icon +
                //   `');">
                //            <a href="javascript:void(0);">
                //            <img width="100" height="100" src="https://api.spella.com/services/logos-mini/` +
                //   element.icon +
                //   `" alt="" />
                //            <p>` +
                //   element.displayName +
                //   `</p></a></li>
                //             `;
              }
            });
          },
          error: function() {
            alert("Not responding error, refresh page ");
          },
          async: false
        });
      });
      $("#serviceinsertdata").html(insert);
      $("#servicelist").modal();
    } else {
      $("#serviceinsertdata").html(insert);
      $("#servicelist").modal();
    }
  }
}

function next_pop(id, element_id, icon) {
  localStorage.removeItem("serviceId");
  localStorage.setItem("serviceId", element_id);
  localStorage.removeItem("serviceIcon");
  localStorage.setItem("serviceIcon", icon);

  var userData = getuserData();
  var userObj = $.parseJSON(userData);

  var user_id = userObj.user_id;

  $(".pageList").html("");
  var val = id;
  // alert(id);
  $(".loader").removeClass("hidden");

  venue = $("#venueName").val();

  var venueType = $("#venueTypes").val();
  // what = "site:"+val+" "+venue+" "+venueType;
  // val = val.replace("https://", "");
  what = venue + " " + "site:" + val;

  //console.log(what);
  //console.log(user_id);

  $.ajax({
    url: "api/add_venue_curl.php",
    type: "POST",
    data: {
      action: "searchWeb",
      userId: user_id,
      what: what,
      venueType: venueType
    },
    success: function(data) {
      var obj = $.parseJSON(data);

      if (obj.status === "success") {
        if (Object.keys(obj.result).length === 0) {
          $(".pageList").html(
            "<li>Sorry no results , please try another Services</li>"
          );
        }

        $.each(obj.result, function(index, element) {
          $(".pageList").append(
            "<li id=" +
              element.link +
              " onclick='pageSelect(this.id);'><a href='javascript:void(0);'>" +
              element.title +
              "</a></li>"
          );
        });
      } else {
        $(".pageList").append(
          "<li>Error, please check your search keywords</li>"
        );
      }

      //console.log(obj);
    },
    error: function() {
      alert(
        "Please refresh the page, contact administrator if the problem still persists"
      );
    }
  });
  $("#Modal6 #customPageURL").attr("data-service-id", val);
  $("#Modal6 #customPageTitle").html("Custom Page (" + val + ")");
  $("#Modal2").modal();
}



//starting flling up with data
$(function() {
  var userData = getuserData();
  var userObj = $.parseJSON(userData);
  //console.log(userObj);
  venuesAppend(userObj);

  $("#loading").hide();
  var langu = localStorage.getItem("lang").toLowerCase();
  // console.log(langu)
  $.ajax({
    url: "api/add_venue_curl.php",
    type: "POST",
    dataType: "json",
    data: { action: "getEditPage", lang: "en" },
    success: function(data) {
      if (data) {
        data.types.sort(function(a, b) {
          return a.tag.localeCompare(b.tag);
        });
        $.each(data.types, function(index, element) {
          $("#compvenueTypes").append(
            `<option val=` +
              element.tag +
              `>` +
              element.locales[langu] +
              `</option>`
          );
        });
      } else {
        $("#compvenueTypes").append(
          `<option val=` + null + `>` + "No Types" + `</option>`
        );
      }
    }
  });
});

function getDates() {
  var dates;
  $.ajax({
    url: "api/venue_details_curl.php",
    type: "POST",
    data: { action: "getDates" },
    success: function(data) {
      dates = data;
    },
    error: function(data) {
      console.log("======" + data);
    },
    async: false
  });
  return dates;
}

function getuserData() {
  var userData;

  $.ajax({
    url: "home.php",
    type: "POST",
    data: { request: "getdata" },
    success: function(data) {
      // console.log("data");
      var jsondata = JSON.parse(data);
      // console.log(jsondata);
      var lang = localStorage.getItem("lang");
      if (lang == null || lang == "") {
        localStorage.setItem("lang", jsondata.preferredLanguage);
      }
      userData = data;
    },
    error: function() {
      alert(
        "Erreur Lors du traitement de la commande, veuillez actualiser la page"
      );
    },
    async: false
  });

  return userData;
}

function venuesAppend(userObj) {
  var color1 = [];
  var percent = [];
  var venuedata;
  var dates = getDates();
  var months = getMonths(dates);
  months = $.parseJSON(months);
  // console.log("=========Months");
  // console.log(months);
  $.ajax({
    url: "api/getVenue_curl.php",
    type: "POST",
    data: {
      action: "getVenues",
      userId: userObj["user_id"],
      graphVersion: null
    },
    success: function(response) {
      console.log(response);
      if (response == Number(0)) {
        $("#loading").fadeOut();
        localStorage.removeItem("venueData");
        localStorage.setItem("venueData", Number(response));
        venueData = response;
        var langu = localStorage.getItem("lang").toLowerCase();
        switch (langu) {
          case "en":
            msg = "No Venues";
            break;
          case "fr":
            msg = "Aucun établissement.";
            break;
        }
        $("#NoVenueData").html(msg);

        //$("#NoVenueData").html("No Venues");
        $("#modify").hide();
      } else {
        localStorage.removeItem("venueData");
        localStorage.setItem("venueData", response);
        venuedata = response;
        console.log(venuedata);
        var serviceslist = localStorage.getItem("serviceslist");
        console.log(serviceslist);
      }
    },
    error: function() {
      alert("Aucune donnée reçue, rafraîchissante");
      location.reload();
    },
    async: false
  });

  if (venuedata == Number(0)) {
    console.log("No data");
    return false;
  } else {
    var venueObj = $.parseJSON(venuedata);
    console.log(venueObj);

    // localStorage["addedVenues"] = venueObj.result.venues.length;
  }
  // console.log("Non-Reverse-array");
  // console.log(venueObj.result.graph);
  // console.log("Reverse-array");
  // console.log(reverseArr(venueObj.result.graph));
  // console.log(reverseArr(venueObj.result.venues));

  function reverseArr(input) {
    var ret = new Array();
    for (var i = input.length - 1; i >= 0; i--) {
      ret.push(input[i]);
    }
    return ret;
  }

  //     $.each(venueObj.result.graph,function(index3,graphelement){
  //     	console.log(graphelement);
  // });

  $.each(venueObj.result.graph, function(index3, element3) {
    $.each(venueObj.result.venues, function(index, element) {
      // console.log(element3.venueId);
      if (element.venueId === element3.venueId) {
        if (element.venueMonthlyMinus1Tendency > element.venueMonthlyTendency) {
          var color = "#C73729";
          var arrow = "down";
        } else {
          var color = "#67BFB3";
          var arrow = "up";
        }
        var current = element.venueMonthlyTendency;
        if (element.venueMonthlyTendency === 0) {
          var arrow = "right";

          if (element.venueMonthlyMinus1Tendency !== 0) {
            current = element.venueMonthlyMinus1Tendency;
          }
        }

        percent[0] = (element.venueMonthlyMinus4Tendency * 100) / 5;
        percent[1] = (element.venueMonthlyMinus3Tendency * 100) / 5;
        percent[2] = (element.venueMonthlyMinus2Tendency * 100) / 5;
        percent[3] = (element.venueMonthlyMinus1Tendency * 100) / 5;
        percent[4] = (element.venueMonthlyTendency * 100) / 5;

        if (element.venueMonthlyTendency <= 3.5) {
          color1[4] = "#C73729";
        } else {
          color1[4] = "#67BFB3";
        }

        if (element.venueMonthlyMinus1Tendency <= 3.5) {
          color1[3] = "#C73729";
        } else {
          color1[3] = "#67BFB3";
        }

        if (element.venueMonthlyMinus2Tendency <= 3.5) {
          color1[2] = "#C73729";
        } else {
          color1[2] = "#67BFB3";
        }

        if (element.venueMonthlyMinus3Tendency <= 3.5) {
          color1[1] = "#C73729";
        } else {
          color1[1] = "#67BFB3";
        }

        if (element.venueMonthlyMinus4Tendency <= 3.5) {
          color1[0] = "#C73729";
        } else {
          color1[0] = "#67BFB3";
        }

        var appen =
          `<li class="ui-state-default"  id="` +
          element.venueId +
          `"><div class="row mr-0">
                   <a href="#" aria-expanded="false" aria-controls="accordion1" 
                   class="col-sm-6 accordion-title accordionTitle js-accordionTrigger">
                   <span class="venuetitle" onclick='javascript:location.href="venue.php?id=` +
          element.venueId +
          `";'>` +
          element.venueName +
          `</span>
                   <span class="pull-right"><span>` +
          current +
          `</span>
                   <i style="color:` +
          color +
          `" class="fas fa-long-arrow-alt-` +
          arrow +
          `"></i></span></a>

                   <a class="col-sm-3  btn btn-competitor" type="submit" onclick="compUp('` +
          element.venueId +
          `','` +
          element.venueName +
          `');">competitor</a> 
                   <a style="display:none; cursor: pointer;" class="col-sm-3 clear-option edit-sec">
                    <span  id="` +
          element.venueId +
          `" onclick="deleted('` +
          element.venueId +
          `',` +
          parseInt(index + 1) +
          `);">
                    <i style='font-size: 15px;' class="fas fa-trash fa-2x"></i></span>
                    <span><i onclick='venueEdit("` +
          element.venueId +
          `");' style='font-size: 15px;' class="fas fa-pencil-alt fa-2x"></i></span>
                    <span><i style='font-size: 15px;' class="fas fa-bars fa-2x"></i></span>
                  </a>  
               </div>
                  <div class="accordion-content accordionItem is-collapsed" id="accordion` +
          parseInt(index + 1) +
          `" aria-hidden="true">
                  <div class="row m-t-25">
                    <div class="col-md-12">
                      <div class="value-chart">
                        <div class="box-chart">
                          <h4 class="value">` +
          element.venueMonthlyMinus4Tendency +
          `</h4>
                          <div style="height: ` +
          percent[0] +
          `%; background-color:` +
          color1[0] +
          `;" class="box-chart-inner">
                          </div>
                        </div>
                        <h5>` +
          months[1] +
          `</h5>
                      </div>
                      <div class="value-chart">
                        <div class="box-chart">
                         <h4 class="value">` +
          element.venueMonthlyMinus3Tendency +
          `</h4>
                         <div style="height: ` +
          percent[1] +
          `%; background-color:` +
          color1[1] +
          `;" class="box-chart-inner">
                         </div>
                       </div>
                       <h5>` +
          months[2] +
          `</h5>
                     </div>
                     <div class="value-chart">
                       <div class="box-chart">
                         <h4 class="value">` +
          element.venueMonthlyMinus2Tendency +
          `</h4>
                         <div style="height: ` +
          percent[2] +
          `%; background-color:` +
          color1[2] +
          `;" class="box-chart-inner">
                         </div>
                       </div>
                       <h5>` +
          months[3] +
          `</h5>
                     </div>

                     <div class="value-chart">
                       <div class="box-chart">
                         <h4 class="value">` +
          element.venueMonthlyMinus1Tendency +
          `</h4>
                         <div style="height: ` +
          percent[3] +
          `%; background-color:` +
          color1[3] +
          `;" class="box-chart-inner">
                         </div>
                       </div>
                       <h5>` +
          months[4] +
          `</h5>
                     </div>

                      <div class="value-chart">
                       <div class="box-chart">
                         <h4 class="value">` +
          element.venueMonthlyTendency +
          `</h4>
                         <div style="height: ` +
          percent[4] +
          `%; background-color:` +
          color1[4] +
          `;" class="box-chart-inner">
                         </div>
                       </div>
                       <h5>` +
          months[5] +
          `</h5>
                     </div>
                   </div>
                  <div class="col-md-10 offset-1"><ul class='compsort' id="vcomp` +
          element.venueId +
          `">`;

        $.each(venueObj.result.graph, function(index2, element2) {
          var current = 0;
          var color = "";
          var arrow = "";

          if (element.venueId === element2.venueId) {
            $.each(element2.venueCompetitors, function(index3, element3) {
              $.each(venueObj.result.venues, function(index4, element4) {
                if (
                  element4.venueMonthlyMinus1Tendency >
                  element4.venueMonthlyTendency
                ) {
                  color = "#C73729";
                  arrow = "down";
                } else {
                  color = "#67BFB3";
                  arrow = "up";
                }
                current = element4.venueMonthlyTendency;
                if (element4.venueMonthlyTendency === 0) {
                  color = "#67BFB3";
                  arrow = "right";
                  if (element4.venueMonthlyMinus1Tendency !== 0) {
                    if (element4.venueMonthlyMinus1Tendency < 3.5) {
                      color = "#C73729";
                    }
                    current = element4.venueMonthlyMinus1Tendency;
                  } else {
                    current = 0;
                  }
                }
                if (element3 === element4.venueId) {
                  appen +=
                    `<div id="venueComp` +
                    parseInt(index3 + 1) +
                    `"" class="competitor row">
                                      <div class="col-sm-6">
                                      <ul id="` +
                    element4.venueId +
                    `">
                                      <li><a href="javascript:void(0);" 
                                      onclick='javascript:location.href="venue.php?id=` +
                    element4.venueId +
                    `";'>` +
                    element4.venueName +
                    `</a></li>
                                      <li class="pull-right">` +
                    current +
                    `
                                      <span class="pull-right">
                                      <i style="color:` +
                    color +
                    `" class="fas fa-long-arrow-alt-` +
                    arrow +
                    `"></i>
                                      </span>
                                      </li>
                                      </ul>
                                      </div>
                                      <div class="col-sm-6">
                      				  <span>
                        			  <span id="` +
                    element3 +
                    `" class="modify-option" style="display:none; cursor: pointer;">
                        			  <span id="` +
                    element3 +
                    `" onclick="Compdeleted('` +
                    element3 +
                    `',` +
                    parseInt(index3 + 1) +
                    `);">
                        			  <i class="fas fa-trash"></i></span>
                        			  <span onclick="venueEdit('` +
                    element3 +
                    `');"><i class="fas fa-pencil-alt"></i></span>
                    				  <span><i class="fas fa-bars"></i></span>
                        			  </span>
                      				  </span>
                    				  </div></div>`;
                }
              });
            });
          }
        });

        appen += `
                  
                </ul></div>
              </div></li>`;

        $("#appendval").append(appen);
      } //checking if in the graph end
    });
  });
  accordion();
  $("#loading").fadeOut(600);
}

// Edit venue

function venueEdit(venueid) {
  localStorage.setItem("editvenue", venueid);
  location.href = "edit_venue.php?id=" + venueid;
}

//modify button action
function reArrange() {
  // console.log("re-arrange");
}

$("#modify").click(function() {
  $(".compsort").sortable({
    change: function(event, ui) {
      // console.log(ui);
    }
  });
  $(".compsort").sortable("enable");
  $("#appendval").sortable({
    update: function(event, ui) {
      //         var newIndex = ui.item.index();
      //         console.log(ui);
      //         // console.log("Old Index: "+oldIndex+" New Index: "+newIndex);
      //       console.log("Re arranged id : "+$(ui.item).attr('id'));
      //       console.log("New position: " + ui.item.index());
      //       var objvenue = jQuery.parseJSON(localStorage.getItem("venueData"));
      //       // var objvenue = objvenue.result.graph;
      //       console.log(objvenue.result.graph);
      //       console.log(array_move(objvenue.result.graph,ui.item.index()));
      // var indexes = new Array(indexes);
      var ids = new Array();
      $("#appendval li").each(function(i) {
        ids.push($(this).attr("id")); // This is your rel value
      });
      // console.log("ids");
      // console.log(ids);

      var objvenue = jQuery.parseJSON(localStorage.getItem("venueData"));
      // console.log(objvenue.result.graph);

      var reArrangedIndex = ui.item.index();
      var reArrangedId = $(ui.item)
        .attr("id")
        .toString();
      var reArrangedGraph = [];
      var tempReArrangedGraph = [];
      var itemIndex = 0;

      var counter = 0;
      objvenue.result.graph.forEach((item, index) => {
        counter++;
        reArrangedGraph[index] = {};
        tempReArrangedGraph.push(item);
        // which index position?
        if (item.venueId == reArrangedId) {
          itemIndex = index;
        }
        if (counter == objvenue.result.graph.length) {
          // push re-arranged item to new position
          reArrangedGraph[reArrangedIndex] = objvenue.result.graph[itemIndex];
          tempReArrangedGraph.splice(itemIndex, 1);

          // push everything else
          var ncounter = 0;
          var tempIndex = 0;
          objvenue.result.graph.forEach((item, index) => {
            ncounter++;
            if (!reArrangedGraph[index].hasOwnProperty("venueId")) {
              reArrangedGraph[index] = tempReArrangedGraph[tempIndex];
              tempIndex++;
            }
            if (ncounter == objvenue.result.graph.length) {
              // Final result
              // console.log('reArrangedGraphAfter', reArrangedGraph);
              localStorage.removeItem("reArrangedGraph");
              localStorage.setItem(
                "reArrangedGraph",
                JSON.stringify(reArrangedGraph)
              );
            }
          });
        }
      });

      function array_move(arr, new_index) {
        var indexes = new Array(indexes);

        var old_index = oldIndex(arr);
        indexes.push({ oldIndex: old_index, newIndex: new_index });
        // console.log(indexes);
        //  if (old_index!=new_index) {
        //    if (new_index >= arr.length) {
        //        var k = new_index - arr.length + 1;
        //        while (k--) {
        //            arr.push(undefined);
        //        }
        //    }
        //    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
        //    return arr; // for testing
        // }
        // else{}
      }

      function oldIndex(objvenue) {
        // console.log(objvenue);
        var prev;
        objvenue.forEach(function(value, index) {
          // console.log("foreach index: "+index);
          if (
            value.venueId.toString() ==
            $(ui.item)
              .attr("id")
              .toString()
          ) {
            // console.log("Previous pos: "+index);
            prev = index;
          }
        });
        return prev;
      }
    }
  });
  $("#appendval").sortable("enable");
  $(".clear-option").slideDown();
  $(".modify-option").slideDown();
  $("#save").slideDown();
  $("#cancel").slideDown();
});

//cancel button action

$("#cancel").click(function() {
  $(".compsort").sortable("disable");
  $("#appendval").sortable("disable");
  $(".modify-option").hide();
  if (deleting_array.length === 0) {
    $(".clear-option").slideUp();
    $("#save").slideUp();
    $("#cancel").slideUp();
  } else {
    deleting_array = [];
    location.reload();
  }
});

//save button

$("#save").click(function() {
  $("#loading").fadeIn();

  var ids = new Array();
  $("#appendval li").each(function(i) {
    ids.push($(this).attr("id")); // This is your rel value
  });
  // console.log(ids);
  var venuedata = $.parseJSON(localStorage.getItem("venueData"));
  // console.log(venuedata.result.graph);
  var sentableArray = new Array();
  $(ids).each(function(index, idss) {
    $(venuedata.result.graph).each(function(index, element) {
      if (idss == element.venueId) {
        var comids = new Array();
        $("#vcomp" + idss + " ul").each(function(i) {
          comids.push($(this).attr("id")); // This is your rel value
        });
        // console.log("comids");
        // console.log(comids);
        element.venueCompetitors = {};
        element.venueCompetitors = comids;
        // console.log("element");
        // console.log(element);
        // $(ids).each(function(index,idss)
        // {

        // });
        sentableArray.push(element);
      }
    });
  });
  // console.log("sentableArray");
  // console.log(sentableArray);
  var userdata = getuserData();
  userdata = $.parseJSON(userdata);
  var user_id = userdata.user_id;
  $.ajax({
    url: "api/home_venue_list_update_curl.php",
    type: "POST",

    data: {
      action: "updateGraph",
      userId: user_id,
      graph: JSON.stringify(sentableArray)
    },
    success: function(data) {
      // console.log(data);
      localStorage.removeItem("reArrangedGraph");
      $(".loader").addClass("hidden");
      location.reload();
    },
    error: function() {
      $(".loader").addClass("hidden");
    }
  });
});

// function addComp(venueId) {
//   var addedcomp = localStorage.getItem("addedcompet");
//   console.log(addedcomp);
//   $("#addcomp").modal();
// }

function addComp(venueId) {

  var numberofaddedcompet = localStorage.getItem("addedcompet");
  
  if (numberofaddedcompet < no_of_competitors) {	

/*var addedcomp = localStorage.getItem("addedcompet");
  if (addedcomp < numberofcompet) {*/

    $("#addcomp").modal();
  } else {
    let msg =
      langu === "en"
        ? "You cannot add more competitors in current plan."
        : "Vous avez atteint le nombre de concurrents de votre abonnement.";
    alert(msg);
  }
}

function guidGenerator() {
  var S4 = function() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  };
  return (
    S4() +
    S4() +
    "-" +
    S4() +
    "-" +
    S4() +
    "-" +
    S4() +
    "-" +
    S4() +
    S4() +
    S4()
  );
}

function addcompet() {
  // localStorage.setItem("ovenueId",ovenueId);
  var addedcompet = localStorage.getItem("addedcompet");
  var venueType = $("#compvenueTypes").val();
  var venue = $("#compvenueName ").val();
  var userData = getuserData();
  var userObj = $.parseJSON(userData);
  var venueId = guidGenerator();
  var user_id = userObj.user_id;

  var i = 0;
  var services = [];
  $("#pageContent > tbody  > tr td:first-child").each(function() {
    var id = $(this).attr("id");
    var url = $(this).attr("data-value");
    var serviceVenueUUID = guidGenerator();
    services[i] = {
      serviceVenueUUID: serviceVenueUUID,
      serviceId: id,
      serviceVenueURL: url,
      serviceVenueStatus: "unknown",
      serviceEnabled: true
    };
    i++;
  });

  $.ajax({
    url: "api/add_venue_curl.php",
    type: "POST",
    data: {
      action: "createVenue",
      userId: user_id,
      venueName: venue,
      venueId: venueId,
      venueType: venueType,
      venueServices: services
    },
    success: function(data) {
      var good = [];
      var result = $.parseJSON(data);
      if (result.status === "success") {
        $.ajax({
          url: "api/add_venue_curl.php",
          type: "POST",
          data: {
            action: "validateVenue",
            userId: user_id,
            venueId: venueId,
            services: services
          },
          success: function(data) {
            var obj_val = $.parseJSON(data);
            if (obj_val.status === "success") {
              updateGraph(user_id, venueId);
              addedcompet = addedcompet + 1;
            }
			localStorage.setItem("addedcompet", addedcompet);
          },
          error(data) {
            var obj = $.parseJSON(data);
            alert(data.message);
          },
          async: false
        });
      }
    },
    error: function() {
      alert("VenueAddition Faliure,Refreshing page");
      loaction.reload();
    },
    async: false
  });
  //localStorage.setItem("addedcompet", addedcompet);
}

function updateGraph(userId, venueId) {
  var newArray = JSON.parse(localStorage.getItem("venueData"));

  $.each(newArray.result.graph, function(index, oelement) {
    var ovenueId = $("#compvenueId").val();
    if (oelement.venueId == ovenueId) {
      // var tempgraph = ;
      newArray.result.graph[index].venueCompetitors.push(venueId);
    }
  });
  var sentable = JSON.stringify(newArray.result.graph);

  $.ajax({
    url: "api/home_venue_list_update_curl.php",
    type: "POST",

    data: {
      action: "updateGraph",
      userId: userId,
      graph: sentable
    },
    success: function(data) {
      localStorage.removeItem("venueData");
      // compCount(venueId);

      $(".loader").addClass("hidden");
      window.location.href = "home.php";
    },
    error: function(data) {
      alert(
        "Error,pleaese refresh or contact adminstrator if problem still persists"
      );
    },
    async: false
  });
}

function compUp(venueId, venuename) {
  console.log("no of comp: " + no_of_competitors);

 /* if (localStorage["pricingStatus"] == "trial") {
    let msg =
      langu === "en"
        ? "You cannot add competitors in trial plan."
        : "Impossible d'ajouter un concurrent car vous êtes en mode évaluation.";
    $("#competitordata").html(msg);
    $("#compadd").hide();
    $("#compUp").modal();
    return;
  }*/

  // var compcount = compCount(venueId);
  // if(compcount == 1){
  //   var msg = "You can add only one competitor in your current plan.";
  //   $("#competitordata").html(msg);
  //   $("#compadd").hide();
  //   $("#compUp").modal();
  //   return;
  // }

  $("#compadd").attr("onclick", "addComp('" + venueId + "');");
  $("#compvenueId").val(venueId);
  $("#venuename").html(venuename + "'s ");
  $("#compUp").modal();
  var venueData = JSON.parse(localStorage.getItem("venueData"));
  getComp(venueData.result);

  function getComp(inarray) {
    var langu = localStorage.getItem("lang").toLowerCase();
    var msg = "";
    switch (langu) {
      case "en":
        msg =
          "No competitors for this venue. Check back after adding competitors";
        break;
      case "fr":
        msg =
          "Aucun concurrent pour ce lieu. Revenez après avoir ajouté des concurrents";
        break;
    }
    $("#competitordata").html(msg);

    var dates = getDates();
    var months = getMonths(dates);
    months = $.parseJSON(months);

    // console.log(months)
    inarray.graph.forEach(function(value, index) {
      if (value.venueId == venueId) {
        // console.log(value.venueCompetitors);
        if (value.venueCompetitors.length > 0) {
          $("#competitordata").html("");
        }
        value.venueCompetitors.forEach(function(value1, index1) {
          inarray.venues.forEach(function(value2, index2) {
            if (value2.venueId == value1) {
              // console.log("value2");
              // console.log(value2);

              if (
                value2.venueMonthlyMinus1Tendency > value2.venueMonthlyTendency
              ) {
                var color = "#C73729";
                var arrow = "down";
              } else {
                var color = "#67BFB3";
                var arrow = "up";
              }
              var percent = new Array();
              var color1 = new Array();

              percent[0] = (value2.venueMonthlyMinus4Tendency * 100) / 5;
              percent[1] = (value2.venueMonthlyMinus3Tendency * 100) / 5;
              percent[2] = (value2.venueMonthlyMinus2Tendency * 100) / 5;
              percent[3] = (value2.venueMonthlyMinus1Tendency * 100) / 5;
              percent[4] = (value2.venueMonthlyTendency * 100) / 5;

              if (value2.venueMonthlyTendency <= 3.5) {
                color1[4] = "#C73729";
              } else {
                color1[4] = "#67BFB3";
              }

              if (value2.venueMonthlyMinus1Tendency <= 3.5) {
                color1[3] = "#C73729";
              } else {
                color1[3] = "#67BFB3";
              }

              if (value2.venueMonthlyMinus2Tendency <= 3.5) {
                color1[2] = "#C73729";
              } else {
                color1[2] = "#67BFB3";
              }

              if (value2.venueMonthlyMinus3Tendency <= 3.5) {
                color1[1] = "#C73729";
              } else {
                color1[1] = "#67BFB3";
              }

              if (value2.venueMonthlyMinus4Tendency <= 3.5) {
                color1[0] = "#C73729";
              } else {
                color1[0] = "#67BFB3";
              }

              $("#competitordata").append(
                `<div class="accordion-content accordionItem is-expanded animateIn" id="accordion1" aria-hidden="false">
                  <div class="row m-t-25">
                    <div class="col-md-12">
                    <p style='float:left;'><a href="venue.php?id=` +
                  value2.venueId +
                  `">` +
                  value2.venueName +
                  `</a></p>
                    <div class='clearfix'></div>
                      <div class="value-chart">
                        <div class="box-chart" style="width: 70px !important;height: 50px !important;">
                          <h4 class="value" style='left: 24px !important;top: 10px !important;'>` +
                  value2.venueMonthlyMinus4Tendency +
                  `</h4>
                          <div style="height: ` +
                  percent[0] +
                  `%; background-color:` +
                  color1[0] +
                  `;" class="box-chart-inner">
                          </div>
                        </div>
                        <h5>` +
                  months[1] +
                  `
</h5>
                      </div>
                      <div class="value-chart">
                        <div class="box-chart" style="width: 70px !important;height: 50px !important;">
                         <h4 class="value" style='left: 24px !important;top: 10px !important;'>` +
                  value2.venueMonthlyMinus3Tendency +
                  `</h4>
                         <div style="height: ` +
                  percent[1] +
                  `%; background-color:` +
                  color1[1] +
                  `;" class="box-chart-inner">
                         </div>
                       </div>
                       <h5>` +
                  months[2] +
                  `
</h5>
                     </div>
                     <div class="value-chart">
                       <div class="box-chart" style="width: 70px !important;height: 50px !important;;">
                         <h4 class="value" style='left: 24px !important;top: 10px !important;'>` +
                  value2.venueMonthlyMinus2Tendency +
                  `</h4>
                         <div style="height: ` +
                  percent[2] +
                  `%; background-color:` +
                  color1[2] +
                  `;" class="box-chart-inner">
                         </div>
                       </div>
                       <h5>` +
                  months[3] +
                  `
</h5>
                     </div>

                     <div class="value-chart">
                       <div class="box-chart" style="width: 70px !important;height: 50px !important;">
                         <h4 class="value" style='left: 24px !important;top: 10px !important;'>` +
                  value2.venueMonthlyMinus1Tendency +
                  `</h4>
                         <div style="height: ` +
                  percent[3] +
                  `%; background-color:` +
                  color1[3] +
                  `;" class="box-chart-inner">
                         </div>
                       </div>
                       <h5>` +
                  months[4] +
                  `
</h5>
                     </div>

                      <div class="value-chart">
                       <div class="box-chart" style="width: 70px !important;height: 50px !important;">
                         <h4 class="value" style='left: 24px !important;top: 10px !important;'>` +
                  value2.venueMonthlyTendency +
                  `</h4>
                         <div style="height: ` +
                  percent[4] +
                  `%; background-color:` +
                  color1[4] +
                  `;" class="box-chart-inner">
                         </div>
                       </div>
                       <h5>` +
                  months[5] +
                  `
</h5>
                     </div>
                   </div>
                   <div class="col-md-10 offset-1" style="margin-bottom: 35px !important;"><ul class="compsort" >
                  
                </ul></div>
              </div></div>`
              );
              // var competitor += `<p>Competitor</p>`;
              // console.log("Matched!: "+index2);
              // console.log(value2);
            }
          });
        });
      }
    });
  }
}
//for finding number of compititors
// function compCount(venueId){
//   var compcount = 1;
//   return(compcount);

// }

function getMonths(dates) {
  var months;
  $.ajax({
    url: "api/venue_details_curl.php",
    type: "POST",
    data: { action: "getMonths", dates: dates },
    success: function(data) {
      months = data;
    },
    error: function(data) {
      console.log(data);
    },
    async: false
  });
  return months;
}

var deleting_array = [];
function deleted(id, accordion) {
  var deleted = id;
  var venueData = $.parseJSON(localStorage.getItem("venueData"));
  var id = "#" + id;
  var accordion = "#accordion" + accordion;

  $(id).remove();

  $.each(venueData.result.graph, function(index, element) {
    if (element.venueId === deleted) {
      if (element.venueCompetitors.length !== 0) {
        $.each(element.venueCompetitors, function(index2, element2) {
          deleting_array.push(element2);
        });
      }
    }
  });
  deleting_array.push(deleted);
  // console.log("=========deleting_array");
  // console.log(deleting_array);
  // deletevenue(deleting_array);
  // deletevenue(deleting_array);
}

// function deletevenue(deleting_array){
//  var originalVenuedata = JSON.parse(localStorage.getItem("venueData"));
//  var originalvenues = originalVenuedata.result.venues;
//  console.log("new venues");
// $.each(deleting_array, function( index, value ) {
//    var returnarray = removeFromArray(originalvenues,value);
//    localStorage.setItem("newtemp",JSON.stringify(returnarray));
// });
// var newtemp = localStorage.getItem("newtemp");
// console.log(newtemp);
// // originalVenuedata.result.venues = newtemp;
// // return newtemp;
// }

// function removeFromArray(array, element) {
//     const index = array.indexOf(element);
//     array.splice(index, 1);
//     return array;
// }

var delete_comp = [];
function Compdeleted(comp_id, index) {
  // console.log(comp_id);
  // console.log("=============index"+index)
  var addedcomp = localStorage.getItem("addedcompet");
  var id = "#venueComp" + index;
  var deleted = comp_id;
  $("#" + comp_id).remove();
  $("#" + comp_id).remove();
  delete_comp.push(deleted);
  addedcomp = addedcomp - 1;
  console.log(addedcomp);
  //localStorage.setItem("addedcompet", addedcomp);
}

function showModal6() {
	$("#Modal6").modal();
}

function validateURL() {
	$("#em_error").hide();
	var err = false;
	var errMsg = "";
	var customURL = $("#customPageURL").val();	
	var lang = localStorage.getItem("lang").toLowerCase();
	if ($("#customPageURL").get(0).checkValidity() == true) {			
		hostDomain = (new URL(customURL)).hostname;
		serviceDomain = $("#customPageURL").attr('data-service-id');	
		console.log(hostDomain);
		console.log(serviceDomain);
		if (hostDomain.indexOf(serviceDomain) >= 0) {
			$("#em_error").hide();
			$("#Modal6").modal("toggle");
			pageSelect(customURL);
		} else {
			err = true;
			if (lang == "en") {
				errMsg = "Should match with the service domain";
			} else if (lang == "fr") {
				errMsg = "Doit correspondre au domaine de service";
			}				
		}
	} else {
		err = true;
		if (lang == "en") {
			errMsg = "Invalid URL";
		} else if (lang == "fr") {
			errMsg = "URL invalide";
		}
	}
	if (err) {		
		$("#em_error").html(errMsg);
		$("#em_error").show();
	}
}