function RedrawRepartition(data , lang, venueDetailData) {

  var using=data;
  var venueD = $.parseJSON(venueDetailData)
  var language=lang;
  
  var sortTemp =[];
  for(let key in using) {
    sortTemp.push({
      key: key,
      value: using[key]
    })
  } 

  sortTemp = sortTemp.sort(function(a,b){
    return b.value - a.value
  })

  
  let label = [];
  let value = [];
$.each(sortTemp,function(index,element) {
  label.push(element.key)
  value.push(element.value)
})
console.log(label)
console.log(value)
// console.log(venueD.result.services)
 if(venueD.result.services.length > 0) {

    //incase the chart is non existent at the current scenario.
    if (!window.myChart_barchart_material) {
      console.log('smeghing wrong')
       var htitle = "";
    switch(localStorage.getItem('lang').toLowerCase())
    {
      case 'en' : htitle='Count'
            break;
      case 'fr' : htitle='compter';
            break;
    }

   var chartData = {
   type: 'horizontalBar',
   data: {
      labels: label,
    datasets: [
        {
            label: htitle,
            backgroundColor: "#6E388A",
            data: value
        }
    ]
   },
   options: {
        barValueSpacing: 20,
        scales: {
            xAxes: [{
              barPercentage: 0.4,
                ticks: {
                    min: 0,
                }
            }]
        }
    }
} 

         window.canvas_barchart_material = document.getElementById('barchart_material');
         window.myChart_barchart_material = new Chart(window.canvas_barchart_material, chartData);

  window.canvas_barchart_material.onclick = function(evt) {
   var activePoint = myChart_barchart_material.getElementAtEvent(evt)[0];
   var data = activePoint._chart.data;
   var datasetIndex = activePoint._datasetIndex;
  var mainlabel=activePoint._model['label'];
   var label = data.datasets[datasetIndex].label;
   var value = data.datasets[datasetIndex].data[activePoint._index];
   // alert(mainlabel+' : '+label+' - '+value);
  // console.log(value)
   MainTopicLoad(mainlabel,language,venueDetailData);
  
   //console.log(mainlabel);
};


    } else {

        window.myChart_barchart_material.data.labels = [];
          label.forEach(element => {
          window.myChart_barchart_material.data.labels.push(element);
          })
          // window.myChart_barchart_material.data.datasets.data = [];
            window.myChart_barchart_material.data.datasets.forEach((dataset) => {
              $.each(dataset.data,function(index, second_val) {
                dataset.data[index] = value[index];
              })
          });
          // window.myChart_barchart_material.data.datasets.forEach((dataset) => {
          //     dataset.data.push(value);
          // });

  window.canvas_barchart_material.onclick = function(evt) {
   var activePoint = window.myChart_barchart_material.getElementAtEvent(evt)[0];
   var data = activePoint._chart.data;
   var datasetIndex = activePoint._datasetIndex;
  var mainlabel=activePoint._model['label'];
   var label = data.datasets[datasetIndex].label;
   var value = data.datasets[datasetIndex].data[activePoint._index];
   // alert(mainlabel+' : '+label+' - '+value);
  // console.log(value)
   MainTopicLoad(mainlabel,language,venueDetailData);
  
   //console.log(mainlabel);
};

            window.myChart_barchart_material.update();
    
    $("#barchart_material_error").html('');
    $("#barchart_material").show()
    }


 } else{
  // console.log('no data man')
      if (window.myChart_barchart_material) {
      window.myChart_barchart_material.data.labels = [];
      window.myChart_barchart_material.data.datasets.forEach((dataset) => {
          $.each(dataset.data,function(index2,element2){
                 delete(dataset.data[index2]);
                })
      });
      window.myChart_barchart_material.update();
      }
    $("#barchart_material_error").html(dataMessage);
    $("#barchart_material").hide()
}

} // end of redrawRepartition

// redraw Impression

function RedrawImpressionGraph(negative,positive,lang,venueDetailData) {

  var language=lang;
    var chartValues = [];
    var venueD = $.parseJSON(venueDetailData);
    var htitle1 = htitle2 = "";
    switch(localStorage.getItem('lang').toLowerCase())
    {
      case 'en' : htitle1='positive'; htitle2 = 'negative';
            break;
      case 'fr' : htitle1='positif'; htitle2 = 'négatif';
            break;
    }

    let label = [];
    let pos = [];
    let neg =[];
 var negPosCount = getCountOfPosNeg(negative,positive);
    var sortedData = []; 
    if(negPosCount.pos_count > 0 && negPosCount.neg_count > 0) {
     sortedData = multiSortForImpression(negative,positive);

    } else {
       if(negPosCount.pos_count > 0 ) {
             sortedData = singularSort('pos',positive);
       } else {
            sortedData = singularSort('neg',negative)
       }
    }
    
    $.each(sortedData,function(index,element) {
    label.push(element.label);  
    pos.push(element.pos);
    neg.push(element.neg);
    })

        if(venueD.result.services.length > 0) {

            if(!window.myChart_new_chart) {

              var chartData = {
       type: 'horizontalBar',
       data: {
          labels: label,
        datasets: [
            {
                label: htitle1,
                backgroundColor: "#70beb1",
                data: pos,
            },
            {
                label: htitle2,
                backgroundColor: "#C73729",
                data: neg,
            }
        ]
       },
       options: {
            barValueSpacing: 20,
            scales: {
                xAxes: [{
                  barPercentage: 0.4,
                    ticks: {
                        min: 0,
                    },
                    scaleLabel: {
                display: true,
             labelString: 'probability'
            }
                }]
            }
        }
    } 

     window.canvas_new_chart = document.getElementById('new_chart');
     window.myChart_new_chart = new Chart(window.canvas_new_chart, chartData);

    window.canvas_new_chart.onclick = function(evt) {
       var activePoint = window.myChart_new_chart.getElementAtEvent(evt)[0];
       var data = activePoint._chart.data;
       var datasetIndex = activePoint._datasetIndex;
      var mainlabel=activePoint._model['label'];
       var label = data.datasets[datasetIndex].label;
       var value = data.datasets[datasetIndex].data[activePoint._index];
       var message = [];
       message.push(mainlabel)
       message.push(label)
       // console.log(message)
       handleMainTopicImpression(message.toString(),language,venueDetailData)

    };

        } else { // incase the graph is existent then updating

          window.myChart_new_chart.data.labels = [];
          label.forEach(element => {
          window.myChart_new_chart.data.labels.push(element);
          })
          // window.myChart_barchart_material.data.datasets.data = [];
          console.log('now checking')
          window.myChart_new_chart.data.datasets.forEach((dataset,index) => {

              if(index == 0) {
                  dataset.data = pos;
              } else if(index == 1){
                  dataset.data = neg;
              }
            });

            window.canvas_new_chart.onclick = function(evt) {
             var activePoint = window.myChart_new_chart.getElementAtEvent(evt)[0];
             var data = activePoint._chart.data;
             var datasetIndex = activePoint._datasetIndex;
            var mainlabel=activePoint._model['label'];
             var label = data.datasets[datasetIndex].label;
             var value = data.datasets[datasetIndex].data[activePoint._index];
             var message = [];
             message.push(mainlabel)
             message.push(label)
             handleMainTopicImpression(message.toString(),language,venueDetailData)
            
          };

    window.myChart_new_chart.update();
  
    $("#myChart_new_chart_error").html('');
    $("#new_chart").show()

        }

    } else {

       // console.log('no data man')
      if (window.myChart_new_chart) {
      window.myChart_new_chart.data.labels = [];
      window.myChart_new_chart.data.datasets.forEach((dataset) => {
           $.each(dataset.data,function(index2,element2){
                 delete(dataset.data[index2]);
                })
      });
      window.myChart_new_chart.update();
      }
    $("#myChart_new_chart_error").html(dataMessage);
    $("#new_chart").hide()

    }


} // end of redrawImpression


function RedrawWordImpression(positive,negative,lang,venueDetailData) {

  var language=lang;
    var chartValues = [];
    var venueD = $.parseJSON(venueDetailData);
    var htitle1 = htitle2 = "";
    switch(localStorage.getItem('lang').toLowerCase())
    {
      case 'en' : htitle1='positive'; htitle2 = 'negative';
            break;
      case 'fr' : htitle1='positif'; htitle2 = 'négatif';
            break;
    }

    let label = [];
    let pos = [];
    let neg =[];
    var negPosCount = getCountOfPosNeg(negative,positive);
    var sortedData = []; 
    if(negPosCount.pos_count > 0 && negPosCount.neg_count > 0) {
     sortedData = multiSort(negative,positive);

    } else {
       if(negPosCount.pos_count > 0 ) {
             sortedData = singularSort('pos',positive);
       } else {
            sortedData = singularSort('neg',negative)
       }
    }
    

    $.each(sortedData,function(index,element) {
    label.push(element.label);  
    pos.push(element.pos);
    neg.push(element.neg);
    })


    if(venueD.result.services.length > 0) {

            if(!window.myChart_WordImpressionGraph) {

              var chartData = {
       type: 'horizontalBar',
       data: {
          labels: label,
        datasets: [
            {
                label: htitle1,
                backgroundColor: "#70beb1",
                data: pos,
            },
            {
                label: htitle2,
                backgroundColor: "#C73729",
                data: neg,
            }
        ]
       },
       options: {
 maintainAspectRatio: false,
            barValueSpacing: 20,
            scales: {
                xAxes: [{
                  barPercentage: 0.4,
                    ticks: {
                        min: 0,
                    },
                    scaleLabel: {
                display: true,
             labelString: 'probability'
            }
                }]
            }
        }
    } 
    


     window.canvas_WordImpressionGraph = document.getElementById('WordImpressionGraph');
     window.myChart_WordImpressionGraph = new Chart(window.canvas_WordImpressionGraph, chartData);

    window.canvas_WordImpressionGraph.onclick = function(evt) {
       var activePoint = window.myChart_WordImpressionGraph.getElementAtEvent(evt)[0];
       var data = activePoint._chart.data;
       var datasetIndex = activePoint._datasetIndex;
       var mainlabel=activePoint._model['label'];
       var label = data.datasets[datasetIndex].label;
       var value = data.datasets[datasetIndex].data[activePoint._index];
       var message = [];
       message.push(mainlabel)
       message.push(label)
       // console.log(message)
       WordImpressionGraphLoad(message.toString(),language,venueDetailData)

    };

    $("#WordImpressionGraph_error").html('');
    $("#WordImpressionGraph").show()
    var height = label.length * 38;
  $("#WordImpressionGraph_container").css('height',height)

  window.addEventListener('resize', function () {
               window.myChart_WordImpressionGraph.resize()
            })

        } else { // incase the graph is existent then updating

  var height = label.length * 38;
  $("#WordImpressionGraph_container").css('height',height)

  window.addEventListener('resize', function () {
               window.myChart_WordImpressionGraph.resize()
            })

          window.myChart_WordImpressionGraph.data.labels = [];
          label.forEach(element => {
          window.myChart_WordImpressionGraph.data.labels.push(element);
          })

          window.myChart_WordImpressionGraph.data.datasets.forEach((dataset,index) => {
              if(index == 0) {
                  dataset.data = pos;
              } else if(index == 1){
                  dataset.data = neg;
              }
            });
          console.log(window.myChart_WordImpressionGraph)

            window.canvas_WordImpressionGraph.onclick = function(evt) {
             var activePoint = window.myChart_WordImpressionGraph.getElementAtEvent(evt)[0];
             var data = activePoint._chart.data;
             var datasetIndex = activePoint._datasetIndex;
             var mainlabel=activePoint._model['label'];
             var label = data.datasets[datasetIndex].label;
             var value = data.datasets[datasetIndex].data[activePoint._index];
             var message = [];
             message.push(mainlabel)
             message.push(label)
             WordImpressionGraphLoad(message.toString(),language,venueDetailData)
            
          };
    window.myChart_WordImpressionGraph.update();
  
    $("#WordImpressionGraph_error").html('');
    $("#WordImpressionGraph").show()
        }

    } else {

      // console.log('no data man')
      if (window.myChart_WordImpressionGraph) {
      window.myChart_WordImpressionGraph.data.labels = [];
      window.myChart_WordImpressionGraph.data.datasets.forEach((dataset) => {
           $.each(dataset.data,function(index2,element2){
                 delete(dataset.data[index2]);
                })
      });
      window.myChart_WordImpressionGraph.update();
      }
        console.log('numma evidem ethum')
        console.log(window.myChart_WordImpressionGraph)

    $("#WordImpressionGraph_error").html(dataMessage);
   $("#WordImpressionGraph").hide()

    }


} // end of redrawImpression