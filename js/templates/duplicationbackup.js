
//declaring global variable for differentiating between graphs	============================================================

// a variable used for giving dynamic ids to different graph boxes
	 var duplicationData={'donutChart':{'dates':[]},         
                         'lineChart' : {'dates': []},
                         'negposChart':{'dates':[]},
                         'starChart':{'dates':[]}
                                          };
       duplicationData=$.parseJSON(JSON.stringify(duplicationData));
   var user_id;
 	 $.ajax({
        url: 'api/profile_curl.php',
        type: "POST",
        data:{"action":"getSession"}, 
        success: function(userData)
        {
         
         var userData=$.parseJSON(userData);
        	user_id=userData.user_id;
        },async:false
    });
   $.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}

var venueid=$.urlParam('id'); 


//================================global variables end===================================================================
function StartFunction(chartid)
{
    $("#loading").fadeIn();

	setTimeout(
			    function() {
    switch(chartid)
    {						     
	case 'curve_chart': LinechartDuplication(chartid);
    						break;
    							     
    case 'bar-chart':     NegPoschartDuplication(chartid);
    							break;
    
    case 'chart_div1': StarChartDuplication(chartid);
    							break;
    
    case 'sales-chart': DonutChartDuplication(chartid);
    							break;
    }

			    }, 400);

}

var l=0;
function DonutChartDuplication(chartid)
{
  
	l++;
	var id= chartid+l;
	console.log(id);
	$(`<div  class="col-lg-6 col-md-6 connectedSortable">
    <div class="box" id="entire`+id+`">
      <div class="box-header with-border">
        <h3 id="Services" class="box-title tservices">Services</h3>
        <div class="box-tools pull-right">
   <button type="button" onclick="StartFunction('sales-chart');" class="btn btn-box-tool"><i class="fas fa-copy"></i>
    </button>
		<button type="button" class="btn btn-box-tool"  onclick="collapser('`+id+`');" data-widget="collapse"
         data-toggle="tooltip" title="Collapse"><i class="fas fa-minus"></i></button>
<button type="button" class="btn btn-box-tool"  onclick="closer('`+id+`');" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fas fa-window-close"></i></button>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-body" id="box`+id+`">
        
                  <div class="row">
           <div class="col-sm-7">
          <p class="top15"><span class="bold">Selected Date Range :</span>
            <span id="DateRange`+id+`" style="padding-left: 5px;">2017-12-12 to 2018-04-12</span></p>
          </div>
        <div class="col-sm-5">
        <input type="text" name="daterange" id="date`+id+`"  class="form-control">
        </div>
    	</div>
        <div class="row">
         <div class="col-lg-12 col-md-12 venue-details">
          <div class="row m-t-25">
            <div class="col-md-6">
             <div class="box-body">
              <div id="clearDonut`+id+`" class="box-body chart-responsive">
                <div class="chart" id="`+id+`" style="min-height: 300px;"></div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="row venue-value-box-`+id+`"> <!--dynamic data coming here-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>`).insertAfter("#donutAnchor")

 $('.connectedSortable').sortable({
    placeholder         : 'sort-highlight',
    connectWith         : '.connectedSortable',
    handle              : '.box-header, .nav-tabs',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });
  $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');

var dateId="#date"+id;

var dates=$(dateId).val();
  duplicationData.donutChart.dates.push(dates);

  localStorage.removeItem("duplicationData");
  localStorage.setItem("duplicationData",JSON.stringify(duplicationData));
$(dateId).daterangepicker({
      dateLimit:  {
        "days": 182
    },
  locale: {
      format: 'YYYY/MM/DD'
    }

    }).on('apply.daterangepicker',function(ev,picker){

    		var id = $(this).attr('id');
    		var dates=picker.element[0].value;
    	
    		CUSTOMWORKINGSTART(dates,id,'sales-chart');

    });

    	DonutDraw(id)

}

function DonutDraw(id)
{
	console.log(id)
   var datesData=$.parseJSON(getDates());
   var dateRange =  datesData.start+" "+"to"+" "+datesData.current;
   var dateRangeId= "#DateRange"+id;
   $(dateRangeId).html(dateRange);

	var venueDetails = $.parseJSON(localStorage.getItem("venueDetails"));

	var venueDetailData= localStorage.getItem("venueDetailData");
          localStorage.removeItem("DonutChartData"+id);
          localStorage.setItem("DonutChartData"+id,venueDetailData);

          DrawDonutChart(id,venueDetailData);
}

function DrawDonutChart(id,venueDetailData)
{

	var tis="#"+id;
	console.log(tis)	
   $(tis).empty();

var venueD= $.parseJSON(venueDetailData);
 var serviceData = $.parseJSON(localStorage.getItem("servicesjson"));
 var j=0;
 var donut_slice=[];
 var total=0;
 var color;
 var percent;
     $.each(venueD.result.services,function(index,element){
        total=total+element.reviews.length;
        });

    $.each(venueD.result.services,function(index,element){
            $.each(serviceData.services,function(index2,element2){

                if(element.serviceId===element2.id)
                      {
                        percent= ((element.reviews.length*100)/total).toFixed(2);;
                        switch(element2.domainKey)
                        {
                          case 'tripadvisor' : color='#2973A1';
                                               break;
                          case 'expedia'     : color='#FABA2E';
                                               break;
                          case 'yelp'      : color='#78C9DB';
                                                break;
                          case 'booking'      : color='#6E388A';
                                                break;                      
                          case 'voyagessncf'      : color='#63BFB2';
                                                break;
                          case 'facebook'      : color='#C93621';
                                                break;
                          case 'googleplus'      : color='#2973A1';
                                                break;
                          case 'foursquare'      : color='#FABA2E';
                                                break;
                          case 'ctrip'      : color='#78C9DB';
                                                break;
                          case 'getyourguide'      : color='#6E388A';
                                                break;
                           case 'pagesjaunes'      : color='#63BFB2';
                                                break;
                          case 'viatorcom'      : color='#C93621';
                                                break;
                          case 'ebookers'      : color='#2973A1';
                                                break;
                          case 'camping2be'      : color='#FABA2E';
                                                break;   
                          case 'thefork'      : color='#78C9DB';
                                                break;  
                          case 'tophotels'      : color='#6E388A';
                                                break;
                          case 'zoover'      : color='#63BFB2';
                                                break;                   
                        }
                            donut_slice[j]={'label':element2.domainKey,'value':percent,'color':color};
                      }

            });
            j++;
    });
  window.donutChart = Morris.Donut({
  element: id,
  data: donut_slice,
  resize: true,
  redraw: true,
  animationEnabled: true


	}).
  on('click', function (i, row) {  

    morrisHan(i,row.label,venueDetailData);
});
  console.log(donut_slice)

var classname=".venue-value-box-"+id; 

$(classname).html('');

  $.each(donut_slice,function(index,element){
    $(classname).append(`<div onClick="morrisHan(`+index+`,'`+element.label+`');" class="col-sm-8">
                      <div class="info-box" style="background-color:`+element.color+` !important">
                     <div class="info-box-content text-center" style="margin-left: 0px !important;">
                      <span class="info-box-text">`+element.label+`</span>
                      <span class="info-box-number">`+element.value+`%</span>
                      <div class="progress">
                        <div class="progress-bar" style="width: `+element.value+`%"></div>
                      </div>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                </div>`);

  });

$("#loading").fadeOut();
}

var i=0; 
function LinechartDuplication(chartid)
{
	i++;
	var id= chartid+i;
console.log(id);
	$(`<div  class="col-lg-6 col-md-6 connectedSortable">
  <div class="box box-info" id="entire`+id+`">
    <div class="box-header with-border">
      <h3 class="box-title tevolution">Evolution of positive opinion</h3>
      <div class="box-tools pull-right">
      	<button type="button" onclick="StartFunction('curve_chart');" class="btn btn-box-tool"><i class="fas fa-copy"></i>
		</button>
        <button type="button" class="btn btn-box-tool"  onclick="collapser('`+id+`');" data-widget="collapse"
         data-toggle="tooltip" title="Collapse"><i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-box-tool"  onclick="closer('`+id+`');" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fas fa-window-close"></i></button>
      </div>
    </div>
    <div class="box-body " id="box`+id+`">
      <div class="row">
           <div class="col-sm-7">
      
          <p class="top15"><span class="bold">Selected Date Range :</span>
            <span id="DateRange`+id+`" style="padding-left: 5px;">2017-12-12 to 2018-04-12</span></p>
          </div>
        <div class="col-sm-5">
        <input type="text" name="daterange" id="date`+id+`"  class="form-control">
        </div>
    </div>
     <div class="chart">
      <!-- Sales Chart Canvas -->
      <div id="`+id+`" style=" min-height: 500px"></div>
    </div>
    <!-- /.chart-responsive -->

    <!-- /.row -->
  </div>
</div>
</div>`).insertAfter( '#linechartAnchor');

	 $('.connectedSortable').sortable({
    placeholder         : 'sort-highlight',
    connectWith         : '.connectedSortable',
    handle              : '.box-header, .nav-tabs',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });
  $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');

var dateId="#date"+id;
var dates=$(dateId).val();
  duplicationData.lineChart.dates.push(dates);

  localStorage.removeItem("duplicationData");
  localStorage.setItem("duplicationData",JSON.stringify(duplicationData));

$(dateId).daterangepicker({
      dateLimit:  {
        "days": 182
    },
  locale: {
      format: 'YYYY/MM/DD'
    }

    }).on('apply.daterangepicker',function(ev,picker){

    		var id = $(this).attr('id');
    		var dates=picker.element[0].value;
    	
    		CUSTOMWORKINGSTART(dates,id,'linechart');

    });


  loadLineGraph(id);

}

function collapser(id)
{

var sliderId= "#box"+id;
$(sliderId).toggle(300);

}

function closer(id) 
{
	var closeId= "#entire"+id;
	$(closeId).toggle();

}


function loadLineGraph(id)
{
   
var datesData=$.parseJSON(getDates());
   var dateRange =  datesData.start+" "+"to"+" "+datesData.current;
   var dateRangeId= "#DateRange"+id;
   $(dateRangeId).html(dateRange);

var venueDetails = $.parseJSON(localStorage.getItem("venueDetails"));

var venueDetailData= localStorage.getItem("venueDetailData");
          localStorage.removeItem("lineData"+id);
          localStorage.setItem("lineData"+id,venueDetailData);
          linechartDraw(id,venueDetailData);

}


function linechartDraw(id,curvedata)
{

 var months=[];
var obj= $.parseJSON(curvedata);
var i=0;
var positive=[];
var negative=[];
 $.each(obj.result.stats,function(index,element){
    months[i]=element.yearMonth;
    var total = parseInt(element.venuePositiveNotesCount+element.venueNegativeNotesCount)
    console.log("================"+total)
    console.log("==========="+element.venuePositiveNotesCount)
    positive[i]=(element.venuePositiveNotesCount/total)*100;
    negative[i]=(element.venueNegativeNotesCount/total)*100;
     i++;
    });
var months_overall =['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

var j=0;
var new_months=[];
$.each(months,function(index,element){
 new_months[j] = months_overall[+element.split('-')[1] - 1];
    j++;
});

var chartData=[];
  chartData.push(['Months', 'Positives','Negatives']);

  for(var i=0;i<new_months.length;i++)
  {
      var pair=[];
      pair.push(new_months[i]);
      pair.push(positive[i]);
      pair.push(negative[i])
      chartData.push(pair);
  }


console.log(chartData)
   google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable(chartData);

        var options = {

            animation: {
          duration: 1000,
          easing: 'out',
          startup: true
          },
          title: 'Percentage of Negative vs positive ',
          curveType: 'function',
          legend: { position: 'bottom' },
          vAxis: { 
      gridlines: {color: '#333'},
    viewWindowMode:'explicit',
    viewWindow: {
        max:100,
        min:0
                }
              }
          
        };

        var chart = new google.visualization.LineChart(document.getElementById(id));

         google.visualization.events.addListener(chart, 'select', selectHandler);    

        function selectHandler() {
      var selectedItem = chart.getSelection()[0];
      if (selectedItem) {
      var month = data.getValue(selectedItem.row,0);
        positiveOpinionReviewLoader(month,curvedata);
      }
    
    }

        chart.draw(data, options);
      }
      $("#loading").fadeOut();

}





var j=0;
 function NegPoschartDuplication(chartid)
 {
 	j++;
	var id= chartid+j;
	console.log(id)
	$("#loading").fadeOut();

	$(`<div class="col-lg-6 col-md-6 connectedSortable" >
  <div class="box" id="entire`+id+`">
    <div class="box-header with-border">
      <h3 class="box-title tnegvspos">Negatives vs Positives</h3>
      <div class="box-tools pull-right">
      <button type="button" onclick="StartFunction('bar-chart');" class="btn btn-box-tool"><i class="fas fa-copy"></i>
		</button>       
		 <button type="button" class="btn btn-box-tool" onclick="collapser('`+id+`');" data-widget="collapse"><i class="fa fa-minus"></i></button>
	   <button type="button" class="btn btn-box-tool"  onclick="closer('`+id+`');" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fas fa-window-close"></i></button>
      </div>
    </div>
    <div class="box-body" id="box`+id+`">
          <div class="row">
           <div class="col-sm-7">
      
          <p class="top15"><span class="bold">Selected Date Range :</span>
            <span id="DateRange`+id+`" style="padding-left: 5px;">2017-12-12 to 2018-04-12</span></p>
          </div>
        <div class="col-sm-5">
        <input type="text" name="daterange" id="date`+id+`"  class="form-control">
        </div>
    </div>
     <div id="`+id+`" ></div>
   </div>
   <!-- /.box-body -->
 </div>
</div>`).insertAfter('#barAnchor');

		 $('.connectedSortable').sortable({
    placeholder         : 'sort-highlight',
    connectWith         : '.connectedSortable',
    handle              : '.box-header, .nav-tabs',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });
  $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');

var dateId="#date"+id;
var dates=$(dateId).val();
  duplicationData.donutChart.dates.push(dates);

  localStorage.removeItem("duplicationData");
  localStorage.setItem("duplicationData",JSON.stringify(duplicationData));

$(dateId).daterangepicker({
      dateLimit:  {
        "days": 182
    },
  locale: {
      format: 'YYYY/MM/DD'
    }

    }).on('apply.daterangepicker',function(ev,picker){

    		var id = $(this).attr('id');
    		var dates=picker.element[0].value;
    	
    		CUSTOMWORKINGSTART(dates,id,'bar-chart');

    });

    loadNegPosGraph(id);

 }

 	function loadNegPosGraph(chartid)
 	{	
 		var id = chartid;
 		var datesData=$.parseJSON(getDates());
   		var dateRange =  datesData.start+" "+"to"+" "+datesData.current;
   		var dateRangeId= "#DateRange"+id;
   		$(dateRangeId).html(dateRange);

		var venueDetails = $.parseJSON(localStorage.getItem("venueDetails"));

		var venueDetailData= localStorage.getItem("venueDetailData");
          localStorage.removeItem("NegPosbarData"+id);
          localStorage.setItem("NegPosbarData"+id,venueDetailData);
          NegPosDraw(id,venueDetailData);

 	}

function NegPosDraw(id,venueDetailData)
{
	console.log(id)
var NPdata= getNPdata(venueDetailData);
  google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable(NPdata);

        var options = {
          animation: {
          duration: 1000,
          easing: 'out',
          startup: true
          },
          chart: {
            title: 'Negatives Vs Positives',
            subtitle: 'Number of Negatives and Positives',
          },
          bars: 'vertical',
          vAxis: {format: 'decimal'},
          height: 400,
          colors: ['#67BFB3', '#C73729']
        };

        var chart = new google.charts.Bar(document.getElementById(id));

        google.visualization.events.addListener(chart, 'select', selectHandler);    

        function selectHandler() {
            var selection = chart.getSelection();
            var message = '';
                      for (var i = 0; i < selection.length; i++) {
                                var item = selection[i];
                                if (item.row != null && item.column != null) {
                                var str = data.getFormattedValue(item.row, item.column);
                                var category = data
                                .getValue(chart.getSelection()[0].row, 0)
                                var type
                                if (item.column == 1) {
                                type = "positive";
                                } else if(item.column == 2){
                                type = "negative";
                                }

                                message += str+','+ category
                                + ',' + type;
                                } else if (item.row != null) {
                                var str = data.getFormattedValue(item.row, 0);
                                message += '{row:' + item.row
                                + ', column:none}; value (col 0) = ' + str
                                + '  The Category is:' + category;
                                } else if (item.column != null) {
                                var str = data.getFormattedValue(0, item.column);
                                message += '{row:none, column:' + item.column
                                + '}; value (row 0) = ' + str
                                + '  The Category is:' + category;
                                }
                                }
                                if (message == '') {
                                message = 'nothing';
                                }
                                  Load_DuplicationRData(message,venueDetailData);
                                }

        chart.draw(data, google.charts.Bar.convertOptions(options));

      }
}

var k=0;
function  StarChartDuplication(chartid)
{
	k++;
	var id = chartid+k;
	$(`<div  class="col-lg-6 col-md-6 connectedSortable">
  <div class="box" id="entire`+id+`">
    <div class="box-header with-border">
      <h3 class="box-title tstarchart">Star Chart</h3>
      <div class="box-tools pull-right">
     <button type="button" onclick="StartFunction('chart_div1');" class="btn btn-box-tool"><i class="fas fa-copy"></i>
    </button>
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
   <button type="button" class="btn btn-box-tool"  onclick="closer('`+id+`');" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fas fa-window-close"></i></button>
      </div>
    </div>
    <div class="box-body" id="box`+id+`">
              <div class="row">
           <div class="col-sm-7">
      
          <p class="top15"><span class="bold">Selected Date Range :</span>
            <span id="DateRange`+id+`" style="padding-left: 5px;">2017-12-12 to 2018-04-12</span></p>
          </div>
        <div class="col-sm-5">
        <input type="text" name="daterange" id="date`+id+`"  class="form-control">
        </div>
    </div>
      <div id="`+id+`" style="height: 500px;"></div>
    </div>
  </div>
</div>`).insertAfter('#starAnchor');

	 $('.connectedSortable').sortable({
    placeholder         : 'sort-highlight',
    connectWith         : '.connectedSortable',
    handle              : '.box-header, .nav-tabs',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });
  $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');

var dateId="#date"+id;
var dates=$(dateId).val();
  duplicationData.donutChart.dates.push(dates);

  localStorage.removeItem("duplicationData");
  localStorage.setItem("duplicationData",JSON.stringify(duplicationData));
$(dateId).daterangepicker({
      dateLimit:  {
        "days": 182
    },
  locale: {
      format: 'YYYY/MM/DD'
    }

    }).on('apply.daterangepicker',function(ev,picker){

    		var id = $(this).attr('id');
    		var dates=picker.element[0].value;
    	
    		CUSTOMWORKINGSTART(dates,id,'starBarChart');

    });


  loadStarGraph(id);

}

function loadStarGraph(id)
{
		console.log(id);
 		var datesData=$.parseJSON(getDates());
   		var dateRange =  datesData.start+" "+"to"+" "+datesData.current;
   		var dateRangeId= "#DateRange"+id;
   		$(dateRangeId).html(dateRange);

		var venueDetails = $.parseJSON(localStorage.getItem("venueDetails"));

		var venueDetailData= localStorage.getItem("venueDetailData");
          localStorage.removeItem("StarbarChart"+id);
          localStorage.setItem("StarbarChart"+id,venueDetailData);
          StarBarDraw(id,venueDetailData);
}

function StarBarDraw(id,venueDetailData)
{
	console.log(id)
var usingVenueData= $.parseJSON(venueDetailData);
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);
function drawBasic() {
      var data = google.visualization.arrayToDataTable([
        ['Stars', 'Number of Notes',],
        ['5(★)', usingVenueData.result.venueNotes5Count],
        ['4(★)', usingVenueData.result.venueNotes4Count],
        ['3(★)', usingVenueData.result.venueNotes3Count],
        ['2(★)', usingVenueData.result.venueNotes2Count],
        ['1(★)', usingVenueData.result.venueNotes1Count]
      ]);
      var options = {
                    animation: {
          duration: 1000,
          easing: 'out',
          startup: true
          },
           colors: ['#67BFB3'],
         title: 'Number of Notes for past five months',
        chartArea: {width: '45%'},
        hAxis: {
          title: 'Number of Notes',
          minValue: 0
        },
        vAxis: {
          textStyle:{color: '#FABA2E', fontSize: 14,}
        }
      };
      var chart = new google.visualization.BarChart(document.getElementById(id));

       google.visualization.events.addListener(chart, 'select', selectHandler);    
function selectHandler() {
    var selectedItem = chart.getSelection()[0];
    if (selectedItem) {
      var value = data.getValue(selectedItem.row,0);
      Load_DuplicationStar(value,venueDetailData);
    }
  }

      chart.draw(data, options);
    }
$("#loading").fadeOut();
}

//=====================================================Custom Chart handlind==============================================//


function CUSTOMWORKINGSTART(dates,id,charttype)
{
var range_operation = dates;
range_operation = range_operation.split('-')
console.log(range_operation)

let startDate = range_operation[0]
let endDate = range_operation[1]

startDate = startDate.split('/')
startDate = startDate[0].trim()+'-'+startDate[1].trim()+'-'+startDate[2].trim()

endDate = endDate.split('/')
endDate = endDate[0].trim()+'-'+endDate[1].trim()+'-'+endDate[2].trim()
Cusdates={"startDate":startDate,"endDate":endDate};


LoadCustomData_ForGraphStarting(dates,Cusdates,id,charttype);

}

function LoadCustomData_ForGraphStarting(dates,Cusdates,id,charttype)
{
	    $("#loading").fadeIn();
  setTimeout(
    function() {
      LoadCustomData_ForGraph(dates,Cusdates,id,charttype);
      
    }, 400);

}



  function LoadCustomData_ForGraph(dates,Cusdates,id,charttype)
 {
 	console.log(Cusdates)
 	
 	console.log(charttype)
	var id= id.replace(/date/gi, "");
	console.log(id)
  $.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}


 var dateRange =  Cusdates.startDate+" "+"to"+" "+Cusdates.endDate;
   var dateRangeId= "#DateRange"+id;
   $(dateRangeId).html(dateRange);

 var user_id;
   $.ajax({
        url: 'api/profile_curl.php',
        type: "POST",
        data:{"action":"getSession"}, 
        success: function(userData)
        {
         
         var userData=$.parseJSON(userData);
          user_id=userData.user_id;
        },async:false
    });
$.ajax({
        url: 'api/venue_details_curl.php',
        type: "POST",
        data:{
    "action": "venueDetails",
    "userId": user_id,
    "venueId": venueid
    }, 
        success: function(venueDetails)
        {
          console.log(venueDetails);
          localStorage.removeItem("venueDetails");
          localStorage.setItem("venueDetails",venueDetails);
        },
        async:false
    });
		var venueDetails = $.parseJSON(localStorage.getItem("venueDetails"));

		 $.ajax({
        url: 'api/venue_details_curl.php',
        type: "POST",
        data:{
    "action": "venueData",
    "userId": user_id,
    "venueId": venueid,
    "venueVersion": venueDetails.venueVersion,
    "checkVersion": false,
    "startDate": Cusdates.startDate,
    "endDate": Cusdates.endDate,
}, 
        success: function(venueDetailData)
        {
        	switch(charttype)
        	{
        		case 'linechart' : localStorage.removeItem("lineData"+id);
                 						   localStorage.setItem("lineData"+id,venueDetailData);
                                duplicationData.lineChart.dates.push(dates);
                                localStorage.removeItem("duplicationData");
                                localStorage.setItem("duplicationData",JSON.stringify(duplicationData));

                 						   break;

        		case 'bar-chart' : localStorage.removeItem("NegPosbarData"+id);
                						   localStorage.setItem("NegPosbarData"+id,venueDetailData);

                                duplicationData.negposChart.dates.push(dates);
                                localStorage.removeItem("duplicationData");
                                localStorage.setItem("duplicationData",JSON.stringify(duplicationData));
                						   break;
        		case 'starBarChart' : 
                                  localStorage.removeItem("starBarChart"+id);
                  							  localStorage.setItem("starBarChart"+id,venueDetailData);
                  							  
                                duplicationData.starChart.dates.push(dates);
                                localStorage.removeItem("duplicationData");
                                localStorage.setItem("duplicationData",JSON.stringify(duplicationData));

                                  break;
        		
        		case 'sales-chart' : localStorage.removeItem("DonutChartData"+id);
                  							 localStorage.setItem("DonutChartData"+id,venueDetailData);
                  							   console.log("==================dates"+dates)
                                duplicationData.donutChart.dates.push(dates);
                                localStorage.removeItem("duplicationData");
                                localStorage.setItem("duplicationData",JSON.stringify(duplicationData));
                                 break;
        	}
	      
        },
        async:false
    });


   var dateRange =  Cusdates.startDate+" "+"to"+" "+Cusdates.endDate;


  		switch(charttype)
  		{
  			case 'linechart' : 		var curvedata = localStorage.getItem("lineData"+id);
            									linechartDraw(id,curvedata)
            									break;

  			case 'bar-chart' :      var bardata = localStorage.getItem("NegPosbarData"+id);
              									NegPosDraw(id,bardata)
              									break;
  			
  			case 'starBarChart' : var starData = localStorage.getItem("starBarChart"+id);
            									StarBarDraw(id,starData);
            									break;
            			
  			case 'sales-chart' : var donutDat= localStorage.getItem("DonutChartData"+id);
            									DrawDonutChart(id,donutDat);
            									break;

  		}
  									$("#loading").fadeOut();

 }