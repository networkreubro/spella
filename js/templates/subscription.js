var lang = localStorage.getItem("lang");
// var lang = "FR";

$(document).ready(function() {
  if (lang == "FR") {
    $("#language").val("FR");
  } else if (lang == "EN") {
    $("#language").val("EN");
  }
});
$(function() {
  var user_subscribed_product = $("#user_subscribed_plan")
    .val()
    .trim();
  var plan_expiry_date = $("#plan_expiry")
    .text()
    .trim();
  var planexpiry_status = 0;
  //autorenew hiding part
  // if (!user_subscribed_product && user_subscribed_product == "") {
  //   $("#div_autorenew").hide();
  // } else {
  if (plan_expiry_date) {
    var date = plan_expiry_date.substring(0, 2);
    var month = plan_expiry_date.substring(3, 5);
    var year = plan_expiry_date.substring(6, 10);

    var dateToCompare = new Date(year, month - 1, date);
    var currentDate = new Date();

    if (currentDate < dateToCompare) {
      planexpiry_status = 1;
    }
  }
  $("#plan_name").text("");

  //getting values to fill product list
  $.ajax({
    url: "api/products_curl.php",
    type: "POST",
    data: { action: "listProducts" },
    success: function(data) {
      var obj = $.parseJSON(data);

      var productListData = "";
      let description,
        btnText,
        style = "";
      // static elite product
      if (lang === "EN") {
        description =
          "You are a group, and/or you own several venues that you need to track separately and/or together, you are an administration in charge of an area…<br/>We have dedicated offers for you.<br/>Contact us of a customised offer.";
        btnText = "Contact Us";
      } else {
        style = "height:auto;white-space: pre-wrap;";
        description =
          "Vous êtes un groupe et/ou disposez de plusieurs établissements que vous souhaitez suivre indépendamment et/ou conjointement, vous êtes un institutionnel en charge d’un territoire…<br/>Nous vous offrons des conditions particulières.<br/>Contactez-nous pour une offre personnalisée.";
        btnText = "Nous contacter";
      }

      let productElite = `<div class="col-lg-3 col-md-6 col-sm-12 m-t-25">
          <div class="product-info">
            <div class="whitebox-title p-4 text-center">
              <div class="title">
                <h3>Platinum</h3>
              </div>
              <div class="btn mt-4">
                <a class="btn btn-app bg-purple" style="${style}" onclick="redirectPlans('elite')">${btnText}</a>
              </div>
              <div class="info">
                <p class="mb-0">${description}</p>
              </div>
            </div>
          </div>
        </div>`;

      obj.result.forEach(function(item) {
        if (item["active"]) {
          var description;
          var id = item["id"].trim();

          var name;
          var price;
          var productData = "";
          var buttonText = "";
          var amount = "";

          switch (lang) {
            case "EN":
              description = item["description-en"];
              name = item["name-en"];
              price = "$" + item["price-USD"];
              buttonText = "Subscribe";
              amount = item["price-USD"];

              break;
            case "FR":
              description = item["description-fr"];
              name = item["name-fr"];
              price = "€" + item["price-EUR"];
              buttonText = "S'abonner";
              amount = item["price-EUR"];

              break;
            default:
              description = item["description-en"];
              name = item["name-en"];
              price = "$" + item["price-USD"];
          }

          if (user_subscribed_product) {
            if (id == user_subscribed_product) {
              $("#plan_name").text(name);
            }
          }

          productData = '<div class="col-lg-3 col-md-6 col-sm-12 m-t-25">';
          productData += '<div class="product-info">';
          productData += '<div class="whitebox-title p-4 text-center">';
          productData += '<div class="title">';
          productData += "<h3>" + name + "</h3>";
          productData += "</div>";
          productData += '<div class="price-view lead mt-3">';
          productData += '<span class="bold red">' + price + "</span>";
          productData += "</div>";
          if (planexpiry_status == 0) {
            productData += '<div class="btn mt-4">';
            productData +=
              `<a class="btn btn-app bg-purple"  onclick="purchase('` +
              id +
              `');" >` +
              buttonText +
              `</a>`;
            productData += "</div>";
          }
          productData += '<div class="info">';
          productData += '<p class="mb-0">' + description + "</p>";
          productData += "</div>";
          productData += "</div>";
          productData += "</div>";
          productData += "</div>";
          productListData += productData;
        }
      });

      $("#productList").append(productListData);
      $("#productList").append(productElite);

      $("#loading").fadeOut();
    }
  });
});

function purchase(Id) {
  $("#loading").show();

  $.ajax({
    url: "api/sca_session_creator.php",
    type: "POST",
    data: {
      productId: Id,
      authscreate: "spSCSESS345xx",
      currency: localStorage["lang"]
    },
    success: function(data) {
      var session_id;
      if (data != "nil") {
        session_id = data.trim();
        const stripe = Stripe("pk_live_8v0kB9r83On7lEZCT07K5Yzs00JDTzC1ju");
        stripe
          .redirectToCheckout({
            // Make the id field from the Checkout Session creation API response
            // available to this file, so you can provide it as parameter here
            // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
            sessionId: session_id
          })
          .then(result => {
            // If `redirectToCheckout` fails due to a browser or network
            // error, display the localized error message to your customer
            // using `result.error.message`.
          });
      }
    }
  });
}
$("#autorenew_checkbox").change(function() {
  var userid = $("#userid").val();
  $.ajax({
    url: "api/index_curl.php",
    type: "POST",
    data: {
      action: "updateUser",
      email: $("#userEmail").val(),
      autorenew: this.checked
    },
    success: function(data) {
      // data = JSON.parse(data);
      var obj = $.parseJSON(data);
      if (obj.status == "success") {
        alert("Status Changed Successfully!");
        $.ajax({
          url: "index.php",
          type: "POST",
          data: obj,
          success: function(data) {
            localStorage["pricingStatus"] = obj.result.status;
            localStorage.setItem(
              "serviceslist",
              JSON.stringify(obj.result.services)
            );
          },
          error: function(data) {
            alert("Server Error");
          }
        });
      } else {
        alert("Failed to update status");
      }
    }
  });
});

function download(Id) {
  $("#loading").show();

  $.ajax({
    url: "api/sca_session_creator.php",
    type: "POST",
    data: { productId: Id, authscreate: "spSCSESS345xx" },
    success: function(data) {
      var session_id;

      if (data != "nil") {
        session_id = data.trim();
        const stripe = Stripe("pk_live_8v0kB9r83On7lEZCT07K5Yzs00JDTzC1ju");
        stripe
          .redirectToCheckout({
            // Make the id field from the Checkout Session creation API response
            // available to this file, so you can provide it as parameter here
            // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
            sessionId: session_id
          })
          .then(result => {
            // If `redirectToCheckout` fails due to a browser or network
            // error, display the localized error message to your customer
            // using `result.error.message`.
          });
      }
    }
  });
}

function viewDetailedInvoice(invoiceId) {
  $.ajax({
    type: "POST",
    url: "api/invoice.php",
    data: {
      action: "getInvoiceDetails",
      invoice_id: invoiceId
    },
    dataType: "JSON",
    success: function(response) {
      data = response.result;
      html = `
      <p>Amount : ${data.amount} ${data.currency}</p>
      <p>VAT : ${data.VATFactor}%</p>
      <p>VAT Amount : ${data.VATAmount} ${data.currency}</p>
      <p>Total :  ${data.amount + data.VATAmount} ${data.currency}</p>
      <p>Purchased On: ${data.dateOfPurchase}</p>
      <p>Expires on: ${data.dateOfExpiration}</p>
      <p>Transaction ID: ${data.paymentIntent}</p>
      <a href="${data.invoicePDFURL}" download>
      <button class="btn btn-sm btn-link pull-right"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>&nbspDownload Invoice</button>
    </a>
      `;
      $("#invoice_body").html(html);
      $("#invoiceModal").modal();
    }
  });
}

function redirectPlans(plan) {
  if (plan == "trial") {
    window.location = "newuser.php";
  } else {
    window.location = "https://spella.com/contact/";
  }
}
