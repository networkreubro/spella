
var dataMessage = "";
 $(function(){    



 $('.connectedSortable').sortable({
    placeholder         : 'sort-highlight',
    connectWith         : '.connectedSortable',
    handle              : '.box-header, .nav-tabs',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });
  $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');


var lang = localStorage.getItem("lang").toLowerCase();

switch(lang){

  case 'fr' : $("#datepicker1").attr("placeholder","date de début");
              $("#datepicker2").attr("placeholder","date de fin");
              dataMessage = "<b>Pas encore de données</b>";
               break; 
  case 'en' : $("#datepicker1").attr("placeholder","Start Date");
              $("#datepicker2").attr("placeholder","End Date");
              dataMessage="<b>No Data Yet</b>"
             break;
  default:  $("#datepicker1").attr("placeholder","date de début");
            $("#datepicker2").attr("placeholder","date de fin");
            dataMessage="<b>No Data Yet</b>"
            break;
}
  

$('#datepicker1').datepicker({
autoclose: true,
format: 'yyyy/mm/dd',
endDate: 'today'
});

$('#datepicker2').datepicker({
autoclose: true,
format: 'yyyy/mm/dd',
endDate: 'today'
});


// graph redraw after window resize for google.
$(window).resize(function() {
    if(this.resizeTO) clearTimeout(this.resizeTO);
    this.resizeTO = setTimeout(function() {
        $(this).trigger('resizeEnd');
    }, 500);
});





var page=location.pathname.substring(1);
if(page==='spella/venue.php'||page==='venue.php')
{
  // $("#servAnchor").show();
  $("#negAnchor").show();
  $("#NotAnchor").show();
  $("#revAnchor").show();
  $("#texAnchor").show();
}

   $.ajax({
        url: 'api/venue_details_curl.php',
        type: "POST",
        data:{"action":"getServices"}, 
        success: function(services)
        {
          localStorage.setItem("servicesjson",services);
        },async:false
    });
 var user_id;
   $.ajax({
        url: 'api/profile_curl.php',
        type: "POST",
        data:{"action":"getSession"}, 
        success: function(userData)
        {
         
         var userData=$.parseJSON(userData);
          user_id=userData.user_id;
        },async:false
    });

$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}

var venueid=$.urlParam('id');    
if(localStorage.getItem("venueDates"+venueid))
{
  datesData = $.parseJSON(localStorage.getItem("venueDates"+venueid));
}
else{
  var datesData=$.parseJSON(getDates());

}

// //console.log(datesData)
   var dateRange =  datesData.start+" "+"to"+" "+datesData.current;
  $("#DateRange").html(dateRange);
   $.ajax({
        url: 'api/venue_details_curl.php',
        type: "POST",
        data:{
    "action": "venueDetails",
    "userId": user_id,
    "venueId": venueid
    }, 
        success: function(venueDetails)
        {
          //console.log(venueDetails);
          localStorage.removeItem("venueDetails"+venueid);
          localStorage.setItem("venueDetails"+venueid,venueDetails);
          localStorage.removeItem("venueDetailsDup"+venueid);
          localStorage.setItem("venueDetailsDup"+venueid,venueDetails);
        },
        async:false
    });
var venueDetails = $.parseJSON(localStorage.getItem("venueDetails"+venueid));
// //console.log(venueDetails)
$("#venueName").html(venueDetails.result.venueName);

// //console.log(venueDetails);
     $.ajax({
        url: 'api/venue_details_curl.php',
        type: "POST",
        data:{
    "action": "venueData",
    "userId": user_id,
    "venueId": venueid,
    "venueVersion": venueDetails.venueVersion,
    "checkVersion": false,
    "startDate": datesData.start,
    "endDate": datesData.current,
}, 
        success: function(venueDetailData)
        {
          //=========================================loding charts=======================
          //console.log("=======================venueDetailData")
          //console.log(venueDetailData)
          localStorage.removeItem("venueDates"+venueid);
          localStorage.setItem("venueDates"+venueid,JSON.stringify(datesData));
          positiveNegativeTendecy(venueDetailData);
          donutChart(venueDetailData);
          DefaultBarchart(venueDetailData);
          starloader(venueDetailData);
          RealTableLoader(venueDetailData)

        },
        async:false
    });

      if(localStorage.getItem("duplicationData-"+venueid))
      {
          var dupliData=localStorage.getItem("duplicationData-"+venueid);
          // //console.log(dupliData)
              dupliData=$.parseJSON(dupliData)
              $.each(dupliData,function(index,element)
              { 
               var temp = index.replace(/[0-9]/g, '');
               // //console.log(element)
                
                if(element!="")
                { 
                  StartFunction(temp);
                  CUSTOMWORKINGSTART(element,index,temp);
                }
              });

        

      }


$("#loading").fadeOut();

 });



function findService($serviceid){
  var servicejson = localStorage.getItem("servicesjson");
  var servicejson = $.parseJSON(servicejson);
  var services = servicejson.services;
}








function DefaultBarchart(venueDetailData)
{

var venueD = $.parseJSON(venueDetailData)

if(venueD.result.services.length>0)
{
  var titles="";
  var subtitles = "";
  switch(localStorage.getItem("lang").toLowerCase())
  {
    case "en" : titles = "Negatives Vs Positives"; htitle1='positive'; htitle2 = 'negative'; xaxislabel= 'count'; yaxislabel='month';
                // subtitles =" Number of negatives vs positives " 
                break;
    case "fr" : titles = "Négatifs vs Positifs"; htitle1='positif'; htitle2 = 'négatif'; xaxislabel= 'compter'; yaxislabel='mois';
                // subtitles = "nombre d'avis positifs et négatifs";
                break;
  }

  var NPdata= getNPdata(venueDetailData);
   console.log(NPdata);
   let label = [];
    let posi = [];
    let nega =[];

    for (var key in NPdata){
    if(key!=0){
   //console.log(NPdata[key][0]);
    label.push(NPdata[key][0]);
    posi.push(NPdata[key][1]);
    nega.push(NPdata[key][2]);
    
    //chartValues.push(pair);
    }
    }

   var chartData = {
       type: 'bar',
       data: {
          labels: label,
        datasets: [
            {
                label: htitle1,
                backgroundColor: "blue",
                data: posi,
            },
            {
                label: htitle2,
                backgroundColor: "red",
                data: nega,
            }
        ]
       },
       options: {
            barValueSpacing: 20,
            scales: {
                yAxes: [{
                  barPercentage: 0.4,
                    ticks: {
                        min: 0,
                    },
                    scaleLabel: {
                   display: true,
                   labelString: xaxislabel
      }
                }],
                xAxes: [{
          scaleLabel: {
          display: true,
          labelString: yaxislabel
            }
             }]
            }
        }
    } 

 var canvas = document.getElementById('bar-chart');
var myChart = new Chart(canvas, chartData);
 /* google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable(NPdata);

        var options = {
          animation: {
          duration: 1000,
          easing: 'out',
          startup: true
          },
          chart: {
            title: titles,
            subtitle: subtitles,
          },
          bars: 'vertical',
          vAxis: {format: 'decimal'},
          height: 400,
          colors: ['#67BFB3', '#C73729']
        };

        var chart = new google.charts.Bar(document.getElementById('bar-chart'));

        google.visualization.events.addListener(chart, 'select', selectHandler);    

        function selectHandler() {
            var selection = chart.getSelection();
            var message = '';
                      for (var i = 0; i < selection.length; i++) {
                                var item = selection[i];
                                if (item.row != null && item.column != null) {
                                var str = data.getFormattedValue(item.row, item.column);
                                var category = data
                                .getValue(chart.getSelection()[0].row, 0)
                                var type
                                if (item.column == 1) {
                                type = "positive";
                                } else if(item.column == 2){
                                type = "negative";
                                }

                                message += category
                                + ',' + type;
                                } else if (item.row != null) {
                                var str = data.getFormattedValue(item.row, 0);
                                message += '{row:' + item.row
                                + ', column:none}; value (col 0) = ' + str
                                + '  The Category is:' + category;
                                } else if (item.column != null) {
                                var str = data.getFormattedValue(0, item.column);
                                message += '{row:none, column:' + item.column
                                + '}; value (row 0) = ' + str
                                + '  The Category is:' + category;
                                return false;
                                }
                                }
                                if (message == '') {
                                message = 'nothing';
                                return false;
                                }
                                  Load_RData(message,venueDetailData);
                                }

        chart.draw(data, google.charts.Bar.convertOptions(options));
        $(window).on('resizeEnd', function() {
        drawChart(data, google.charts.Bar.convertOptions(options));
        });


      }*/

}else{

  $("#bar-chart").html(dataMessage)
}
  
}

function donutChart(venueDetailData) {

 var venueD= $.parseJSON(venueDetailData);

if(venueD.result.services.length>0)
{


 var serviceData = $.parseJSON(localStorage.getItem("servicesjson"));
 var j=0;
 var donut_slice=[];
 var total=0;
 var color;
 var percent;

     $.each(venueD.result.services,function(index,element){
        total=total+element.reviews.length;
        });
// //console.log("===========garaging")
    $.each(venueD.result.services,function(index,element){
            $.each(serviceData.services,function(index2,element2){
                if(element.serviceId===element2.id)
                      {
                        // //console.log(element2.domainKey)
                        percent= ((element.reviews.length*100)/total).toFixed(2);
                            donut_slice[j] = {
                              'label':element2.displayName,
                              'value':percent,
                              'color':element2.rgb,
                              'hidden':element2.id,
                              'hidden-name': element2.shortName
                            };
                        // //console.log(donut_slice[j])
                      }

            });
            j++;
    });


//console.log(donut_slice)


    var randomColor = function(opacity) {
        return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';
    };

    var ctx = document.getElementById("sales-chart");
var myChart = new Chart(ctx, {
  type: 'doughnut',
  data: {
    labels: ["Red", "Green", "Yellow"],
    datasets: [{
      
      data: [12, 19, 3],
      backgroundColor: [
        '#F7464A',
        '#46BFBD',
        '#FDB45C'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)'
      ],
      borderWidth: 1,
      label: '# of Votes',
    }]
  },
  options: {
    segmentShowStroke : true,
    segmentStrokeColor : "#fff",
    segmentStrokeWidth : 2,
    percentageInnerCutout : 50,
    animationSteps : 100,
    animationEasing : "easeOutBounce",
    animateRotate : true,
    animateScale : false,
    responsive: true,
    maintainAspectRatio: true,
    showScale: true,
    animateScale: true
  }
});




//console.log("===============================venueDetails")
/*    var venueDetails = $.parseJSON(localStorage.getItem("venueDetails"+venueid));

    for(var i=0; i< venueDetails.result.services.length; i++ )
    {
      for(var j=0;j<donut_slice.length;j++)
      {
        if(venueDetails.result.services[i].serviceName === donut_slice[j]['hidden-name'])
        {
            delete(venueDetails.result.services[i]);
            break;
        }
      }

    }
      venueDetails = venueDetails.result.services.filter(function( element ) {
         return element !== undefined;
      });



$.each(serviceData.services,function(index,element){

      $.each(venueDetails,function(index2,element2){


         if(element.shortName === element2.serviceName)
         {
              element2.serviceId = element.id;
              element2.displayName = element.displayName;
              element2.rgb = element.rgb;
         } 


      })

});

//console.log(venueDetails)


$.each(venueDetails,function(index,element){
  donut_slice.push({
                              'label':element.displayName,
                              'value':0,
                              'color':element.rgb,
                              'hidden':element.serviceId,
                              'hidden-name': element.serviceName
                            })
})


//console.log(donut_slice)



  window.donutChart = Morris.Donut({
  element: 'sales-chart',
  data: donut_slice,
  resize: true,
  redraw: true,
  animationEnabled: true

  }).
  on('click', function (i, row) {  

    morrisHan(i,row.hidden,venueDetailData);

});

$(".venue-value-box").html('');
  $.each(donut_slice,function(index,element){
    $(".venue-value-box").append(`<div style="pointer-events:none;" onClick="morrisHan(`+index+`,'`+element.label+`');" class="col-sm-6">
                      <div class="info-box" style="background-color:`+element.color+` !important">
                     <div class="info-box-content text-center">
                      <span class="info-box-text">`+element.label+`</span>
                      <span class="info-box-number">`+element.value+`%</span>
                      <div class="progress">
                        <div class="progress-bar" style="width: `+element.value+`%"></div>
                      </div>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                </div>`);

  });*/



}
else{
    $("#sales-chart").html(dataMessage);
    $("#sales-chart").css("min-height",0);

}


}





//getting custom months
function getNPdata(data)
{
    var months=[];
    var obj= $.parseJSON(data);
    var i=0;
    $.each(obj.result.stats,function(index,element){
        months[i]=element.yearMonth;
        i++;
    });

var months_overall = []


switch(localStorage.getItem('lang').toLowerCase())
{
  case 'en' : months_overall = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

                      break;
  case 'fr' : months_overall = ['Janv.','Févr.','Mars','Avr.','Mai','Juin','Juil.','Août','Sept.','Oct.','Nov.','Déc.'];
                      break;


}

//console.log(months_overall)

var j=0;
var new_months=[];
$.each(months,function(index,element){
 new_months[j] = months_overall[+element.split('-')[1] - 1];
    j++;
});
//console.log("monthing............................................................")
//console.log(new_months)

var NPdata=[];
i=1;
var j=0;
var monu="";
var neg="";
var pos= ""; 
switch(localStorage.getItem("lang").toLowerCase())
{
  case 'en' : monu = "Months";
              neg = "Negatives";
              pos = "Positives";
              break;
  case 'fr' : monu =  "Mois";
              neg = "Négatifs"
              pos = "Positifs"
              break;
}
NPdata[0]=[monu, pos, neg];
$.each(obj.result.stats,function(index,element){

  NPdata[i]=[new_months[j],element.venuePositiveNotesCount,element.venueNegativeNotesCount];
i++;
j++;
});
    //console.log("npying the data/....")
    //console.log(NPdata)
    return NPdata;
}


function starloader(venueDetailData)
{

/*new chart*/



var usingVenueData= $.parseJSON(venueDetailData);

if(usingVenueData.result.services.length>0)
{
  var titles = "";
  var titles2 = "";
  var starts = "";
switch(localStorage.getItem("lang").toLowerCase())
{
  case 'en' : titles = "Number of Notes";
              titles2 = "Number of Notes for past five months";
              stars = "Stars";
              break;
  case 'fr' : titles =  "Nombre de notes";
              titles2 = "Nombre de notes pour les cinq derniers mois";
              stars = "étoiles";
              break;
}

var chartData = {
   type: 'horizontalBar',
   data: {
      labels: ["1(★)","2(★)","3(★)","4(★)","5(★)"],
    datasets: [
        {
            label: titles,
            backgroundColor: "blue",
            data: [usingVenueData.result.venueNotes1Count,usingVenueData.result.venueNotes2Count,usingVenueData.result.venueNotes3Count,usingVenueData.result.venueNotes4Count,usingVenueData.result.venueNotes5Count]
        }
       
    ]
   },
   options: {
        barValueSpacing: 20,
        scales: {
            yAxes: [{
                scaleLabel: {
          display: true,
          labelString: stars
            },
                ticks: {
                    min: 0,
                }
            }],
            xAxes: [{
          scaleLabel: {
          display: true,
          labelString: titles2
            }
             }]
        }
    }
} 

 var canvas = document.getElementById('chart_divl');
var myChart = new Chart(canvas, chartData);
   /* google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);
function drawBasic() {
      var data = google.visualization.arrayToDataTable([
        [stars, titles],
        ['5(★)', usingVenueData.result.venueNotes5Count],
        ['4(★)', usingVenueData.result.venueNotes4Count],
        ['3(★)', usingVenueData.result.venueNotes3Count],
        ['2(★)', usingVenueData.result.venueNotes2Count],
        ['1(★)', usingVenueData.result.venueNotes1Count]
      ]);
      var options = {
                    animation: {
          duration: 1000,
          easing: 'out',
          startup: true
          },
           colors: ['#67BFB3'],
         title: titles2,
        chartArea: {width: '45%'},
        hAxis: {
          title: titles,
          minValue: 0
        },
        vAxis: {
          textStyle:{color: '#FABA2E', fontSize: 14,}
        }
      };
      var chart = new google.visualization.BarChart(document.getElementById('chart_divl'));

       google.visualization.events.addListener(chart, 'select', selectHandler);    
function selectHandler() {
    var selectedItem = chart.getSelection()[0];
    if (selectedItem) {
      if(selectedItem.row!==null)
      {
      var value = data.getValue(selectedItem.row,0);
      Load_Star(value,venueDetailData);
      }

    }
  }

      chart.draw(data, options);
        $(window).on('resizeEnd', function() {
        chart.draw(data, options);
        });
    }
$("#chart_divl").css("height",500);
*/


}
else{

    $("#chart_divl").html(dataMessage);
    $("#chart_divl").css("height",15);

}


}


//Function for filling up the reviews in 

function RealTableLoader(venueDetailData)
{
  // //console.log(venueDetailData);
    var venueD = $.parseJSON(venueDetailData);


      var temp = [];
      $.each(venueD.result.services,function(index,element){
        var service= getName(element.serviceId);
          var serviceUrl=getServiceUrl(service.idService,venueid)
        $.each(element.reviews,function(index1,element1){
          element1.service = service;
          element1.serviceUrl = serviceUrl;
          temp.push(element1);
        })
      })
      venueD.result.services = temp;
      venueD.result.services.sort(function(a,b){
        var dateA=new Date(a.date), dateB=new Date(b.date)
        return dateB-dateA //sort by date ascending
      });
      // //console.log(venueD)
    var ratings=0;
    var starnumber=0;
              
    var transButton='';
    var ratingData= {
      count:0,
      totalRating:0,
      sentence:[]
    }
    var langT = localStorage.getItem("lang").toLowerCase();
    $(".list").html("")
      switch (langT)
      {
        case 'fr' : transButton = "Traduction";
              revertButton = "Revenir";
                    ratingData.sentence[0]=' commentaires sur ';
                  ratingData.sentence[1]=' avis';
                  break;
        case  'en' : transButton = "Translate";
                 revertButton ="Revert";
               ratingData.sentence[0]=' comments on '
                     ratingData.sentence[1]= ' reviews ';
                   break;

        default : transButton = "Traduction";
              revertButton = "Revert";
              break;
      }
  if(venueD.status==='success')
  {
    $.each(venueD.result.services,function(index1,element1){
      ratings= parseInt(ratings+element1.note);
              starnumber=parseInt(starnumber+1);          
                var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var simpleDate = element1.date;
                var date = getFormattedDate(element1.date)
                ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
                if(sentence.length > 0){
            $(".list").append(`
      <li class="venue-review ">
      <p style="display: none;" class="realDate">`+simpleDate+`</p>
            <div class="venue-logo">
              <a href="`+element1.serviceUrl+`" target="_blank"><img src="`+element1.service.idName+`" class="img-fluid" alt=""></a>
              <p style="display:none" class="service">`+element1.service.idServiceName+`</p>  
            </div>
             <div class="venue-title">
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
                if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }
}
    });




  }
  var head = ratingData.count+ratingData.sentence[0]+ratingData.totalRating+ ratingData.sentence[1];

  $(".reviewsHeading").html(head)


var star = ratings/starnumber;


    star = Number(Math.round(star+'e2')+'e-2');
    // //console.log("hurrah")
     // //console.log(star)
    $("#starOuterVal").html(star);
    $("#starVal").html(star);
  const starPercentage = (star / 5) * 100;



  const starPercentageRounded = `${(Math.round(starPercentage))}%`;


  document.querySelector(`.stars-inner`).style.width = starPercentageRounded; 
  // $("#starOuterVal").html(starPercentageRounded);
var monkeyList = new List('test-list', {
  valueNames: ['disdate','diftitle','title-en','full','full-en','service','realDate'],
  page: 10,
  pagination: true
});

}







//==============================================================Custom Data======================================================//

$("#dateValidator").on('click',function(){

var start = $("#datepicker1").val();
var end = $("#datepicker2").val();

if(start!== "" && end!== "")
{
  var lang = localStorage.getItem("lang").toLowerCase();
  if(start > end )
  {
    switch(lang){
      case 'en' : alert("Start Date Greater than end date");
              break;
      case 'fr' : alert("de début Supérieure à la date de fin");
              break;
          }
      return false;
  }
  if( start === end)
  {
    switch(lang){
      case 'en' : alert("Dates cannot be equal");
              break;
      case 'fr' : alert("les dates ne peuvent pas être égales");
              break;
          }
      return false;

  }

$("#loading").fadeIn();

startDate = start.split('/')
startDate = startDate[0].trim()+'-'+startDate[1].trim()+'-'+startDate[2].trim()

endDate = end.split('/')
endDate = endDate[0].trim()+'-'+endDate[1].trim()+'-'+endDate[2].trim()
Cusdates={"startDate":startDate,"endDate":endDate};

LoadCustomDataStarting(Cusdates);

}
else{

  alert("Enter Dates")
}



});

//==============================================CustomData====================================================================//

function LoadCustomData(Cusdates)
{
  $.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}

var venueid=$.urlParam('id');    

 var user_id;
   $.ajax({
        url: 'api/profile_curl.php',
        type: "POST",
        data:{"action":"getSession"}, 
        success: function(userData)
        {
         
         var userData=$.parseJSON(userData);
          user_id=userData.user_id;
        },async:false
    });
$.ajax({
        url: 'api/venue_details_curl.php',
        type: "POST",
        data:{
    "action": "venueDetails",
    "userId": user_id,
    "venueId": venueid
    }, 
        success: function(venueDetails)
        {
          //console.log(venueDetails);
          localStorage.removeItem("venueDetails"+venueid);
          localStorage.setItem("venueDetails"+venueid,venueDetails);
        },
        async:false
    });
var venueDetails = $.parseJSON(localStorage.getItem("venueDetails"+venueid));
   var dateRange =  Cusdates.startDate+" "+"to"+" "+Cusdates.endDate;
  $("#DateRange").html(dateRange);
     $.ajax({
        url: 'api/venue_details_curl.php',
        type: "POST",
        data:{
    "action": "venueData",
    "userId": user_id,
    "venueId": venueid,
    "venueVersion": venueDetails.venueVersion,
    "checkVersion": false,
    "startDate": Cusdates.startDate,
    "endDate": Cusdates.endDate,
}, 
        success: function(venueDetailData)
        {
          //=========================================loding charts=======================
          var datesData= {

                start: Cusdates.startDate,
                current: Cusdates.endDate
                    };

          localStorage.removeItem("venueDates"+venueid);
          localStorage.setItem("venueDates"+venueid,JSON.stringify(datesData))       
          CustomdonutChart(venueDetailData);
          DefaultBarchart(venueDetailData);
          positiveNegativeTendecy(venueDetailData);
          starloader(venueDetailData);
          RealTableLoader(venueDetailData);
        },
        async:false
    });

     $("#loading").fadeOut();
}


 
function CustomdonutChart(venueDetailData) {

var venueD= $.parseJSON(venueDetailData);
 var serviceData = $.parseJSON(localStorage.getItem("servicesjson"));
 var j=0;
 var donut_slice=[];
 var total=0;
 var color;
 var percent;
 $("#sales-chart").empty()

    $.each(venueD.result.services,function(index,element){
        total=total+element.reviews.length;
        });
//console.log(total)
    $.each(venueD.result.services,function(index,element){
            $.each(serviceData.services,function(index2,element2){
                if(element.serviceId===element2.id)
                      {
                        // //console.log(element2.domainKey)
                        percent= ((element.reviews.length*100)/total).toFixed(2);
                            donut_slice[j] = {
                              'label':element2.displayName,
                              'value':percent,
                              'color':element2.rgb,
                              'hidden':element2.id,
                              'hidden-name': element2.shortName
                            };
                        // //console.log(donut_slice[j])
                      }

            });
            j++;
    });


console.log(donut_slice);  

//console.log("===============================venueDetails")
   var venueDetails = $.parseJSON(localStorage.getItem("venueDetails"+venueid));

    for(var i=0; i< venueDetails.result.services.length; i++ )
    {
      for(var j=0;j<donut_slice.length;j++)
      {
        if(venueDetails.result.services[i].serviceName === donut_slice[j]['hidden-name'])
        {
            delete(venueDetails.result.services[i]);
            break;
        }
      }

    }
      venueDetails = venueDetails.result.services.filter(function( element ) {
         return element !== undefined;
      });



$.each(serviceData.services,function(index,element){

      $.each(venueDetails,function(index2,element2){


         if(element.shortName === element2.serviceName)
         {
              element2.serviceId = element.id;
              element2.displayName = element.displayName;
              element2.rgb = element.rgb;
         } 


      })

});

//console.log(venueDetails)


$.each(venueDetails,function(index,element){
  donut_slice.push({
                              'label':element.displayName,
                              'value':0,
                              'color':element.rgb,
                              'hidden':element.serviceId,
                              'hidden-name': element.serviceName
                            })
})


//console.log(donut_slice)



  window.donutChart = Morris.Donut({
  element: 'sales-chart',
  data: donut_slice,
  resize: true,
  redraw: true,
  animationEnabled: true

  }).
  on('click', function (i, row) {  

    morrisHan(i,row.hidden,venueDetailData);

});

$(".venue-value-box").html('');
  $.each(donut_slice,function(index,element){
    $(".venue-value-box").append(`<div style="pointer-events:none;" onClick="morrisHan(`+index+`,'`+element.label+`');" class="col-sm-6">
                      <div class="info-box" style="background-color:`+element.color+` !important">
                     <div class="info-box-content text-center">
                      <span class="info-box-text">`+element.label+`</span>
                      <span class="info-box-number">`+element.value+`%</span>
                      <div class="progress">
                        <div class="progress-bar" style="width: `+element.value+`%"></div>
                      </div>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                </div>`);

  });
}




//=================================text-analysis page============

function text_go()
{
  $.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}

var venueid=$.urlParam('id');  

  window.location.href= "text_analysis.php?id="+venueid;
}


  function positiveNegativeTendecy(data)
  {

    //console.log("=============================dataintegration");
    var dataobj= $.parseJSON(data);
    //console.log(dataobj)
    var monthlyTendency = dataobj.result.venueMonthlyTendency;
    var weeklyTendency = dataobj.result.venueWeeklyTendency;
    var prevWeekTendency = dataobj.result.venueWeeklyMinus1Tendency;
    var positive = 0;
    var negative = 0;

    var venueDataCurve=data;
    var lengthofReviews=0;
    curvechart(venueDataCurve);
    $.each(dataobj.result.services,function(index,element){
      
      lengthofReviews = lengthofReviews + element.reviews.length;          

        $.each(element.reviews,function(index2,element2) {
            
            if(element2.note>3.5)
            {
              positive++;
            }
            else
            {
              negative++;
            }

        })


    });

     $("#totalReviews").html(lengthofReviews);
     $("#positiveReviews").html(positive);
     $("#NegativeReviews").html(negative);
     $("#ThisMonthtendency").html(monthlyTendency);
     $("#ThisWeektendency").html(weeklyTendency);
     $("#PrevWeektendency").html(prevWeekTendency);
  }


function curvechart(curvedata)
{
   
 var months=[];
var obj= $.parseJSON(curvedata);
var i=0;
var positive=[];
var negative=[];

if(obj.result.services.length>0)
{

  $.each(obj.result.stats,function(index,element){
    months[i]=element.yearMonth;
    var total = parseInt(element.venuePositiveNotesCount+element.venueNegativeNotesCount)
    //console.log("================"+total)
    //console.log("==========="+element.venuePositiveNotesCount)
    positive[i]=(element.venuePositiveNotesCount/total)*100;
    negative[i]=(element.venueNegativeNotesCount/total)*100;
     i++;
    });

 var months_overall =[];
  var titles = "";
  var monu ="";
  var neg = "";
  var pos ="";
 switch(localStorage.getItem('lang').toLowerCase())
 {
  case 'en' : months_overall = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 
                                 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
              titles="Percentage of Negative vs positive";
              monu = "Months";
              neg = "Negatives";
              pos = "Positives";
              break;

  case 'fr' : months_overall = ['Janv.','Févr.','Mars','Avr.','Mai','Juin',
                                'Juil.','Août','Sept.','Oct.','Nov.','Déc.'];
              titles = "Pourcentage de négatif vs positif";
              monu =  "Mois";
              neg = "Négatifs"
              pos = "Positifs"
              break;

  default :  months_overall = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
              titles="Percentage of Negative vs positive";
              break;

 }

var j=0;
var new_months=[];
$.each(months,function(index,element){
 new_months[j] = months_overall[+element.split('-')[1] - 1];
    j++;
});

//var chartData=[];
  //chartData.push([monu, pos,neg]);
  let label = [];
    let posi = [];
    let nega =[];
  for(var i=0;i<new_months.length;i++)
  {
      //var pair=[];
      label.push(new_months[i]);
      posi.push(positive[i]);
      nega.push(negative[i])
     // chartData.push(pair);
  }


//console.log(chartData)
      
    var canvas = document.getElementById("curve_chart");
    var ctx = canvas.getContext('2d');

// Global Options:
//Chart.defaults.global.defaultFontColor = 'black';
//Chart.defaults.global.defaultFontSize = 16;

var data = {
  labels: label,
  datasets: [{
      label: pos,
      lineTension: 0.1,
      borderColor: "rgb(167, 105, 0)",
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "white",
      pointBackgroundColor: "black",
      pointBorderWidth: 1,
      pointHoverRadius: 8,
      pointHoverBackgroundColor: "brown",
      pointHoverBorderColor: "yellow",
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      pointHitRadius: 10,
      // notice the gap in the data and the spanGaps: false
      data: posi,
      spanGaps: false,
    },
    {
      label: neg,
      lineTension: 0.1,
      borderColor: "red", // The main line color
      borderCapStyle: 'square',
      borderDash: [], // try [5, 15] for instance
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "black",
      pointBackgroundColor: "white",
      pointBorderWidth: 1,
      pointHoverRadius: 8,
      pointHoverBackgroundColor: "yellow",
      pointHoverBorderColor: "brown",
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      pointHitRadius: 10,
      // notice the gap in the data and the spanGaps: true
      data: nega,
      spanGaps: true,
    }

  ]
};

// Notice the scaleLabel at the same level as Ticks
var options = {
  scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                },
                scaleLabel: {
                     display: true,
                     labelString: titles,
                     
                  }
            }],
            xAxes: [{
                scaleLabel: {
                     display: true,
                     labelString: monu,
                     
                  }
            }]             
        },
        elements: {
                        line: {
                                fill: false
                        }
                    } 
};

// Chart declaration:
var myBarChart = new Chart(ctx, {
  type: 'line',
  data: data,
  options: options
});

 
// positiveOpinionReviewLoader(message,curvedata)
                         
$("#curve_chart").show();

}else{

  $("#curve_chart").html(dataMessage)
  $("#curve_chart").hide();

}
 
}

function positiveOpinionReviewLoader(message,curvedata)
{

//console.log(message)

var new_message = message.split(",")

var transButton='';
var revertButton = '';
var langT = localStorage.getItem("lang").toLowerCase();
var month;
  var ratings=0;
var starnumber=0;


var enMonths = {
  'Jan':01,
  'Feb':02,
  'Mar':03,
  'Apr':04,
  'May':05,
  'Jun':06,
  'Jul':07,
  'Aug':08,
  'Sep':09,
  'Oct':10,
  'Nov':11,
  'Dec':12
};


var frMonths = {
  'Janv.':01,
  'Févr.':02,
  'Mars':03,
  'Avr.':04,
  'Mai':05,
  'Juin':06,
  'Juil.':07,
  'Août':08,
  'Sept.':09,
  'Oct.':10,
  'Nov.':11,
  'Déc.':12
};

    var ratingData= {
      count:0,
      totalRating:0,
      sentence:[]
    }
      switch (langT)
      {
        case 'fr' : month= frMonths[new_message[1]];
              transButton = "Traduction";
              revertButton = "Revenir";
                    ratingData.sentence[0]=' commentaires sur ';
                  ratingData.sentence[1]=' avis ';
                  break;
        case  'en' : month= enMonths[new_message[1]];
               transButton = "Translate";
                 revertButton ="Revert";
               ratingData.sentence[0]=' comments on '
                     ratingData.sentence[1]= ' reviews ';
                   break;

        default : transButton = "Traduction";
              revertButton = "Revert";
              break;
      }

 


$(".list").html("")

var curveD= $.parseJSON(curvedata);

var temp = [];
$.each(curveD.result.services,function(index,element){
  var service= getName(element.serviceId);
  var serviceUrl=getServiceUrl(service.idService,venueid)
  $.each(element.reviews,function(index1,element1){
    element1.service = service;
    element1.serviceUrl = serviceUrl;
    temp.push(element1);
  })
})
curveD.result.services = temp;
curveD.result.services.sort(function(a,b){
  var dateA=new Date(a.date), dateB=new Date(b.date)
  return dateB-dateA //sort by date ascending
});


var revMonth=[];

  $.each(curveD.result.services,function (index1,element1) {
      
      revMonth = element1.date.split("-");

      if(Number(revMonth[1].toString())==Number(month.toString()))
      {
        if(new_message[2] == 'positive')
        {

          if(Number(element1.note) > Number(3.5) )
          {
            ratings= parseInt(ratings+element1.note);
              starnumber=parseInt(starnumber+1);          
                var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var simpleDate = element1.date;
                var date = getFormattedDate(element1.date)
                ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
                if(sentence.length > 0)
                { 

                  $(".list").append(`
      <li class="venue-review ">
            <p style="display: none;" class="realDate">`+simpleDate+`</p>
            <div class="venue-logo">
              <a href="`+element1.serviceUrl+`" target="_blank"><img src="`+element1.service.idName+`" class="img-fluid" alt=""></a>
              <p class="service" style="display:none;">`+element1.service.idServiceName+`</p>
            </div>
             <div class="venue-title">
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)

              if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }

                }
            
                  
          }

        }else if(new_message[2]=='negative'){
          if(Number(element1.note)<Number(3.5))
          {
            ratings= parseInt(ratings+element1.note);
              starnumber=parseInt(starnumber+1);          
                var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var simpleDate = element1.date;
                var date = getFormattedDate(element1.date)
                ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
                if(sentence.length > 0){
  $(".list").append(`
      <li class="venue-review ">
            <p style="display: none;" class="realDate">`+simpleDate+`</p>
            <div class="venue-logo">
              <a href="`+element1.serviceUrl+`" target="_blank"><img src="`+element1.service.idName+`" class="img-fluid" alt=""></a>
              <p class="service" style="display:none;">`+element1.service.idServiceName+`</p>
            </div>
             <div class="venue-title">
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
              if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }
                  
                }
          
              

          }
        }



      }


      });
  
var monkeyList = new List('test-list', {
  valueNames: ['disdate','diftitle','title-en','full','full-en','service','realDate'],
  page: 10,
  pagination: true
});
 var head = ratingData.count+ratingData.sentence[0]+ratingData.totalRating+ ratingData.sentence[1];

  $(".reviewsHeading").html(head)

$('html, body').animate({
        scrollTop: $("#tableAnchor").offset().top
    }, 2000);

var star = ratings/starnumber;
  $("#starOuterVal").html(star);

    star = Number(Math.round(star+'e2')+'e-2');
    $("#starVal").html(star);
  const starPercentage = (star / 5) * 100;

  const starPercentageRounded = `${(Math.round(starPercentage))}%`;

  document.querySelector(`.stars-inner`).style.width = starPercentageRounded;





}




// function for selecting date range periods
function testfunction()
{
  var x=$('#DateCustom').val();

 var mnths = { 
                Jan:"01", Feb:"02", Mar:"03", Apr:"04", May:"05", Jun:"06",
                Jul:"07", Aug:"08", Sep:"09", Oct:"10", Nov:"11", Dec:"12"
                 };

  if(x!=='NULL')
  {

      switch(x)
      {
      case  'crMonth' :   var date = new Date(), y = date.getFullYear(), m = date.getMonth();
                          var firstDay = new Date(y, m, 1).toString();
                          var lastDay = new Date().toString();
                           
                         
                              var fdarr=[];
                                  fdarr=firstDay.split(" ");
                              var ldarr=[];
                                  ldarr=lastDay.split(" ");

                               firstDay = [ fdarr[3], mnths[fdarr[1]], fdarr[2] ].join("-");
                               lastDay = [ ldarr[3], mnths[ldarr[1]], ldarr[2] ].join("-");

                                Cusdates={"startDate":firstDay,"endDate":lastDay};
                                 var datesData= {
                                                      start: Cusdates.startDate,
                                                      current: Cusdates.endDate
                                                          };
                                            localStorage.removeItem("venueDates"+venueid);
                                            localStorage.setItem("venueDates"+venueid,JSON.stringify(datesData))  
                                LoadCustomDataStarting(Cusdates);
                          break;
      case  '3months' :   
                          var start;
                          var current;
                          $.ajax({

                            url: "api/dateFinder.php",
                            type:"POST",
                            data: {"action":"3months"},
                            success: function (dates) {
                                
                              var dates = $.parseJSON(dates);
                               start = dates.start.split("-"); 
                                  start[2]='01';  
                                  start=start.join('-');
                               current= dates.current;
                                Cusdates={"startDate":start,"endDate":current};
                                            var datesData= {
                                                      start: Cusdates.startDate,
                                                      current: Cusdates.endDate
                                                          };
                                            localStorage.removeItem("venueDates"+venueid);
                                            localStorage.setItem("venueDates"+venueid,JSON.stringify(datesData))                            
                                                  LoadCustomDataStarting(Cusdates)
                                  
                            },error:function (argument) {
                              //console.log(argument)
                            },async:false

                          });

                                        

                         break;
      case  '6months' : 
                          var start;
                          var current;
                          $.ajax({

                            url: "api/dateFinder.php",
                            type:"POST",
                            data: {"action":"6months"},
                            success: function (dates) {
                                
                              var dates = $.parseJSON(dates);
                               start = dates.start.split("-"); 
                                  start[2]='01';  
                                  start=start.join('-');
                               current= dates.current;
                                Cusdates={"startDate":start,"endDate":current};
                                         var datesData= {
                                                      start: Cusdates.startDate,
                                                      current: Cusdates.endDate
                                                          };
                                            localStorage.removeItem("venueDates"+venueid);
                                            localStorage.setItem("venueDates"+venueid,JSON.stringify(datesData))  

                                LoadCustomDataStarting(Cusdates)
                                  
                            },error:function (argument) {
                              //console.log(argument)
                            },async:false

                          }); 
                         break;
      case  'crYear' :  
                           var start;
                          var current;
                          $.ajax({

                            url: "api/dateFinder.php",
                            type:"POST",
                            data: {"action":"crYear"},
                            success: function (dates) {
                              var dates = $.parseJSON(dates);
                               start = dates.start.split("-"); 
                                  start[2]='01';  
                                  start=start.join('-');
                               current= dates.current;
                                Cusdates={"startDate":start,"endDate":current};
                                            var datesData= {
                                                      start: Cusdates.startDate,
                                                      current: Cusdates.endDate
                                                          };
                                            localStorage.removeItem("venueDates"+venueid);
                                            localStorage.setItem("venueDates"+venueid,JSON.stringify(datesData))                               
                                             LoadCustomDataStarting(Cusdates)
                                  
                            },error:function (argument) {
                              //console.log(argument)
                            },async:false

                          });   

                         break;
      case  '2years' :  
                        var start;
                          var current;
                          $.ajax({

                            url: "api/dateFinder.php",
                            type:"POST",
                            data: {"action":"2years"},
                            success: function (dates) {

                              var dates = $.parseJSON(dates);
                               start = dates.start.split("-"); 
                                  start[2]='01';  
                                  start=start.join('-');
                               current= dates.current;
                                Cusdates={"startDate":start,"endDate":current};
                                 var datesData= {
                                                      start: Cusdates.startDate,
                                                      current: Cusdates.endDate
                                                          };
                                            localStorage.removeItem("venueDates"+venueid);
                                            localStorage.setItem("venueDates"+venueid,JSON.stringify(datesData))
                                LoadCustomDataStarting(Cusdates)
                                  
                            },error:function (argument) {
                              //console.log(argument)
                            },async:false

                          });   
                         break;
      case  '3years' :    
                          var start;
                          var current;
                          $.ajax({

                            url: "api/dateFinder.php",
                            type:"POST",
                            data: {"action":"3years"},
                            success: function (dates) {
                              //console.log(dates)

                              var dates = $.parseJSON(dates);
                               start = dates.start.split("-"); 
                                  start[2]='01';  
                                  start=start.join('-');
                               current= dates.current;
                                Cusdates={"startDate":start,"endDate":current};
                                 var datesData= {
                                                      start: Cusdates.startDate,
                                                      current: Cusdates.endDate
                                                          };
                                            localStorage.removeItem("venueDates"+venueid);
                                            localStorage.setItem("venueDates"+venueid,JSON.stringify(datesData))
                                LoadCustomDataStarting(Cusdates)
                                  
                            },error:function (argument) {
                              //console.log(argument)
                            },async:false

                          });   
                         break;

      }

  }
$('#DateCustom').val('NULL');
}

function LoadCustomDataStarting(Cusdates)
{
  $("#loading").fadeIn();
    setTimeout(
    function() {
      LoadCustomData(Cusdates);
      
    }, 400);
}

