
var dataMessage = "";
var dateGreatError ="";
var selectTxt = '';
var datforListing={};
var mainTopiclabel = ''
$(function() {

 $('.connectedSortable').sortable({
    placeholder         : 'sort-highlight',
    connectWith         : '.connectedSortable',
    handle              : '.box-header, .nav-tabs',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });
  $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');

    $("#loading").show();
var lang = localStorage.getItem("lang").toLowerCase();
 
switch(lang){
  case 'en' : mainTopiclabel = "Count";
              dataMessage = "<b>No Data</b>";
              dateGreatError="Start date not allowed to be greater than end date"
              selectTxt = "Select Venues";
                break;
  case 'fr' : mainTopiclabel = "Nombre";
              dataMessage = "<b>pas de données</b>";
          		selectTxt = "Sélectionnez des lieux";
              dateGreatError="La date de début ne doit pas être supérieure à la date de fin";   
                break;
  default: dataMessage = "<b>No Data</b>";
              break;
}  


  var userData= getuserData();
  var userData = JSON.parse(userData);
/*getting venues list*/
$.ajax({

           url: "api/getVenue_curl.php",
           type: "POST",
           data: {
             "action": "getVenues",
             "userId":userData.user_id,
             "graphVersion":  null   
           },
           success:function(response)
           {
             venueData = JSON.parse(response);
             console.log(venueData)
             var options = ``;
             if(venueData.status === 'success')
             { 
                $.each(venueData.result.venues,function(index,element){
                  datforListing[element.venueId] = element.venueName;
                  options+= `<option value="`+element.venueId+`">`+element.venueName+`</option>`

                })
              $("#homeVenues").append(options)
             }
            },
          error:function(){
            alert("Aucune donnée reçue, rafraîchissante")
            location.reload();
          },
          async:false
        });



$('.multi').multi_select({
  selectColor: 'aqua',
  selectSize: 'small',
  selectText: selectTxt,
  duration: 300,
  easing: 'slide',
  listMaxHeight: 300,
  selectedCount: 4,
  sortByText: true,
  fillButton: true,
  data: datforListing,
  onSelect: function(values) {
    console.log('return values: ', values);
  }
  });

  


     $.ajax({
        url:"api/add_venue_curl.php",
        type:"POST",
        dataType:"json",
        data:{"action":"getEditPage","lang":"en"},
        success:function(data)
        {
        
          if(data)
          {
    data.types.sort(function (a, b) {
     return a.tag.localeCompare(b.tag);
    });
      $.each(data.types,function(index,element){
    $("#venueTypes").append(`<option value=`+element.tag+`>`+element.locales[lang]+`</option>`)
    
    })
          }
          else{
    $("#venueTypes").append(`<option value=`+null+`>`+"No Types"+`</option>`)
          }

        }
    });




     $.ajax({
        url:"api/add_venue_curl.php",
        type:"POST",
        dataType:"json",
        data:{"action":"getEditPage","lang":lang},
        success:function(data)
        {
          console.log(data)
          if(data.length!==0)
          {
                // $("#venuetypes").html("");
              data.types.sort(function (a, b) {
               return a.tag.localeCompare(b.tag);
              });
           var select2 = `<select id="venue_type" class="form-control select2" multiple="multiple" data-placeholder="Select a type" style="width: 100%;">`;
            // $.each(data, function(key, val){
            //     select2 += `<option>`+val+`</option>`;  
            // });
                  $.each(data.types,function(index,element){
    select2+=`<option value=`+element.tag+`>`+element.locales[lang]+`</option>`
    
    })
                select2 += `</select>`;
                $("#venuetypes").html(select2);
                $('#venue_type').select2({minimumResultsForSearch: -1});            
            $("#loading").hide();
          }
          else{
            // $("#venuetypes").append('<option >Sorry No data loaded</option>');
          }
        }
    });


  
var data = {"action":"loadGroups"};
$.ajax({
      url:"api/groups_curl.php",
      type:"POST",
      data: data,
      success:function(response){
  console.log(response)
        response = JSON.parse(response);
        localStorage.removeItem("savedGroups");
        localStorage.setItem("savedGroups",JSON.stringify(response.result.groups));
var sg = `<ul style="list-style: none;" id="sortable">`;
console.log("here");
         $.each(response.result.groups,function(index,element){

          console.log(element)
          
          //checking for existing components
          if (element.groupDisplayName == "") {
            var grpname = element.groupName;
          }else{
            var grpname = element.groupDisplayName;
          }
          var groupVenues = '';
          var icon = '';
          if(element.groupVenues)
          {
           groupVenues = element.groupVenues;
          }else{
            groupVenues = 'none';
          }


          //finding the right icon
         if(element.groupKind == '' || element.groupKind == 'dynamic' || !element.groupKind)
         {
          icon = '<i class="fa fa-search mr-1" aria-hidden="true"></i>';
         } 
         else {
          icon = '<i class="fa fa-check mr-1" aria-hidden="true"></i>';
         }

      sg += `<li style="display:inline;float:left;" 
      id="`+index+`" groupName = "`+element.groupName+`" 
      groupDisplayName = "`+element.groupDisplayName+`" 
      groupType="`+element.groupType+`"
      groupKind="`+element.groupKind+`"
      groupAddress="`+element.groupAddress+`"
      groupVenues="`+groupVenues+`"><span style="float:left;font-size: 15px;" class="btn bg-purple cancel_button"
           onclick="saved_ajax('`+element.groupName+`','`+element.groupKind+`','`+element.groupVenues+`','`+element.groupDisplayName+`');" >`+icon+`
           `+grpname+`</span>
           <span class='group-i'><i onclick="deleteGroup('`+index+`');" style="color:red;cursor : pointer;"
            class="fas fa-trash"></i>&nbsp;<i class="fa fa-bars"></i></span></li>`;
      });
sg += `</ul>`;

$("#saved_searches").html(sg);
$( "#sortable" ).sortable({
  update: function( event, ui ) {

    $("#cancel").show();
    $("#savechanges").show();


  }
});

      }
 });    

    
    var saved_searches = "["+localStorage.getItem("saved_groups")+"]";
    // console.log("saved searchessssssssssssssssssssssss")
    // console.log(saved_searches)
    if (saved_searches!=null) {
      $("#saved_searches").append(`<h5>Saved Groups</h5>`);
    }
  
$("#loading").hide();

});

$(window).resize(function() {
    if(this.resizeTO) clearTimeout(this.resizeTO);
    this.resizeTO = setTimeout(function() {
        $(this).trigger('resizeEnd');
    }, 500);
});

function removefromgrp(id) {
  var venname = $("#venue_name").val();
  var tdval = $("#td"+id).html();
  $("#venue_name").val(venname+" -"+tdval);

  $("#tr"+id).remove();
}



var trim = (function () {
    "use strict";
    
    function escapeRegex(string) {
        return string.replace(/[\[\](){}?*+\^$\\.|\-]/g, "\\$&");
    }

    return function trim(str, characters, flags) {
        flags = flags || "g";
        if (typeof str !== "string" || typeof characters !== "string" || typeof flags !== "string") {
            throw new TypeError("argument must be string");
        }

        if (!/^[gi]*$/.test(flags)) {
            throw new TypeError("Invalid flags supplied '" + flags.match(new RegExp("[^gi]*")) + "'");
        }

        characters = escapeRegex(characters);

        return str.replace(new RegExp("^[" + characters + "]+|[" + characters + "]+$", flags), '');
    };
}());

function deleteGroup(id){
$('#'+id).remove();
$("#cancel").show();
$("#savechanges").show();
}

function saveChanges(){

  var groups = new Array();

  // console.log("i");
$('#sortable li').each(function(i)
{
    var groupVenues = $(this).attr('groupVenues');
    console.log(groupVenues)
  var groupName = $(this).attr('groupname');
  var groupDisplayName = $(this).attr('groupdisplayname');
  var groupType = $(this).attr('grouptype');
  var groupAddress = $(this).attr('groupaddress');

      if(groupVenues !== 'none' && groupVenues){

        var splitedVenues = groupVenues.split(",")
        console.log(splitedVenues)
        var group = {
    "groupKind":"static",
    "groupDisplayName":groupDisplayName.trim(),
    "groupVenues":splitedVenues
                  };
      }else {
    var group = {
    "groupName":groupName.trim(),
    "groupKind":"dynamic",
    "groupAddress":groupAddress.trim(),
    "groupType":groupType.trim(),
    "groupDisplayName":groupDisplayName.trim()
                  };
      }  
     groups.push(group);

  
});



  var userData= getuserData();
  var userData = JSON.parse(userData);
  if (groups=="") {
    var groups="";
  }
  
$.ajax({
      url:"api/groups_curl.php",
      type:"POST",
      data: {
        action : "saveGroups",
        groups : groups
      },
      success:function(response){
        location.reload();
      }
  });
}

function saveGroup(){
var group_name = $("#group_name").val();

if (group_name!="") {
var name = $("#venue_name").val();
// name = name.replace(",",";");
var address = $("#venue_address").val();
// address = address.replace(",",";");
var type = $("#venue_type").val();
// type = type.replace(",",";");
var sdate = $("#startdate").val();
var edate = $("#enddate").val();

$("#sortable").append(`<li style="display:inline; display:none;" id="" 
  groupName = "`+name+`" groupDisplayName = "`+group_name+`" 
  groupType="`+type+`" groupAddress="`+address+`">
  <span class="btn bg-purple cancel_button"
           onclick="saved_ajax('');" ></span>
          </li>`);

alert("Group Saved!");
saveChanges();
// location.reload();
}
else{
  alert("Group Name is required to save!"); return false;
}
        }



function saved_ajax(data,kind,venuesForStatic,displayName){

var fromData = data;
var groupKind;
if(kind)
{
groupKind = kind;  
}

if(kind === 'undefined'){
  groupKind = 'dynamic';
}

if(groupKind === 'dynamic')
{

  var sdate = "";
var edate = "";
var sdate = $("#startdate").val();
var edate = $("#enddate").val();
if(sdate>edate)
{
  alert(dateGreatError);
  return false;
}

if(sdate ==="" && edate ==="")
{
  var dates = $.parseJSON(getDates());
  console.log("==================dating")
  console.log(dates)
  sdate  = dates.start;
  edate = dates.current;
}

  $("#venandtend").show();
  $("#notesdiv").show();
  $("#loading").show();
  $("#pills-home-tab").click();
var totCount = 0;
var saved_searches = localStorage.getItem("savedGroups");
var ss = JSON.parse(saved_searches);

       $.each(ss,function(index,element){
        var sentable = {
        "action":"matchVenues",
        "venueType":element.groupType,
        "venueName":element.groupName,
        "venueAddress":element.groupAddress,
        "startDate":sdate, 
        "endDate":edate 
      };

if (element.groupName == fromData) {
  $("#venue_type").val(element.groupType);
  $("#venue_name").val(element.groupName);
  $("#venue_address").val(element.groupAddress);
  $.ajax({
      url:"api/groups_curl.php",
      type:"POST",
      data: sentable,
      success:function(response){
        var response = jQuery.parseJSON(response);	


        localStorage.removeItem("responseVenueArray");
        localStorage.setItem("responseVenueArray",JSON.stringify(response.result.venues));

        $("#loading").hide();
        $("#tendencies").hide();
        $("#tendencies").html("");

        if (response.result.stats.length!=0) {
          tendency(response.result.stats);
          if(window.updateCapable) {
          reDrawstarloader(response.result.stats);
          reDrawBigChart(response.result.stats);
          
          } else {
          starloader(response.result.stats);
          BigChart(response.result.stats);
          }

        }

$("#loading").show();
$("#demo").html("");
var serviceVenues = new Array();
var type;
    $.each(response.result.venues,function(index,element){
      if (element.venueType==null) { var typ = "-"; } else { var typ = element.venueType; }
      if (element.venueAddress==null) { var addr = "-"; } else { var addr = element.venueAddress; }
       if (type!=""&&type!=element.venueType) {
        type = "default";
      }
      else{
        type = element.venueType;
      }
    $.each(element.serviceVenues,function(svindex,svelement){
      serviceVenues.push(svelement);
    });
  $("#demo").append(`<li id="tr`+index+`" class="row matched-venue li-item">
              <div class="col-md-3 col-sm-3 col-12">
                   <p class="name" id="td`+index+`">`+element.venueName+`</p>
              </div>
              <div class="col-md-3 col-sm-3 col-12">
                             <p class="name">`+typ+`</p>
              </div>
              <div class="col-md-3 col-sm-3 col-12">
                 <p class="name">`+addr+`</p>
              </div>
               <div class="col-md-3 col-sm-3 col-12">
               
                   <p><span onclick="addasVenue(`+index+`);"><i class="fa fa-plus" aria-hidden="true"></i></span>
                   <span onclick="removefromgrp(`+index+`)"><i class="fa fa-minus" aria-hidden="true"></i></span></p>
               </div>
              </li>`)


      
});
$("#demo").cPager({
  // how many items per page
  pageSize: 8,

  // container ID
  pageid: "pager",
  // item class02

  itemClass: "li-item",

  pageIndex: 1

});


$("#savesearch").show();    
$("#group_name").attr("style","display:inline");



  var allReviews = new Array();
  var data = {};
  var sdate = $("#startdate").val();
  var edate = $("#enddate").val();

     if (edate!="" || sdate!="") {

    data = {
      action:"serviceVenueData",
      venueServices:serviceVenues,
      startDate:sdate,endDate:edate};   
    
    }else{

      var dates = $.parseJSON(getDates());
      sdate = dates.start;
      edate = dates.current;

     data = {
      action:"serviceVenueData",
      venueServices:serviceVenues,
      startDate:sdate,endDate:edate};      
    }
    
     $.ajax({
      url:"api/groups_curl.php",
      type:"POST",
      data: data,
      success:function(response){
        response = JSON.parse(response);
        console.log("review-response");
        console.log(response);
     var venueNames=$.parseJSON(localStorage.getItem("responseVenueArray"));
     var venuename=undefined;
    $.each(response.result.services,function(rindex,relement){
        
          $.each(venueNames,function(index2,element2){
              if(element2.serviceVenues==relement.venueServiceId)
              {     
                  venuename=element2.venueName
              }       

          });
      $.each(relement.reviews,function(irindex,irelement){
          irelement.venueName= venuename; 
          allReviews.push(irelement);
      });
    
    });

    var maxlimit = $("#max_limit").val();
      if (allReviews.length<=maxlimit) {
      var lang = localStorage.getItem("lang");
   	 
   	  // a flat for showing if it is update capable

      TextAnalysisDataCollection(type,lang,allReviews);

    }
    else{
      if (lang=="en") {
      $("#condmsg").html("This group is too large for topic modeling.");
    }else{
      $("#condmsg").html("Ce groupe est trop grand pour effectuer une analyse des avis.");
    }
    }
    showReviews(allReviews);

      }
    });


      }
 });
}
});

}else {

   /* static group management starts here */
  $("#venandtend").show();
  $("#notesdiv").show();

  // $("#loading").show();
  var sdate = $("#startdate").val();
  var edate = $("#enddate").val();
  $("#demo").html('');
  console.log(kind,venuesForStatic)
   $("#pills-profile-tab").click();

  var dates = $.parseJSON(getDates());

if(sdate ==="" && edate ==="")
{
  sdate  = dates.start;
  edate = dates.current;
}

  var splittedVenues = venuesForStatic.split(",");
  var sentable = {
    "action":"matchStaticVenues",
    "venues":splittedVenues,
    "startDate": sdate.trim(),
    "endDate": edate.trim()
   }

console.log(sentable)
    for(var obj in sentable){
      if(sentable[obj]=== null || sentable[obj]=== undefined || sentable[obj]=== "" )
      {
        delete sentable[obj];
      }
    }

    $.ajax({
      url: "api/groups_curl.php",
      type: "POST",
      data: sentable,
      success: function(data){
        if(data){
         var response = JSON.parse(data);
         console.log("responsing")

        $("#tendencies").hide();
        $("#tendencies").html("");
        var valvenues={};
        if (response.result.stats.length!=0) {
        	
        	$("#staticName").val(displayName)
        	 $("#startdate").val(response.request.startDate);
			 $("#enddate").val(response.request.endDate);

       $('#multi').multi_select('clearValues');
       $('.multi').multi_select({
              data:datforListing,
              selectedIndexes:response.request.venues,
              sortByText:true});
          tendency(response.result.stats);
          if(window.updateCapable) { // checking if the chartts need to be updated or drawn first time
          reDrawstarloader(response.result.stats);
          reDrawBigChart(response.result.stats);
          
          } else {
          starloader(response.result.stats);
          BigChart(response.result.stats);
          }
        }
         $("#loading").show();
        $("#demo").html("");
        var serviceVenues = new Array();
        var type;
        var venueNames = response.result.venues; 
    $.each(response.result.venues,function(index,element){
      if (element.venueType==null) 
        { var typ = "-"; } 
      else 
        { var typ = element.venueType; }
      if (element.venueAddress==null) 
        { var addr = "-"; } 
      else 
        { var addr = element.venueAddress; }
       if (type!=""&&type!=element.venueType) {
        type = "default";
      }
      else{
        type = element.venueType;
      }

    $.each(element.serviceVenues,function(svindex,svelement){
      serviceVenues.push(svelement);
    });
  $("#demo").append(`<li id="tr`+index+`" class="row matched-venue li-item">
              <div class="col-md-3 col-sm-3 col-12">
                   <p class="name" id="td`+index+`">`+element.venueName+`</p>
              </div>
              <div class="col-md-3 col-sm-3 col-12">
                             <p class="name">`+typ+`</p>
              </div>
              <div class="col-md-3 col-sm-3 col-12">
                 <p class="name">`+addr+`</p>
              </div>
               <div class="col-md-3 col-sm-3 col-12">
                   <p>
                   
                   </p>
               </div>
              </li>`)
      
            });
            $("#demo").cPager({
              // how many items per page
              pageSize: 8,

              // container ID
              pageid: "pager",
              // item class02

              itemClass: "li-item",
              pageIndex: 1

            });


  var allReviews = new Array();
  var sending = {};
  var sdate = $("#startdate").val();
  var edate = $("#enddate").val();

   if (edate!="" || sdate!="") {
    sending = {action:"serviceVenueData",venueServices:serviceVenues,startDate:sdate,endDate:edate};       
  }else{
      var dates = $.parseJSON(getDates());
      sdate = dates.start;
      edate = dates.current;
     sending = {action:"serviceVenueData",venueServices:serviceVenues,startDate:sdate,endDate:edate};      
    }

console.log(sending)
    $.ajax({
      url: "api/groups_curl.php",
      type: "POST",
      data: sending,
      success: function(data){

        if(data)
        {
          var reviewResponse = JSON.parse(data)
          var allReviews = new Array();
          if(reviewResponse.status === 'success'){
            console.log(reviewResponse)
             var venuename=undefined;
              console.log(venueNames)

            $.each(reviewResponse.result.services,function(rindex,relement){
                  $.each(venueNames,function(index,element){
                      $.each(element.serviceVenues,function(index1,element1){
                          
                          if(relement.venueServiceId == element1)
                          {
                            venuename = element.venueName; 
                          }

                      })
                  })
                     $.each(relement.reviews,function(irindex,irelement){
                      irelement.venueName= venuename; 
                      allReviews.push(irelement);
                  });

            });
    
     var maxlimit = $("#max_limit").val();
      if (allReviews.length<=maxlimit) {
      var lang = localStorage.getItem("lang");

      TextAnalysisDataCollection(type,lang,allReviews);
      showReviews(allReviews);

    }
    else{
      if (lang=="en") {
      $("#condmsg").html("This group is too large for topic modeling.");
    }else{
      $("#condmsg").html("Ce groupe est trop grand pour effectuer une analyse des avis.");
    }
    }


          }//checking status of serviceVenueData 

        }
      }
    })

      }//if section of data return in ajax
    }// succes section of staticMatchVenues
      ,error: function(data){
        console.log(data)
        $("#loading").hide();
        return;
      }
    })

}


}

function search(){
var sdate = "";
var edate = "";
var name = "";
var address = "";
var type = "";
$("#demo").html("")
var name = $("#venue_name").val();
name = name.replace(",",";");
var address = $("#venue_address").val();
address = address.replace(",",";");
var btype = $("#venue_type").select2("val");
$.each(btype,function(index,element){
  type += element+";";
});
// type = type.replace(",",";");
var sdate = $("#startdate").val();
var edate = $("#enddate").val();

if(sdate>edate)
{
  alert(dateGreatError);
  return false;
}

if(sdate ==="" && edate ==="")
{
  var dates = $.parseJSON(getDates());
  console.log("==================dating")
  console.log(dates)
  sdate  = dates.start;
  edate = dates.current;
}

var data = {
  "action":"matchVenues",
  "venueName":name,
  "venueAddress":address,
  "venueType":type,
  "startDate":sdate, 
  "endDate":edate 
};

   var del;   
      $.each(data,function(index,element){

          if(element.length==0)
          {
            del=index;
      delete data[del];

          }

      })
    $("#loading").fadeIn();
    $.ajax({
      url:"api/groups_curl.php",
      type:"POST",
      data: data,
      success:function(response){
        var response = jQuery.parseJSON(response);
        // console.log("ajax-response");
         console.log(response);
        $("#tendencies").hide();
        if (!response.result.stats) { $("#loading").hide(); }
        if (response.result.stats.length!=0) {
          tendency(response.result.stats);
          if(window.updateCapable) {
          reDrawstarloader(response.result.stats);
          reDrawBigChart(response.result.stats);
          	} else {
			starloader(response.result.stats);
          	BigChart(response.result.stats);

          }

        }

if (response.result.venues.length == 0) {
  $("#chart_div1div").hide();
  $("#main_chartdiv").hide();

}
else{
  $("#chart_div1div").show();
  $("#notesdiv").show();
  $("#venandtend").show();

  $("#venandtend").show();

  

  $("#main_chartdiv").show();
}

    
localStorage.removeItem("responseVenueArray");
localStorage.setItem("responseVenueArray",JSON.stringify(response.result.venues));
   $("#easyPaginate").html("");
var serviceVenues = new Array();
var type;
    $.each(response.result.venues,function(index,element){
      if (element.venueType==null) { var typ = "-"; } else { var typ = element.venueType; }
      if (element.venueAddress==null) { var addr = "-"; } else { var addr = element.venueAddress; }
      if (type!=""&&type!=element.venueType) {
        type = "default";
      }
      else{
        type = element.venueType;
      }
    $.each(element.serviceVenues,function(svindex,svelement){
      serviceVenues.push(svelement);
    });
  $("#demo").append(`<li id="tr`+index+`" class="row matched-venue li-item">
              <div class="col-md-3 col-sm-3 col-12">
                   <p class="name" id="td`+index+`">`+element.venueName+`</p>
              </div>
              <div class="col-md-3 col-sm-3 col-12">
                             <p class="name">`+typ+`</p>
              </div>
              <div class="col-md-3 col-sm-3 col-12">
                 <p class="name">`+addr+`</p>
              </div>
               <div class="col-md-3 col-sm-3 col-12">
               
                   <p><span onclick="addasVenue(`+index+`);"><i class="fa fa-plus" aria-hidden="true"></i></span>
                   <span onclick="removefromgrp(`+index+`)"><i class="fa fa-minus" aria-hidden="true"></i></span></p>
               </div>
              </li>`)


      
});
$("#demo").cPager({
  // how many items per page
  pageSize: 8,

  // container ID
  pageid: "pager",
  // item class02

  itemClass: "li-item",
  pageIndex: 1


});
$("#savesearch").show();    
$("#group_name").attr("style","display:inline");


    $("#loading").show();
    var sdate = $("#startdate").val();
    var edate = $("#enddate").val();

    if (edate!="" || sdate!="") {
      var data = {action:"serviceVenueData",venueServices:serviceVenues,startDate:sdate,endDate:edate};   
    }else{
      var dates = $.parseJSON(getDates());
      sdate = dates.start;
      edate = dates.current;

      var data = {action:"serviceVenueData",venueServices:serviceVenues,startDate:sdate,endDate:edate};      
    }
    console.log("datatification================")
    console.log(data)
    var allReviews = new Array();
     $.ajax({
      url:"api/groups_curl.php",
      type:"POST",
      data: data,
      success:function(response){
       var allReviews = new Array();

        response = JSON.parse(response);
        console.log("response");
        console.log(response);

     var venueNames=$.parseJSON(localStorage.getItem("responseVenueArray"));
      var venuename=undefined;
    $.each(response.result.services,function(rindex,relement){
          $.each(venueNames,function(index2,element2){
              if(element2.serviceVenues==relement.venueServiceId)
              {     
                  venuename=element2.venueName
              }       
          });
      $.each(relement.reviews,function(irindex,irelement){
          irelement.venueName=venuename;
          allReviews.push(irelement);
      });
    
    });


    var maxlimit = $("#max_limit").val();
    if (allReviews.length<=maxlimit) {
      var lang = localStorage.getItem("lang");
      // console.log(allReviews)
      TextAnalysisDataCollection(type,lang,allReviews);
      showReviews(allReviews);

    }
    else{
     if (lang=="en") {
      $("#condmsg").html("This group is too large for topic modeling.");
    }else{
      $("#condmsg").html("Ce groupe est trop grand pour effectuer une analyse des avis.");
    }
    }

      }
    });
        
      },
      error:function(data){
        console.log(data)
      },async:true
 });
}

function showReviews(allReviews) {
  // console.log('someghing wrong')
  // console.log(allReviews)
  $("#reviewdiv").show();
  $('#reviewTable').DataTable().destroy();
  $(".list").html('');
        // console.log("allReviews");
    var revertButton = '';
    var transButton = '';
    var ratingData= {
      count:0,
      totalRating:0,
      sentence:[]
    }

    allReviews.sort(function(a,b){
      var dateA=new Date(a.date), dateB=new Date(b.date)
        return dateB-dateA //sort by date descending
    })

    switch(localStorage.getItem("lang").toLowerCase())
    {
      case 'en' : 
                  transButton = "Translate";
                  revertButton="Revert";
                  ratingData.sentence[0]=' comments on '
             	  ratingData.sentence[1]= ' reviews ';
                  break;
    
      case 'fr' : transButton = "Traduction";
                   ratingData.sentence[0]=' commentaires sur ';
              	   ratingData.sentence[1]=' avis ';
                  revertButton = "Revenir";
                  break;
    
      default :   transButton = "Translate";
                  ratingData.sentence[0]=' comments on '
             	  ratingData.sentence[1]= ' reviews ';
                  revertButton="Revert";
                   break;
    }
    

    $.each(allReviews,function(index1,element1){
                var note= getStar(element1.note);
                var sentence= trimByWord(element1.full,index1);
                var date = getFormattedDate(element1.date)
                 ratingData.totalRating=ratingData.totalRating+1;
                if(sentence.length > 0)
                {
                  ratingData.count=ratingData.count+1;

                }
                  if(sentence.length > 0)
                {
                   $(".list").append(`
      <li class="venue-review ">
            <div class="venue-logo">
              
            </div>
             <div class="venue-title">
             <h3>`+element1.venueName+`</h3>
      <div class="star-rating">
       `+note+`
      </div>
             </div>
             <div class="clearfix"></div>
      <div class="venue-info">
        <p class="diftitle" id="defaulttitle`+index1+`"><b>`+element1.title+`</b></p>
        <p class="title-en" style="display:none;" id="transtitle`+index1+`"><b>`+element1['title-en']+`</b></p>
        <p class="disdate">`+date+`</p>
        <p id="short`+index1+`">`+sentence+`</p>
        <p style="display:none;" id="default`+index1+`" class="full">`+element1.full+'<span class="less" onclick="less('+index1+')">..less</span>'+`</p>
        <p style="display:none;" id="translate`+index1+`" class="full-en">`+element1['full-en']+`</p>
        
        <p>
        <button id="transb`+index1+`" onclick="transl_call(`+index1+`,`+element1['title-en'].length+`);"
        class="btn btn-green pull-right">`+transButton+`</button>
        <button  style="display:none;" id='rever`+index1+`' onclick="revert_call(`+index1+`);"
        class="btn default pull-right">`+revertButton+`</button>
        </p>
      </div>
      </li>`)
                if(element1['full-en']=="")
                {
                  var btn = "#transb"+index1;
                  $(btn).hide();
                }
                }
           

      
  });

var monkeyList = new List('test-list', {
  valueNames: ['disdate','diftitle','title-en','full','full-en'],
  page: 10,
  pagination: true
});
var head = ratingData.count+ratingData.sentence[0]+ratingData.totalRating+ ratingData.sentence[1];
$(".reviewsHeading").html(head)
    $("#loading").hide();

}


function tendency(tendencydata){

        $("#tendencies").show();
        $("#tendencybox").show();
        $("#tendencies").html("");
    $.each(tendencydata,function(index,element){
        var percent= (element.venueMonthlyTendency*100)/5;

        if(element.venueMonthlyTendency<=3.5)
        {
          var color1="#C73729"; }
        else{
          var color1="#67BFB3"; }
          var month = element.yearMonth.split("-");

          var datahtml = `<div class="value-chart">
                        <div class="box-chart">
                          <h4 class="value">`+element.venueMonthlyTendency+`</h4>
                          <div style="height: `+percent+`%; background-color:`+color1+`;" class="box-chart-inner">
                          </div>
                        </div>
                        <h5>`+element.yearMonth+`</h5>`

$("#tendencies").append(datahtml);

    });
$("#tendencies").show();

}


function starloader(venueDetailData)
{
  $("#chart_div1div").fadeIn();

if(venueDetailData.length>0)
{

console.log("data here")
$("#chart_div1div").fadeIn();

var venueNotes1Count=0;
var venueNotes2Count=0;
var venueNotes3Count=0;
var venueNotes4Count=0;
var venueNotes5Count=0;

if(venueDetailData.length!==0)
{
  var NoteCount = venueDetailData;
}


$.each(NoteCount,function(index,element){
 venueNotes1Count=parseInt(element.venueNotes1Count+venueNotes1Count);
 venueNotes2Count=parseInt(element.venueNotes1Count+venueNotes1Count);
 venueNotes3Count=parseInt(element.venueNotes3Count+venueNotes3Count);
 venueNotes4Count=parseInt(element.venueNotes4Count+venueNotes4Count);
 venueNotes5Count=parseInt(element.venueNotes5Count+venueNotes5Count);

});

var chartData = {
   type: 'horizontalBar',
   data: {
      labels: ["1(★)","2(★)","3(★)","4(★)","5(★)"],
    datasets: [
        {
            label: "Number of Notes",
            backgroundColor: "#6E388A",
            data: [venueNotes1Count,venueNotes2Count,venueNotes3Count,venueNotes4Count,venueNotes5Count]
        }
       
    ]
   },
   options: {
        barValueSpacing: 20,
        scales: {
            yAxes: [{
                ticks: {
                    min: 0,
                }
            }]
        }
    }
} 

window.canvas_chart_div1 = document.getElementById('chart_div1');
window.myChart_chart_div1 = new Chart(window.canvas_chart_div1, chartData);

  $("#chart_div1_error").html('');
  $("#chart_div1").show();
}
else{

  $("#chart_div1_error").html(dataMessage);
  $("#chart_div1").hide();

}

}

function addasVenue(venueArrayId){
  $("#loading").show();
// console.log("venueArrayId"+venueArrayId);
var responseVenueArray = JSON.parse(localStorage.getItem("responseVenueArray"));
// console.log(responseVenueArray);
var venuedata;
$.each( responseVenueArray, function( key, value ) {
  if (key==venueArrayId) {
    venuedata = value;
  }
});
 console.log("venuedata");
 console.log(venuedata.venueType);
$("#venueName").val(venuedata.venueName);
$("#venueTypes").val(venuedata.venueType);

$.ajax({
      url:"api/groups_curl.php",
      type:"POST",
      data:{"action":"serviceVenueData","venueServices":venuedata.serviceVenues},
      success:function(response){
          $("#addasVenue").modal();
        var response = JSON.parse(response);
        // console.log(response);
        var serviceinsert;
       $.each(response.result.services, function( key, value ) {

        $.ajax({

                    url:"api/add_venue_curl.php",
                    type:"POST",
                    data:{"action":"getServices"},
                    success:function(data){

                        obj= $.parseJSON(data);

                        // console.log(data);
                        $.each(obj.services, function(index, element) 
                        {
                          if (element.id == value.serviceId) {
                            // console.log("met");
var deletiid= Math.floor(1000 + Math.random() * 9000);
serviceinsert+=`<tr><td data-value=`+value.serviceVenueURL+` id=`+value.serviceId+`>
<img width="50" height="50" src="https://api.spella.com/services/logos-mini/`+element.icon+`" alt="" /></td>
<td  id=`+deletiid+`>`+value.serviceVenueURL+`</td></tr>`;
$("#save_button").attr("style","display:inline;");
                          }
// <td><span class="red">
// <a onclick="deletion(`+deletiid+`);" >
// <i  class='fas fa-trash-alt'></i>
// </a></span></td>
          
                      });

                    },error:function(){

                        alert("Not responding error, refresh page ");
                    },
                    async:false
                });
        $("#pageTable").html(serviceinsert);
  });
  $("#loading").hide();
 }
});




}

function BigChart(data)
{
// console.log("==================BigChart")
// console.log(data)

  var negative=0;
  var positive=0;
  var chartValues = [];

    var htitle1 = htitle2 = xaxislabel = yaxislabel = "";
    switch(localStorage.getItem('lang').toLowerCase())
    {
      case 'en' : htitle1='positive'; htitle2 = 'negative'; xaxislabel= 'count'; yaxislabel='month';
            break;
      case 'fr' : htitle1='positif'; htitle2 = 'négatif'; xaxislabel= 'compter'; yaxislabel='mois';
            break;
    }

    let label = [];
    let posi = [];
    let nega =[];

if(data.length!==0)
{
  
	var using= data;
	  $.each(using,function(index,element){
	    //let pair = [];
	    label.push(element.yearMonth);
	    nega.push(element.venueNegativeNotesCount);
	    posi.push(element.venuePositiveNotesCount);
	    //chartValues.push(pair);

	  });


     var chartData = {
       type: 'horizontalBar',
       data: {
          labels: label,
        datasets: [
            {
                label: htitle1,
                backgroundColor: "#67BFB3",
                data: posi,
            },
            {
                label: htitle2,
                backgroundColor: "#C73729",
                data: nega,
            }
        ]
       },
       options: {
            barValueSpacing: 20,
            scales: {
                xAxes: [{
                  barPercentage: 0.4,
                    ticks: {
                        min: 0,
                    },
                    scaleLabel: {
                   display: true,
                   labelString: xaxislabel
      }
                }],
                yAxes: [{
          scaleLabel: {
          display: true,
          labelString: yaxislabel
            }
             }]
            }
        }
    } 

window.canvas_main_chart = document.getElementById('main_chart');
window.myChart_main_chart = new Chart(window.canvas_main_chart, chartData);
$("#main_chartdiv").fadeIn();
$("#main_chart_error").html('');
 } else {
 	$("#main_chart_error").html(dataMessage);
 	$("#main_chartdiv").hide();

 }

}

//=========================save button click=============================================================

$("#save_button").click(function(){

// alert()
$("#loading").fadeIn();


  var venueType= $("#venueTypes").val();
  var venue=$("#venueName").val();
  var userData= getuserData();
  // console.log("userdata");
  // console.log(userData);
  var userObj = $.parseJSON(userData);
  var venueId=guidGenerator();
  var user_id = userObj.user_id;

  var i=0;
  var services=[];
  $('#pageContent > tbody  > tr td:first-child').each(function() {

    var id =$(this).attr('id');
    var url= $(this).attr('data-value');
    var serviceVenueUUID =guidGenerator();
    services[i]={
           "serviceVenueUUID":serviceVenueUUID,
           "serviceId":id,
           "serviceVenueURL":url  ,
           "serviceVenueStatus":"valid",
           "serviceEnabled": true 
          }
    i++;      

  });
  
  
  $.ajax({

    url:"api/add_venue_curl.php",
    type:"POST",
    data:{
          "action": "createVenue",
          "userId": user_id,
          "venueName": venue,
          "venueId":  venueId,
          "venueType": venueType,
          "venueServices": services
    },
    success:function(data){

        // console.log("=======venue creation"+data);
        // console.log("creation ends");
        var good=[];
        var result= $.parseJSON(data);
        if(result.status==='success')
        {
          $.ajax({

            url:"api/add_venue_curl.php",
            type:"POST",
            data:{
                "action":"validateVenue",
                "userId":user_id,
                "venueId": venueId,
                "services":services
                },
            success:function(data){
            // console.log(data);
            var obj_val= $.parseJSON(data);
            if(obj_val.status==='success')
            {
              // console.log("validation success");
              updateGraph(user_id,venueId,services);
            }

            },error(data){
              // console.log(data);
              var obj= $.parseJSON(data);
              alert(data.message);
            },async:false

          });
        }
    },error:function(){

      alert("VenueAddition Faliure,Refreshing page");
      loaction.reload();
    },async:false


});


});//save button operation ends


function guidGenerator() {
var S4 = function() {
return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
                    };
 return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
                }


function updateGraph(userId,venueId,services)
{


  if(localStorage.getItem("venueData"))
  {
  var venueData = localStorage.getItem("venueData");

  }
  else{
    alert("Oups données errr, veuillez actualiser")
  }
  var good=[];
  var arr1=[];
  var arr=[];

         if(venueData==Number(0)||venueData===null)
         { 

          arr1={
              "venueId":venueId,
              "venueCompetitors": []
            }
          good.push(arr1);  
          }
          else{
            var usableList=$.parseJSON(venueData);
            $.each(usableList.result.graph,function(index,element)
            {

            arr={
                  "venueId":element.venueId,
                  "venueCompetitors":element.venueCompetitors
                  }
                good.push(arr);

            });    
          arr1={
              "venueId":venueId,
              "venueCompetitors": []
            }
          good.push(arr1);  
          }


        var newArray = good.filter(value => Object.keys(value).length !== 0);
  

              var sentable=JSON.stringify(newArray);


                    $.ajax({

                      url: "api/home_venue_list_update_curl.php",
                      type:"POST",

                      data:{ 
                        "action":"updateGraph",
                        "userId":userId,
                        "graph":sentable
                        },
                      success:function(data)
                      {
                      
                          updateLocalStorage();
                          alert("Group Added As Venue");
                          $("#addasVenue").modal("hide");
                        
                      },error:function(data)
                      { 

                        alert("Error,pleaese refresh or contact adminstrator if problem still persists");

                      },
                      async:false

                    });

      
}


function getuserData()
{
  var userData;

  $.ajax({

    url:"home.php",
    type:"POST",
    data:{"request":"getdata"},
    success:function(data){
      userData= data;
    },error:function(){

      alert("Erreur Lors du traitement de la commande, veuillez actualiser la page");

    },
    async:false
  });

  return userData;
}


function  TextAnalysisDataCollection(type,lang,allReviews)
{

  var venueType = type;
  // console.log("TextAnalysis begins")
  var reviews = allReviews;
    var language=lang;
        $.ajax({
      url: "api/text_analysis_curl.php",
      type: "POST",
      data :{"action":"venueType","type":venueType},
      success:function(data){

        localStorage.removeItem("Dictionary");
        localStorage.setItem("Dictionary",data);
        var lang=language.toLowerCase();
        // console.log(lang)
        text_analysis(reviews,lang,data);
      },error:function(data)
      {
        alert("No Data, Please Refresh the page, if the problem still persist contact administrator");
      },async:false
    });

}

function text_analysis(reviewsF,lang,Dictionary)
{



  var DictionaryD=$.parseJSON(Dictionary);
  var innerchecker;
  var counter=[];
  var i=0;
  var countOfInnerCheckerNeg=[];
  var countOfInnerCheckerPos=[];
  var countOfInnerChecker=[];
  var j=0;
  var ReviewArray=[];
  var negCount=[];
  var posCount=[];
  var k=0;


        $.each(DictionaryD.topics,function(index3,element3){
              var result;
              var checkedNew;

        //creating a good checking variable
        if(element3.locales.hasOwnProperty(lang+'-short'))
        {
          var splitting_var=lang+'-short';

            var splitter=element3.locales[splitting_var].split(" ")
            checkedNew = splitter[0].split('.').join("");
        }
        else{
          checkedNew= element3.locales[lang];
        }

        // console.log(checkedNew)

      checkedNew=checkedNew.toLowerCase();
      counter[i,checkedNew]=0;
      negCount[i,checkedNew]=0;
      posCount[i,checkedNew]=0;
      $.each(element3.keys,function(index4,element4)
      {
          k=0;
          innerchecker= element4.locales[lang];
          innerchecker=innerchecker.toLowerCase();
          countOfInnerChecker[j,innerchecker]=0;
          countOfInnerCheckerNeg[j,innerchecker]=0;
          countOfInnerCheckerPos[j,innerchecker]=0;

          $.each(reviewsF,function (index2,element2) {
      
                  var sentense = element2.title+" "+element2.full;
                  var res=sentense.toLowerCase();   

            result=countInstances(res,innerchecker);
            counter[i,checkedNew]=parseInt(counter[i,checkedNew]+result);

            countOfInnerChecker[j,innerchecker]=parseInt(countOfInnerChecker[j,innerchecker]+result);
            if(element2.note<3.5)
            {
            countOfInnerCheckerNeg[j,innerchecker]=parseInt(countOfInnerCheckerNeg[j,innerchecker]+result);
            negCount[i,checkedNew]=parseInt(negCount[i,checkedNew]+result);
            }
            else{
            posCount[i,checkedNew]=parseInt(posCount[i,checkedNew]+result);
            countOfInnerCheckerPos[j,innerchecker]=parseInt(countOfInnerCheckerPos[j,innerchecker]+result);
            } 

          });

          j++;

        });

i++;
    });

console.log("reviewsing.............")
console.log(reviewsF.length)

if(reviewsF.length>0)
{

	if(window.updateCapable){
	
	console.log('updating the charts');
    
    RedrawRepartition(reviewsF,counter,lang)
    RedrawImpressionGraph(reviewsF,negCount,posCount,lang);
    RedrawWordImpression(reviewsF,countOfInnerCheckerPos,countOfInnerCheckerNeg,lang);


	} else {
  window.updateCapable = true;
  Repartition(reviewsF,counter,lang);
  ImpressionGraph(reviewsF,negCount,posCount,lang);
  WordImpressionGraph(reviewsF,countOfInnerCheckerPos,countOfInnerCheckerNeg,lang);
  $("#topic").fadeIn();
  $("#topicImpression").fadeIn();
  $("#wordImpression").fadeIn();
  $("#reviewdiv").fadeIn();
	}

}
else{
  console.log("sorry")
    $("#topic").fadeOut();
  $("#topicImpression").fadeOut();
  $("#reviewdiv").fadeOut();
  $("#wordImpression").fadeOut();
}



}

function Repartition(reviewsF,data,lang)
{

  var using=data;
  var language=lang;
  var sortTemp =[];
    for(let key in using) {
      sortTemp.push({
        key: key,
        value: using[key]
      })
    } 

  sortTemp = sortTemp.sort(function(a,b){
    return b.value - a.value
  })

     
    let word = [];
    let occ = [];

 $.each(sortTemp,function(index,element) {
  word.push(element.key)
  occ.push(element.value)
})





   var chartData = {
   type: 'horizontalBar',
   data: {
      labels: word,
    datasets: [
        {
            label: mainTopiclabel,
            backgroundColor: "#6E388A",
            data: occ
        }
       
    ]
   },
   options: {
        barValueSpacing: 20,
        scales: {
            yAxes: [{
                ticks: {
                    min: 0,
                }
            }]
        }
    }
} 

window.canvas_barchart_material = document.getElementById('barchart_material');
window.myChart_barchart_material = new Chart(window.canvas_barchart_material, chartData);
// MainTopicLoadGroup(reviewsF,value,language);

window.canvas_barchart_material.onclick = function(evt) {
   var activePoint = window.myChart_barchart_material.getElementAtEvent(evt)[0];
   var data = activePoint._chart.data;
   var datasetIndex = activePoint._datasetIndex;
  var mainlabel=activePoint._model['label'];
   var label = data.datasets[datasetIndex].label;
   var value = data.datasets[datasetIndex].data[activePoint._index];
   // alert(mainlabel+' : '+label+' - '+value);
  // console.log(value)
   // MainTopicLoad(mainlabel,language,venueDetailData);
   MainTopicLoadGroup(reviewsF,mainlabel,language);
   //console.log(mainlabel);
};

  
}

function ImpressionGraph(reviewsF,neg,pos,lang)
{
  var negative=neg;
  var positive=pos;
  var language=lang;
    var chartValues = [];
   // chartValues.push(['Word', 'negative','positive']);
    var htitle1 = htitle2 = xaxislabel = yaxislabel = "";
    switch(localStorage.getItem('lang').toLowerCase())
    {
      case 'en' : htitle1='positive'; htitle2 = 'negative'; xaxislabel= 'count'; yaxislabel='word';
            break;
      case 'fr' : htitle1='positif'; htitle2 = 'négatif'; xaxislabel= 'compter'; yaxislabel='mot';
            break;
    }
    
     let label = [];
    let posi = [];
    let nega =[];

var negPosCount = getCountOfPosNeg(negative,positive);
    var sortedData = []; 
    if(negPosCount.pos_count > 0 && negPosCount.neg_count > 0) {
     sortedData = multiSortForImpression(negative,positive);

    } else {
       if(negPosCount.pos_count > 0 ) {
             sortedData = singularSort('pos',positive);
       } else {
            sortedData = singularSort('neg',negative)
       }
    }
    
    $.each(sortedData,function(index,element) {
    label.push(element.label);  
    posi.push(element.pos);
    nega.push(element.neg);
    })
if(reviewsF.length > 0) {
	 var chartData = {
       type: 'horizontalBar',
       data: {
          labels: label,
        datasets: [
            {
                label: htitle1,
                backgroundColor: "#67BFB3",
                data: posi,
            },
            {
                label: htitle2,
                backgroundColor: "#C73729",
                data: nega,
            }
        ]
       },
       options: {
            barValueSpacing: 20,
            scales: {
                xAxes: [{
                  barPercentage: 0.4,
                    ticks: {
                        min: 0,
                    },
                    scaleLabel: {
                   display: true,
                   labelString: xaxislabel
      }
                }],
                yAxes: [{
          scaleLabel: {
          display: true,
          labelString: yaxislabel
            }
             }]
            }
        }
    } 

window.canvas_new_chart = document.getElementById('new_chart');
window.myChart_new_chart = new Chart(window.canvas_new_chart, chartData);

window.canvas_new_chart.onclick = function(evt) {
   var activePoint = window.myChart_new_chart.getElementAtEvent(evt)[0];
   var data = activePoint._chart.data;
   var datasetIndex = activePoint._datasetIndex;
  var mainlabel=activePoint._model['label'];
   var label = data.datasets[datasetIndex].label;
   var value = data.datasets[datasetIndex].data[activePoint._index];
   var message = [];
   message.push(mainlabel)
   message.push(label)

   // console.log(message)
   // handleMainTopicImpression(message.toString(),language,venueDetailData)
   handleMainTopicImpressionGroup(reviewsF,message.toString(),language);
};

 $("#new_chart").show();
 $("#new_chart_error").html(''); 

} else {

	$("#new_chart").hide();
	$("#new_chart_error").html(dataMessage);

}
    

}

function WordImpressionGraph(reviewsF,pos,neg,lang)
{ 

  var negative=neg;
  var positive=pos;
  var chartValues=[];
     // chartValues.push(['Word', 'negative','positive']);

       var htitle1 = htitle2 = xaxislabel = yaxislabel = "";
    switch(localStorage.getItem('lang').toLowerCase())
    {
      case 'en' : htitle1='positive'; htitle2 = 'negative'; xaxislabel= 'count'; yaxislabel='word';
            break;
      case 'fr' : htitle1='positif'; htitle2 = 'négatif'; xaxislabel= 'compter'; yaxislabel='mot';
            break;
    }

     let label = [];
    let posi = [];
    let nega =[];

 var negPosCount = getCountOfPosNeg(negative,positive);
    var sortedData = []; 
    if(negPosCount.pos_count > 0 && negPosCount.neg_count > 0) {
     sortedData = multiSort(negative,positive);

    } else {
       if(negPosCount.pos_count > 0 ) {
             sortedData = singularSort('pos',positive);
       } else {
            sortedData = singularSort('neg',negative)
       }
    }
    

    $.each(sortedData,function(index,element) {
    label.push(element.label);  
    posi.push(element.pos);
    nega.push(element.neg);
    })

if(reviewsF.length > 0) {

	 // console.log(chartValues);
      var chartData = {
       type: 'horizontalBar',
       data: {
          labels: label,
        datasets: [
            {
                label: htitle1,
                backgroundColor: "#67BFB3",
                data: posi,
            },
            {
                label: htitle2,
                backgroundColor: "#C73729",
                data: nega,
            }
        ]
       },
       options: {
       	maintainAspectRatio: false,
            barValueSpacing: 20,
            scales: {
                xAxes: [{
                  barPercentage: 0.4,
                    ticks: {
                        min: 0,
                    },
                    scaleLabel: {
                   display: true,
                   labelString: xaxislabel
                     }
                }],
                yAxes: [{
                scaleLabel: {
                display: true,
                labelString: yaxislabel
                  }
               }]
          }
        }
    } 

 window.canvas_WordImpressionGraph = document.getElementById('WordImpressionGraph');
 window.myChart_WordImpressionGraph = new Chart(window.canvas_WordImpressionGraph, chartData);

 window.canvas_WordImpressionGraph.onclick = function(evt) {
   var activePoint = window.myChart_WordImpressionGraph.getElementAtEvent(evt)[0];
   var data = activePoint._chart.data;
   var datasetIndex = activePoint._datasetIndex;
  var mainlabel=activePoint._model['label'];
   var label = data.datasets[datasetIndex].label;
   var value = data.datasets[datasetIndex].data[activePoint._index];
   var message = [];
   message.push(mainlabel)
   message.push(label)

   WordImpressionGraphLoadGroup(reviewsF,message.toString(),lang);

};
  $("#WordImpressionGraph").show();
  var height = label.length * 38;
  $("#WordImpressionGraph_container").css("height",height);
    window.addEventListener('resize', function () {
               window.myChart_WordImpressionGraph.resize()
            });


} else {
	$("#WordImpressionGraph_error").html(dataMessage)
    $("#WordImpressionGraph").hide()
}
   



}



function countInstances(string, word) {
   return string.split(word).length - 1;
}

function updateLocalStorage()
{
  var userData= getuserData();
  var userObj = $.parseJSON(userData);
  var venueId=guidGenerator();
  var user_id = userObj.user_id;
  $.ajax({

           url: "api/getVenue_curl.php",
           type: "POST",
           data: {
             "action": "getVenues",
             "userId":user_id,
             "graphVersion":  null   
           },
           success:function(response)
           {
            console.log(response)
             if(response==Number(0))
            {
              $("#loading").fadeOut();
                localStorage.removeItem("venueData");
                localStorage.setItem("venueData",Number(response));
                venueData=response;
                $("#NoVenueData").html("Add Venues")
            }
            else
            {
              $("#loading").fadeOut();
            localStorage.removeItem("venueData");
            localStorage.setItem("venueData",response);
            venuedata = response;
            console.log(venuedata)
            }
            },
          error:function(){
            alert("Aucune donnée reçue, rafraîchissante")
            location.reload();
          },
          async:false
        });
}


 function getDates()
{
  var dates;
  $.ajax({
    url:"api/venue_details_curl.php",
    type:"POST",
    data:{"action":"getDates"},
    success:function(data) {  
       dates=data;
       console.log(data);
        },error:function(data)
        {
          console.log(data);
        },
        async: false
       });
  return dates;
}

$("#staticVenueSave").on("click",function(){

var name = $("#staticName").val();
  if(name.length===0){
    $("#staticNameError").html("Enter Name").css('color','red')
    $("#staticNameError").show();
    return;
  }else{
    $("#staticNameError").hide();
  }
var venues = $('#multi').multi_select('getSelectedValues')
if(venues.length === 0){
  $("#venueNameError").html("Select any venues").css('color','red')
  $("#venueNameError").show();
  return;
}else{
  $("#venueNameError").hide();
}

var groups = [];
var staticGroupObject = {
                          "groupKind":"static",
                          "groupDisplayName":name,
                          "groupVenues":venues };

groups.push(staticGroupObject);
$('#sortable li').each(function(i)
{
  var groupName = $(this).attr('groupname');
  var groupDisplayName = $(this).attr('groupdisplayname');
  var groupType = $(this).attr('grouptype');
  var groupAddress = $(this).attr('groupaddress');
  var groupVenues = $(this).attr('groupvenues');

      if(groupVenues !== 'none'){
        var splitedVenues = groupVenues.split(",")
        console.log(splitedVenues)
        var group = {
    "groupKind":"static",
    "groupDisplayName":groupDisplayName.trim(),
    "groupVenues":splitedVenues
                  };
      }else {
    var group = {
    "groupName":groupName.trim(),
    "groupKind":"dynamic",
    "groupAddress":groupAddress.trim(),
    "groupType":groupType.trim(),
    "groupDisplayName":groupDisplayName.trim()
                  };
      }    

   groups.push(group);
});

$.ajax({
      url:"api/groups_curl.php",
      type:"POST",
      data: {
        action : "saveGroups",
        groups : groups
      },
      success:function(response){
        location.reload();
      }
  });

})