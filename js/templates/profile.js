  var lang = localStorage.getItem("lang");

$( document ).ready(function() {


if (lang=="FR") {
  $('#language').val("FR");
}
else if(lang=="EN"){
  $('#language').val("EN");
}
      
});
       $(function(){    
        $.ajax({
        url: 'Json/newCountries.json',
        dataType: 'json',    
        success: function(data){


        	var here = '';
        	switch(lang){
        		case "FR" : here = 'fr';
        					break;
        		case "EN" : here = 'name';
        					break;
        		default : here = "name";
        					break; 
        	}
    data.sort(function (a, b) {
     return a[here].localeCompare(b[here]);
    });

            $.each(data, function(key, val){
            $("#country").append('<option value='+val.code+' data-dial_code='+val.dial_code+'>' + val[here] +'</option>');
            
        });
        }
        });

        //getting values to fill in the user fields        
        $.ajax({
        url: 'api/profile_curl.php',
        type: "POST",
        data:{"action":"getSession"}, 
        success: function(data)
        {
          console.log("sessiondata");
          console.log(data);
          var data2=null;
          var obj=$.parseJSON(data);
          console.log(obj)
          $("#username").html(obj['name']);
          $("#name").val(obj['name']);
          $("#email").val(obj['email']);
          if (obj.pushActive==true || obj.pushActive=="true") {
            $("#radio_7").attr("checked",true);
          }
          else{
            $("#radio_9").attr("checked",true);
          }


          if (obj.reportWeekly==true || obj.reportWeekly=="true") {
            $("#radiow_7").attr("checked",true);
          }
          else{
            $("#radiow_9").attr("checked",true);
          }


          if (obj.reportDaily==true || obj.reportDaily=="true") {
            $("#radiod_7").attr("checked",true);
          }
          else{
            $("#radiod_9").attr("checked",true);
          }

          $("#country").val(obj['countryCode']);
          $("#push").val(obj['pushActive'].toString());
          // var value= $('#country').find(':selected').data('dial_code');
          // $('#prefix').val(value);
          // $("#phnumber").val(obj['user_cellphone'].replace(value,""));
          $("#phnumber").val(obj['user_cellphone']);
          $("#report_range").val(obj.reportRange);
          $("#email_on_weekday").val(obj.reportWeekDay);
          $("#daily_alert_threshold").val(obj.reportThreshold);

          localStorage.setItem("name",obj['name']);
          localStorage.setItem("email",obj['email']);
          localStorage.setItem("phnumber",obj['user_cellphone']);
          localStorage.setItem("country",obj['countryCode']);
          localStorage.setItem("push",obj['pushActive']);

          $("#loading").fadeOut();
        }
        });
        
          

        });


          var name_val= false;
          var email_val= false;
          var ph_val=false;
          var pass_val=false;
          var country_val=false;
          // var prefix_val=false;
          var phnumber_val=false;

          //fields are not editable by default
          $("#name").prop("readonly",true);
          $("#email").prop("readonly",true);
          $("#password").prop("readonly",true);
          $("#phnumber").prop("readonly",true);
          $("#prefix").prop("readonly",true);
          $('#country').attr("disabled", true);
          $("input[type=radio]").attr("disabled",true);
          $("#email_on_weekday").attr("disabled",true);
          $("#report_range").attr("disabled",true);
          $("#daily_alert_threshold").attr("disabled",true);

          

          //upon calling the function fields become editable
    function edit(){
          $("#name").prop("readonly",false);
          $("#email").prop("readonly",false);
          $("#password").prop("readonly",false);
          $("#phnumber").prop("readonly",false);
          // $("#prefix").prop("readonly",false);
          $("#country").prop("readonly",false);
          $('#country').attr("disabled", false);
          $('#push').attr("disabled", false);
          $(".save_button").fadeIn();
          $(".cancel_button").fadeIn();
          $(".modify").fadeOut(500);
          $("input[type=radio]").attr("disabled",false);
          $("#email_on_weekday").attr("disabled",false);
          $("#report_range").attr("disabled",false);
          $("#daily_alert_threshold").attr("disabled",false);
         }
         function password_check(){
          var pass = $("#password").val();
          var here='';
          if (pass == "" || pass.length<6) {
              switch(lang){
            case "EN" : here = "Password should be equal to or more than 6 characters";
                  break;
            case "FR" : here = 'Le mot de passe doit être égal ou supérieur à 6 caractères.';
                  break;
            default : here = "Password should be equal to or more than 6 characters";
                  break; 
          }
            $("#pass_up").html(here).css("color","red");
            pass_val = true;
          }else{
            pass_val = false;
            $("#pas_up").html('');
          }
         }
         function prefix_inser()
        {
          var value= $('#country').find(':selected').data('dial_code');
          $('#phnumber').val(value);
        }
                     function check_email()
                     {
                        var email=$("#email").val();
                        var len=$("#email").val().length;
                        email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
           var here='';
           switch(lang){
            case "EN" : here = "Please provide your Email";
                  break;
            case "FR" : here = "S'il vous plaît fournir votre email";
                  break;
            default : here = "Please provide your Email";
                  break; 
          }

                        if (len<=0)
                        {
                        $("#email").attr("placeholder", here);
                        $("#email").show();
                        email_val=true;
                        }  
                        else{ 
                        if(!email_regex.test(email))
                        {
                        $("#email_up").html(here).css('color', 'red');
                        $("#email_up").show();
                        email_val=true;
                        } 
                        else{
                        $("#email_up").hide();
                        email_val=false;

                        }  
                        }        
                     }

                    function check_name()
                    {
                      var here='';
           switch(lang){
            case "EN" : here = "Please provide your Name";
                  break;
            case "FR" : here = "S'il vous plaît fournir votre nom";
                  break;
            default : here = "Please provide your Name";
                  break; 
          }

                      var user= $("#name").val().length;

                      if (user<=0)
                      {
                      $("#name").attr("placeholder", here);
                      $("#name").show();
                      name_val=true;
                      } 
                      else 
                      {
                      name_val=false;
  
                      }       
                    }

                    
                      function check_phNumber()
                      {
                          ph= $("#phnumber").val();
                  var here='';
           switch(lang){
            case "EN" : here = "Enter Correct PhoneNumber";
                  break;
            case "FR" : here = "Entrez le numéro de téléphone correct";
                  break;
            default : here = "Enter Correct PhoneNumber";
                  break; 
          }
                          	$.ajax({
                          		url: "api/phonetest_curl.php",
                          		type: "POST",
                          		data:{"phone_number":ph},
                          		success:function (data) {
                          			console.log(data)
                          			data=$.parseJSON(data)
								if(data.hasOwnProperty('country_code')){
                        		$("#ph_up").hide();
								}
								else{
								$("#ph_up").html(here).css('color', 'red');
		                        $("#ph_up").show();
		                        phnumber_val=true;
								}                          		
							},async:false
                          	});

                      }                    

                    function check_prefix()
                      {
                        var intRegex = /[+0-9]$/;
                        prefix= $("#prefix").val();
                        p_len= $("#prefix").val().length;
                        
                        if(p_len<=0)
                        {
                        $("#ph_up").html("Prefix Necessary").css('color', 'red');
                        $("#ph_up").show();
                        prefix_val=true;  
                        }
                        else if(!intRegex.test(prefix))
                        {
                          $("#ph_up").html("No alphabets allowed").css('color', 'red');
                          $("#ph_up").show();
                          prefix_val=true;  
                        }
                        else{
                            $("#ph_up").hide();
                            prefix_val=false;  

                        }
                      }

                        function check_country()
                        {
                            country=$("#country").val();
                            if(country=="")
                            {
                                $("#country_up").html("select your country").css('color', 'red');
                                $("#country_up").show();
                                country_val=true;
                            }
                            else{
                                $("#country_up").hide();
                                country_val=false;

                            }
                        }

                      function cancel()
                      {
                                  $("#name").val(localStorage.getItem("name"));
                                  $("#email").val(localStorage.getItem("email"));
                                  $("#phnumber").val(localStorage.getItem("phnumber"));
                                  $("#country").val(localStorage.getItem("country"));
                                  $("#push").val(localStorage.getItem("push"));
                                  $("#password").val("");
                                  $("#name").prop("readonly",true);
                                  $("#email").prop("readonly",true);
                                  $("#password").prop("readonly",true);
                                  $("#phnumber").prop("readonly",true);
                                  // $("#prefix").prop("readonly",true);
                                  $("#country").attr("disabled", true);
                                  $("#push").attr("disabled",true);
                                  $(".modify").fadeIn("hidden");      
                                  $(".save_button").fadeOut();
                                  $(".cancel_button").fadeOut(); 
                                  $("input[type=radio]").attr("disabled",true);
          $("#email_on_weekday").attr("disabled",true);
          $("#report_range").attr("disabled",true);
          $("#daily_alert_threshold").attr("disabled",true);
                      }
                      
         function save()
         {
            $("#loading").fadeIn();
            check_name();
            check_email();
            check_phNumber();
            // check_prefix();
            check_country();
            password_check();

    


         var name=$("#name").val();
         var pass=$("#password").val();
         var country=$("#country").val();
         var prefix=$("#prefix").val();
         var phnumber=$("#phnumber").val();
         var phnumber= phnumber.replace(prefix,"");
         var email= $("#email").val();
         var language = $('#language').find(':selected').val();

         var reportThreshold = $('#daily_alert_threshold').find(':selected').val();
         var reportWeekDay = $('#email_on_weekday').find(':selected').val();
         var reportRange = $('#report_range').find(':selected').val();

         if (document.getElementById('radio_7').checked) {
          var push = 'true';
         }
         else{
          var push = 'false';
         }

         if (document.getElementById('radiod_7').checked) {
          var reportDaily = 'true';
         }
         else{
          var reportDaily = 'false';
         }

         if (document.getElementById('radiow_7').checked) {
          var reportWeekly = 'true';
         }
         else{
          var reportWeekly = 'false';
         }


          if(name_val==false&&email_val==false&&pass_val==false&&country_val==false&&phnumber_val==false)
          {
          	console.log("yes")
          	 var sendingData = {
              action:"save",
              name:name,
              email:email,
              password:pass,
              country:country,
              phone:phnumber,
              pushActive:push,
              preferredLanguage:language,
              reportThreshold: reportThreshold,
              reportDaily: reportDaily, 
              reportWeekly: reportWeekly, 
              reportWeekDay: reportWeekDay, 
              reportRange: reportRange
                };

              $.ajax({
              url: "api/profile_curl.php",
              type: "POST",
              data:sendingData,
              success:function(data){
		         localStorage.removeItem("lang");
		         localStorage.setItem("lang",language);
              console.log("after save");
              console.log(data)
                 var obj= $.parseJSON(data);
               if(obj.status== 'error')
               {
                 alert("status update unsuccessfull , please check  your changes once more");
                 $("#loading").fadeOut();
                 return;
               }
               else{
                 console.log(obj.result);
                 alert("status update success");

                 $("#name").val(obj.result.name);
                 $("#password").val("");
                 $("#country").val(obj.result.countryCode);
         
          var value= $('#country').find(':selected').data('dial_code');
          var str1=value;

          $("#prefix").val(str1);
          $("#phnumber").val(obj.result.cellphone);

                 $("#email").val(obj.result.email);
                 console.log("pushinghere")
                 console.log(obj.result.pushActive)
                 $("#push").val(obj.result.pushActive.toString());

                  $("#name").prop("readonly",true);
                  $("#email").prop("readonly",true);
                  $("#password").prop("readonly",true);
                  $("#phnumber").prop("readonly",true);
                  // $("#prefix").prop("readonly",true);
                  $('#country').attr("disabled", true);
                  $("#push").attr("disabled",true);


                    $(".save_button").fadeOut();
                    $(".cancel_button").fadeOut();  
                    $(".modify").fadeIn();
                    $("#loading").fadeOut();
                    location.reload();
               }

                     },
                     error:function()
                     {
                      alert("Unexpected error please refresh the page");
                     }



            });

          }
          else{
          	$("#loading").fadeOut();
          }
          
         }







