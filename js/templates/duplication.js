
//declaring global variable for differentiating between graphs  ============================================================

// a variable used for giving dynamic ids to different graph boxes
   var duplicationData={};
//messages data

  var dataMessage   = "";

window.canvas_array = [];
window.chartData_array = [];
var can_counter = 0;

  $(function(){
    switch(localStorage.getItem('lang').toLowerCase())
    {
      case 'en' : monu = "Months";
                  neg = "Negatives";
                  pos = "Positives"; 
                  dataMessage = "<b>No Data Yet</b>";
                                  break;
      case 'fr' : monu = "Mois";
                  pos = "Positifs";
                  neg = "Négatifs";
                  dataMessage = "<b>Pas encore de données</b>";
                                    break;
      default: dataMessage = "<b>No Data Yet</b>";
                                    break;

    }
  })




   var user_id;
   $.ajax({
        url: 'api/profile_curl.php',
        type: "POST",
        data:{"action":"getSession"}, 
        success: function(userData)
        {
         
         var userData=$.parseJSON(userData);
          user_id=userData.user_id;
        },async:false
    });
   $.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}

var venueid=$.urlParam('id'); 

// graph redraw after window resize for google.
$(window).resize(function() {
    if(this.resizeTO) clearTimeout(this.resizeTO);
    this.resizeTO = setTimeout(function() {
        $(this).trigger('resizeEnd');
    }, 500);
});


//================================global variables end===================================================================
function StartFunction(chartid)
{
    $("#loading").fadeIn();

  setTimeout(
          function() {
    switch(chartid)
    {                
    case 'curve_chart': LinechartDuplication(chartid);
                break;
                       
    case 'bar_chart':   NegPoschartDuplication(chartid);
                  break;
    
    case 'chart_divl': StarChartDuplication(chartid);
                  break;
    
    case 'sales-chart': DonutChartDuplication(chartid);
                  break;
    }

          }, 400);

}

var l=0;
function DonutChartDuplication(chartid)
{
  
  l++;
  var id= chartid+l;
  console.log(id);
  $(`<div  class="col-lg-6 col-md-6 subanchor connectedSortable">
    <div class="box" id="entire`+id+`">
      <div class="box-header with-border">
        <h3 id="Services" class="box-title tservices">Services</h3>
        <div class="box-tools pull-right">
   <button type="button" onclick="StartFunction('sales-chart');" class="btn btn-box-tool"><i class="fas fa-copy"></i>
    </button>
    <button type="button" class="btn btn-box-tool"  onclick="collapser('`+id+`');" data-widget="collapse"
         data-toggle="tooltip" title="Collapse"><i class="fas fa-minus"></i></button>
<button type="button" class="btn btn-box-tool"  onclick="closer('`+id+`');" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fas fa-window-close"></i></button>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-body minheighter" id="box`+id+`">
        
                  <div class="row">
           <div class="col-sm-7">
          <p class="top15"><span class="bold tselectedrange">Selected Date Range :</span>
            <span id="DateRange`+id+`" style="padding-left: 5px;">2017-12-12 to 2018-04-12</span></p>
          </div>
        <div class="col-sm-5">
        <input type="text" name="daterange" id="date`+id+`"  class="form-control">
        </div>
      </div>
        <div class="row">
         <div class="col-lg-12 col-md-12 venue-details">
          <div class="row m-t-25">
            <div class="col-md-6">
             <div class="box-body">
              <div id="clearDonut`+id+`" class="box-body chart-responsive">
                <div class="chart" id="`+id+`" style="min-height: 300px;"></div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="row venue-value-box venue-value-box-`+id+`"> <!--dynamic data coming here-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>`).insertAfter("#donutAnchor")

 $('.connectedSortable').sortable({
    placeholder         : 'sort-highlight',
    connectWith         : '.connectedSortable',
    handle              : '.box-header, .nav-tabs',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });
  $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');

var dateId="#date"+id;
var dates=$(dateId).val();

duplicationData[id]=dates;
localStorage.removeItem("duplicationData-"+venueid)
localStorage.setItem("duplicationData-"+venueid,JSON.stringify(duplicationData))


$(dateId).daterangepicker({
      dateLimit:  {
        "days": 182
    },
  locale: {
      format: 'YYYY/MM/DD'
    },
    maxDate: new Date()

    }).on('apply.daterangepicker',function(ev,picker){

        var id = $(this).attr('id');
        var dates=picker.element[0].value;
      
        CUSTOMWORKINGSTART(dates,id,'sales-chart');

    });

      DonutDraw(id)
      readTextFile();
}

function DonutDraw(id)
{
  console.log(id)
   var datesData=$.parseJSON(localStorage.getItem("venueDates"+venueid));
   var dateRange =  datesData.start+" "+"to"+" "+datesData.current;
   var dateRangeId= "#DateRange"+id;
   $(dateRangeId).html(dateRange);

  var venueDetails = $.parseJSON(localStorage.getItem("venueDetailsDup"+venueid));

     $.ajax({
        url: 'api/venue_details_curl.php',
        type: "POST",
        data:{
    "action": "venueData",
    "userId": user_id,
    "venueId": venueid,
    "venueVersion": venueDetails.venueVersion,
    "checkVersion": false,
    "startDate": datesData.start,
    "endDate": datesData.current,
}, 
        success: function(venueDetailData)
        {
          var venueD = $.parseJSON(venueDetailData);

          if(venueD.result.services.length>0)
          {
            DrawDonutChart(id,venueDetailData);
          }
          else{
            var id2 = "#"+id;
            $(id2).html(dataMessage);
            $(id2).css("min-height",0)
            $("#loading").fadeOut();
          }
          localStorage.removeItem("DonutChartData"+id);
          localStorage.setItem("DonutChartData"+id,venueDetailData);
        },
        async:false
    });         


}

function DrawDonutChart(id,venueDetailData)
{

  var tis="#"+id;
  console.log(tis)  
   $(tis).empty();

var venueD= $.parseJSON(venueDetailData);
 var serviceData = $.parseJSON(localStorage.getItem("servicesjson"));
 var j=0;
 var donut_slice=[];
 var total=0;
 var color;
 var percent=0;
     $.each(venueD.result.services,function(index,element){
        total=total+element.reviews.length;
        });
console.log(total)

    $.each(venueD.result.services,function(index,element){
            $.each(serviceData.services,function(index2,element2){
                if(element.serviceId===element2.id)
                      {
                        // console.log(element2.domainKey)
                        percent= ((element.reviews.length*100)/total).toFixed(2);
                        console.log(element.reviews.length)
                            donut_slice[j] = {
                              'label':element2.displayName,
                              'value':percent,
                              'color':element2.rgb,
                              'hidden':element2.id,
                              'hidden-name': element2.shortName
                            };
                      }

            });
            j++;
    });


console.log("===============================venueDetails")
    var venueDetails = $.parseJSON(localStorage.getItem("venueDetailsDup"+venueid));
    console.log(venueDetails)

    for(var i=0; i< venueDetails.result.services.length; i++ )
    {
      for(var j=0;j<donut_slice.length;j++)
      {
        if(venueDetails.result.services[i].serviceName === donut_slice[j]['hidden-name'])
        {
            delete(venueDetails.result.services[i]);
            break;
        }
      }

    }
      venueDetails = venueDetails.result.services.filter(function( element ) {
         return element !== undefined;
      });



$.each(serviceData.services,function(index,element){

      $.each(venueDetails,function(index2,element2){

        if(element2.serviceName == 'gplus' && element.shortName == 'googleplus'){
              element2.serviceId = element.id;
              element2.displayName = element.displayName;
              element2.rgb = element.rgb;

        }


         if(element.shortName === element2.serviceName)
         {
              element2.serviceId = element.id;
              element2.displayName = element.displayName;
              element2.rgb = element.rgb;
         } 


      })

});


$.each(venueDetails,function(index,element){
  donut_slice.push({
                              'label':element.displayName,
                              'value':0,
                              'color':element.rgb,
                              'hidden':element.serviceId,
                              'hidden-name': element.serviceName
                            })
})


donut_slice = donut_slice.reduce(function (p, c) {

  if (!p.some(function (el) { return el.hidden === c.hidden; })) p.push(c);
  return p;
}, []);


  window.donutChart = Morris.Donut({
  element: id,
  data: donut_slice,
  resize: true,
  redraw: true,
  animationEnabled: true


  }).
  on('click', function (i, row) {  

    morrisHan(i,row.hidden,venueDetailData);
});

var classname=".venue-value-box-"+id; 

$(classname).html('');

  $.each(donut_slice,function(index,element){
    $(classname).append(`<div style="pointer-events:none;" onClick="morrisHan(`+index+`,'`+element.label+`');" class="col-sm-6 col-6">
                      <div class="info-box" style="background-color:`+element.color+` !important">
                     <div class="info-box-content text-center">
                      <span class="info-box-text">`+element.label+`</span>
                      <span class="info-box-number">`+element.value+`%</span>
                      <div class="progress">
                        <div class="progress-bar" style="width: `+element.value+`%"></div>
                      </div>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                </div>`);
  });

$("#loading").fadeOut();
}



//starting the line chart duplication

var i=0; 
function LinechartDuplication(chartid)
{
  i++;
  var id= chartid+i;
console.log(id);
  $(`<div  class="col-lg-6 col-md-6 subanchor connectedSortable">
  <div class="box box-info" id="entire`+id+`">
    <div class="box-header with-border">
      <h3 class="box-title tevolution">Evolution of positive opinion</h3>
      <div class="box-tools pull-right">
        <button type="button" onclick="StartFunction('curve_chart');" class="btn btn-box-tool"><i class="fas fa-copy"></i>
    </button>
        <button type="button" class="btn btn-box-tool"  onclick="collapser('`+id+`');" data-widget="collapse"
         data-toggle="tooltip" title="Collapse"><i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-box-tool"  onclick="closer('`+id+`');" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fas fa-window-close"></i></button>
      </div>
    </div>
    <div class="box-body " id="box`+id+`">
      <div class="row">
           <div class="col-sm-7">
      
          <p class="top15"><span class="bold tselectedrange">Selected Date Range :</span>
            <span id="DateRange`+id+`" style="padding-left: 5px;">2017-12-12 to 2018-04-12</span></p>
          </div>
        <div class="col-sm-5">
        <input type="text" name="daterange" id="date`+id+`"  class="form-control">
        </div>
    </div>
     <div class="chart">
      <!-- Sales Chart Canvas -->
      <canvas id="`+id+`" height="380" width="350"></canvas>
      <div id="curve_chart_error"></div>
    </div>
    <!-- /.chart-responsive -->

    <!-- /.row -->
  </div>
</div>
</div>`).insertAfter( '#linechartAnchor');
console.log(id)
   $('.connectedSortable').sortable({
    placeholder         : 'sort-highlight',
    connectWith         : '.connectedSortable',
    handle              : '.box-header, .nav-tabs',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });
  $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');

var dateId="#date"+id;
var dates=$(dateId).val();
duplicationData[id]=dates;
localStorage.removeItem("duplicationData-"+venueid)
localStorage.setItem("duplicationData-"+venueid,JSON.stringify(duplicationData));

console.log(duplicationData)

$(dateId).daterangepicker({
      dateLimit:  {
        "days": 182
    },
  locale: {
      format: 'YYYY/MM/DD'
    },
        maxDate: new Date()

    }).on('apply.daterangepicker',function(ev,picker){

        var id = $(this).attr('id');
        var dates=picker.element[0].value;
      
        CUSTOMWORKINGSTART(dates,id,'curve_chart');

    });


loadLineGraph(id);
readTextFile();
}

function collapser(id)
{

var sliderId= "#box"+id;
$(sliderId).toggle(300);

}

function closer(id) 
{ 
    console.log(duplicationData)  
    var temp= $.parseJSON(JSON.stringify(duplicationData));

    $.each(temp,function(index,element){
        
        if(index==id)
        {
            delete(temp[index])
        }

    })
    console.log("after deleetion")
    console.log(temp);
    duplicationData=temp;
    localStorage.removeItem("duplicationData-"+venueid);
    localStorage.setItem("duplicationData-"+venueid,JSON.stringify(duplicationData));
  var closeId= "#entire"+id;
  $(closeId).toggle();

}


function loadLineGraph(id)
{
   
   var datesData=$.parseJSON(localStorage.getItem("venueDates"+venueid));
   var dateRange =  datesData.start+" "+"to"+" "+datesData.current;
   var dateRangeId= "#DateRange"+id;
   $(dateRangeId).html(dateRange);

var venueDetails = $.parseJSON(localStorage.getItem("venueDetailsDup"+venueid));

    $.ajax({
        url: 'api/venue_details_curl.php',
        type: "POST",
        data:{
    "action": "venueData",
    "userId": user_id,
    "venueId": venueid,
    "venueVersion": venueDetails.venueVersion,
    "checkVersion": false,
    "startDate": datesData.start,
    "endDate": datesData.current,
}, 
        success: function(venueDetailData)
        {
          console.log("id testing")
          var venueD = $.parseJSON(venueDetailData)
          localStorage.removeItem("lineData"+id);
          localStorage.setItem("lineData"+id,venueDetailData);
          if(venueD.result.services.length>0)
          {
          linechartDraw(id,venueDetailData);
          }
          else{
            var id2 = "#"+id;
            $(id2).html(dataMessage);
            $(id2).css("min-height",0)
            $("#loading").fadeOut();
          }
        },
        async:false
    });      


readTextFile();
}


function linechartDraw(id,curvedata)
{



 var months=[];
var obj= $.parseJSON(curvedata);
var i=0;
var positive=[];
var negative=[];
 $.each(obj.result.stats,function(index,element){
    months[i]=element.yearMonth;
    var total = parseInt(element.venuePositiveNotesCount+element.venueNegativeNotesCount)
    console.log("================"+total)
    console.log("==========="+element.venuePositiveNotesCount)
    positive[i]=(element.venuePositiveNotesCount/total)*100;
    negative[i]=(element.venueNegativeNotesCount/total)*100;
     i++;
    });
var months_overall =[];
var titles = [];
var monu=[];

 switch(localStorage.getItem('lang').toLowerCase())
 {
  case 'en' : months_overall = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 
                                 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
              titles="Percentage of Negative vs positive";
              monu[0] = "Months";
              monu[1] = "Negatives";
              monu[2] = "Positives";
              break;

  case 'fr' : months_overall = ['Janv.','Févr.','Mars','Avr.','Mai','Juin',
                                'Juil.','Août','Sept.','Oct.','Nov.','Déc.'];
              titles = "Pourcentage de négatif vs positif";
              monu[0] =  "Mois";
              monu[1] = "Négatifs"
              monu[2] = "Positifs"
              break;

  default :  months_overall = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
              titles="Percentage of Negative vs positive";
              break;

 }


var j=0;
var new_months=[];
$.each(months,function(index,element){
 new_months[j] = months_overall[+element.split('-')[1] - 1];
    j++;
});

var chartData=[];
  chartData.push([monu[0], monu[1],monu[2]]);

  for(var i=0;i<new_months.length;i++)
  {
      var pair=[];
      pair.push(new_months[i]);
      pair.push(positive[i]);
      pair.push(negative[i])
      chartData.push(pair);
  }


let label = [];
    let posi = [];
    let nega =[];
  for(var i=0;i<new_months.length;i++)
  {
      label.push(new_months[i]);
      posi.push(positive[i]);
      nega.push(negative[i])
  }



var data = {
  labels: label,
  datasets: [{
      label: pos,
      lineTension: 0.5,
      backgroundColor: "#42BD92",
      borderColor: "#42BD92",
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "white",
      pointBackgroundColor: "black",
      pointBorderWidth: 1,
      pointHoverRadius: 6,
      pointHoverBackgroundColor: "#42BD92",
      pointHoverBorderColor: "black",
      pointHoverBorderWidth: 1,
      pointRadius: 4,
      pointHitRadius: 5,
      data: posi,
      spanGaps: false,
    },
    {
      label: neg,
      lineTension: 0.5,
      backgroundColor: "#BF3A31",
      borderColor: "#BF3A31", // The main line color
      borderCapStyle: 'butt',
      borderDash: [], // try [5, 15] for instance
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "black",
      pointBackgroundColor: "white",
      pointBorderWidth: 1,
      pointHoverRadius: 6,
      pointHoverBackgroundColor: "red",
      pointHoverBorderColor: "black",
      pointHoverBorderWidth: 1,
      pointRadius: 4,
      pointHitRadius: 5,
      data: nega,
      spanGaps: true,
    }

  ]
};

// Notice the scaleLabel at the same level as Ticks
var options = {
  
  scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                },
                scaleLabel: {
                     display: true,
                     labelString: titles,
                     
                  }
            }],
            xAxes: [{
                scaleLabel: {
                     display: true,
                     labelString: monu,
                     
                  },
                  gridLines: {
                    display : false
                  }
            }]             
        },
        elements: {
                        line: {
                                fill: false,

                        }
                    } 
};

// Chart declaration:
window.canvas_array[can_counter] = document.getElementById(id);
window.chartData_array[can_counter] = new Chart(window.canvas_array[can_counter], {
  type: 'line',
  data: data,
  options: options
});



window.canvas_array[can_counter].onclick = function(evt) {
  var activePoint;
   if(!window.chartData_array[can_counter])
   {
    activePoint = window.myCurveChart.getElementAtEvent(evt)[0];
   } else {
    activePoint = window.chartData_array[can_counter].getElementAtEvent(evt)[0];
   }
   console.log(activePoint)
   var data = activePoint._chart.data;
   var datasetIndex = activePoint._datasetIndex;
    var mainlabel = data.labels[activePoint._index];
    var label = data.datasets[datasetIndex].label;
   var value = data.datasets[datasetIndex].data[activePoint._index];
    
    let message = [];
    message.push(label.toLowerCase())
    message.push(mainlabel)
  positiveOpinionReviewLoader(message.toString(),curvedata);

 
};



window.graphData.push({
    id : id,
    canvas_data : window.canvas_array[can_counter],
    chart_data : window.chartData_array[can_counter]
  
});

can_counter++;
console.log(can_counter)
console.log(window.graphData)
  
$("#loading").fadeOut();

}





var j=0;
 function NegPoschartDuplication(chartid)
 {
  j++;
  var id= chartid+j;
  console.log(id)
  $("#loading").fadeOut();

  $(`<div class="col-lg-6 col-md-6 subanchor connectedSortable" >
  <div class="box" id="entire`+id+`">
    <div class="box-header with-border">
      <h3 class="box-title tnegvspos">Negatives vs Positives</h3>
      <div class="box-tools pull-right">
      <button type="button" onclick="StartFunction('bar_chart');" class="btn btn-box-tool"><i class="fas fa-copy"></i>
    </button>       
     <button type="button" class="btn btn-box-tool" onclick="collapser('`+id+`');" data-widget="collapse"><i class="fa fa-minus"></i></button>
     <button type="button" class="btn btn-box-tool"  onclick="closer('`+id+`');" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fas fa-window-close"></i></button>
      </div>
    </div>
    <div class="box-body" id="box`+id+`">
          <div class="row">
           <div class="col-sm-7">
      
          <p class="top15"><span class="bold tselectedrange">Selected Date Range :</span>
            <span id="DateRange`+id+`" style="padding-left: 5px;">2017-12-12 to 2018-04-12</span></p>
          </div>
        <div class="col-sm-5">
        <input type="text" name="daterange" id="date`+id+`"  class="form-control">
        </div>
    </div>
     <canvas id="`+id+`" height="424" width="424"></canvas>
   </div>
   <!-- /.box-body -->
 </div>
</div>`).insertAfter('#barAnchor');

     $('.connectedSortable').sortable({
    placeholder         : 'sort-highlight',
    connectWith         : '.connectedSortable',
    handle              : '.box-header, .nav-tabs',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });
  $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');

var dateId="#date"+id;
var dates=$(dateId).val();
duplicationData[id]=dates;
localStorage.removeItem("duplicationData-"+venueid)
localStorage.setItem("duplicationData-"+venueid,JSON.stringify(duplicationData))
$(dateId).daterangepicker({
      dateLimit:  {
        "days": 182
    },
  locale: {
      format: 'YYYY/MM/DD'
    },
        maxDate: new Date()

    }).on('apply.daterangepicker',function(ev,picker){

        var id = $(this).attr('id');
        var dates=picker.element[0].value;
      
        CUSTOMWORKINGSTART(dates,id,'bar_chart');

    });

    loadNegPosGraph(id);
readTextFile();
 }

  function loadNegPosGraph(chartid)
  { 
    var id = chartid;
   var datesData=$.parseJSON(localStorage.getItem("venueDates"+venueid));
      var dateRange =  datesData.start+" "+"to"+" "+datesData.current;
      var dateRangeId= "#DateRange"+id;
      $(dateRangeId).html(dateRange);

    var venueDetails = $.parseJSON(localStorage.getItem("venueDetailsDup"+venueid));

     $.ajax({
        url: 'api/venue_details_curl.php',
        type: "POST",
        data:{
    "action": "venueData",
    "userId": user_id,
    "venueId": venueid,
    "venueVersion": venueDetails.venueVersion,
    "checkVersion": false,
    "startDate": datesData.start,
    "endDate": datesData.current,
}, 
        success: function(venueDetailData)
        {

          localStorage.removeItem("NegPosbarData"+id);
          localStorage.setItem("NegPosbarData"+id,venueDetailData);
          var venueD = $.parseJSON(venueDetailData)
          if(venueD.result.services.length>0)
          {
          NegPosDraw(id,venueDetailData);
          }
          else{
            var id2 = "#"+id;
            $(id2).html(dataMessage);
            $(id2).css("min-height",0)
            $("#loading").fadeOut();
          }
        },
        async:false
    });            

readTextFile();
  }

function NegPosDraw(id,venueDetailData)
{
  console.log(id)



  var titles="";
  var htitle1 = htitle2 = "";
  var subtitles = "";
  switch(localStorage.getItem("lang").toLowerCase())
  {
    case "en" : titles = "Negatives Vs Positives"; htitle1='positive'; htitle2 = 'negative'; xaxislabel= 'count'; yaxislabel='month';
                // subtitles =" Number of negatives vs positives " 
                break;
    case "fr" : titles = "Négatifs vs Positifs"; htitle1='positif'; htitle2 = 'négatif'; xaxislabel= 'compter'; yaxislabel='mois';
                // subtitles = "nombre d'avis positifs et négatifs";
                break;
  }
    

var NPdata= getNPdata(venueDetailData);

  let label = [];
    let posi = [];
    let nega =[];

    for (var key in NPdata){
    if(key!=0){
   //console.log(NPdata[key][0]);
    label.push(NPdata[key][0]);
    posi.push(NPdata[key][1]);
    nega.push(NPdata[key][2]);
    
    //chartValues.push(pair);
    }
    }

   var chartData = {
       type: 'bar',
       data: {
          labels: label,
        datasets: [
            {
                label: htitle1,
                backgroundColor: "#67BFB3",
                data: posi,
            },
            {
                label: htitle2,
                backgroundColor: "#C73729",
                data: nega,
            }
        ]
       },
       options: {
            barValueSpacing: 20,
            scales: {
                yAxes: [{
                  barPercentage: 0.4,
                    ticks: {
                        min: 0,
                    },
                    scaleLabel: {
                   display: true,
                   labelString: xaxislabel
      }
                }],
                xAxes: [{
          scaleLabel: {
          display: true,
          labelString: yaxislabel
            }
             }]
            }
        }
    } 

window.canvas_array[can_counter] = document.getElementById(id);
window.chartData_array[can_counter] = new Chart(window.canvas_array[can_counter] , chartData);

window.canvas_array[can_counter].onclick = function(evt) {
   var activePoint;
   if (!window.chartData_array[can_counter]) {
    activePoint = window.myChart_bar_chart.getElementAtEvent(evt)[0];
   } else {
    activePoint = window.chartData_array[can_counter].getElementAtEvent(evt)[0];
   }
   var data = activePoint._chart.data;
   var datasetIndex = activePoint._datasetIndex;
  var mainlabel=activePoint._model['label'];
   var label = data.datasets[datasetIndex].label;
   var value = data.datasets[datasetIndex].data[activePoint._index];
   var message = [];
   message.push(mainlabel)
   message.push(label)
   // handleMainTopicImpression(,language,venueDetailData)
   Load_RData(message.toString(),venueDetailData);

};

//saving data for updation and future actions

window.graphData.push({
    id : id,
    canvas_data : window.canvas_array[can_counter],
    chart_data : window.chartData_array[can_counter]
  
});
  
  can_counter++;  
}

var k=0;
function  StarChartDuplication(chartid)
{
  k++;
  var id = chartid+k;
  $(`<div  class="col-lg-6 col-md-6 subanchor connectedSortable">
  <div class="box" id="entire`+id+`">
    <div class="box-header with-border">
      <h3 class="box-title tstarchart">Star Chart</h3>
      <div class="box-tools pull-right">
     <button type="button" onclick="StartFunction('chart_divl');" class="btn btn-box-tool"><i class="fas fa-copy"></i>
    </button>
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
   <button type="button" class="btn btn-box-tool"  onclick="closer('`+id+`');" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fas fa-window-close"></i></button>
      </div>
    </div>
    <div class="box-body" id="box`+id+`">
              <div class="row">
           <div class="col-sm-7">
      
          <p class="top15"><span class="bold tselectedrange">Selected Date Range :</span>
            <span id="DateRange`+id+`" style="padding-left: 5px;">2017-12-12 to 2018-04-12</span></p>
          </div>
        <div class="col-sm-5">
        <input type="text" name="daterange" id="date`+id+`"  class="form-control">
        </div>
    </div>
      <canvas id="`+id+`" height="420" width="420" ></canvas>
    </div>
  </div>
</div>`).insertAfter('#starAnchor');

   $('.connectedSortable').sortable({
    placeholder         : 'sort-highlight',
    connectWith         : '.connectedSortable',
    handle              : '.box-header, .nav-tabs',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });
  $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');

var dateId="#date"+id;
var dates=$(dateId).val();
duplicationData[id]=dates;
localStorage.removeItem("duplicationData-"+venueid)
localStorage.setItem("duplicationData-"+venueid,JSON.stringify(duplicationData))
$(dateId).daterangepicker({
      dateLimit:  {
        "days": 182
    },
  locale: {
      format: 'YYYY/MM/DD'
    },
     maxDate: new Date()

    }).on('apply.daterangepicker',function(ev,picker){

        var id = $(this).attr('id');
        var dates=picker.element[0].value;
      
        CUSTOMWORKINGSTART(dates,id,'chart_divl');

    });


  loadStarGraph(id);
readTextFile();
}

function loadStarGraph(id)
{
    console.log(id);
   var datesData=$.parseJSON(localStorage.getItem("venueDates"+venueid));
      var dateRange =  datesData.start+" "+"to"+" "+datesData.current;
      var dateRangeId= "#DateRange"+id;
      $(dateRangeId).html(dateRange);

    var venueDetails = $.parseJSON(localStorage.getItem("venueDetailsDup"+venueid));

     $.ajax({
        url: 'api/venue_details_curl.php',
        type: "POST",
        data:{
    "action": "venueData",
    "userId": user_id,
    "venueId": venueid,
    "venueVersion": venueDetails.venueVersion,
    "checkVersion": false,
    "startDate": datesData.start,
    "endDate": datesData.current,
}, 
        success: function(venueDetailData)
        {

          localStorage.removeItem("StarbarChart"+id);
          localStorage.setItem("StarbarChart"+id,venueDetailData);
          var venueD = $.parseJSON(venueDetailData);
          if(venueD.result.services.length>0)
          {
            StarBarDraw(id,venueDetailData);
          } 
          else{
            var id2 = "#"+id;
            $(id2).html(dataMessage);
            $(id2).css("height",15)
            $("#loading").fadeOut();
          }

            
        },
        async:false
    });           
}

function StarBarDraw(id,venueDetailData)
{

var titles = '';
var titles2 = '';
var stars = '';
switch(localStorage.getItem("lang").toLowerCase())
  {
  
  case 'en' : titles = "Number of Notes";
              titles2 = "Number of Notes for selected months";
              stars = "Stars";
              break;
  case 'fr' : titles =  "Nombre de notes";
              titles2 = "Nombre de notes pour les mois sélectionnés";
              stars = "étoiles";
              break;
  }
var usingVenueData= $.parseJSON(venueDetailData);

var venueNotes1Count = 0;
  var venueNotes2Count = 0;
  var venueNotes3Count = 0;
  var venueNotes4Count = 0;
  var venueNotes5Count = 0;

  $.each(usingVenueData.result.services,function(index,element){
    $.each(element.reviews,function(index2,element2) {
      switch(element2.note) {
        case 1 : venueNotes1Count++;
                  break;
        case 2 : venueNotes2Count++;
                  break;
        case 3 : venueNotes3Count++;
                  break;
        case 4 : venueNotes4Count++;
                  break;
        case 5 : venueNotes5Count++;
                  break;
      } 
    })
  })

var maxVal = Math.max(venueNotes1Count,venueNotes2Count,venueNotes3Count,venueNotes4Count,venueNotes5Count)

var adder = parseInt(0.30*maxVal);
var maxVal = adder+maxVal;


var chartData = {
   type: 'horizontalBar',
   data: {
      labels: ["1(★)","2(★)","3(★)","4(★)","5(★)"],
    datasets: [
        {
            label: titles,
            backgroundColor: "#67BFB3",
            data: [venueNotes1Count,venueNotes2Count,venueNotes3Count,venueNotes4Count,venueNotes5Count]
        }
       
    ]
   },
   options: {
        barValueSpacing: 20,
        scales: {
            yAxes: [{
                scaleLabel: {
          display: true,
          labelString: stars
            },
                ticks: {
                    min: 0,
                }
            }],
            xAxes: [{
              ticks : {
          beginAtZero: true,
          max : maxVal
          },
          scaleLabel: {
          display: true,
          labelString: titles2
            }
             }]
        }
    }
} 

 window.canvas_array[can_counter] = document.getElementById(id);
 window.chartData_array[can_counter] = new Chart(window.canvas_array[can_counter], chartData);


window.canvas_array[can_counter].onclick = function(evt) {
   var activePoint;
   if(!window.chartData_array[can_counter]) {
    activePoint = window.myChart_chart_divl.getElementAtEvent(evt)[0]; /* using the same variable as the parent
     graph. because of the asynchronus nature of javascript. this code is run before the chart
     is initialised so we are using this procedure. */
   } else {
    activePoint = window.chartData_array[can_counter].getElementAtEvent(evt)[0];
   }
   var data = activePoint._chart.data;
   var datasetIndex = activePoint._datasetIndex;
    var mainlabel = data.labels[activePoint._index];
    var label = data.datasets[datasetIndex].label;
    var value = mainlabel.split('')[0];
      
    Load_Star(value,venueDetailData);

 
};


window.graphData.push({
    id : id,
    canvas_data : window.canvas_array[can_counter],
    chart_data : window.chartData_array[can_counter]
  
});

console.log(window.graphData);

can_counter++;

$("#loading").fadeOut();
}

//=====================================================Custom Chart handlind==============================================//


function CUSTOMWORKINGSTART(dates,id,charttype)
{
var range_operation = dates;
range_operation = range_operation.split('-')
console.log(range_operation)

let startDate = range_operation[0]
let endDate = range_operation[1]

startDate = startDate.split('/')
startDate = startDate[0].trim()+'-'+startDate[1].trim()+'-'+startDate[2].trim()

endDate = endDate.split('/')
endDate = endDate[0].trim()+'-'+endDate[1].trim()+'-'+endDate[2].trim()
Cusdates={"startDate":startDate,"endDate":endDate};


LoadCustomData_ForGraphStarting(dates,Cusdates,id,charttype);

}

function LoadCustomData_ForGraphStarting(dates,Cusdates,id,charttype)
{
      $("#loading").fadeIn();
  setTimeout(
    function() {
      LoadCustomData_ForGraph(dates,Cusdates,id,charttype);
      
    }, 400);

}



  function LoadCustomData_ForGraph(dates,Cusdates,id,charttype)
 {
  console.log(Cusdates)
  
  console.log(charttype)
  var id= id.replace(/date/gi, "");
  console.log(id)
  $.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}


 var dateRange =  Cusdates.startDate+" "+"to"+" "+Cusdates.endDate;
   var dateRangeId= "#DateRange"+id;
   $(dateRangeId).html(dateRange);

 var user_id;
   $.ajax({
        url: 'api/profile_curl.php',
        type: "POST",
        data:{"action":"getSession"}, 
        success: function(userData)
        {
         
         var userData=$.parseJSON(userData);
          user_id=userData.user_id;
        },async:false
    });
$.ajax({
        url: 'api/venue_details_curl.php',
        type: "POST",
        data:{
    "action": "venueDetails",
    "userId": user_id,
    "venueId": venueid
    }, 
        success: function(venueDetails)
        {
          console.log(venueDetails);
          localStorage.removeItem("venueDetailsDup"+venueid);
          localStorage.setItem("venueDetailsDup"+venueid,venueDetails);
        },
        async:false
    });
    var venueDetails = $.parseJSON(localStorage.getItem("venueDetailsDup"+venueid));

     $.ajax({
        url: 'api/venue_details_curl.php',
        type: "POST",
        data:{
    "action": "venueData",
    "userId": user_id,
    "venueId": venueid,
    "venueVersion": venueDetails.venueVersion,
    "checkVersion": false,
    "startDate": Cusdates.startDate,
    "endDate": Cusdates.endDate,
}, 
        success: function(venueDetailData)
        {
          switch(charttype)
          {
            case 'curve_chart' : localStorage.removeItem("lineData"+id);
                               localStorage.setItem("lineData"+id,venueDetailData);
                                 duplicationData[id]=dates;
                                 console.log(duplicationData)                               
                                  break;

            case 'bar_chart' : localStorage.removeItem("NegPosbarData"+id);
                               localStorage.setItem("NegPosbarData"+id,venueDetailData);
                                  duplicationData[id]=dates;
                                 console.log(duplicationData)                              
                                 break;
            case 'chart_divl' : localStorage.removeItem("starBarChart"+id);
                                  localStorage.setItem("starBarChart"+id,venueDetailData);
                                 duplicationData[id]=dates;
                                 console.log(duplicationData)
                                  break;
            
            case 'sales-chart' : localStorage.removeItem("DonutChartData"+id);
                                 localStorage.setItem("DonutChartData"+id,venueDetailData);
                                  duplicationData[id]=dates;
                                 console.log(duplicationData)
                                 break;
          }
          console.log("==========hello")
          console.log(JSON.stringify(duplicationData))
        localStorage.removeItem("duplicationData-"+venueid);
        localStorage.setItem("duplicationData-"+venueid,JSON.stringify(duplicationData))
        },
        async:false
    });


   var dateRange =  Cusdates.startDate+" "+"to"+" "+Cusdates.endDate;


      switch(charttype)
      {
        case 'curve_chart' :  var curvedata = localStorage.getItem("lineData"+id);
                              var cur = $.parseJSON(curvedata);
                              if(cur.result.services.length>0)
                              {
                              RedrawpositiveNegativeTendecy(id,curvedata)  
                              }
                              else
                              {
                              console.log("hi")
                              var id2 = "#"+id;
                              $(id2).html(dataMessage);
                              $(id2).css("min-height",0)
                              $("#loading").fadeOut();
                              }
                              
                              readTextFile();
                              break;

        case 'bar_chart' :      var bardata = localStorage.getItem("NegPosbarData"+id);
                                var barc = $.parseJSON(bardata)
                                if(barc.result.services.length>0)
                                {
                                   RedrawDefaultBarchart(id,bardata)
                                }
                                else
                                {
                                console.log("hi")
                                var id2 = "#"+id;
                                $(id2).html(dataMessage);
                                $(id2).css("min-height",0)
                                $("#loading").fadeOut();
                                }
                                readTextFile();
                                break;
        
        case 'chart_divl' : var starData = localStorage.getItem("starBarChart"+id);
                            var starc = $.parseJSON(starData);
                              if(starc.result.services.length>0)
                              {
                               Redrawstarloader(id,starData);
                              }else
                                {
                                console.log("hi")
                                var id2 = "#"+id;
                                $(id2).html(dataMessage);
                                $(id2).css("min-height",0)
                                $("#loading").fadeOut();
                                }
                              readTextFile();
                              break;
                  
        case 'sales-chart' : var donutDat= localStorage.getItem("DonutChartData"+id);
                             var donutc = $.parseJSON(donutDat);
                              if(donutc.result.services.length>0)
                              {
                              DrawDonutChart(id,donutDat);
                              }
                              else{
                                 console.log("hi")
                                var id2 = "#"+id;
                                $(id2).html(dataMessage);
                                $(id2).css("min-height",0)
                                $("#loading").fadeOut();
                              }
                              readTextFile();
                              break;

      }
                    $("#loading").fadeOut();

 }