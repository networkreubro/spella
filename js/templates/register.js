$(function(){
    $.ajax({
      url: 'Json/countries.json',
      dataType: 'json',    
      success: function(data){
        $.each(data, function(key, val){
          $("#country").append('<option value='+val.code+' data-dial_code='+val.dial_code+'>' + val.name +'</option>');

        });
      }
    });

  });
$("#loading").fadeOut();
  function prefix_inser()
  {
    var value= $('#country').find(':selected').data('dial_code');
    $('#phnumber').val(value);
  }





  var name_val=false;
  var email_val=false;
  var pass_val=false;
  var country_val=false;
  var pass_val=false;
  var cpass_val=false;
  // var prefix_val=false;
  var phnumber_val=false;
  var lang_val = false;
  $(function(){

    //Name validation
    $("#name").focusout(function(){

      check_name();

    });

    //Email validation
    $("#email").focusout(function(){

     check_email();

   });
    // Password Validation      
    $("#password").focusout(function(){

      check_pas();        
      confirm_pass(); 
    });

   // confirm password validation        
   $("#cpassword").on('input',function(){

    confirm_pass();

  });
    //Confirm country selection
    $("#country").focusout(function(){

      confirm_country();

    });

    $("#language").focusout(function(){

      check_language();

    });
    // $("#prefix").focusout(function(){

    //   confirm_prefix();

    // });

    //Confirm phone Number
    $("#phnumber").focusout(function(){

      confirm_phNumber();

    });
  });


function check_language() {
  var language = $('#language').find(':selected').val();
  if (language=="") {
     $("#language_up").html("Please select a language").css('color', 'red').css('font-size','12px');
        $("#language_up").show();
        lang_val=true;  
  }
  else{
        lang_val=false;  
        $("#language_up").hide();
  }
}

  function check_name()
  {
    var user= $("#name").val().length;
    if (user<=0)
    {
      $("#name").attr("placeholder", "Please provide your Name");
      $("#name").show();
      name_val=true;
    } 
    else 
    {
      name_val=false;

    }             
  }

  function check_email()
  {

    var email=$("#email").val();
    var len=$("#email").val().length;
    email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
    if (len<=0)
    {
      $("#email").attr("placeholder", "Please provide your Email");
      $("#email").show();
      email_val=true;
    }  
    else{ 
     if(!email_regex.test(email))
     {
      $("#email_up").html("Please provide correct Email").css('color', 'red').css('font-size','12px');
      $("#email_up").show();
      email_val=true;
    } 
    else{
      $("#email_up").hide();
      email_val=false;

    }  
  }        
}



function check_pas()
{
  var pas= $("#password").val().length;
  if (pas<=0)
  {
    $("#pass_up").html( "Please provide a password").css('color', 'red').css('font-size','12px');
        //$("#pass_up").attr("placeholder", "Please provide a password").css('color', 'green');
        $("#pass_up").show();
        pass_val=true;
      }
      else if(pas <6)
      {
        $("#pass_up").html( "Password must atleast be 6 characters").css('color', 'red').css('font-size','12px');
      }  
      else{
        $("#pass_up").hide();
        pass_val=false;    
      }            
    }


    function confirm_pass()
    {
      console.log("hi")

      var pas= $("#password").val();
      var cpas= $("#cpassword").val();
      var cpas_len=$("#cpassword").val().length;
      if(cpas_len<=0)
      {
        $("#cpass_up").html("Please re-enter your password").css('color', 'red').css('font-size','12px');
        $("#cpassword").show();
        cpass_val=true;     
      }
      if(pas!=cpas)
      {
       $("#cpass_up").html("Passwords don't match").css('color', 'red').css('font-size','12px');
       $("#cpass_up").show();
       cpass_val=true;        
     }
     else{
      $("#cpass_up").hide();
      cpass_val=false;        

    }



  }


  function confirm_country()
  {
    country=$("#country").val();
    if(country=="")
    {
      $("#country_up").html("select your country").css('color', 'red');
      $("#country_up").show();
      country_val=true;
    }
    else{
      $("#country_up").hide();
      country_val=false;

    }
  }

  function confirm_prefix()
  {
    var intRegex = /[+0-9]$/;
    prefix= $("#prefix").val();
    p_len= $("#prefix").val().length;
    
    if(p_len<=0)
    {
      $("#ph_up").html("Prefix Necessary").css('color', 'red');
      $("#ph_up").show();
      prefix_val=true;  
    }
    else if(!intRegex.test(prefix))
    {
      $("#ph_up").html("No alphabets allowed").css('color', 'red');
      $("#ph_up").show();
      prefix_val=true;  
    }
    else{
      $("#ph_up").hide();
      prefix_val=false;  

    }
  }

  function confirm_phNumber()
  {                   
      ph= $("#phnumber").val();


      	$.ajax({
      		url: "api/phonetest_curl.php",
      		type: "POST",
      		data:{"phone_number":ph},
      		success:function (data) {
      			console.log(data)
      			data=$.parseJSON(data)
			if(data.hasOwnProperty('country_code')){
				$("#phnumber").val(data.phone_number);
        phnumber_val = false;
    		$("#ph_up").hide();
			}
			else{
			$("#ph_up").html("Enter Correct PhoneNumber").css('color', 'red');
            $("#ph_up").show();
            phnumber_val=true;
			}                          		
		},async:false
      	});
  }


function validationStart(){

$("#loading").fadeIn();
  setTimeout(function(){

    validate_call();
  },400)

}


  function validate_call(){



    check_name();
    check_email();
    check_pas();        
    confirm_pass();
    confirm_country();
    // confirm_prefix();
    confirm_phNumber();
    // return false;
    check_language();
    var name=$("#name").val();
    var email=$("#email").val();
    var country=$("#country").val();
    // var prefix= $("#prefix").val();
    var dial_code= $('#country').find(':selected').data('dial_code');
    var phnumber=$("#phnumber").val();
    var pas= $("#password").val();
    var language = $('#language').find(':selected').val();
    // phnumber = prefix+''+phnumber;
    if(lang_val==false&&name_val==false&&email_val==false&&pass_val==false&&country_val==false&&phnumber_val==false&&pass_val==false&&cpass_val==false)  
    {
    	    console.log("here")

      $.ajax({

        url: "api/sign_up_curl.php",
        type: "POST",
        data : {
         "action": "createUser",
         "email": email,
         "password": pas,
         "name": name,
         "cellphone": phnumber,
         "countryCode": country,
         "status": "subscribed",
         "role": "user",
         "pushActive": true,
         "preferredLanguage": language

       },

       success:function(data)
       {
        var obj= $.parseJSON(data);
        console.log(obj)
        if(obj['status']==='success')
        {
          $("#error-box").hide();
          window.location.href="user_validate.php?email="+email; 
        }else{
          $("#register-error").html(obj.result.message)
          $("#error-box").show();
          $("#loading").fadeOut();
        }

      }, error: function(data) 
      {
      	$("#loading").fadeOut();

        console.log(data)
        
      }
    });

    } else {
    	$("#loading").fadeOut();
    }

  }



 $("#email").on('change keyup paste',function(){
    $(this).val($(this).val().toLowerCase());
     })