// Multilanguage management file
$( document ).ready(function() {

var language = localStorage.getItem("lang");
console.log("language: "+language);
        $("#langs").val(language);
readTextFile();
});

function readTextFile()
{
   var file = "js/lang.json";
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                var allText = rawFile.responseText;
                // alert(allText);
                var data = jQuery.parseJSON(allText);
        var language = localStorage.getItem("lang");
                $.each(data, function(index,element){
            if (language=="EN") {
              $("."+element.class).html(element.en);
            }
            else{
              $("."+element.class).html(element.fr);
              $("#settings").attr("data-original-title","Réglages");
              $("#swlanguage").attr("data-original-title","Langue");
              $("#blogout").attr("data-original-title","Déconnexion");
            }

            if (element.classplace) {
                if (language=="EN") {
                  $("."+element.classplace).attr("placeholder", element.en);
                }
                else{
                  $("."+element.classplace).attr("placeholder", element.fr);
                }
            }
        });

            }
        }
    }
    rawFile.send(null);
}


function singleTranslate(classname)
{
   var file = "js/lang.json";
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                var allText = rawFile.responseText;
                // alert(allText);
                var data = jQuery.parseJSON(allText);
        var language = localStorage.getItem("lang");
                $.each(data, function(index,element){
                    if (element.class==classname) {
            if (language=="EN") {
              $("."+element.class).html(element.en);
            }
            else{
              $("."+element.class).html(element.fr);
            }
        }
        });

            }
        }
    }
    rawFile.send(null);
}

function switchLanguage(){
        var lang = $("#langs").val();
        localStorage.setItem("lang",lang);
        location.reload();
}