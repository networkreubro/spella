</div>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 class="taddvenue">Ajouter un lieu</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="home.php"><i class="fa fa-tachometer-alt"></i><span class="thome"> Home</span></a></li>
        <!-- <li class="breadcrumb-item"><a href="#">Examples</a></li> -->
        <li class="breadcrumb-item active taddvenue">Add Venue</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
	  
      {* <input type="hidden" id="productsubscribed"> *}
      {* {foreach from=$products key=k item=v}
       {$v.id}
      {/foreach} *}
      {* {section name=foo loop=$count} 
      <p>{$smarty.section.foo.iteration}</p>
      {/section} *}
	  {*{if ((($product_status eq "trial")||($product_status eq "unsubscribed")) && ($no_of_venues_added gt 0))}*}
	  <!----------------->	  
	  
      {if ((($product_status eq "trial")||($product_status eq "unsubscribed")) && ($no_of_venues_added >= $no_of_venues))}
      <div class="box p-3">
        <h3 class="trialthree">You are in trial mode</h3>
        <p class="trialfour">You are using Spella in trial mode, with {$no_of_venues} venue/s only</p>
      </div>	 
      {elseif $no_of_venues_added >= $no_of_venues}
      <div class="box p-3">
        <p><span class="tpackage">You are using Spella with</span> {$current_product.name} <span class="tpackspan1">package , with </span> {$no_of_venues} <span class="tpackspan2"> venues only</span></p>

      </div>
      {else}
      <div class="box">
        <div class="box-header with-border">
          <!-- <h3 class="box-title">Nom de la place</h3> -->

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
            <i class="fa fa-minus"></i></button>
      <!--       <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button> -->
            </div>
          </div>
      <!--     <div class="box-body">
          <a class="btn btn-app bg-purple">
               modify
              </a>
          </div> -->
          <!-- /.box-body -->
          <div class="box-footer">
            <div class="row">
             <div class="col-lg-8 col-md-8 col-xl-6">
              <div class="create-profile">
          
                <form class="row">
<div class="col-sm-6 col-xs-12">
  <div class="form-group">
      <label class="tname">Name</label>
    <input  placeholder="Name" type="text" id="venueName" required>
    <label class="add-venue-error" id="nameError"></label>
  </div>
  </div>


<div class="col-sm-6">
  <div class=" form-group">
    <label class="ttypesVenues">Types de lieux</label>
   <select id="venueTypes" placeholder="Country">
     <option value="">Select Type</option>
   </select> 
   <label id="selectError" class="add-venue-error"></label>
  </div>
  </div>
<!--   <div class="col-sm-12">
    <div class=" form-group pull-right">
  <button class="btn" type="button">Cancel</button>
  <button class="btn" type="button">Save</button>
</div>
</div> -->
<div class="clearfix">
<!-- Modal -->
<div class="modal fade" id="Modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" class="tservices">Services</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">Back</span>
        </button>
      </div>
      <div  class="modal-body sevicelist text-center">
        <ul id="logos">
          
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary tcancel" data-dismiss="modal">Cancel</button>
        
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="Modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">pages</h5>
        <button id="pagesBack" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">Back</span>
        </button>
      </div>
      <div  class="modal-body">
        <ul class="pageList" style="align-content: center;">
          
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary tcancel" onclick="modal2close();">Cancel</button>
		<button type="button" class="btn btn-secondary tother" onclick="showModal6();"></button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="Modal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><input type="text" id="urlInsert"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">Back</span>
        </button>
      </div>
      <div  class="modal-body">
         <a id="openUrl" href="" data-toggle="tooltip" title="Click to view the originalSite" target="_blank"> <img src="" id="pageInsert" style="width:100%;"   /></a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="modal3close();">Cancel</button>
         <button type="button" class="btn btn-secondary" onclick="pagesUtilize();">Save</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="Modal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">
        	<input type="text" id="Modal4urlInsert">
        </h5>
        <h5 class="modal-title" id="validtorAlerter"></h5>
        <button type="button" class="close" onclick="goValidator();" aria-label="Close">
          <span aria-hidden="true">Go</span>
        </button>
      </div>
      <div  class="modal-body">
      <a id="openUrl" href="" data-toggle="tooltip" title="Click to view the originalSite" target="_blank"> <img src="" id="Modal4pageInsert" style="width:100%;"/></a>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="modal3close();">Cancel</button>
         <button type="button" class="btn btn-secondary" id="Modal4Save" onclick="Modal4pagesUtilize();" disabled>Save</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="Modal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title tservices" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div  class="modal-body sevicelist text-center">
        <ul id="logos5">
          
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary tcancel" data-dismiss="modal"></button>
        <button type="button" class="btn btn-secondary" onclick="addSource();">Submit</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="Modal6" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <h5 class="modal-title"><span class="tcustomtitle">Custom Page</span><span id="customPageTitle"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div  class="modal-body">
          <input type="url" id="customPageURL" class="Tcustomurl" required name="customPageURL" value="" placeholder="Custom Page URL" />
		  <div class="col-sm-12 my-3">
			<span class="validation-info tcustomurlmessage" id="em_error" style="color: red;display:none">Enter a Valid URL</span>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary tcancel" data-dismiss="modal">Cancel</button>
         <button type="button" class="btn btn-secondary tvalidate" onclick="validateURL();"></button>
      </div>
    </div>
  </div>
</div>

</div>
<div class="col-sm-12 m-t-25">
  <div class="whitebox-title">
  <h3><a  onclick="popupOpen();" style="text-decoration:none;"  href="javascript:void(0);">
    <span><i class="fas fa-plus"></i></span>&nbsp;&nbsp;<span class="taddnewpage">Add a Page</span></a></h3>
  </div>
  <hr>


        <div class="table-responsive m-t-25">
        <table id="pageContent" class="table table-bordered table-hover">
            <thead>
            <tr>
            <th class="tservice">Service</th>
            <th>URL</th>
            <th class="toption">Option</th>
            </tr>
          </thead>
          <tbody id="pageTable">
            
          </tbody>
          <tfoot>
            <tr>
              <td colspan="5"><button type="button" id="save_button" style="display: none;" class="btn main-btn pull-right">save</td>
            </tr>
          </tfoot>
        </table>
      </div><!--end of .table-responsive-->


</div>

</form>


              </div>
             
</div>
</div>

<div class="col-lg-2 col-xl-3 col-md-3">

</div>
</div>

</div>

{/if}
<!-- /.box-footer-->
</div>
<!-- /.box -->

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<footer class="main-footer">
  <div class="pull-right d-none d-sm-inline-block">
  </div>Copyright &copy; 2018 <a href="https://www.datastitute.fr/">Spella Corp</a>. <span class="tallrights" ></span>
</footer>
<!-- ./wrapper -->
<script>
let numberofvenue = {$no_of_venues_added}
</script>

<!-- jQuery 3 -->
<script src="assets/vendor_components/jquery/dist/jquery.min.js"></script>

<!-- popper -->
<script src="assets/vendor_components/popper/dist/popper.min.js"></script>

<!-- Bootstrap v4.0.0-beta -->
<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- SlimScroll -->
<script src="assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->
<script src="assets/vendor_components/fastclick/lib/fastclick.js"></script>

<!-- maximum_admin App -->
<script src="js/template.js"></script>

<!-- maximum_admin for demo purposes -->
<script src="js/demo.js"></script>
<script src="js/all.js"></script>

<script src="js/templates/add_venue.js"></script>
<script src="js/multilang.js"></script>

</body>
</html>
