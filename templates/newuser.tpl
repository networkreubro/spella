<body class="hold-transition login-page">
 <!--        <div id="loading" class="loading-body" style="z-index: 5000; height: 100% !important; position: fixed; top: 0;">
    <div class="loading-container">
    <div class="loading"></div>
    <div id="loading-text"><img src="images/logo-small.png" alt=""></div>
</div>
</div> -->
<div class="login-box">
      <div class="login-logo">
      <img src="images/logo-spella.png" alt="" class="img-fluid" / >
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">

    <p class="login-box-msg bold">Sign up now</p>

    <div class="form-group has-feedback top15">
      <center><p id="commonmessage"></p></center>
        <input type="text" id="name" class="form-control" placeholder="Name">
          <div class="col-sm-12" >
        <span class="validation-info pull-right" id="name_error"></span>
      </div>
      </div>
     
      <div class="form-group has-feedback top15">
        <input type="text" id="email" class="form-control" placeholder="Email">
          <div class="col-sm-12" >
        <span class="validation-info pull-right" id="email_error"></span>
      </div>
      </div>
      <div class="form-group has-feedback top15">
        <input type="password"  id="password" class="form-control" placeholder="Password">
        <div class="col-sm-12" >
        <span class="pull-right validation-info" id="pass_error"></span>
        </div> 
      </div>  
      <div class="clearfix"></div>
      <div class="col-sm-12">
          <div class=" form-group row">
          
             <select onchange="" id="language" class="form-control">
               <option class="tEnglish" value="EN">English</option>
               <option class="tFrench" value="FR">French</option>
             </select> 
           
         </div>
       </div> 

      <div class="col-sm-12">
          <div class=" form-group row">
          
             <select data-toggle="tooltip" title="Country" onclick="" onchange="" id="country" class="form-control">
               <option class="tEnglish" value="EN">Country</option>
             </select> 
           
         </div>
       </div> 

      <div class="clearfix"></div>   
      
        <div class="col-12 text-center">
          <button type="button" class="btn btn-block btn-purple margin-top-10" onclick="newuser()">REGISTER</button>
        </div>
        <!-- /.col -->
      </div>
    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <!-- <a href="#" class="btn btn-social-icon btn-circle btn-facebook"><i class="fa fa-facebook"></i></a>
      <a href="#" class="btn btn-social-icon btn-circle btn-google"><i class="fa fa-google-plus"></i></a> -->
    </div>
    <!-- /.social-auth-links -->
    <div class="margin-top-30 text-center">
      <p>Already have an account? <a href="index.php" target="_blank" class="text-info m-l-5">LOGIN</a></p>
    </div>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<div class="modal fade" id="forgotUp" tabindex="-1" role="dialog" aria-labelledby="compUp" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Forgot Password?</h5>
        <button  type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div  class="modal-body sevicelist text-center">


<div class="login-box-body">
    <p class="login-box-msg" id="rp">Enter your registred email to reset the password.</p>
    <input type="text" id="femail" hidden>
      <div class="form-group has-feedback" id="replace">
        <input type="text" id="forgotemail" class="form-control" placeholder="Email">
          <div class="col-sm-12">
        <span class="validation-info pull-right" id="forgot_error" style="display: none;"></span>
      </div>
      </div>
      <div class="clearfix"></div>   
      <div class="row">

        <div class="col-12 text-center" id="rbutton">
          <button type="button" class="btn btn-block btn-purple margin-top-10" onclick="resetpassword();">PROCEED</button>
        </div>
        <!-- /.col -->
      </div>
  </div>



      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> -->
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="codeUp" tabindex="-1" role="dialog" aria-labelledby="codeUp" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Enter Passcode</h5>
        <button  type="button" class="close" data-dismiss="modal"  aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div  class="modal-body sevicelist text-center">


<div class="login-box-body">
    <p class="login-box-msg" id="rp">Enter your code  to reset the password.</p>
    <input type="text" id="fcode" class="form-control" placeholder="Code">
    <div id="fcode_error"></div>
    <div class="col-sm-12"> <span class="validation-info pull-right" id="forgot_error" style="display: none;"></span> </div>
      <div class="clearfix"></div>   
      <div class="row">

        <div class="col-12 text-center" id="rbutton">
          <button type="button" class="btn btn-block btn-purple margin-top-10" onclick="check_code();">PROCEED</button>
        </div>
        <!-- /.col -->
      </div>
  </div>



      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> -->
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="resetUp" tabindex="-1" role="dialog" aria-labelledby="resetUp" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Password</h5>
        <button  type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div  class="modal-body sevicelist text-center">


<div class="login-box-body">
    <p class="login-box-msg" id="rp">Enter the new password.</p>
    <input type="text" id="femail" hidden>
      <div class="form-group has-feedback" id="replace">
        <input type="password" id="npassword" class="form-control" placeholder="">
          <div class="col-sm-12">
        <span class="validation-info pull-right" id="forgot_error1" style="display: none;"></span>
      </div>
      </div>
      <div class="clearfix"></div>   
      <div class="row">

        <div class="col-12 text-center" id="rbutton">
          <button type="button" class="btn btn-block btn-purple margin-top-10" onclick="updatePass();">PROCEED</button>
        </div>
        <!-- /.col -->
      </div>
  </div>



      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> -->
      </div>
    </div>
  </div>
</div>


</body>
{literal}
  <!-- jQuery 3 -->
  <script src="assets/vendor_components/jquery/dist/jquery.min.js"></script>
  <!-- jQuery cookie -->
  <script src="assets/vendor_components/jquery/dist/jquery.cookie.js"></script>
  <!-- popper -->
  <script src="assets/vendor_components/popper/dist/popper.min.js"></script>
  <!-- Bootstrap v4.0.0-beta -->
  <script src="assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

  <script src="js/templates/new_user.js"></script>

{/literal} 



</html>
