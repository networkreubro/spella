function getName(data) {
  var id = data;
  var idName;
  $.ajax({
    url: "api/add_venue_curl.php",
    type: "POST",
    data: { action: "getServices" },
    success: function(data) {
      obj = $.parseJSON(data);
      $.each(obj.services, function(index, element) {
        if (element.id === id) {
          idName = "https://spella.com/services/logos-small/" + element.icon;
        }
      });
    },
    error: function() {
      alert("Not responding error, refresh page ");
    },
    async: false
  });
  return idName;
}

function getStar(data) {
  var i;
  var str = 0;
  for (i = 0; i < data; i++) {
    str += "&#9733;";
  }
  var s = str.replace(/^0+/, "");

  return s;
}

function trimByWord(sentence, index1) {
  var result = sentence;
  var resultArray = result.split(" ");
  if (resultArray.length > 10) {
    resultArray = resultArray.slice(0, 50);
    result =
      resultArray.join(" ") +
      " " +
      `<span class="more" onclick="more(` +
      index1 +
      `);">more...</span>`;
  }
  return result;
}

function transl_call(value, title) {
  if (Number(title) !== Number(0)) {
    var shortid = "#short" + value;
    var transb = "#transb" + value;
    var transtitle = "#transtitle" + value;
    $(transb).hide();
    var org = "#default" + value;
    var orgtitle = "#defaulttitle" + value;
    var translate = "#translate" + value;
    var rever = "#rever" + value;
    $(shortid).hide();
    $(orgtitle).hide();
    $(org).hide();
    $(transtitle).fadeIn();
    $(translate).fadeIn("500");
    $(rever).fadeIn();
  }
}
function revert_call(value) {
  var orgtitle = "#defaulttitle" + value;
  var transtitle = "#transtitle" + value;

  var transb = "#transb" + value;
  var translate = "#translate" + value;
  var org = "#short" + value;
  var rever = "#rever" + value;

  $(transtitle).hide();
  $(translate).hide();
  $(orgtitle).fadeIn();
  $(org).fadeIn();
  $(transb).fadeIn();
  $(rever).hide();
}

function more(index1) {
  fullid = "#default" + index1;
  shortid = "#short" + index1;

  $(shortid).hide();
  $(fullid).fadeIn();
}

function less(index1) {
  fullid = "#default" + index1;
  shortid = "#short" + index1;

  $(fullid).hide();
  $(shortid).fadeIn();
}
