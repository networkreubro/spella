<!-- header start -->
<header class="main-header fixed-top">
  <header class="main-header fixed-top">
    <!-- Logo -->
    <a href="index.php" class="logo" style="background-color: #ffffff;">
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="images/logo.svg" alt=""/></span>
    </a>

    <nav class="navbar navbar-static-top" style="background-color: #ffffff;">
      <!-- Sidebar toggle button-->
      <div class="ml-auto mr-2">
        <select class="form-control" name="langs" id="langs">
          <option value="EN">English</option>
          <option value="FR">French</option>
        </select>
      </div>
    </nav>
  </header>
</header>
<!-- Header end -->
<!-- Content Wrapper. Contains page content -->

<!-- preloader start -->
<div
  id="loading"
  class="loading-body"
  style="z-index: 5000; height: 100% !important; position: fixed; top: 0;"
>
  <div class="loading-container">
    <div class="loading"></div>
    <div id="loading-text">
      <img src="images/logo-small.png" alt="" />
    </div>
  </div>
</div>
<!-- preloader end -->

<div class="box">
  <div class="box-body">
    <h3 class="display-4 text-center tpricing">Pricing Plans</h3>
    {* {if product_status eq 'trial'} *}
    <!-- trial expire card start -->
    {if $product_status == "trial"}
    <div class="card mt-3">
      <div class="px-3 py-2">
        <div class="d-block w-100">
          <div>
            {if $plan_expiry_days <= 10} {if $plan_expiry_days == 0}
            <h4><b>Your trial has been ended!</b></h4>
            {else}
            <h4><b>Your trial will end soon!</b></h4>
            <p>
              Your trial will end in { $plan_expiry_days } days. Subscribe to
              any plan to enjoy uninterupted service
            </p>
            {/if} {else}
            <h4>
              <b>You are in <b>trial </b> mode</b>
            </h4>
            <p>
              Your trial will end in { $plan_expiry_days } days. Subscribe to
              any plan to enjoy uninterupted service
            </p>
          </div>
          <div>
            <a href="home.php">
              <button
                class="btn px-3 btn-sm bg-purple my-auto"
                style="font-size: 15px;"
              >
                Continue to dashboard
              </button>
            </a>
          </div>
          {/if}
        </div>
      </div>
    </div>
    {else}
    <div class="card mt-3">
      <div class="px-3 py-2">
        <div class="d-block w-100">
          <div>
            {if $plan_expiry_days <= 10} {if $plan_expiry_days == 0}
            <h4><b>Your plan has been ended!</b></h4>
            {else}
            <h4><b>Your plan will end soon!</b></h4>
            <p>
              Your current plan will end in { $plan_expiry_days } days.
              Subscribe to any plan to enjoy uninterupted service
            </p>
            {/if} {else}

            <h4>
              Your current plan will end in { $plan_expiry_days } days.
            </h4>
          </div>
          <div>
            <a href="home.php">
              <button
                class="btn px-3 btn-sm bg-purple my-auto"
                style="font-size: 15px;"
              >
                Continue to dashboard
              </button>
            </a>
          </div>
          {/if}
        </div>
      </div>
    </div>
    {/if}
    <!-- trial expire card end -->
    {* {/if} *}

    <div class="px-10">
      <div class="row" id="productList">
        <!-- --------product list div--------------->
      </div>
    </div>

    <!-- footer start -->
    <footer class="main-footer ml-0">
      Copyright &copy; 2018
      <a href="https://www.datastitute.fr/">Spella Corp</a>
      <span class="tallrights"></span>
    </footer>
    <!-- footer end -->
  </div>
</div>

<!-- ./wrapper -->

{literal}
<!-- jQuery 3 -->
<script src="assets/vendor_components/jquery/dist/jquery.min.js"></script>

<!-- popper -->
<script src="assets/vendor_components/popper/dist/popper.min.js"></script>

<!-- Bootstrap v4.0.0-beta -->
<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- SlimScroll -->
<script src="assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->
<script src="assets/vendor_components/fastclick/lib/fastclick.js"></script>

<!-- maximum_admin App -->
<script src="js/template.js"></script>

<!-- maximum_admin for demo purposes -->
<script src="js/demo.js"></script>
<script src="js/all.js"></script>

<!-- <script src="js/templates/profile.js"></script> -->
<script src="js/templates/pricing.js"></script>
<script src="js/multilang.js"></script>
<script src="https://js.stripe.com/v3/"></script>

{/literal}
