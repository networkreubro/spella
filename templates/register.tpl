
<body class="hold-transition register-page">
	      <div id="loading" class="loading-body" style="z-index: 5000; height: 100% !important; position: fixed; top: 0;">
    <div class="loading-container">
    <div class="loading"></div>
    <div id="loading-text"><img src="images/logo-small.png" alt=""></div>
</div>
</div>
  <div class="register-box">
    <div class="register-logo">
      <!-- <a href="index.php"><b>Maximum</b>Admin</a> -->

    <a href="index.php">  <img src="images/logo-spella.png" alt="" class="img-fluid" / ></a>

    </div>

    <div class="register-box-body">
      <p class="login-box-msg">Register a new membership</p>
     <div  id="error-box" style="display: none;" class="form-group text-center">
     <label id="register-error" class="register-page-error">  </label>
    </div>

            <div class="form-group has-feedback">
              <div class="input-group">
                <input type="text" id="name" class="form-control" placeholder="Full name">
                <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
              </div>
              <div class="validation-info pull-right" id="name_up"></div>
            </div>

            <div class="form-group has-feedback">
              <div class="input-group">
                <input autocapitalize="lower" type="email" id="email" class="form-control" placeholder="Email">
                <span class="input-group-addon"><i class="ion ion-email" aria-hidden="true"></i></span>
              </div>
              <div class="validation-info pull-right" id="email_up"></div>
            </div>

            <div class="form-group has-feedback">
              <div class="input-group">
                <input type="password" id="password" class="form-control" placeholder="Password">
                <span class="input-group-addon"><i class="ion ion-locked" aria-hidden="true"></i></span>
              </div>
              <div class="validation-info pull-right" id="pass_up"></div>
            </div>

            <div class="form-group has-feedback">
              <div class="input-group">
                <input type="password" onchange="confirm_pass();" id="cpassword" class="form-control" placeholder="Retype password">
                <span class="input-group-addon"><i class="ion ion-locked" aria-hidden="true"></i></span>
              </div>
               <div class="validation-info pull-right" id="cpass_up"></div>
               </div>

            <div class="form-group has-feedback  ">
              <select class="form-control select2" id="country" onchange='prefix_inser()' style="width: 100%;">
                <option value="">Select Country</option>
              </select>
               <div class="validation-info pull-right" id="country_up"></div>
            </div>

            <div class="form-group has-feedback  ">
              <select class="form-control select2" id="language" style="width: 100%;">
                <option value="">Select Language</option>
                <option value="en">English</option>
                <option value="fr">French</option>
              </select>
               <div class="validation-info pull-right" id="language_up"></div>
            </div>
           
            <div class="form-group has-feedback">
 
          </div>
          <div class="form-group has-feedback" >
           <div class="input-group">
            <input type="text" id="phnumber"  class="form-control" placeholder="Phone Number">
            <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
          </div>
          <div class="validation-info pull-right" id="ph_up"></div>
        </div>

        <!-- /.col -->
        <div class="form-group text-center">
          <button type="submit" onclick='validationStart()' class="btn btn-block btn-purple margin-top-10">SIGN UP</button>
        </div>
        <!-- /.col -->

        <div class="margin-top-20 text-center">
         <p>Already have an account?<a href="index.php" class="text-info m-l-5"> Sign In</a></p>
       </div>

     </div>
     <!-- /.form-box -->
   </div>
   <!-- /.register-box -->


 </body>
 {literal}

 <!-- jQuery 3 -->
 <script src="assets/vendor_components/jquery/dist/jquery.min.js"></script>

 <!-- popper -->
 <script src="assets/vendor_components/popper/dist/popper.min.js"></script>

 <!-- Bootstrap v4.0.0-beta -->
 <script src="assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!--Custom js for register-->
 <script src="js/templates/register.js"></script>

{/literal}



</html>
