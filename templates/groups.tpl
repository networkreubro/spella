</div>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
            <div class="row">
 <div class="col-sm-4">
      <h1 class="tgroups">Groups</h1></div>

        <div class="col-sm-4">
      <div class=" form-group row">
    <label class="col-lg-3 col-sm-4 tsdate">Start Date</label>
      <div class="col-lg-9 col-sm-8">
    <input autocomplete="off" id="startdate" placeholder="" class="form-control"  value="" readonly=""></div>
  </div>
</div>
        <div class="col-sm-4">
  <div class=" form-group row">
     <label class="col-lg-3 col-sm-4 tedate">End Date</label>
       <div class="col-lg-9 col-sm-8">
    <input autocomplete="off" id="enddate" placeholder="" class="form-control" value="" readonly=""></div>
  </div>
  </div>
</div>
    </section>
    <!-- Main content -->
    <section class="content">


<!-- /.box -->
<!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title tvenuelist">Search Groups</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fa fa-minus"></i></button>
      </div>
    </div>

    <div class="box-body static-group">
      <ul class="nav nav-pills mb-3 justify-content-start" id="pills-tab" role="tablist">
    {if $dynGroupAccess == true}
    <li class="nav-item">
      <a class="nav-link active tdynamic" id="pills-home-tab" data-toggle="pill" href="#groups-tab-1" role="tab" aria-controls="pills-home" aria-selected="true">Par recherche</a>
    </li>
  {/if}
  
  <li class="nav-item">
    <a class="nav-link tstatic" id="pills-profile-tab" data-toggle="pill" href="#groups-tab-2" role="tab" aria-controls="pills-profile" aria-selected="false">Par selection</a>
  </li>


</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade {if $dynGroupAccess == false} show  active {/if}" id="groups-tab-2" role="tabpanel" aria-labelledby="pills-profile-tab">
     <div class="Container">
      <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="form-group">
               <label class="tgroupname">Group Name</label>
              <input type="text" name="" class="form-control" id="staticName" >
              <div id="staticNameError" style="display: none;"></div>
            </div>
         <div class="form-group">
          <label class="tvenuelist">Venue List</label>
<!--           <select multiple data-placeholder="Select Venues" id="homeVenues">
          
      </select> -->
      <div class="multi" id="multi"></div>

      <div id="venueNameError" style="display: none;"></div>
              </div>


<!-- <div class="row">
      <div class="col-sm-6 form-group row">
    <label class="col-sm-4 tsdate">Start Date</label>
      <div class="col-sm-8">
    <input autocomplete="off" id="staticstartdate" placeholder="" class="form-control"  value="" readonly="">
  </div>

</div>
  <div class="col-sm-6 form-group row">
     <label class="col-sm-4 tedate">End Date</label>
       <div class="col-sm-8">
    <input autocomplete="off" id="staticenddate" placeholder="" class="form-control" value="" readonly="">
  </div>
  </div>
</div> -->
    <div class="form-group">
    <button class="btn btn-blue cancel_button tsavegroup pull-right mr-0" id="staticVenueSave">Save</button>
  </div>
    </div>
      </div>
    </div>
  </div>

  <div class="tab-pane fade  show  active" id="groups-tab-1" role="tabpanel" aria-labelledby="pills-home-tab">
            <div class="row">
            {if $dynGroupAccess == "true"}
             <div class="col-lg-10 col-md-12 col-sm-12 m-t-25">
                    <form class="row">
<div class="col-sm-6">
  <div class="form-group row">
      <label class="col-lg-3 col-sm-4 tname">Name</label>
      <div class="col-lg-9 col-sm-8">
    <input autocomplete="off" id="venue_name"   placeholder="" type="text" class="form-control" value="" required>
  </div>
  </div>
</div>
<div class="col-sm-6">
    <div class="form-group row">
       <label class="col-lg-3 col-sm-4 taddress">Address</label>
         <div class="col-lg-9 col-sm-8">
  <input autocomplete="off" id="venue_address" placeholder="" class="form-control" type="text" value="" >
  <!-- <span class="validation-text">Please enter a valid email address.</span> -->
</div>
</div>
</div>
<div class="col-sm-6">
  <div class=" form-group row">
     <label class="col-lg-3 col-sm-4 ttype">Type</label>
       <div class="col-lg-9 col-sm-8">

                 <span id="venuetypes"> </span>
  <!-- </select> -->
  </div>
</div>
<!-- <div class=" form-group row">
     <label class="col-lg-3 col-sm-4 tmaxlimit">Max. Limit</label>
       <div class="col-lg-9 col-sm-8">
  <input type="number" autocomplete="off" id="max_limit" placeholder=""  min="0" class="form-control" value="25000">
  </div>
</div> -->
</div>
  <div class="col-sm-6">
      <div class=" form-group row">
    <label class="col-lg-3 col-sm-4 tmaxlimit">Max. Limit</label>
      <div class="col-lg-9 col-sm-8">
    <input type="number" autocomplete="off" id="max_limit" placeholder=""  min="0" class="form-control" value="25000">
</div>
  </div>
 <!--  <div class=" form-group row">
     <label class="col-lg-3 col-sm-4 tedate">End Date</label>
       <div class="col-lg-9 col-sm-8">
    <input autocomplete="off" id="enddate" placeholder="" class="form-control" value="" readonly=""></div>
  </div> -->
  </div>
  <div class="col-sm-6">
    <div class=" form-group pull-right">
  <!-- <button class="btn bg-purple save_button" style="" onclick="clear();" type="button">S</button> -->
  <button class="btn btn-blue cancel_button tsearch mr-0" style="" onclick="search();" type="button">Search</button>
  <input autocomplete="off" id="group_name" placeholder="Group Name" style="display: none;"  class="form-control" value="">
  <button class="btn btn-blue cancel_button tsavegroup" id="savesearch" style="display: none;" onclick="saveGroup();" type="button">Save Group</button>
</div>

</div>
</form>
</div>
{else}
<div class="text-center w-100">
  <p class="tgroupsone">Current plan doesnot support groups by search.</p>
  <button class="btn bt-sm btn-link mt-3"><a href="plans.php" class="tplans">Change Plan</a></button>
</div>
  {/if}
</div>
  </div>
  {* end of group dynamic *}
  
</div>

   </div>
   <!-- /.box-body -->
<!-- /.box-footer-->
</div>
<!-- /.box -->

<div class="box" style="" id="">
            <div class="box-header with-border">
              <h3 class="box-title savedgroups">Saved Groups</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
             </div>
            </div>
            <div class="box-body">
            <div class="col-md-12 col-sm-12 clearfix">
              <span id="saved_searches" class="save-groups"></span>
            </div>


<!-- <ul> -->
<div class="col-md-12 col-sm-12">
<button class="btn bg-purple cancel_button tcancel" style="font-size: 10px;margin-left: 10px;display: none;" id="cancel" onclick="javascript:location.reload();" type="button">Cancel</button>

<button class="btn bg-purple cancel_button" style="font-size: 10px;display: none;" id="savechanges" onclick="saveChanges();" type="button">Save Changes</button>

</div>

            </div>
            <!-- /.box-body -->
          </div>


<!-- tendency starting -->



<div class="box" id="tendencybox" style="display: none;" >
            <div class="box-header with-border">
              <h3 class="box-title ttendency">Tendencies</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
             </div>
            </div>
            <div class="box-body">
            
              <div id="tendencies" style="display: none;" class="col-md-12 tendencies"></div>
              
              <div class="clearfix"><br/><br/></div>
              <center><div id="condmsg"></div></center>
            </div>
            <!-- /.box-body -->
          </div>



<!-- tendency ending -->



<!-- <div class="box" style="display: none;" >
            <div class="box-header with-border">
              <h3 class="box-title">Matched Venues</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
             </div>
            </div>
            <div class="box-body">   
              <div class="clearfix"><br/><br/></div>
              <span id="datatablespan"></span>
              <center><div id="condmsg"></div></center>
            </div>
          </div> -->
<!-- ---------------------newbox------------- -->
        <div class="box" style="display: none;" id="venandtend">
            <div class="box-header with-border">
              <h3 class="box-title tMatchVenues"></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
             </div>
            </div>


            <div class="box-body" class="test-list">
        <!--       <div class="col-sm-3 form-group pull-right">
                  <input type="text" class="search form-control pull-right" placeholder="Search" />
              </div> -->

            <ul style="list-style-type: none; " id="demo">

            </ul>
 <div class="clearfix"></div>
            <div class="turn-page" id="pager"></div>

            <!-- <ul class="pagination"></ul>  -->
            </div>
            <!-- /.box-body -->
          </div>
<!-- ---------------------newbox------------- -->


<div class="row ">
<div class="col-lg-6 col-md-6 subanchor connectedSortable">
            <div class="box" style="display: none;" id="notesdiv">
            <div class="box-header with-border">
              <h3 class="box-title">Number of notes</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
             </div>
            </div>
            <div class="box-body">
             <canvas width="300" height="300" id="chart_div1" ></canvas> 
             <div id="chart_div1_error"></div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>


<div class="col-lg-6 col-md-6 subanchor connectedSortable">
            <div class="box" style="display: none;" id="main_chartdiv">
            <div class="box-header with-border">
              <h3 class="box-title tnegvspos">Negatives vs positives</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
             </div>
            </div>
            <div class="box-body">
             <canvas width="300" height="300" id="main_chart" ></canvas>
             <div id="main_chart_error"></div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>

  <div class="col-lg-6 col-md-6 subanchor connectedSortable" id="topic" style="display: none;">
    <div class="box">
      <div class="box-header with-border">
        <h3 id="Services" class="box-title tdistoftopics">Main Topics</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
          title="Collapse">
          <i class="fa fa-minus"></i></button>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-body">
        <div class="row">
         <div class="col-lg-12 col-md-12 venue-details">
          <div class="row m-t-25">
            <div class="col-md-5 ml-auto">

            </div>
          </div>
          <div class="clearfix"></div>
          <div class="box-body">
            <canvas id="barchart_material" width="300" height="300"></canvas>
           </div>
        </div>
      </div>
    </div>
  </div>
</div>


  <div class="col-lg-6 col-md-6 connectedSortable subanchor" id="topicImpression" style="display: none;">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title tnegvspos">Negatives vs Positives</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <canvas id="new_chart" width="300" height="300"></canvas>
      <div id="new_chart_error"></div>
    </div>
    <!-- /.box-body -->
  </div>
</div>

  <div class="col-lg-12 col-md-12 connectedSortable subanchor" id="wordImpression" style="display: none;">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title tnegvspos">Negatives vs Positives</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body" id="WordImpressionGraph_container">
      <canvas id="WordImpressionGraph"></canvas> 
      <div id="WordImpressionGraph_error"></div>
    </div>
    <!-- /.box-body -->
  </div>
</div>
</div>

 <div class="box" style="display: none;" id="reviewdiv">
            <div class="box-header with-border">
              <h3 class="box-title reviewsHeading"></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
             </div>
            </div>
            <div class="box-body" id="test-list">

            <!-- Appending reviews start  -->
            <input type="text" class="search form-control pull-right"  style="border-radius: 5px; width: 15%; background: #e8e8e8;"/>

            <ul class="list reviewMaster" style="list-style-type: none;">

            </ul>
                <div class="clearfix"></div>
                <ul class="pagination"></ul>
      <!-- Appending reviews end  -->
                </div>
            <!-- /.box-body -->
          </div>


</section>
</div>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Add as venue Modal -->
<div class="modal fade show" id="addasVenue" tabindex="-1" role="dialog" aria-labelledby="addasVenue" style="display: none; padding-left: 17px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body sevicelist text-center">
        <div class="col-lg-12 col-md-8">
    <div class="create-profile">

        <form class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="tname">Nom</label>
                    <input placeholder="Name" type="text" id="venueName" required="">
                </div>
            </div>
            <div class="col-sm-6">
                <div class=" form-group">
                    <label class="ttype">Types</label>
                    <select id="venueTypes" placeholder="Country">
     <option value="">Select Type</option>
   

   </select>
                </div>
            </div>

            <div class="clearfix">
                <!-- Modal -->
              </div>
            <div class="col-sm-12 m-t-25">



                <div class="table-responsive m-t-25">
                    <table id="pageContent" style="width: 100%" class="table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Service</th>
                                <th>Url</th>
                                <!-- <th>Option</th> -->
                            </tr>
                        </thead>
                        <tbody id="pageTable">

                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">
                                <input type="hidden" id="venueId" >
                                <button type="button" id="save_button" style="display: none;" class="btn main-btn pull-right tsave">save</button></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!--end of .table-responsive-->
            </div>
        </form>
    </div>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary tcancel" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- End Modal -->

<footer class="main-footer">
  <div class="pull-right d-none d-sm-inline-block">
  </div>Copyright &copy; 2018 <a href="https://www.datastitute.fr/">Spella Corp</a>. <span class="tallrights" ></span>
</footer>

<!-- ./wrapper -->

{literal}
<!-- jQuery 3 -->
<script src="assets/vendor_components/jquery/dist/jquery.min.js"></script>
<script src="js/cPager.js"></script>

<!-- popper -->
<script src="assets/vendor_components/popper/dist/popper.min.js"></script>

<!-- Bootstrap v4.0.0-beta -->
<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- SlimScroll -->
<script src="assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->
<script src="assets/vendor_components/fastclick/lib/fastclick.js"></script>

   <script src="js/dist/Chart.min.js"></script>
  <script src="js/dist/utils.js"></script>
  <!--Google Charts-->
<!-- <script type="text/javascript" src="https://www.google.com/jsapi" ></script> -->
 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


<!-- maximum_admin for demo purposes -->
<script src="js/demo.js"></script>
<script src="js/all.js"></script>
<script src="js/bootstrap-tagsinput.js"></script>
<link rel="stylesheet" type="text/css" href="css/bootstrap-tagsinput.css">
<link rel="stylesheet" type="text/css" href="css/select2.min.css">
<link rel="stylesheet" type="text/css" href="css/multi.select.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script>
  $( function() {
    $( "#startdate" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );

    $( function() {
    $( "#enddate" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );

        $( function() {
    $( "#staticenddate" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );

            $( function() {
    $( "#staticstartdate" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );

  </script>
<script src="js/template.js"></script>
<!-- <script src="assets/vendor_components/select2/dist/js/select2.full.js"></script> -->
<script src="js/select2.full.js"></script>
<script src="js/templates/groups.js"></script>
<script src="js/templates/groupChartRedrawer.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>

<script src="js/multilang.js"></script>
<script src="js/templates/groupChartHandler.js"></script>
<script src="js/templates/CommonFunctionsForText&Detail.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="js/multi.select.js"></script>

{/literal}
</body>
</html>
