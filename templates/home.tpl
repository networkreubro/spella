﻿</div>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h3 class="box-title thome" >Home Page</h3>
    <ol class="breadcrumb">
<!--         <li class="breadcrumb-item"><a href="#"><i class="fas fa-tachometer-alt"></i> Home</a></li>
  <li class="breadcrumb-item active">Dashboard</li> -->
</ol>
</section>
<!-- Main content -->

<section class="content">
  <!-- Default box -->
  {if $product_status eq "trial"}
      <div class="box p-3">
        <h4><span class="spanone">Your trial will end in </span> { $plan_expiry_days } <span class="spanfour"> days. </span></h4>
        {* <small class="text-info btn btn-sm"> <a class="text-info" href="https://spella.com/notre-offre/">Upgrade your plan</a> </small> *}
     </div>
  {/if}
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title tvenuelist">Venue List</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <a id="modify" class="btn bg-purple tmodify">
       modify
     </a>
   </div>
   <!-- /.box-body -->
   <div class="box-footer" id="NoVenueData">
    <div class="row">
     <div class="col-lg-12 col-md-12 accordion-list">
      <div class="row">
        <div class="col-sm-12">
         <div class="accordion"  id="append_values">
          <!-- </div>accordion div -->
        </div>
        <input type="hidden" id="productid" value="{$productid}">
        <ul class="accordion" id="appendval" style="list-style-type: none;">
        </ul>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>	
  <div style="padding-top: 10px;">
    <a style="display: none;" id="cancel" class="btn  bg-purple pull-right tcancel">Cancel</a>
    <a style="display: none;" id="save" class="btn bg-purple pull-righ tsave">Save</a>
  </div>
</div>
</div>
<!-- /.box-footer-->
</div>
<!-- /.box -->






</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Copetitors PopUp -->
<div class="modal fade" id="compUp" tabindex="-1" role="dialog" aria-labelledby="compUp" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><span id="venuename"></span>
          <label class="tcompetitor">Competitor(s)</label></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div  class="modal-body sevicelist text-center">
        <input type="hidden" id="numberofcompet" value={$no_of_competitors}>
          <span id="competitordata">

          </span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-blue tcancel" data-dismiss="modal">Cancel</button>
          <div id="compadd" class="fixed-btn">
            <button><svg class="svg-inline--fa fa-plus fa-w-14" aria-hidden="true" data-prefix="fas" data-icon="plus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M448 294.2v-76.4c0-13.3-10.7-24-24-24H286.2V56c0-13.3-10.7-24-24-24h-76.4c-13.3 0-24 10.7-24 24v137.8H24c-13.3 0-24 10.7-24 24v76.4c0 13.3 10.7 24 24 24h137.8V456c0 13.3 10.7 24 24 24h76.4c13.3 0 24-10.7 24-24V318.2H424c13.3 0 24-10.7 24-24z"></path></svg><!-- <i class="fas fa-plus"></i> --></button>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="addcomp" tabindex="-1" role="dialog" aria-labelledby="compUp" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel" class="taddcompetitor"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body sevicelist text-center">
          <div class="col-lg-12 col-md-8">
            <div class="create-profile">

              <form class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="tname">Nom</label>
                    <input placeholder="" type="text" id="compvenueName" required="">
                  </div>
                </div>


                <div class="col-sm-6">
                  <div class=" form-group">
                    <label class="ttype">Types de lieux</label>
                    <select id="compvenueTypes" placeholder="Country">
                     <option value="">Select Type</option>

                   </select>
                 </div>
               </div>
            <!--   <div class="col-sm-12">
    <div class=" form-group pull-right">
  <button class="btn" type="button">Cancel</button>
  <button class="btn" type="button">Save</button>
</div>
</div> -->
<div class="clearfix">
  <!-- Modal -->
</div>
<div class="col-sm-12 m-t-25">
  <div class="whitebox-title">
    <h3><a onclick="popupOpen();" style="text-decoration:none;" href="javascript:void(0);">
      <span><svg class="svg-inline--fa fa-plus fa-w-14" aria-hidden="true" data-prefix="fas" data-icon="plus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M448 294.2v-76.4c0-13.3-10.7-24-24-24H286.2V56c0-13.3-10.7-24-24-24h-76.4c-13.3 0-24 10.7-24 24v137.8H24c-13.3 0-24 10.7-24 24v76.4c0 13.3 10.7 24 24 24h137.8V456c0 13.3 10.7 24 24 24h76.4c13.3 0 24-10.7 24-24V318.2H424c13.3 0 24-10.7 24-24z"></path></svg><!-- <i class="fas fa-plus"></i> --></span>&nbsp;&nbsp;<span class="taddnewpage">Ajouter une page</span></a></h3>
    </div>
    <hr>


    <div class="table-responsive m-t-25">
      <table id="pageContent" class="table-bordered table-hover">
        <thead>
          <tr>
            <th class="tservice">Service</th>
            <th>URL</th>
            <th class="toption">Option</th>
          </tr>
        </thead>
        <tbody id="pageTable">

        </tbody>
        <tfoot>
          <tr>
            <td colspan="5"><input type="hidden" id="compvenueId">
              <button type="button" id="save_button" onclick="addcompet();" style="display: none;" class="btn main-btn pull-righ tsave">save</button></td>
            </tr>
          </tfoot>
        </table>
      </div>
      <!--end of .table-responsive-->


    </div>

  </form>


</div>

</div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-blue tcancel" data-dismiss="modal">Cancel</button>
</div>
</div>
</div>
</div>



<div class="modal fade" id="servicelist" tabindex="3" role="dialog" aria-labelledby="compUp" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><span id="venuename"></span>Competitor(s)</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div  class="modal-body sevicelist text-center">
        <ul style="list-style: none;" id="serviceinsertdata">
          <center>Loading...</center>
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-blue tcancel" data-dismiss="modal">Cancel</button>
        <div id="compadd" class="fixed-btn">
          <!-- <button><svg class="svg-inline--fa fa-plus fa-w-14" aria-hidden="true" data-prefix="fas" data-icon="plus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M448 294.2v-76.4c0-13.3-10.7-24-24-24H286.2V56c0-13.3-10.7-24-24-24h-76.4c-13.3 0-24 10.7-24 24v137.8H24c-13.3 0-24 10.7-24 24v76.4c0 13.3 10.7 24 24 24h137.8V456c0 13.3 10.7 24 24 24h76.4c13.3 0 24-10.7 24-24V318.2H424c13.3 0 24-10.7 24-24z"></path></svg><i class="fas fa-plus"></i></button> -->
        </div>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="Modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">pages</h5>
        <button id="pagesBack" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div  class="modal-body">
        <ul class="pageList" style="align-content: center;">

        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-blue tcancel" onclick="modal2close();"></button>
		<button type="button" class="btn btn-blue tother" onclick="showModal6();"></button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="Modal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
	
      <div class="modal-header create-profile">
        <h5 class="modal-title" id="exampleModalLabel"><input type="text" id="urlInsert"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  
      <div  class="modal-body">
       <a id="openUrl" href="" data-toggle="tooltip" title="Click to view the originalSite" target="_blank"> <img src="" id="pageInsert" style="width:100%;"   /></a>

     </div>
	 
     <div class="modal-footer">
      <!-- <button type="button" class="btn btn-secondary" onclick="modal3close();">Cancel</button> -->
      <button type="button" class="btn btn-blue tsave" onclick="pagesUtilize();">Save</button>
    </div>
	
  </div>
</div>
</div>

				
				
<div class="modal fade" id="Modal6" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <h5 class="modal-title" id="customPageTitle">Custom Page</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body create-profile">
          <input type="url" id="customPageURL" required name="customPageURL" value="" placeholder="Custom Page URL" />
		  <div class="col-sm-12 my-3">
			<span class="validation-info" id="em_error" style="color: red;display:none">Enter a Valid URL</span>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-blue" data-dismiss="modal">Cancel</button>
         <button type="button" class="btn btn-blue tvalidate" onclick="validateURL();"></button>
      </div>
    </div>
  </div>
</div>

<!-- Poup ends -->
<footer class="main-footer">
  <div class="pull-right d-none d-sm-inline-block">
  </div>Copyright &copy; 2018 <a href="https://www.datastitute.fr/">Spella Corp</a>. <span class="tallrights" ></span>
</footer>
<div class="fixed-btn">
  <button id="addVenue"><i class="fas fa-plus"></i></button>
</div>
<!-- ./wrapper -->
<script>
let no_of_competitors = {$no_of_competitors}
let numberofaddedcompet = {$no_of_competitors_added}
let cAddedVenues = {$no_of_venues_added}
let cTotalVenues = {$no_of_venues}
</script>
{literal}

<!-- jQuery 3 -->
<script src="assets/vendor_components/jquery/dist/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>


<!-- popper -->
<script src="assets/vendor_components/popper/dist/popper.min.js"></script>

<!-- Bootstrap v4.0.0-beta -->
<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- SlimScroll -->
<script src="assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->
<script src="assets/vendor_components/fastclick/lib/fastclick.js"></script>

<!-- maximum_admin App -->
<script src="js/template.js"></script>

<!-- maximum_admin for demo purposes -->

<script src="js/demo.js"></script>
<script src="js/all.js"></script>
<script src="js/templates/home.js"></script>
<script src="js/multilang.js"></script>
<script src="js/templates/homestate.js"></script>
<!-- testing -->
{/literal}
</body>
</html>
