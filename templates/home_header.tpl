<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="description" content="">
  <meta name="author" content="">
    <!-- <link rel="icon" href="images/favicon.ico"> -->
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
    <title>Spella</title>
  {literal}
  <script src="js/templates/logout.js"></script>
  {/literal}
  <!-- Bootstrap v4.0.0-beta -->
  <link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- daterange picker --> 
  <link rel="stylesheet" href="assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker --> 
  <link rel="stylesheet" href="assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">  

  <!--Css for pagination-->


<link rel="stylesheet" href="css/cPager.css">


    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/vendor_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="assets/vendor_components/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/vendor_components/glyphicons/glyphicon.css">
    <!-- Theme style -->
    <link href="https://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700" rel="stylesheet">
    <link href="css/loader.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/master_style.css">
 

    <!-- Morris charts -->
    <link rel="stylesheet" href="assets/vendor_components/morris.js/morris.css">
</head>