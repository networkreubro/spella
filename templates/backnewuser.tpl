 <link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- daterange picker --> 
  <link rel="stylesheet" href="assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker --> 
  <link rel="stylesheet" href="assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">  

  <!--Css for pagination-->


<link rel="stylesheet" href="css/cPager.css">


    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/vendor_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="assets/vendor_components/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/vendor_components/glyphicons/glyphicon.css">
    <!-- Theme style -->
    <link href="https://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700" rel="stylesheet">
    <link href="css/loader.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/master_style.css">

<body class="hold-transition login-page">
  <div class="container">
    <div class="row justify-content-md-center">
      <div class="col-md-11 col-sm-11 col-12">
              <div class="login-logo">
      <img src="images/logo-spella.png" alt="" class="img-fluid" style="width: 30%;">
  </div>
        <div class="newuser_box box p-4">
          <form class="row  py-5">
            <div class="col-sm-6">
              <div class="form-group row">
                <label class="col-lg-3 col-sm-4 tname">Name</label>
                <div class="col-lg-9 col-sm-8">
                  <input id="name" placeholder="Name" type="text" class="form-control" value="" required="">
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group row">
               <label class="col-lg-3 col-sm-4 temail">Email</label>
               <div class="col-lg-9 col-sm-8">
                <input id="email" placeholder="Email address" class="form-control" type="email" value="" required="">
               </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class=" form-group row">
             <label class="col-lg-3 col-sm-4 tpassword">Password</label>
             <div class="col-lg-9 col-sm-8">
              <input id="password" placeholder="Password" class="form-control" type="Password" value="" required="">
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class=" form-group row">
           <label class="col-lg-3 col-sm-4 treportinglang">Language for email reports</label>
           <div class="col-lg-9 col-sm-8">
             <select onchange="" id="language" class="form-control">
               <option class="tEnglish" value="EN">English</option>
               <option class="tFrench" value="FR">French</option>
             </select> 
           </div>
         </div>
       </div>
       <div class="col-sm-6">
        <div class=" form-group row">
         <label class="col-lg-3 col-sm-4 tcntry">Country</label>
         <div class="col-lg-9 col-sm-8">
          <select class="form-control" placeholder="Country">
           <option value="" class="">Select Country</option>
           <option value="" >Afghanistan</option>
           <option value="" >america</option>
           <option value="" >china</option>
           <option value="" >canada</option>
           <option value="" >france</option>
           <option value="" >india</option>
           <option value="" >pakistan</option>
         </select>
       </div>
     </div>
   </div>
   <div class="col-sm-6">
    <div class=" form-group row">
     <label class="col-lg-3 col-sm-4 tphone">Phone</label>
     <div class="col-lg-9 col-sm-8">
      <input placeholder="Phone" class="form-control" type="text" value="" required="">
    </div>
  </div>
</div>
<div class="col-sm-12">
  <div class="form-group row">
    <div class="col-sm-12">
     <span style="font-weight: 600;" class="tweeklyemails">Weekly Emails</span></div>
<!--  <div class="rss">
    <input type="checkbox" id="buttonThree" />
  <label for="buttonThree">
    <i></i>
  </label>
  </div> -->
    <div class="col-sm-10 box-body">
            
        <div class="demo-radio-button">
          <input name="group1" type="radio" id="radiow_7" class="radio-col-red" checked="checked">
          <label class="tOn" for="radiow_7">On</label>
          <input name="group1" type="radio" id="radiow_9" class="radio-col-purple">
          <label class="tOff" for="radiow_9">Off</label>
           </div>
            
            </div>
  </div>
  </div>
<div class="col-sm-6">
  <div class=" form-group row">
   <label class="col-lg-3 col-sm-4 tweekdaymails">Email on weekday</label>
   <div class="col-lg-9 col-sm-8">
     <select onchange="" id="email_on_weekday" class="form-control" placeholder="Country">
       <option value="0" class="tsunday">Sunday</option>
       <option value="1" class="tmonday">Monday</option>
       <option value="2" class="ttuesday">Tuesday</option>
       <option value="3" class="twednesday">Wednesday</option>
       <option value="4" class="tthursday">Thursday</option>
       <option value="5" class="tfriday">Friday</option>
       <option value="6" class="tsaturday">Saturday</option>
     </select> 
   </div>
 </div>
</div>
<div class="col-sm-6">
  <div class=" form-group row">
   <label class="col-lg-3 col-sm-4 treportRange">Report Range</label>
   <div class="col-lg-9 col-sm-8">
     <select onchange="" id="report_range" class="form-control" placeholder="Country">
       <option value="1" class="tcrMonth">Current Month</option>
       <option value="2" class="t2months">2 months</option>
       <option value="3" class="t3months">3 Months</option>
       <option value="4" class="t4months">4 months</option>
       <option value="5" class="t5months">5 months</option>
       <option value="6" class="t6months">6 Months</option>
       <option value="7" class="t7months">7 months</option>
       <option value="8" class="t8months">8 months</option>
       <option value="9" class="t9months">9 months</option>
       <option value="10" class="t10months">10 months</option>
       <option value="11" class="t11months">11 months</option>
       <option value="12" class="t12months">12 months</option>
     </select> 
   </div>
 </div>
</div>
<div class="col-sm-12">
  <div class="form-group row">
    <div class="col-sm-12">
     <span style="font-weight: 600;" class="tdailyemails">Daily Emails</span></div>

    <div class="col-sm-10 box-body">
            
        <div class="demo-radio-button">
          <input name="group2" type="radio" id="radiod_7" class="radio-col-red" checked="checked">
          <label class="" for="radiod_7">On</label>
          <input name="group2" type="radio" id="radiod_9" class="radio-col-purple">
          <label class="" for="radiod_9">Off</label>
           </div>
            
            </div>
  </div>
  </div>
<div class="col-sm-6">
  <div class=" form-group row">
     <label class="col-lg-3 col-sm-4">Daily alert threshold</label>
       <div class="col-lg-9 col-sm-8">
   <select class="form-control" placeholder="Country">
     <option value="1">1</option>
     <option value="1.5">1.5</option>
     <option value="2">2</option>
     <option value="2.5">2.5</option>
     <option value="3">3</option>
     <option value="3.5">3.5</option>
     <option value="4">4</option>
     <option value="4.5">4.5</option>
     <option value="5">5 (include all)</option>
   </select> 
  </div>
</div>
  </div>

<div class="col-sm-12">
  <div class="form-group row">
    <div class="col-sm-12">
     <span style="font-weight: 600;" class="tpushNotifications">Push Notifications</span></div>
<!--  <div class="rss">
    <input type="checkbox" id="buttonThree" />
  <label for="buttonThree">
    <i></i>
  </label>
  </div> -->
    <div class="col-sm-10 box-body">
            
        <div class="demo-radio-button">
          <input name="group3" type="radio" id="radio_7" class="radio-col-red" checked="checked">
          <label class="tOn" for="radio_7">On</label>
          <input name="group3" type="radio" id="radio_9" class="radio-col-purple">
          <label class="tOff" for="radio_9">Off</label>
           </div>
            
            </div>
  </div>
  </div>
  <div class="col-sm-12">
    <div class=" form-group pull-right">
<a href="index.php" class="pl-3">Already have an account? Login Here</a>
<a class="btn bg-purple tsave" type="button" onclick="newuser()">Register</a>


</div>
</div>












</form>




</div>
</div>
</div>
</div>

<!-- /.login-box -->
<div class="modal fade" id="forgotUp" tabindex="-1" role="dialog" aria-labelledby="compUp" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Forgot Password?</h5>
        <button  type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div  class="modal-body sevicelist text-center">


        <div class="login-box-body">
          <p class="login-box-msg" id="rp">Enter your registred email to reset the password.</p>
          <input type="text" id="femail" hidden>
          <div class="form-group has-feedback" id="replace">
            <input type="text" id="forgotemail" class="form-control" placeholder="Email">
            <div class="col-sm-12">
              <span class="validation-info pull-right" id="forgot_error" style="display: none;"></span>
            </div>
          </div>
          <div class="clearfix"></div>   
          <div class="row">

            <div class="col-12 text-center" id="rbutton">
              <button type="button" class="btn btn-block btn-purple margin-top-10" onclick="resetpassword();">PROCEED</button>
            </div>
            <!-- /.col -->
          </div>
        </div>



      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> -->
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="codeUp" tabindex="-1" role="dialog" aria-labelledby="codeUp" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Enter Passcode</h5>
        <button  type="button" class="close" data-dismiss="modal"  aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div  class="modal-body sevicelist text-center">


        <div class="login-box-body">
          <p class="login-box-msg" id="rp">Enter your code  to reset the password.</p>
          <input type="text" id="fcode" class="form-control" placeholder="Code">
          <div id="fcode_error"></div>
          <div class="col-sm-12"> <span class="validation-info pull-right" id="forgot_error" style="display: none;"></span> </div>
          <div class="clearfix"></div>   
          <div class="row">

            <div class="col-12 text-center" id="rbutton">
              <button type="button" class="btn btn-block btn-purple margin-top-10" onclick="check_code();">PROCEED</button>
            </div>
            <!-- /.col -->
          </div>
        </div>



      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> -->
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="resetUp" tabindex="-1" role="dialog" aria-labelledby="resetUp" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Password</h5>
        <button  type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div  class="modal-body sevicelist text-center">
        <div class="login-box-body">
          <p class="login-box-msg" id="rp">Enter the new password.</p>
          <input type="text" id="femail" hidden>
          <div class="form-group has-feedback" id="replace">
            <input type="password" id="npassword" class="form-control" placeholder="">
            <div class="col-sm-12">
              <span class="validation-info pull-right" id="forgot_error1" style="display: none;"></span>
            </div>
          </div>
          <div class="clearfix"></div>   
          <div class="row">
            <div class="col-12 text-center" id="rbutton">
              <button type="button" class="btn btn-block btn-purple margin-top-10" onclick="updatePass();">PROCEED</button>
            </div>
            <!-- /.col -->
          </div>
        </div>



      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> -->
      </div>
    </div>
  </div>
</div>

<?php include ;
echo ;
?>


</body>
{literal}
<!-- jQuery 3 -->
<script src="assets/vendor_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery cookie -->
<script src="assets/vendor_components/jquery/dist/jquery.cookie.js"></script>
<!-- popper -->
<script src="assets/vendor_components/popper/dist/popper.min.js"></script>
<!-- Bootstrap v4.0.0-beta -->
<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="js/templates/new_user.js"></script>

{/literal} 
</html>
