<body class="hold-transition sidebar-mini inner-body">
      <div id="loading" class="loading-body" style="z-index: 5000; height: 100% !important; position: fixed; top: 0;">
    <div class="loading-container">
    <div class="loading"></div>
    <div id="loading-text"><img src="images/logo-small.png" alt=""></div>
</div>
</div>
  <!-- Site wrapper -->
  <div class="wrapper">
    <header class="main-header fixed-top">
      <!-- Logo -->
      <a href="index.php" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><img src="images/logo-small.png" alt="" /></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img src="images/logo.svg" alt="" /></span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
      </nav>
    </header>
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar fixed-top">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
         <li onclick="redirect('home');" class="treeview">
          <a  href="home.php">
            <i class="fas fa-tachometer-alt"></i>
            <span class="thome">Home</span>
          </a>
        </li>
        <li onclick="redirect('profile');" class="treeview">
        <a  href="profile.php" class="link"  title="" data-original-title="Settings">
          <!-- <i class="fa fa-cog fa-spin"></i> -->
          <i class="fas fa-user"></i>
          <span class="tuserarea">UserArea</span></a>
        </li>
        <li onclick="redirect('add_venue');" class="treeview">
        <a  href="add_venue.php" class="link"  title="" data-original-title="Settings">
          <i class="fa fa-plus"></i>
          <span class="taddvenue">Add Venue</span></a>
        </li>
        <li onclick="redirect('groups');" class="treeview">
        <a href="groups.php" class="link"  title="" data-original-title="Settings">
          <!-- <i class="fa fa-cog fa-spin"></i> -->
          <i   class="fas fa-search"></i>
          <span  class="tgroups">Groups</span></a>
        </li>
          <li onclick="redirect('subscription');" class="treeview">
        <a href="subscription.php" class="link"  title="" data-original-title="Settings">
          <!-- <i class="fa fa-cog fa-spin"></i> -->
<i class="fas fa-ticket-alt"></i>          <span  class="tsubscription">Subscription</span></a>
        </li>
          <li onclick="redirect('Services');" style="display: none;" id="servAnchor" class="treeview hidden">
         <a href="" class="link"  title="" data-original-title="Logout"><i class="fas fa-chart-pie"></i>
          <span  class="tservices">Services</span></a>
        </li>
          <li onclick="redirect('Negatives');" style="display: none;" id="negAnchor" class="treeview">
         <a href="" class="link"  title="" data-original-title="Logout"><i class="far fa-chart-bar"></i>
          <span  class="tnegvspos">Negatives vs Positives</span></a>
        </li>
          <li  onclick="redirect('notes')" style="display: none;" id="NotAnchor" class="treeview">
         <a href="" class="link"  title="" data-original-title="Logout"><i class="far fa-chart-bar"></i>
          <span  class="tnotes">Notes</span></a>
        </li>
          <li onclick="redirect('reviews')" style="display: none;" id="revAnchor" class="treeview">
         <a href="" class="link"  title="" data-original-title="Logout"><i class="fas fa-users"></i>
          <span  class="treviews">Reviews</span></a>
        </li>
          <li onclick="text_go();" style="display: none;" id="texAnchor" class="treeview">
         <a href="" class="link"  title="" data-original-title="Logout"><i class="fas fa-chart-area"></i>
          <span  class="ttextanalysis">Text Analysis</span></a>
        </li>
        <li onclick="logout();" class="treeview">
         <a href="" class="link"  title="" data-original-title="Logout"><i class="fa fa-power-off"></i>
          <span  class="tlogout">Logout</span></a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
       <div class="sidebar-footer">
           <select style="display: none;" onchange="switchLanguage();" id="langs" class="form-control" placeholder="Country">
      <option value="FR">French</option>
      <option value="EN">English</option>
    </select>
      <!-- item-->
      <a id="settings" onclick="redirect('profile');" style="cursor: pointer;" class="link" data-toggle="tooltip" title="" data-original-title="Settings"><i class="fa fa-cog fa-spin"></i></a>
      <!-- item-->
      <a id="swlanguage" onclick="javascript:$('#langs').toggle();" class="link" data-toggle="tooltip" title="" data-original-title="Language"> <i class="fa fa-globe"></i></a>
      <!-- item-->
      <a id="blogout" onclick="logout();" class="link" style="cursor: pointer;"  data-toggle="tooltip" title="" data-original-title="Logout"><i class="fa fa-power-off"></i></a>

    </div>
  </aside>
  <div style="display: none;">