</div>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h2 class="center" id="venueName"></h2>
    <ol class="breadcrumb">
      <!-- <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
      <!-- <li class="breadcrumb-item"><a href="#">Examples</a></li> -->
      <!-- <li class="breadcrumb-item active">Dashboard</li> -->
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-5">
        <table class="venue-rating">
          <tbody><tr>
            <td class="bold taveragenote">Average note:<span id="starVal"></span></td>
            <td>
              <div class="stars-outer">
                <div class="stars-inner" ></div>
              </div>
            </td>
          </tr><tr>
          <td class="bold tselectedrange">Selected Date Range :</td>
              <td id="DateRange" style="padding-left: 5px;"></td></tr>
        </tbody></table>
      </div>
      <div class="col-md-7">
        <div class="row">
        <div class="col-sm-12 form-group">
        <div class="row venue-periode">
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control" id="datepicker1">
              </div>
               <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control" id="datepicker2">
              </div>
               <div class="button">
              <button class="btn btn-blue tapply" id="dateValidator">Apply</button>
            </div>
      <div class="form-group">
        <select onchange="testfunctionText();" id="DateCustomText" class="form-control">
          <option class="tselectrange" value="NULL">Select Range</option>
          <option class="tthismonth" value="crMonth">This Month</option>
          <option class="t3months" value="3months">3 Months</option>
          <option class="t6months" value="6months">6 Months</option>
          <option class="tthisyear" value="crYear">This Year</option>
          <option class="t2years" value="2years">2 Years</option>
          <option class="t3years" value="3years">3 Years</option>
        </select>
      </div>
    </div>
  </div>
</div>
 </div>
 <div class="clearfix"></div>
      <div class="col">
          <button class="btn btn-purple pull-right" onclick="BackToVenueDetail();" style="padding: 10px 25px; border-radius: 3px;"><i class="fas fa-angle-double-left"></i>&nbsp;Venue Details</button>
      </div>
    </div>
        <div class="row top15">
          <!--up to here-->
  <div class="col-lg-6 col-md-6 connectedSortable">
    <div class="box">
      <div class="box-header with-border">
        <h3 id="Services" class="box-title tdistoftopics">Main Topics</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
          title="Collapse">
          <i class="fa fa-minus"></i></button>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-body">
        <div class="row">
         <div class="col-lg-12 col-md-12 venue-details">
          <div class="row m-t-25">
            <div class="col-md-5 ml-auto">

            </div>
          </div>
          <div class="clearfix"></div>
          <div class="box-body">
            <!-- <div id="barchart_material" style="height: 500px;"></div> -->
            <canvas id="barchart_material"  width="300" height="300"></canvas>
            <div id="barchart_material_error"></div>
           </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <div class="col-lg-6 col-md-6 connectedSortable">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title timpbytopics">Main Topic Negatives vs Positives</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
    <!--   <div id="new_chart" style="height: 500px;"></div> -->
     <canvas id="new_chart"  width="300" height="300"></canvas>
     <div id="myChart_new_chart_error"></div>
    </div>
    <!-- /.box-body -->
  </div>
</div>
  <div class="col-lg-12 col-md-12 connectedSortable">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title ttopics">Words per Negatives vs Positives</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body" id="WordImpressionGraph_container">
     <!--  <div id="WordImpressionGraph" ></div> -->
     <canvas id="WordImpressionGraph" width="300" ></canvas>
     <div id="WordImpressionGraph_error"></div>
    </div>
    <!-- /.box-body -->
  </div>
</div>


<div id="tableAnchor" class="col-lg-12 col-md-12 connectedSortable">
 <div class="box">
  <div class="box-header with-border">
    <h3 class="box-title reviewsHeading"></h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
  </div>
  <div class="box-body " id="test-list">
  
  <!-- Appending reviews start  -->
    <input type="text" class="search form-control pull-right"  style="border-radius: 5px; width: 15%; background: #e8e8e8;"/>

  <ul class="list">

  </ul>
  <div class="clearfix"></div>
  <ul class="pagination"></ul>
  <!-- Appending reviews end  -->


  </div>
  <!-- /.box-body -->
</div>
</div>



</div>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<footer class="main-footer">
  <div class="pull-right d-none d-sm-inline-block">
  </div>Copyright &copy; 2018 <a href="https://www.datastitute.fr/">Spella Corp</a>. <span class="tallrights" ></span>
</footer>

<!-- ./wrapper -->

{literal}
<!-- jQuery 3 -->
<script src="assets/vendor_components/jquery/dist/jquery.min.js"></script>


<!-- popper -->
<script src="assets/vendor_components/popper/dist/popper.min.js"></script>

<!-- Bootstrap v4.0.0-beta -->
<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- SlimScroll -->
<script src="assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->
<script src="assets/vendor_components/fastclick/lib/fastclick.js"></script>

<!-- maximum_admin App -->
<script src="js/template.js"></script>
<!-- maximum_admin for demo purposes -->
<script src="js/demo.js"></script>
<script src="js/all.js"></script>
<!-- jQuery Knob -->
<script src="assets/vendor_components/jquery-knob/js/jquery.knob.js"></script>
<!-- maximum_admin for inline Chart purposes -->
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>

<!-- <script src="js/widget-inline-charts.js"></script> -->
<script type="text/javascript" src="js/loader.js" ></script>
  <script src="js/dist/Chart.min.js"></script>
  <script src="js/dist/utils.js"></script>
<!-- <script type="text/javascript" src="https://www.google.com/jsapi"></script> -->

<!-- maximum_admin for Morris Chart purposes -->
<!-- <script src="js/pages/widget-morris-charts.js"></script> -->
<!--Google Charts-->
<script type="text/javascript" src="https://www.google.com/jsapi" ></script>
<!-- DataTables -->
<script src="assets/vendor_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- This is data table -->
<script src="assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
<!-- script by dijo -->
<script src="js/templates/text_analysis_graph_redrawer.js"></script>

<script src="js/templates/text_analysis.js"></script>
<script src="js/templates/CommonFunctionsForText&Detail.js"></script>
<script src="js/templates/chartHandler.js"></script>
<script src="js/templates/duplication.js"></script>
<script src="js/templates/duplicationchartHandler.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="js/multilang.js"></script>

<!--date range picker-->
<script src="assets/vendor_components/moment/min/moment.min.js"></script>
<script src="assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js"></script>

  <!-- bootstrap datepicker -->
  <script src="assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>


{/literal}
</body>
</html>
