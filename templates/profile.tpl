</div>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h3 id="username"></h3>
      <ol class="breadcrumb">
        <!-- <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
        <!-- <li class="breadcrumb-item"><a href="#">Examples</a></li> -->
        <!-- <li class="breadcrumb-item active">Dashboard</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title tuserarea">User Area</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
            <i class="fa fa-minus"></i></button>
      <!--       <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button> -->
            </div>
          </div>
          <div class="box-body">
          <a  onclick="edit();" class="btn btn-app bg-purple modify tmodify">
               modify
              </a>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <div class="row">
             <div class="col-lg-10 col-md-12 col-sm-12 m-t-25">
                    <form class="row">
<div class="col-sm-6">
  <div class="form-group row">
      <label class="col-lg-3 col-sm-4 tname">Name</label>
      <div class="col-lg-9 col-sm-8">
    <input id="name" placeholder="Name" type="text" class="form-control" value="" required>
  </div>
  </div>
</div>
<div class="col-sm-6">
    <div class="form-group row">
       <label class="col-lg-3 col-sm-4 temail">Email</label>
         <div class="col-lg-9 col-sm-8">
  <input id="email" placeholder="Email address" class="form-control" type="email" value="" required>
  <span id="email_up"></span>
</div>
</div>
</div>
<div class="col-sm-6">
  <div class=" form-group row">
     <label class="col-lg-3 col-sm-4 tpassword">Password</label>
       <div class="col-lg-9 col-sm-8">
  <input id="password" placeholder="Password" class="form-control" type="Password" value="" required>
  <span id="pass_up"></span>
</div>
</div>
</div>

<div class="col-sm-6">
  <div class=" form-group row">
     <label class="col-lg-3 col-sm-4 treportinglang">Language</label>
       <div class="col-lg-9 col-sm-8">
   <select onchange='' id="language" class="form-control">
     <option class="tEnglish" value="EN"> English</option>
     <option class="tFrench" value="FR"> French</option>


   </select> 
  </div>
</div>
  </div>

<div class="col-sm-6">
  <div class=" form-group row">
     <label class="col-lg-3 col-sm-4 tcntry">Country</label>
       <div class="col-lg-9 col-sm-8">
   <select onchange='prefix_inser()' id="country" class="form-control" placeholder="Country">
     <option value="" class="tselectCountry">Select Country</option>
   </select> 
   <span id="country_up"></span>
  </div>
</div>
  </div>

  <div class="col-sm-6">

  <div class=" form-group row">
     <label class="col-lg-3 col-sm-4 tphone">Phone</label>
       <div class="col-lg-9 col-sm-8">
    <input id="phnumber" placeholder="Phone" class="form-control" type="text" value="" required>
    <span id="ph_up"></span>
    
  </div>
  </div>
  </div>

  <div class="col-sm-12">
  <div class="form-group row">
    <div class="col-sm-12">
     <span style="font-weight: 600;" class="tweeklyemails">Weekly Emails</span></div>
<!--  <div class="rss">
    <input type="checkbox" id="buttonThree" />
  <label for="buttonThree">
    <i></i>
  </label>
  </div> -->
    <div class="col-sm-10 box-body">
            
        <div class="demo-radio-button">
          <input name="group1" type="radio" id="radiow_7" class="radio-col-red" />
          <label class="tOn" class="tOn" for="radiow_7">On</label>
          <input name="group1" type="radio" id="radiow_9" class="radio-col-purple"  />
          <label class="tOff" class="tOff" for="radiow_9">Off</label>
           </div>
            
            </div>
  </div>
  </div>

  <div class="col-sm-6">
  <div class=" form-group row">
     <label class="col-lg-3 col-sm-4 tweekdaymails">Email on weekday</label>
       <div class="col-lg-9 col-sm-8">
   <select onchange='' id="email_on_weekday" class="form-control" placeholder="Country">
     <option value="0" class="tsunday">Sunday</option>
     <option value="1" class="tmonday">Monday</option>
     <option value="2" class="ttuesday">Tuesday</option>
     <option value="3" class="twednesday">Wednesday</option>
     <option value="4" class="tthursday">Thursday</option>
     <option value="5" class="tfriday">Friday</option>
     <option value="6" class="tsaturday">Saturday</option>
   </select> 
   <span id="country_up"></span>
  </div>
</div>
  </div>

  <div class="col-sm-6">
  <div class=" form-group row">
     <label class="col-lg-3 col-sm-4 treportRange">Report range</label>
       <div class="col-lg-9 col-sm-8">
   <select onchange='' id="report_range" class="form-control" placeholder="Country">
     <option value="1" class="tcrMonth">Current Month</option>
     <option value="2" class="t2months">2 months</option>
     <option value="3" class="t3months">3 months</option>
     <option value="4" class="t4months">4 months</option>
     <option value="5" class="t5months">5 months</option>
     <option value="6" class="t6months">6 months</option>
     <option value="7" class="t7months">7 months</option>
     <option value="8" class="t8months">8 months</option>
     <option value="9" class="t9months">9 months</option>
     <option value="10" class="t10months">10 months</option>
     <option value="11" class="t11months">11 months</option>
     <option value="12" class="t12months">12 months</option>

   </select> 
   <span id="country_up"></span>
  </div>
</div>
  </div>

  <div class="col-sm-12">
  <div class="form-group row">
    <div class="col-sm-12">
     <span style="font-weight: 600;" class="tdailyemails">Daily Emails</span></div>

    <div class="col-sm-10 box-body">
            
        <div class="demo-radio-button">
          <input name="group2" type="radio" id="radiod_7" class="radio-col-red" />
          <label class="tOn" for="radiod_7">On</label>
          <input name="group2" type="radio" id="radiod_9" class="radio-col-purple"  />
          <label class="tOff" for="radiod_9">Off</label>
           </div>
            
            </div>
  </div>
  </div>

  <div class="col-sm-6">
  <div class=" form-group row">
     <label class="col-lg-3 col-sm-4 tdailyReportThreshold">Daily alert threshold</label>
       <div class="col-lg-9 col-sm-8">
   <select onchange='' id="daily_alert_threshold" class="form-control" placeholder="Country">
     <option value="1">1</option>
     <option value="1.5">1.5</option>
     <option value="2">2</option>
     <option value="2.5">2.5</option>
     <option value="3">3</option>
     <option value="3.5">3.5</option>
     <option value="4">4</option>
     <option value="4.5">4.5</option>
     <option value="5">5 (include all)</option>
   </select> 
   <span id="country_up"></span>
  </div>
</div>
  </div>


  <div class="col-sm-12">
  <div class="form-group row">
    <div class="col-sm-12">
     <span style="font-weight: 600;" class="tpushNotifications">Push Notifications</span></div>
<!--  <div class="rss">
    <input type="checkbox" id="buttonThree" />
  <label for="buttonThree">
    <i></i>
  </label>
  </div> -->
    <div class="col-sm-10 box-body">
            
        <div class="demo-radio-button">
          <input name="group3" type="radio" id="radio_7" class="radio-col-red" />
          <label class="tOn" for="radio_7">On</label>
          <input name="group3" type="radio" id="radio_9" class="radio-col-purple"  />
          <label class="tOff" for="radio_9">Off</label>
           </div>
            
            </div>
  </div>
  </div>
  <div class="col-sm-12">
    <div class=" form-group pull-right">
  <button class="btn bg-purple save_button tcancel" style="display: none;" onclick="cancel();" type="button">Cancel</button>
  <button class="btn bg-purple cancel_button tsave" style="display: none;" onclick="save();" type="button">Save</button>
</div>
</div>
</form>
    
             
</div>
</div>

<div class="col-lg-2 col-md-3">

</div>
</div>

</div>
<!-- /.box-footer-->
</div>
<!-- /.box -->

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<footer class="main-footer">
  <div class="pull-right d-none d-sm-inline-block">
  </div>Copyright &copy; 2018 <a href="https://www.datastitute.fr/">Spella Corp</a>. <span class="tallrights" ></span>
</footer>

<!-- ./wrapper -->

{literal}
<!-- jQuery 3 -->
<script src="assets/vendor_components/jquery/dist/jquery.min.js"></script>

<!-- popper -->
<script src="assets/vendor_components/popper/dist/popper.min.js"></script>

<!-- Bootstrap v4.0.0-beta -->
<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- SlimScroll -->
<script src="assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->
<script src="assets/vendor_components/fastclick/lib/fastclick.js"></script>

<!-- maximum_admin App -->
<script src="js/template.js"></script>

<!-- maximum_admin for demo purposes -->
<script src="js/demo.js"></script>
<script src="js/all.js"></script>

<script src="js/templates/profile.js"></script>
<script src="js/multilang.js"></script>


{/literal}
</body>
</html>
