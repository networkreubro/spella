<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
    
    <title>Spella Dashboard </title>
	<!-- Bootstrap v4.0.0-beta -->
	<link rel="stylesheet" href="assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="assets/vendor_components/font-awesome/css/font-awesome.min.css">

	<!-- Ionicons -->
	<link rel="stylesheet" href="assets/vendor_components/Ionicons/css/ionicons.min.css">

	<!-- Theme style -->
	<link rel="stylesheet" href="css/master_style.css">
	<link rel="stylesheet" href="css/loader.css">

	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/skins/_all-skins.css">	
	<link href="https://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700" rel="stylesheet">
	

</head>