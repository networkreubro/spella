</div>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <!-- <h1>Venue List</h1> -->
    <h2 class="center" id="venueName"></h2>
    <ol class="breadcrumb">
      <!-- <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
      <!-- <li class="breadcrumb-item"><a href="#">Examples</a></li> -->
      <!-- <li class="breadcrumb-item active tdash">Dashboard</li> -->
    </ol>
  </section>
  <!-- Main content -->
  <section  class="content">
    <div class="row">
      <div class="col-md-6 col-lg-5 col-sm-12">
        <table class="venue-rating">
          <tr>
            <td class="bold taveragenote">
              <div class="ipd-width">
                Average note:<span id="starVal"></span>
              </div>
              </td>
            <td>
              <div class="stars-outer ">
                <div class="stars-inner"></div>
                <div class="bold" id="starOuterVal" style=" float: right; padding-top: 5px;"></div>
              </div>
            </td>
          </tr><tr>
          <td class="bold tselectedrange">Selected Date Range :</td>
              <td id="DateRange" style="padding-left: 5px;"></td></tr>
        </table>
      </div>
      <div class="col-md-6 col-lg-7 col-sm-12">
        <div class="row">
       <div class="col-sm-12 form-group">
       	<div class="venue-periode">
			        <div class="input-group">
			          <div class="input-group-addon">
			            <i class="fa fa-calendar"></i>
			          </div>
			          <input type="text" class="form-control" id="datepicker1">
			        </div>
     
         
			         <div class="input-group">
			          <div class="input-group-addon">
			            <i class="fa fa-calendar"></i>
			          </div>
			          <input type="text" class="form-control" id="datepicker2">
			        </div>
      
            <div class="button">
              <button class="btn btn-blue tapply" id="dateValidator">Apply</button>
            </div>

                  <div class="form-group">
        <select onchange="testfunction();" id="DateCustom" class="form-control">
          <option class="tselectrange" value="NULL">Select Range</option>
          <option class="tthismonth" value="crMonth">This Month</option>
          <option class="t3months" value="3months">3 Months</option>
          <option class="t6months" value="6months">6 Months</option>
          <option class="tthisyear" value="crYear">This Year</option>
          <option class="t2years" value="2years">2 Years</option>
          <option class="t3years" value="3years">3 Years</option>
        </select>
      </div>
		</div>

      </div>

    </div>
      </div>
  </div>
  <hr />
  <div class="review-header">
   <div class="row">

    <div style="pointer-events:none;" class="col-12 col-md-4 col-sm-4 col-lg-2">
      <div class="info-box">
        <h3 class="info-box-text upper-text text-center ttotalreviews">Total Reviews</h3>
                <div class="info-box-content">
          <span class="info-box-icon bg-blue"><i class="ion ion-stats-bars"></i></span>
          <span class="info-box-number" id="totalReviews"></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div style="pointer-events:none;" class="col-12 col-md-4 col-sm-4 col-lg-2">
      <div class="info-box">
        <h3 class="info-box-text text-center tgoodreviews" >Good Reviews</h3>
               <div class="info-box-content">
                 <span class="info-box-icon bg-green"><i class="ion ion-thumbsup"></i></span>
          <span class="info-box-number" id="positiveReviews"></span>
        </div>

        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
   
    <div style="pointer-events:none;" class="col-12 col-md-4 col-sm-4 col-lg-2">
      <div class="info-box">
       <h3 class="info-box-text text-center tbadreviews">Bad Reviews</h3>
       <div class="info-box-content">
       <span class="info-box-icon bg-red"><i class="fa fa-thumbs-down"></i></span>
      <span class="info-box-number" id="NegativeReviews"></span>

      </div>

      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div style="pointer-events:none;"  class="col-12 col-md-4 col-sm-4 col-lg-2">
    <div class="info-box">
     <h3 class="info-box-text text-center tmothlytend">Monthly Tendency</h3>
     <div class="info-box-content">
     <span class="info-box-icon bg-cyan"><i class="fa fa-calendar-minus"></i></span>
      <span class="info-box-number" id="ThisMonthtendency"></span>
    </div>

    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
<div style="pointer-events:none;" class="col-12 col-md-4 col-sm-4 col-lg-2">
  <div class="info-box">
   <h3 class="info-box-text text-center tpreviousWeek">Previous Week Tendency</h3>
     <div class="info-box-content">
     <span class="info-box-icon bg-cyan"><i class="fa fa-calendar-minus"></i></span>
    <span class="info-box-number" id="PrevWeektendency">sf</span>
  </div>


  <!-- /.info-box-content -->
</div>
<!-- /.info-box -->
</div>
<!-- /.col -->
<div style="pointer-events:none;" class="col-12 col-md-4 col-sm-4 col-lg-2">
  <div class="info-box">
   <h3 class="info-box-text text-center tweeklytend">Weekly Tendency</h3>
   <div class="info-box-content">
       <span class="info-box-icon bg-cyan"><i class="fa fa-calendar-minus"></i></span>
    <span class="info-box-number" id="ThisWeektendency">sf</span>
  </div>


  <!-- /.info-box-content -->
</div>
<!-- /.info-box -->
</div>
<!-- /.col -->
</div>
</div>

<div id="contentManager" class="row">
  <!-- <ul class="connectedSortable"> -->

   <div id="donutAnchor" class="col-lg-6 col-md-6 connectedSortable">
    <div class="box">
      <div class="box-header with-border">
        <h3 id="Services" class="box-title tservices">Services</h3>
        <div class="box-tools pull-right">
   <button type="button" onclick="StartFunction('sales-chart');" class="btn btn-box-tool"><i class="fas fa-copy"></i>
    </button>
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
          title="Collapse">
          <i class="fa fa-minus"></i></button>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-body minheighter">
        <div class="row">
         <div class="col-lg-12 col-md-12 venue-details">
          <div class="row m-t-25">
            <div class="col-md-6 no-padd">
             <div class="box-body">
              <div id="clearDonut" class="box-body chart-responsive">
                <div class="chart" id="sales-chart" style="min-height: 300px;"></div>
                <div id="sales_chart_error"></div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="row venue-value-box"> <!--dynamic data coming here-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


<div id="linechartAnchor" class="col-lg-6 col-md-6 connectedSortable">
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title tevolution">Evolution of positive opinion</h3>
      <div class="box-tools pull-right">
      	<button type="button" onclick="StartFunction('curve_chart');" class="btn btn-box-tool"><i class="fas fa-copy"></i>
		</button>
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">

     <div class="chart">

      <canvas id="curve_chart" height="380" width="350"></canvas>
      <div id="curve_chart_error"></div>

    </div>

  </div>
</div>
</div>

<div class="col-lg-6 col-md-6 connectedSortable" id="barAnchor">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title tnegvspos">Negatives vs Positives</h3>
      <div class="box-tools pull-right">
    <button type="button" onclick="StartFunction('bar_chart');" class="btn btn-box-tool"><i class="fas fa-copy"></i>
    </button>
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
     <div id="bar_chart_error" ></div>
     <canvas id="bar_chart" height="380" width="350"></canvas>
   </div>
   <!-- /.box-body -->
 </div>
</div>

<div id="starAnchor" class="col-lg-6 col-md-6 connectedSortable">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title tstarchart">Star Chart</h3>
      <div class="box-tools pull-right">
     <button type="button" onclick="StartFunction('chart_divl');" class="btn btn-box-tool"><i class="fas fa-copy"></i>
    </button>
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <div id="chart_divl_error"></div>
      <canvas id="chart_divl" height="380" width="350"></canvas>
    </div>
  </div>
</div>

<div id="tableAnchor" class="col-lg-12 col-md-12 connectedSortable">
 <div class="box">
  <div class="box-header with-border">
    <h3 class="box-title reviewsHeading"></h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
  </div>
  <div class="box-body " id="test-list">
	
	<!-- Appending reviews start  -->
    <input type="text" class="search form-control pull-right" id="searching" />
    <!-- <button class="sort" data-sort="realDate">Sort by date</button> -->
	<ul class="list">

	</ul>
  <div class="clearfix"></div>
	<ul class="pagination"></ul>
	<!-- Appending reviews end  -->


  </div>
  <!-- /.box-body -->
</div>
</div>




<!--       </ul> -->
</div><!-- row-end -->

</div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<footer class="main-footer">
  <div class="pull-right d-none d-sm-inline-block">
  </div>Copyright &copy; 2018 <a href="https://www.datastitute.fr/">Spella Corp</a>. <span class="tallrights" ></span>
</footer>

<!-- ./wrapper -->

{literal}
<!-- jQuery 3 -->
<script src="assets/vendor_components/jquery/dist/jquery.min.js"></script>
<!-- popper -->
<script src="assets/vendor_components/popper/dist/popper.min.js"></script>

<!-- Bootstrap v4.0.0-beta -->
<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- SlimScroll -->
<script src="assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->
<script src="assets/vendor_components/fastclick/lib/fastclick.js"></script>

<!-- maximum_admin App -->
<script src="js/template.js"></script>
<!-- maximum_admin for demo purposes -->
<script src="js/demo.js"></script>
<script src="js/all.js"></script>

<script src="js/list.js"></script>

<!-- jQuery Knob -->
<script src="assets/vendor_components/jquery-knob/js/jquery.knob.js"></script>
<!-- maximum_admin for inline Chart purposes -->
  
<!-- ChartJS -->
<script src="assets/vendor_components/chart-js/chart.js"></script>

<!-- <script src="js/widget-inline-charts.js"></script> -->
<script type="text/javascript" src="js/loader.js" ></script>
<!-- <script type="text/javascript" src="https://www.google.com/jsapi"></script> -->
<script src="js/dist/Chart.min.js"></script>
<script src="js/dist/utils.js"></script>
<!-- Morris.js charts -->
<script src="assets/vendor_components/raphael/raphael.min.js"></script>
<script src="assets/vendor_components/morris.js/morris.min.js"></script>

<!--Google Charts-->
<script type="text/javascript" src="https://www.google.com/jsapi" defer="defer"></script>
<!-- DataTables -->
<script src="assets/vendor_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- This is data table -->
<script src="assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
<!-- script by dijo -->
<script src="js/templates/venuedetail.js"></script>
<script src="js/templates/CommonFunctionsForText&Detail.js"></script>
<script src="js/templates/venuedetail_graph_redrawer.js"></script>
<script src="js/templates/duplication.js"></script>
<script src="js/templates/duplicationchartHandler.js"></script>
<script src="js/templates/chartHandler.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="js/multilang.js"></script>

  <!-- date-range-picker -->
  <script src="assets/vendor_components/moment/min/moment.min.js"></script>
  <script src="assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  
  <!-- bootstrap datepicker -->
  <script src="assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  



{/literal}
</body>
</html>
