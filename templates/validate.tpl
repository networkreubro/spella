
<body class="hold-transition register-page">
  <div class="register-box">
    <div class="register-logo">
      <!-- <a href="index.php"><b>Maximum</b>Admin</a> -->
      <a href="index.php"><img src="images/logo-spella.png" alt="" class="img-fluid" / ></a>
    </div>
    <div class="register-box-body">
      <div class="validation-box">
      <div class="col-sm-12 text-center">
       <h1>Validation</h1>
     </div>
     <hr>
     <div class="col-sm-12 margin-top-40">
      <p class="text-center">
        Dear user, please enter the validation code sent to your phone for verifying your account
      </p>
    </div>
    <div class="form-group">
     <input type="password" id="valCode" value="8888" class="form-control">
      <div class="validation-info" id="valUp"></div>
   </div>
   <div class="margin-top-40 text-center">
     <button type="button" onclick="validateFirst();" class="btn btn-info btn-block btn-flat margin-top-10">VALIDATE</button>
   </div>
 </div>
</div>
</div>
<!-- /.register-box -->


</body>
{literal}

<!-- jQuery 3 -->
<script src="assets/vendor_components/jquery/dist/jquery.min.js"></script>

<!-- popper -->
<script src="assets/vendor_components/popper/dist/popper.min.js"></script>

<!-- Bootstrap v4.0.0-beta -->
<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!--Custom Js for validation-->
<script src="js/templates/user_validate.js"></script>
<script type="text/javascript">
	$(function(){

		validateFirst();
	})
</script>>

{/literal}



</html>




