</div>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
  
    <h3 id="username"></h3>
    <ol class="breadcrumb">
      <!-- <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
      <!-- <li class="breadcrumb-item"><a href="#">Examples</a></li> -->
      <!-- <li class="breadcrumb-item active">Dashboard</li> -->
    </ol>
  </section>

  <!-- Main content -->
  <section class="content subscription">
{if $product_status eq "trial"}
      <div class="box p-3">
        <h4><span class="spanone"> Your trial will end in </span> {$plan_expiry_days} <span class="spantwo"> days. Please select a plan to continue using Spella. </span></h4>
        {* <small class="text-info btn btn-sm"> <a class="text-info" href="https://spella.com/notre-offre/">Upgrade your plan</a> </small> *}
     </div>
  {/if}
    <!-- Default box -->
    <div class="box">
      <input type ="hidden" id="user_subscribed_plan" value="{$productid}">
      
      <div class="box-header with-border">
        <h3 class="box-title tuser">User Info</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
          title="Collapse">
          <i class="fa fa-minus"></i></button>
      <!--       <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button> -->
      </div>
    </div>

    <div class="box-body">
      <div class="row">
       <div class="col-lg-9 col-md-9 col-sm-12 m-t-25">
        <div class="user-info">
          <ul class="list-unstyled">
            <li><span class="right_list tname">Name </span><span>{$name}</span></li>
            <li><span class="right_list tsubplan">Subscribed Plan </span><span id="plan_name"></span></li>
            <li><span class="right_list tsubon">Subscribed on  </span><span>{$subscribed_date}</span></li>
            <li><span class="right_list texpon">Expires on  </span><span id="plan_expiry">{$plan_expiry}</span></li>
          </ul>
        </div>


      </div>
      {if $product_status eq "subscribed"}
      <div class="col-lg-3 col-md-5 col-sm-12 m-t-25">
      <input type ="hidden" id="user_autorenew" value="{$autorenew}">
      <input type ="hidden" id="userid" value="{$userid}">
      <input type="hidden" id="userEmail" value="{$user_email}">
      {if $autorenew eq "true"}
       <div class="right-btn">
          <div class="checkbox" id="div_autorenew">
            <input type="checkbox" class="autorenew" name="autorenew" id="autorenew_checkbox" checked>
            <label for="autorenew_checkbox" style="color: #111;"><p class="tautorenew"></p></label>
          </div>
       </div>

       {/if}
       {if $autorenew eq "false"}
       <div class="right-btn">
          <div class="checkbox" id="div_autorenew">
            <input type="checkbox" class="autorenew" name="autorenew" id="autorenew_checkbox">
            <label for="autorenew_checkbox" style="color: #111;"><p class="tautorenew"></p></label>
          </div>
       </div>
       {/if}
       
     </div>
     {/if}
   </div>
 </div>

</div>
<!-- /.box-body-->



<!-- --------------2-products------------------ -->

<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title tproducts"></h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
      title="Collapse">
      <i class="fa fa-minus"></i></button>
      <!--       <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button> -->
      </div>
    </div>

    <div class="box-body">

      <div class="row" id="productList">
    <!-- --------product list div--------------->
  
 </div>
</div>
</div>
<!-- history -->

<div class="box">
      <div class="box-header with-border">
        <h3 class="box-title thistory">History of Previous Purchases</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
          title="Collapse">
          <i class="fa fa-minus"></i></button>
           <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button> 
      </div>
    </div>

    <div class="box-body">
      <div class="row">
       <div class="col-lg-9 col-md-9 col-sm-12 m-t-25">
      </div>
      <div class="col-lg-3 col-md-5 col-sm-12 m-t-25">
       {* <div class="right-btn">
         <a class="btn btn-app bg-purple tdownall"></a>
       </div> *}
     </div> 
   </div>
   <div class="row">
           <div class="col-lg-12 col-md-12 col-sm-12 m-t-25">
          <ul style="list-style: none;">
            {if $invoices != ""}
            { foreach from=$invoices item=invoice key=index }
              <li>
                <span><i class="fas fa-file-pdf" style="color:red;">
               </i> Invoice {$index}</span>
               <button class="btn btn-sm btn-link pull-right" onclick=viewDetailedInvoice("{$invoice}")>
                 <span  class="tdown">View</span>
               </button>
             </li>
              <hr>
           {/foreach}
           {else}
           <li class="tno_history">Nothing to show here</li>
           {/if}
     </ul>
   </div>
   </div>
 </div>

</div>

</div>
<!-- /.box -->

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!------------Modal start ---------------->

<div class="modal fade" id="invoiceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Invoice Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="invoice_body">

      </div>
    </div>
  </div>
</div>

<!------------Modal end ---------------->

<footer class="main-footer">
  <div class="pull-right d-none d-sm-inline-block">
  </div>Copyright &copy; 2018 <a href="https://www.datastitute.fr/">Spella Corp</a>
  <span class="tallrights" ></span>
</footer>

<!-- ./wrapper -->

{literal}
<!-- jQuery 3 -->
<script src="assets/vendor_components/jquery/dist/jquery.min.js"></script>

<!-- popper -->
<script src="assets/vendor_components/popper/dist/popper.min.js"></script>

<!-- Bootstrap v4.0.0-beta -->
<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- SlimScroll -->
<script src="assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->
<script src="assets/vendor_components/fastclick/lib/fastclick.js"></script>

<!-- maximum_admin App -->
<script src="js/template.js"></script>

<!-- maximum_admin for demo purposes -->
<script src="js/demo.js"></script>
<script src="js/all.js"></script>

<script src="js/common.js"></script>
<script src="js/templates/profile.js"></script>
<script src="js/templates/subscription.js"></script>
<script src="js/multilang.js"></script>
<script src="https://js.stripe.com/v3/"></script>


{/literal}
</body>
</html>



