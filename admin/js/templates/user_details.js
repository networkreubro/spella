function openseeUsDet() {
  $("#logSearchBox").hide();
  $("#userMainDetailBox").fadeIn();
  $("#seeUsDet").hide();
  $("#userlogbu").fadeIn();
}

var userListDataChecker;

$(function() {
  userListDataChecker = localStorage.getItem("userList");
  var user = $.parseJSON(localStorage.getItem("userList"));

  $.urlParam = function(name) {
    var results = new RegExp("[?&]" + name + "=([^&#]*)").exec(
      window.location.href
    );
    if (results == null) {
      return null;
    } else {
      return decodeURI(results[1]) || 0;
    }
  };

  var userid = $.urlParam("id");
  var email = $.urlParam("email");

  localStorage.removeItem("pageUserId");
  localStorage.setItem("pageUserId", userid);

  localStorage.removeItem("pageUserEmail");
  localStorage.setItem("pageUserEmail", email);

  if (userid !== "null") {
    $.each(user.result, function(index, element) {
      if (userid === element.userId) {
        $("#name").html(element.name);
        $("#email").html(element.email);
        $("#phone").html(element.cellphone ? element.cellphone : "-");
        $("#country").html(element.countryCode);
        $("#language").html(element.preferredLanguage);
        $("#role").html(element.role);
        $("#userid").html(element.userId);

        $("#userName").val(element.name);
        $("#userEmail").val(element.email);
        $("#userCell").val(element.cellphone);
        $("#usercntCode").val(element.countryCode);
        $("#userPreflang").val(element.preferredLanguage);
        $("#userRole").val(element.role);
        $("#userId").val(element.userId);

        if (element.pushActive === true) {
          $("#push").html("true");
          $("#userpushActive").val("true");
        } else {
          $("#push").html("false");
          $("#userpushActive").val("false");
        }
        $("#status").html(element.status);
      } else if (email === element.email) {
        $("#name").html(element.name);
        $("#email").html(element.email);
        $("#phone").html(element.cellphone ? element.cellphone : "-");
        $("#country").html(element.countryCode);
        $("#language").html(element.preferredLanguage);
        $("#role").html(element.role);
        $("#userid").html("NULL");
        if (element.pushActive === true) {
          $("#push").html("true");
        } else {
          $("#push").html("false");
        }
        $("#status").html(element.status);
      }
    });

    var adminData = getuserData();
    adminData = $.parseJSON(adminData);
    var adminId = adminData.admin_user_id;

    VenueListMaker(adminId, userid);
  } else {
    $.each(user.result, function(index, element) {
      if (email === element.email) {
        $("#name").html(element.name);
        $("#email").html(element.email);
        $("#phone").html(element.cellphone ? element.cellphone : "-");
        $("#country").html(element.countryCode);
        $("#language").html(element.preferredLanguage);
        $("#role").html(element.role);
        $("#userid").html("NULL");
        if (element.pushActive === true) {
          $("#push").html("true");
        } else {
          $("#push").html("false");
        }
        $("#status").html(element.status);
      }
    });
  }
});

function getuserData() {
  var userData;

  $.ajax({
    url: "home.php",
    type: "POST",
    data: { request: "getdata" },
    success: function(data) {
      userData = data;
    },
    error: function() {
      alert(
        "Erreur Lors du traitement de la commande, veuillez actualiser la page"
      );
    },
    async: false
  });

  return userData;
}

function VenueListMaker(adminId, userid) {
  var appending;
  if (adminId.length > 0 && userid.length > 0) {
    $.ajax({
      url: "api/scrapper_api.php",
      type: "POST",
      data: { action: "userVenuesStatus", adminId: adminId, userId: userid },
      success: function(data) {
        if (data) {
          var venueList = $.parseJSON(data);
          if (venueList.status === "success") {
            $.each(venueList.result.venues, function(index, element) {
              appending =
                `
						   <div class="services-venue">
					       <p class="lead">Venue name  : ` +
                element.venueName +
                ` (` +
                element.venueType +
                `)</p>
					       <h4>Services</h4>
					       <div class="col-md-10 col-lg-10">
					        <table id="venueMainBody` +
                index +
                `" class="table table-bordered table-hover table-responsive services">
					        <thead>
					                  <tr>
					                  <th>Service Name</th>
					                  <th>serviceVenueURL</th>
					                  <th>serviceVenueStatus</th>
					                  <th>serviceVenueUUID</th>
					                  <th>serviceVenueScrapDate</th>
					                  <th>Options</th>
					                  </tr>
					        </thead>
					                <tbody id="venueBody` +
                index +
                `">`;
              $.each(element.services, function(index2, element2) {
                appending +=
                  `<tr>
					          		<td>` +
                  element2.serviceName +
                  `</td>
					          		<td>` +
                  element2.serviceVenueURL +
                  `</td>
					          		<td>` +
                  element2.serviceVenueStatus +
                  `</td>
					          		<td>` +
                  element2.serviceVenueUUID +
                  `</td>
					          		<td>` +
                  element2.serviceVenueScrapDate +
                  `</td>
					          		<td><button onclick="logOpener('` +
                  element2.serviceVenueURL +
                  `');" type="button" 
					          		class="btn btn-default btn-flat margin">Log Fetch</button></td>
					          		</tr>`;
              });
              appending += ` </tbody>
					            </table>
					     </div>
							<hr class="hr-line mt-25">
					           			</div>`;
              $("#venueListComp").append(appending);
            });
          }
        }
      },
      error: function(data) {
        console.log(data);
      }
    });
  }
}

function Invalidate() {
  var admin = getuserData();
  admin = $.parseJSON(admin);
  var adminId = admin.admin_user_id;
  var user_email = localStorage.getItem("pageUserEmail");
  var findingId = localStorage.getItem("pageUserId");

  var r = confirm("Are you sure,You want to delete");
  if (r == true) {
    if (findingId === "null") {
      alert("already invalidated");
    } else {
      $.ajax({
        url: "api/admin_home_curl.php",
        type: "POST",
        data: { action: "invalidateUser", adminId: adminId, email: user_email },
        success: function(data) {
          console.log(data);
          if (data) {
            var dataobj = $.parseJSON(data);
            if (dataobj.status === "success") {
              $("#userid").html("NULL");
              updateuserlist(adminId);
              alert("Success");
            }
          }
        },
        error: function(data) {
          console.log(data);
        }
      });
    }
  } else {
    alert("canceled");
  }
}

function LogFinder() {
  $("#userMainDetailBox").fadeOut();
  $("#seeUsDet").fadeIn();
  $("#logSearchBox").fadeIn();
  $("#userlogbu").hide();
}

function logfetcher() {
  $("#loglist").html("");

  var adminData = getuserData();
  adminData = $.parseJSON(adminData);
  var adminId = adminData.admin_user_id;

  $.urlParam = function(name) {
    var results = new RegExp("[?&]" + name + "=([^&#]*)").exec(
      window.location.href
    );
    if (results == null) {
      return null;
    } else {
      return decodeURI(results[1]) || 0;
    }
  };

  var userid = $.urlParam("id");
  var email = $.urlParam("email");

  var startDate = $("#start").val();
  var endDate = $("#end").val();
  if (startDate != "" && endDate != "") {
    data = {
      action: "loggedData",
      adminId: adminId,
      logType: "users",
      startDate: startDate,
      endDate: endDate,
      userId: userid
    };
  } else {
    data = {
      action: "loggedData",
      adminId: adminId,
      logType: "users",
      userId: userid
    };
  }
  $("#scrapperLoader").fadeIn();
  console.log(data);

  $.ajax({
    url: "api/scrapper_api.php",
    type: "POST",
    data: data,
    success: function(data) {
      if (data) {
        var log = $.parseJSON(data);
        if (log.status === "success") {
          if (log.result.events.length > 0) {
            $.each(log.result.events, function(index, element) {
              $("#loglist").append(
                `
							 <dt>Event Type:</dt>
			                 <dd>` +
                  element.eventType +
                  `</dd>
			                 <dt>UserId<dt>
			                 <dd>` +
                  element.userId +
                  `</dd>
			                 <dt>Event Date</dt>
			                 <dd>` +
                  element.eventDate +
                  `</dd>
							<hr class="hr-line mt-25">

			                 `
              );
            });
          } else {
            $("#loglist").append("No events");
          }
        }
      }
    },
    error: function(data) {
      console.log(data);
    },
    async: false
  });
  $("#scrapperLoader").fadeOut();
}
var useurl;
function logOpener(url) {
  $("#loglist2").html("");

  useurl = "";
  $("#myModal").modal("show");
  useurl = url;
}

function logfetcher2() {
  var adminData = getuserData();
  adminData = $.parseJSON(adminData);
  var adminId = adminData.admin_user_id;

  $.urlParam = function(name) {
    var results = new RegExp("[?&]" + name + "=([^&#]*)").exec(
      window.location.href
    );
    if (results == null) {
      return null;
    } else {
      return decodeURI(results[1]) || 0;
    }
  };

  var userid = $.urlParam("id");
  var email = $.urlParam("email");

  var startDate = $("#start1").val();
  var endDate = $("#end1").val();
  console.log(startDate);
  console.log(endDate);

  if (startDate != "" && endDate != "") {
    data = {
      action: "loggedData",
      adminId: adminId,
      logType: "scraps",
      startDate: startDate,
      endDate: endDate,
      serviceVenueURL: useurl
    };
  } else {
    data = {
      action: "loggedData",
      adminId: adminId,
      logType: "scraps",
      serviceVenueURL: useurl
    };
  }
  $("#scrapperLoader").fadeIn();
  console.log(data);

  $.ajax({
    url: "api/scrapper_api.php",
    type: "POST",
    data: data,
    success: function(data) {
      if (data) {
        console.log(data);
        // return false;
        var log = $.parseJSON(data);
        if (log.status === "success") {
          $.each(log.result.events, function(index, element) {
            $("#loglist2").append(
              `
							 <dt>serviceVenueURL:</dt>
			                 <dd>` +
                element.serviceVenueURL +
                `</dd>
			                 <dt>eventDate<dt>
			                 <dd>` +
                element.eventDate +
                `</dd>
			                 <dt>scrapStatus</dt>
			                 <dd>` +
                element.scrapStatus +
                `</dd>
							<hr class="hr-line mt-25">

			                 `
            );
          });
        }
      }
    },
    error: function(data) {
      console.log(data);
    },
    async: false
  });
  $("#scrapperLoader").fadeOut();
}

function updateuserlist(adminId) {
  $.ajax({
    url: "api/admin_home_curl.php",
    type: "POST",
    data: { action: "listUsers", adminId: adminId },
    success: function(UserListdata) {
      if (UserListdata) {
        var obj = $.parseJSON(UserListdata);
        if (obj.status === "success") {
          localStorage.removeItem("userList");
          localStorage.setItem("userList", UserListdata);
        }
      }
    },
    error: function(data) {
      console.log(data);
    },
    async: false
  });
}

window.setInterval(function() {
  checkLocalStorageChange();
}, 3000);

function checkLocalStorageChange() {
  var listingdata = userListDataChecker;
  var localStorageData = localStorage.getItem("userList");

  if (listingdata == localStorageData) {
    console.log("equal");
  } else {
    userListDataChecker = localStorage.getItem("userList");
    location.reload();
  }
}

function editUser() {
  $.urlParam = function(name) {
    var results = new RegExp("[?&]" + name + "=([^&#]*)").exec(
      window.location.href
    );
    if (results == null) {
      return null;
    } else {
      return decodeURI(results[1]) || 0;
    }
  };

  var userid = $.urlParam("id");
  var email = $.urlParam("email");

  window.location.href = "edit_user.php?id=" + userid + "&email=" + email;
}
