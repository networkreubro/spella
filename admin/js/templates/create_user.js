

$(function() {
		
    $.ajax({
      url: 'Json/countries.json',
      dataType: 'json',    
      success: function(data){
        $.each(data, function(key, val){
          $("#country").append('<option value='+val.code+' data-dial_code='+val.dial_code+'>' + val.name +'</option>');

        });
      }
    });  
	
	var langu = localStorage.getItem("lang").toLowerCase();
	/*$.ajax({
		//url: 'Json/venuetypes.json',
		url: 'https://api.spella.com/services/venuetypes.json',
		dataType: 'json',    
		success: function(data){
			console.log(data);			
			if (data) {
				data.types.sort(function(a, b) {
					return a.tag.localeCompare(b.tag);
				});
				$.each(data.types, function(index, element) {
					$("#limType").append( `<option value=` + element.tag + ` selected >` + element.locales[langu] + `</option>` );
				});
				$("#limType").multiSelect({});
			} else {
				$("#limType").append(`<option value=` + null + `>` + "No Types" + `</option>`);
			}
		}
    });	*/
});
  
  function prefix_inser()
  {
    var value= $('#country').find(':selected').data('dial_code');
    $('#phnumber').val(value);
  }


function getuserData()
{
  var userData;

  $.ajax({

    url:"home.php",
    type:"POST",
    data:{"request":"getdata"},
    success:function(data){
      userData= data;
    },error:function(){

      alert("Erreur Lors du traitement de la commande, veuillez actualiser la page");

    },
    async:false

  });

  return userData;
}



  var name_val=false;
  var email_val=false;
  var pass_val=false;
  var country_val=false;
  var pass_val=false;
  var cpass_val=false;
  var prefix_val=false;
  var phnumber_val=false;
  var venue_val=false;
  var comp_val=false;
  
  $(function(){

    //Name validation
    $("#name").focusout(function(){

      check_name();

    });

    //Email validation
    $("#email").focusout(function(){

     check_email();

   });
    // Password Validation      
    $("#password").focusout(function(){

      check_pas();        
      confirm_pass(); 
    });

   // confirm password validation        
   $("#cpassword").focusout(function(){

    confirm_pass();

  });
    //Confirm country selection
    $("#country").focusout(function(){

      confirm_country();

    });
    $("#prefix").focusout(function(){

      confirm_prefix();

    });

    //Confirm phone Number
    $("#phnumber").focusout(function(){

      confirm_phNumber();

    });
  });



  function check_name()
  {
    var user= $("#name").val().length;
    if (user<=0)
    {
      $("#name_up").html("Please provide Name").css('color', 'red');
      $("#name_up").show();
      name_val=true;
    } 
    else 
    {
      name_val=false;
      $("#name_up").hide();
    }             
  }

  function check_email()
  {

    var email=$("#email").val();
    var len=$("#email").val().length;
    email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
    if (len<=0)
    {
      $("#email_up").html("Please provide  Email").css('color', 'red');
      $("#email_up").show();
      email_val=true;
    }  
    else{ 
     if(!email_regex.test(email))
     {
      $("#email_up").html("Please provide correct Email").css('color', 'red').css('font-size','12px');
      $("#email_up").show();
      email_val=true;
    } 
    else{
      $("#email_up").hide();
      email_val=false;

    }  
  }        
}



function check_pas()
{
  var pas= $("#password").val().length;
  if (pas<=0)
  {
    $("#pass_up").html( "Please provide a password").css('color', 'red').css('font-size','12px');
        //$("#pass_up").attr("placeholder", "Please provide a password").css('color', 'green');
        $("#pass_up").show().css('color', 'red');
        pass_val=true;
      }
      else if(pas <6)
      {
        $("#pass_up").html( "Password must atleast be 6 characters").css('color', 'red').css('font-size','12px');
      }  
      else{
        $("#pass_up").hide();
        pass_val=false;    
      }            
    }


    function confirm_pass()
    {
      var pas= $("#password").val();
      var cpas= $("#cpassword").val();
      var cpas_len=$("#cpassword").val().length;
      if(cpas_len<=0)
      {
        $("#cpass_up").html("Please re-enter your password").css('color', 'red').css('font-size','12px');
        $("#cpassword").show();
        cpass_val=true;     
      }
      if(pas!=cpas)
      {
       $("#cpass_up").html("Passwords don't match").css('color', 'red').css('font-size','12px');
       $("#cpass_up").show();
       cpass_val=true;        
     }
     else{
      $("#cpass_up").hide();
      cpass_val=false;        

    }



  }


  function confirm_country()
  {
    country=$("#country").val();
    if(country=="")
    {
      $("#country_up").html("select your country").css('color', 'red');
      $("#country_up").show();
      country_val=true;
    }
    else{
      $("#country_up").hide();
      country_val=false;

    }
  }


  function confirm_phNumber()
  {
    var intRegex = /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/;
    prefix= $("#phnumber").val();
    if(!intRegex.test(prefix))
    {
      $("#ph_up").html("Enter correct number").css('color', 'red');
      $("#ph_up").show();
      phnumber_val=true;  
    }
    else{
      $("#ph_up").hide();
      phnumber_val=false;  

    }
  }
  
  function check_venue() {
	let venue_count = $("#numberOfVenues").val();	
	if ((venue_count != '') && (venue_count < 1)) {	
		$("#error_venue").html("Minimum value is 1").css('color', 'red');
		$("#error_venue").show();
		venue_val=true;
	}
	else {
		$("#error_venue").hide();
		venue_val=false;
	}
 }
 
 function check_comp() {
	let comp_count = $("#numberOfCompetitors").val();
	if (comp_count < 0) {	
		$("#error_comp").html("Minimum value is 0").css('color', 'red');
		$("#error_comp").show();
		comp_val=true;
	}
	else {
		$("#error_comp").hide();
		comp_val=false;
	}
 } 

  function CreateUser() {

    check_name();
    check_email();
    check_pas();        
    confirm_pass();
    confirm_country();
    // confirm_prefix();
    confirm_phNumber();
	check_venue();
	check_comp();
	
	if (name_val == true) {
		$("#name").focus();
	} else if (email_val == true) {
		$("#email").focus();
	} else if (pass_val == true) {
		$("#password").focus();
	} else if (country_val == true) {
		$("#country").focus();
	} else if (phnumber_val == true) {
		$("#phnumber").focus();
	} else if (cpass_val == true) {
		$("#cpassword").focus();
	} else if (venue_val == true) {
		$("#numberOfVenues").focus();
	} else if (comp_val == true) {
		$("#numberOfCompetitors").focus();
	}	
		
	var userData= getuserData();
	var userObj = $.parseJSON(userData);
	var adminId = userObj.admin_user_id;	

    var name		= $("#name").val();
    var email		= $("#email").val();
    var country		= $("#country").val();    
    var dial_code	= $('#country').find(':selected').data('dial_code');
    var phnumber	= $("#phnumber").val();
    var pas			= $("#password").val();    
    var pushActive	= $("#push").val();
    var preferredLanguage	= $("#language").val();
	var role = $("#role").val();	
    var status	= $("#status").val();	
    var dynGroupAccess	= $("#dynGroupAccess").val();	
	var numberOfVenues	= $("#numberOfVenues").val();
	var numberOfCompetitors = $("#numberOfCompetitors").val();
	var expiryDate = $("#expiryDate").val();
	
	/*if ($("#expiry_never").is(':checked')) {      
	   $("#expiryDate").val('Never');
    }
    else {      
	   $("#expiryDate").val($("#expirationDate").val());
    }*/
	
	
	// var prefix	= $("#prefix").val();
    // phnumber		= prefix+''+phnumber;
	//var namelim		= $("#limName").val();
	//var Addresslim	= $("#limAddress").val();
	//var Typelim		= $("#limType").val();
	// var Serlim	= $("#limSer").val();
	//var Arealim		= $("#limArea").val();
	

 if(name_val==false&email_val==false&&pass_val==false&&country_val==false&&prefix_val==false&&phnumber_val==false&&pass_val==false&&cpass_val==false&&venue_val==false&&comp_val==false)  
    {

	var data = {
		"action" : "createUser",
		"adminId" : adminId,
		"email" : email,
		"password" : pas,
		"name" : name,
		"cellphone" : phnumber,
		"countryCode" : country,
		"status" : status,
		"role" : role,
		"pushActive" : pushActive,
		"preferredLanguage" : preferredLanguage,
		"dynGroupAccess" : dynGroupAccess,
		"numberOfVenues" : numberOfVenues,
		"numberOfCompetitors" : numberOfCompetitors,
		"expiryDate" : expiryDate
		
		//"limitationName" : namelim,
		//"limitationAddress" : Addresslim,
		//"limitationType" : Typelim,
		//"limitationService" : Serlim,
		//"limitationArea" : Arealim,
		
	};


    for (var propName in data) { 
    if (data[propName] === null || data[propName] === undefined || data[propName] == "") 
    {
      delete data[propName];
    }
  }

console.log(data);


      $.ajax({

        url: "api/sign_up_curl.php",
        type: "POST",
        data : data,

       success:function(data)
       {
        var obj= $.parseJSON(data);
        console.log(obj);
        // return false;
        if(obj['status']==='success')
        {
          window.location.href="user_validate.php?email="+email; 
        } else {
          alert(obj.result.message);
        }
        $(".loader").addClass("hidden");

      }, error: function() 
      {
        $(".loader").addClass("hidden");
      }
    });

    }

  }
  
  // If the expiry date need to be set as never
 /* $("#expiry_never").on("click", function() {
	if ($(this).is(':checked')) {
       $("#expireNever").show().val('Never');
       $("#expirationDate").hide();	  
    }
    else {
       $("#expireNever").hide();	  
       $("#expirationDate").show().val('');	   
    }
  });*/