function getuserData()
{
  var userData;

  $.ajax({

    url:"home.php",
    type:"POST",
    data:{"request":"getdata"},
    success:function(data){
      userData= data;
    },error:function(){

      alert("Erreur Lors du traitement de la commande, veuillez actualiser la page");

    },
    async:false

  });

  return userData;
}

function addProduct(){
	$(".errormsg").hide();
var productnameen = $('#productnameen').val();
var productnamefr = $('#productnamefr').val();
var duration = $('#duration').val();
var descriptionen = $('#descriptionen').val();
var descriptionfr = $('#descriptionfr').val();
var pricedollar = $('#pricedollar').val();
var priceeuro  = $('#priceeuro').val();

  if (productnameen!="" && productnamefr!="" && duration!="" && descriptionen!="" && descriptionfr!="" && pricedollar!="" && priceeuro!="") {

  	var userData= getuserData();
    var userObj = $.parseJSON(userData);

$.ajax({

    url:"api/product_curl.php",
    type:"POST",
    data:{"action":"createProduct","adminId":userObj.admin_user_id,"name-en":productnameen,"name-fr":productnamefr,"description-en":descriptionen,"description-fr":descriptionfr,"price-EUR":priceeuro,"price-USD":pricedollar,"active":"true"},
    success:function(data){
    	var res = $.parseJSON(data);
    	if (res.status=="success") { alert('Product added'); location.href="productlist.php"; }
    },
    error:function(){

    },
    async:false

  });
    	
  }else{
  	if (productnameen=="") { $('#valnameen').html("<span style='color:red' class='errormsg'> Please enter the product name in english </span>")  }
  	if (productnamefr=="") { $('#valnamefr').html("<span style='color:red' class='errormsg'> Please enter the product name in french </span>")  }
  	if (duration=="") { $('#valduration').html("<span style='color:red' class='errormsg'> Please enter duration in months </span>")  }
  	if (descriptionen=="") { $('#valdescen').html("<span style='color:red' class='errormsg'> Please enter description in english </span>")  }
  	if (descriptionfr=="") { $('#valdescfr').html("<span style='color:red' class='errormsg'> Please enter description in french </span>")  }
  	if (pricedollar=="") { $('#valpricedollar').html("<span style='color:red' class='errormsg'> Please enter the price in dollar </span>")  }
  	if (priceeuro=="") { $('#valproceeuro').html("<span style='color:red' class='errormsg'> Please enter the price in euro </span>")  }
  }
}