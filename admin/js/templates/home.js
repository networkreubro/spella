
  $(function(){

    var userData= getuserData();
   var userObj = $.parseJSON(userData);
     UserListing(userObj);

    $('#adminListTable').dataTable({
    responsive: true });
   $('#developerListTable').dataTable({
     responsive: true });

   $('#userListTable').dataTable({
       responsive: true });
    $('#customSearchTable').dataTable({
       responsive: true }); 
  });

function getuserData()
{
  var userData;

  $.ajax({

    url:"home.php",
    type:"POST",
    data:{"request":"getdata"},
    success:function(data){
      userData= data;
    },error:function(){

      alert("Erreur Lors du traitement de la commande, veuillez actualiser la page");

    },
    async:false

  });

  return userData;
}

var userListDataChecker;

function UserListing(userObj)
{
	// console.log(findingId)
	// return false;
  var user_id= userObj.admin_user_id;
  console.log(user_id)
  var userList;
  $.ajax({

    url: "api/admin_home_curl.php",
    type:"POST",
    data:{"action":"listUsers","adminId":user_id},
    success:function(UserListdata)
    {
      // console.log(UserListdata)
      if(UserListdata)
      {
          var obj=$.parseJSON(UserListdata);
          if(obj.status==='success')
          {
            userList=UserListdata;
            userListDataChecker=UserListdata;
            localStorage.removeItem("userList");
            localStorage.setItem("userList",UserListdata)
          }

      }

    },
    error:function(data)
    {
      console.log(data)
    },async:false

  });

    var usingListUser= $.parseJSON(userList);
    var adminArr=[];
    var userArr=[];
    var devArr=[];
    $.each(usingListUser.result,function(index,element){

      if(element.role==='admin' || element.role==='developer')
      {
        adminArr.push(element);
      }
      else if(element.role==='user')
      {
        userArr.push(element)
      }


    });

fillAdmin(adminArr);
fillUser(userArr);
}

function fillAdmin(adminArr)
{

  var usingUserArr=adminArr;
  var i=1;
  $.each(usingUserArr,function(index,element)
  {  	if(element.userId===null)
  		{
  		var	color="red"
  		}
  		else{
  		var	color="white";
  		}
    $("#adminListTableBody").append(`<tr style="color:`+color+`"><td>`+i+`</td>
        <td>`+element.name+`</td>
        <td style="cursor: pointer;">`+element.email+`</td>
        <td>`+element.role+`</td>
        <td id='`+element.userId+`' >
        <span onclick="invalidate('`+element.email+`','adminListTableBody','`+element.userId+`');">
        <button class="btn btn-default btn-flat margin">invalidate</button></span>
        <span onclick="userdetail('`+element.userId+`','`+element.email+`');">
        <button class="btn btn-default btn-flat margin">User Info</button></span>
        <span onclick="editUser('`+element.userId+`','`+element.email+`')"><button class="btn btn-default btn-flat margin">Edit User</button></span>
        </td>
            </tr>`);
    i++;
  });


}

// function fillDeveloper(devArr)
// {

// var usingUserArr=devArr;
//   var i=1;
//   $.each(usingUserArr,function(index,element)
//   {
//   	  		if(element.userId===null)
//   		{
//   		var	color="red"
//   		}
//   		else{
//   		var	color="white";
//   		}
//     $("#developerListTableBody").append(`<tr style="color:`+color+`"><td>`+i+`</td>
//         <td>`+element.name+`</td>
//         <td style="cursor: pointer;">`+element.email+`</td>
//         <td>`+element.role+`</td>
//         <td id='`+element.userId+`' >
//         <span onclick="invalidate('`+element.email+`','adminListTableBody','`+element.userId+`');">
//         <button class="btn btn-default btn-flat margin">invalidate</button></span>
//         <span onclick="userdetail('`+element.userId+`','`+element.email+`');">
//         <button class="btn btn-default btn-flat margin">User Info</button></span>
//         <span onclick="editUser('`+element.userId+`','`+element.email+`')"><button class="btn btn-default btn-flat margin">Edit User</button></span>
//         </td>
//             </tr>`);
//     i++;
//   });


// }

function fillUser(userArr)
{

var usingUserArr=userArr;
  var i=1;
  var arr=[];
  $.each(usingUserArr,function(index,element)
  {
  	  	if(element.userId===null)
  		{
  		var	color="red"
  		console.log('red')
  		}
  		else{
  		var	color="white";
  		}

    $("#userListTableBody").append(`<tr style="color:`+color+`"><td>`+i+`</td>
        <td>`+element.name+`</td>
        <td style="cursor: pointer;">`+element.email+`</td>
        <td>`+element.role+`</td>
        <td id='`+element.userId+`' >
        <span onclick="invalidate('`+element.email+`','userListTableBody','`+element.userId+`');">
        <button class="btn btn-default btn-flat margin">invalidate</button></span>
        <span onclick="userdetail('`+element.userId+`','`+element.email+`');">
        <button class="btn btn-default btn-flat margin">User Info</button></span>
        <span onclick="editUser('`+element.userId+`','`+element.email+`')"><button class="btn btn-default btn-flat margin">Edit User</button></span>
        </td>
            </tr>`);
    i++;
  });
}

function userSearch()
{
  var userData=getuserData();
      userData=$.parseJSON(userData);
  var adminId= userData.admin_user_id;

 

   var name=$("#userName").val();
   var email=$("#email").val();

   var data = {"action":"searchUser","adminId":adminId,"userName":name,"userEmail":email};
   var del;   
      $.each(data,function(index,element){

          if(element.length===0)
          {
            del=index;
          }

      })

      delete data[del];
      console.log(data)
if(email!==undefined||name!==undefined)
{
   $("#adminbox").fadeOut();
    $("#devbox").fadeOut();
    $("#userbox").fadeOut();
   $.ajax({

      url: "api/admin_home_curl.php",
      type: "POST",
      data: data,
      success: function(data)
      {

          if(data)
          {
            var obj= $.parseJSON(data);
            if(obj.status==='success')
            {
              customSearchListing(data);
            }
          }

      },error:function(data)
      {
        console.log(data);
      },async:false

    });
}
   else{
    alert("provide any search parameters");
   }

    // $("#customSearchbox").fadeIn();



}

function customSearchListing(data)
{
      $("#customSearchbox").fadeOut();
$("#Previous").fadeIn();
  $("#customSearchTableBody").html("");
  var usingList= $.parseJSON(data);
var i=1;
  $.each(usingList.result,function(index,element){
  		if(element.userId===null)
  		{
  		var	color="red"

  		}
  		else{
  		var	color="white";
  		}

          $("#customSearchTableBody").append(`<tr style="color:`+color+`"><td>`+i+`</td>
        <td>`+element.name+`</td>
        <td style="cursor: pointer;">`+element.email+`</td>
        <td>`+element.role+`</td>
        <td id='`+element.userId+`' >
        <span onclick="invalidate('`+element.email+`','customSearchTableBody','`+element.userId+`');">
        <button class="btn btn-default btn-flat margin">invalidate</button></span>
        <span onclick="userdetail('`+element.userId+`','`+element.email+`');">
        <button class="btn btn-default btn-flat margin">User Info</button></span>
        <span onclick="editUser('`+element.userId+`','`+element.email+`')"><button class="btn btn-default btn-flat margin">Edit User</button></span>
        </td>
            </tr>`);
i++;
  });

    $("#customSearchbox").fadeIn();

}

function PreviousMenu()
{
        $("#customSearchbox").fadeOut();

        $("#adminbox").fadeIn();
        $("#devbox").fadeIn();
        $("#userbox").fadeIn();
		$("#Previous").fadeOut();


  }

  function invalidate(email,tableid,findingId)
  {
  	var findingId=findingId;
  	console.log(findingId)
  	// return false;
		var table='#'+tableid;  	
		var admin=getuserData();
  		admin=$.parseJSON(admin);
  	var	adminId= admin.admin_user_id;
  	var user_email=email;
  	var r = confirm("Are you sure,You want to delete");
			if (r == true) {
			   if(findingId==='null')
			   {
			    	alert("already invalidated");

			    }
			    else{
			   			    $.ajax({
			    	url:"api/admin_home_curl.php",
			    	type:"POST",
			    	data:{"action":"invalidateUser","adminId":adminId,"email":user_email},
			    	success:function(data)
			    	{
			    		console.log(data)
			    		if(data)
			    		{
			    			var dataobj=$.parseJSON(data);
			    			if(dataobj.status==='success')
			    			{
			    				// alert("hi")
			    				$(table).find("td[id="+findingId+"]").parent().css("color","red");
			    				location.reload();
			    			}
			    		}	

			    	},
			    	error:function(data)
			    	{
			    		console.log(data)
			    	}
			    });
			    }
			} 
			else
			 {
				alert("canceled");	    
			}
  	
  }

  function userdetail(userId,email)
  {

   var url=`user_details.php?id=`+userId+`&email=`+email;
   window.open(url, '_blank');

  }

  function editUser(userId,email)
  {
   var url=`edit_user.php?id=`+userId+`&email=`+email;
   window.open(url, '_blank');
  }



  window.setInterval(function(){

  	checkLocalStorageChange();

}, 3000);


function checkLocalStorageChange(){

var listingdata = userListDataChecker;
var localStorageData= localStorage.getItem("userList");

if(listingdata == localStorageData)
{
	console.log("equal");
}
else{
	userListDataChecker = localStorage.getItem("userList");
	location.reload();
}




}
