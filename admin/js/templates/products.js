$(function() {
  var userData = getuserData();
  var userObj = $.parseJSON(userData);
  ProductsListing(userObj);

  $("#adminListTable").dataTable({
    responsive: true
  });
  $("#developerListTable").dataTable({
    responsive: true
  });

  $("#userListTable").dataTable({
    responsive: true
  });
  $("#customSearchTable").dataTable({
    responsive: true
  });
});

function getuserData() {
  var userData;

  $.ajax({
    url: "home.php",
    type: "POST",
    data: { request: "getdata" },
    success: function(data) {
      userData = data;
    },
    error: function() {
      alert(
        "Erreur Lors du traitement de la commande, veuillez actualiser la page"
      );
    },
    async: false
  });

  return userData;
}

var userListDataChecker;

function ProductsListing(userObj) {
  // console.log(findingId)
  // return false;
  var user_id = userObj.admin_user_id;
  console.log(user_id);
  var userList;
  $.ajax({
    url: "api/product_curl.php",
    type: "POST",
    data: { action: "listProducts", adminId: user_id },
    success: function(ProductListdata) {
      if (ProductListdata) {
        var obj = $.parseJSON(ProductListdata);
        if (obj.status === "success") {
          localStorage.setItem("productsdata", JSON.stringify(obj.result));
          $.each(obj.result, function(index, value) {
            console.log(value);
            var n = index + 1;
            if (value.active) {
              var activatebutton =
                ` <a class="btn btn-default btn-flat margin" style="color: white;" onclick="deactivate('` +
                value.id +
                `')">Deactivate</a>`;
            } else {
              var activatebutton =
                ` <a class="btn btn-default btn-flat margin" style="color: white;" onclick="activate('` +
                value.id +
                `')">Activate</a>`;
            }
            $("#productlistTableBody").append(
              `<tr>
              <td>` +
                n +
                `</td>
              <td>` +
                value["name-en"] +
                ` | ` +
                value["name-fr"] +
                `</td>
              <td> €` +
                value["price-EUR"] +
                ` | $` +
                value["price-USD"] +
                `</td>
              <td>
                <div class="actions">
                  <a class="btn btn-default btn-flat margin" style="color: white;" onclick="view('` +
                value.id +
                `')">View</a>
                  <a class="btn btn-default btn-flat margin" style="color: white;" onclick="editProduct('` +
                value.id +
                `')">Edit</a>
                  <a class="btn btn-default btn-flat margin" style="color: white;" onclick="deleteplan('` +
                value.id +
                `')">Delete</a>    
                  ` +
                activatebutton +
                `             
                </div>
              </td>
            </tr>`
            );
          });
        }
      }
    },
    error: function(data) {
      console.log(data);
    },
    async: false
  });
}

function activate(id) {
  var userData = getuserData();
  var userObj = $.parseJSON(userData);
  $.ajax({
    url: "api/product_curl.php",
    type: "POST",
    data: {
      action: "updateProduct",
      adminId: userObj.admin_user_id,
      id: id,
      active: true
    },
    success: function(res) {
      res = $.parseJSON(res);
      if (res.status == "success") {
        alert("Product Activated");
        location.reload();
      }
    }
  });
}

function deactivate(id) {
  var userData = getuserData();
  var userObj = $.parseJSON(userData);
  console.log(userObj);

  $.ajax({
    url: "api/product_curl.php",
    type: "POST",
    data: {
      action: "updateProduct",
      adminId: userObj.admin_user_id,
      id: id,
      active: null
    },
    success: function(res) {
      res = $.parseJSON(res);
      console.log(res);
      if (res.status == "success") {
        alert("Product Deactivated");
        location.reload();
      }
    }
  });
}

function editProduct(id) {
  window.location.href = "editProduct.php?id=" + id;
}

function view(id) {
  window.location.href = "viewProduct.php?id=" + id;
}

function deleteplan(id) {
  confirm("Do you really want to delete this product?");
  var userData = getuserData();
  var userObj = $.parseJSON(userData);
  $.ajax({
    url: "api/product_curl.php",
    type: "POST",
    data: { action: "deleteProduct", adminId: userObj.admin_user_id, id: id },
    success: function(res) {
      res = $.parseJSON(res);
      console.log(res.result);
      if (res.status == "success") {
        alert("Product Deleted");
        location.reload();
        return false;
      }
    }
  });
}
