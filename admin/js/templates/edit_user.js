window.onload = editServices();

var adminId = 0;

$(function() {
  var user = $.parseJSON(localStorage.getItem("userList"));
  console.log(user);

  $.urlParam = function(name) {
    var results = new RegExp("[?&]" + name + "=([^&#]*)").exec(
      window.location.href
    );
    if (results == null) {
      return null;
    } else {
      return decodeURI(results[1]) || 0;
    }
  };

  var userid = $.urlParam("id");
  var email = $.urlParam("email");

  localStorage.removeItem("pageUserId");
  localStorage.setItem("pageUserId", userid);

  localStorage.removeItem("pageUserEmail");
  localStorage.setItem("pageUserEmail", email);

  var userData = getuserData();
  var userObj = $.parseJSON(userData);
  adminId = userObj.admin_user_id;
  console.log("adminId", adminId);
  $.ajax({
    url: "api/admin_home_curl.php",
    type: "POST",
    data: { action: "poseAsUser", adminId: adminId, email: email },
    // data: { action: "login", password: "vishnu@123", email: email },
    success: function(data) { 
      console.log(data);
      data = JSON.parse(data);
      console.log("response data here", data);
      localStorage.removeItem("serviceslist");
      localStorage["serviceslist"] = JSON.stringify(data.result.services);
	  console.log(localStorage["serviceslist"]);
	  
	  // show default values as placeholder on venue and competitor limit fields
      let $productId = data.result.productId; 	 
      if ($productId != null) {		
		$.ajax({
			url: "api/edit_user_curl.php",
			type: "POST",
			data: { action: "listProducts" },
			success: function(data) {
        console.log(data);
				data = JSON.parse(data);  
				let arrProducts = data.result;		
				$.each(arrProducts, function(key, val) {
					if (val['id'] == $productId) {				
						$("#numberOfVenues").attr("placeholder", val['numberOfVenues']);
						$("#numberOfCompetitors").attr("placeholder", val['numberOfCompetitors']);
					}
				});    
			}
		});  
	  } else {
		$("#numberOfVenues").attr("placeholder", 1);
		$("#numberOfCompetitors").attr("placeholder", 0);
	  }
	  // 
	  
	  $("#expirationDate").val(data.result.expirationDate);
	  $("#numberOfVenues").val(data.result.numberOfVenues);
	  $("#numberOfCompetitors").val(data.result.numberOfCompetitors);
	  //$("#numberOfServices").val(data.result.services.length);
	  
      
    },
    error: function(argument) {
      console.log(argument);
    },
    async: false
  });
  
  // code to fill venue type dropdown with data from Json/venuetypes.json
	/*var langu = localStorage.getItem("lang").toLowerCase();
	$.ajax({
		url: 'Json/venuetypes.json',
		dataType: 'json',    
		success: function(data){
			console.log(data);			
			if (data) {
				data.types.sort(function(a, b) {
					return a.tag.localeCompare(b.tag);
				});
				$.each(data.types, function(index, element) {
					$("#limType").append( `<option value=` + element.tag + ` selected >` + element.locales[langu] + `</option>` );
				});
				$("#limType").multiSelect({});
			} else {
				$("#limType").append(`<option value=` + null + `>` + "No Types" + `</option>`);
			}
		}
    });*/

  if (userid !== "null") {	
    $.each(user.result, function(index, element) {
		
      if (userid === element.userId) {
      
        $("#name").val(element.name);
        $("#email").val(element.email);
        $("#phone").val(element.cellphone);
        $("#hide_country").val(element.countryCode);
        $("#language").val(element.preferredLanguage);
        $("#role").val(element.role);
		$("#status").val(element.status);		
        $("#userid").val(element.userId);		
        if (element.pushActive === true) {
          $("#push").val("true");
        } else {
          $("#push").val("false");
        }
        if (element.dynGroupAccess === true) {
          $("#dynaccess").val("true");
        } else {
          $("#dynaccess").val("false");
        }	
		
      } else if (email === element.email) {
        $("#name").val(element.name);
        $("#email").val(element.email);
        $("#phone").val(element.cellphone);
        $("#hide_country").val(element.countryCode);
        $("#language").val(element.preferredLanguage);
        $("#role").val(element.role);
		$("#status").val(element.status);
        $("#userid").val("NULL");		
       if (element.pushActive === true) {
          $("#push").val("true");
        } else {
          $("#push").val("false");
        }
        if (element.dynGroupAccess === true) {
          $("#dynaccess").val("true");
        } else {
          $("#dynaccess").val("false");
        }
      }
    });
  } else {
    $.each(user.result, function(index, element) {
      if (email === element.email) {
        $("#name").val(element.name);
        $("#email").val(element.email);
        $("#phone").val(element.cellphone);
        $("#hide_country").val(element.countryCode);
        $("#role").val(element.role);
		$("#status").val(element.status);
        $("#userid").val("NULL");			
        if (element.pushActive === true) {
          $("#push").val("true");
        } else {
          $("#push").val("false");
        }		
      }
    });
  }

  var select = document.getElementById("country");
  var country = $("#hide_country").val();
  $.ajax({
    url: "Json/countries.json",
    dataType: "json",
    success: function(data) {
      $.each(data, function(key, val) {
        $("#country").append(
          "<option value=" +
            val.code +
            " data-dial_code=" +
            val.dial_code +
            ">" +
            val.name +
            "</option>"
        );
      });
      $(select).val(country);
    }
  });
});

function prefix_inser()
  {
    var value= $('#country').find(':selected').data('dial_code');
    $('#phone').val(value);
  }

function getuserData() {
  var userData;

  $.ajax({
    url: "home.php",
    type: "POST",
    data: { request: "getdata" },
    success: function(data) {
      console.log(data)
      userData = data;
    },
    error: function() {
      alert(
        "Erreur Lors du traitement de la commande, veuillez actualiser la page"
      );
    },
    async: false
  });

  return userData;
}

function updateUser() {
  
  var adminData = getuserData();
  adminData = $.parseJSON(adminData);
  adminId = adminData.admin_user_id;	
    
  
  var name = $("#name").val();
  var email = $("#email").val();
  var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
  var phone = $("#phone").val();
  var country = $("#country").val();
  var lang = $("#language").val(); 
  var userid = $("#userid").val();
  var push = $("#push").val();
  var role = $("#role").val();
  var status = $("#status").val();
  var letters = /^[a-zA-Z\s]+$/;
  var dynGroupAccess = $("#dynaccess").val();
  var autorenew = $("#autorenew").val();
  var ph_regex = /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/;

  //hide all error divs
  $("#error_name").hide();
  $("#error_email").hide();
  $("#error_phone").hide();
  if (
    name !== "" &&
    email !== "" &&
    phone !== "" &&
    country !== "" &&
    lang !== "" &&
    role !== "" &&
    userid !== "" &&
    push !== ""
  ) {
    if (!name.match(letters)) {
      $("#name").focus();
      $("#error_name").show();
      $("#error_name").html("Name should be letters");
      return false;
    } else if (!email_regex.test(email)) {
      $("#email").focus();
      $("#error_email").show();
      $("#error_email").html("Email format is incorrect");
      return false;
    } else if (!ph_regex.test(phone)) {
      $("#phone").focus();
      $("#error_phone").show();
      $("#error_phone").html("Enter correct number");
      return false;
    } else {
		
		var data = {
          action: "updateUser",
          adminId: adminId,
          userId: userid,
          email: email,
          name: name,
          cellphone: phone,
          countryCode: country,
          preferredLanguage: lang,
          pushActive: push,
          role: role,
          dynGroupAccess: dynGroupAccess,
          autorenew: autorenew,
          status: status
        };
		
       $.ajax({
        url: "api/edit_user_curl.php",
        type: "POST",
        data : data,
        success: function(data) {
          console.log(data);
          if (data) {
            var obj = $.parseJSON(data);
            if (obj.status === "success") {
              updateuserlist(adminId);
              window.location.href =
                "user_details.php?id=" +
                obj.result.userId +
                "&email=" +
                obj.result.email;
            } else {
              alert(obj.result.message);
            }
          }
        },
        error: function(argument) {
          console.log(argument);
        },
        async: false
      });
    }
  } else {
	  
  if (phone == "") { $("#error_phone").show().html("Phone Required"); $("#phone").focus(); }
  if (email == "") { $("#error_email").show().html("Email Required"); $("#email").focus(); }
  if (name == "") { $("#error_name").show().html("Name Required"); $("#name").focus(); }
  
    //alert("Check again some field might be missing");
  }
}
  
function updateuserlist(adminId) {
  $.ajax({
    url: "api/admin_home_curl.php",
    type: "POST",
    data: { action: "listUsers", adminId: adminId },
    success: function(UserListdata) {
      if (UserListdata) {
        var obj = $.parseJSON(UserListdata);
        if (obj.status === "success") {
          localStorage.removeItem("userList");
          localStorage.setItem("userList", UserListdata);
        }
      }
    },
    error: function(data) {
      console.log(data);
    },
    async: false
  });
}

function updateLimitationToUser() {
	
	$("#error_venue").hide();
	$("#error_comp").hide();
	$("#error_date").hide();
	
	var err = false;
	
  if ($("#numberOfVenues").val() < 1) {
    $("#error_venue").show().html("Minimum value is 1");
    err = true;
  } 
	else if ($("#numberOfVenues").val() < $("#numberOfVenues").data("added-venues")) {
		$("#error_venue").show().html("Cannot be less than already added number of venues. (This user has " + $("#numberOfVenues").data("added-venues") + " venues)");
		err = true;
	}  
	
  if ($("#numberOfCompetitors").val() < 0) {
    $("#error_comp").show().html("Minimum value is 0");
    err = true;
  } 
	else if ($("#numberOfCompetitors").val() < $("#numberOfCompetitors").data("added-comps")) {
		$("#error_comp").show().html("Cannot be less than already added number of competitors. (This user has " + $("#numberOfCompetitors").data("added-comps") + " competitors");
		err = true;
	} 

   if ($("#numberOfCompetitors").val() == "") {
		$("#error_comp").show().html("Cannot be empty");
		err = true;
	}
	
	if ($("#expirationDate").val() == '') {
		$("#error_date").show().html("Cannot be empty");
		err = true;
	}
	
	if(err) return false;  
		
		var adminData = getuserData();
		adminData = $.parseJSON(adminData);
		adminId = adminData.admin_user_id;			    
	 
	  var name = $("#name").val();
	  var email = $("#email").val();
	  var phone = $("#phone").val();
	  var country = $("#country").val();
	  var lang = $("#language").val();	  
	  var userid = $("#userid").val();
	  var push = $("#push").val();
	  var role = $("#role").val();
	  var status = $("#status").val();
	  var dynGroupAccess = $("#dynaccess").val();
	  var autorenew = $("#autorenew").val();
	  
	  //var numberOfServices		= JSON.parse(localStorage["serviceslist"]).length;
	  var expirationDate		= $("#expirationDate").val();
	  var numberOfVenues		= $("#numberOfVenues").val();
	  var numberOfCompetitors	= $("#numberOfCompetitors").val();
	  
	  //var namelim = $("#limName").val();
	  //var Addresslim = $("#limAddress").val();
	  //var Typelim = $("#limType").val();
	  //var Arealim = $("#limArea").val(); 

	  if (
		name !== "" &&
		email !== "" &&
		phone !== "" &&
		country !== "" &&
		lang !== "" &&
		role !== "" &&
		userid !== "" &&
		push !== ""
	  ) {	 
		var data = {
		  action: "updateUser",
		  adminId: adminId,
		  userId: userid,
		  email: email,
		  name: name,
		  cellphone: phone,
		  countryCode: country,
		  preferredLanguage: lang,
		  pushActive: push,
		  role: role,
		  status: status,
		  dynGroupAccess: dynGroupAccess,
		  autorenew: autorenew,		       
		  expirationDate: expirationDate,
		  numberOfVenues: numberOfVenues,
		  numberOfCompetitors: numberOfCompetitors
		  
		  //limitationName: namelim,
		  //limitationAddress: Addresslim,
		  //limitationType: Typelim,
		 // limitationArea: Arealim,
		  //maxNumberOfServices: numberOfServices, 
		};		
		
		for (var propName in data) { 
			if (data[propName] === null || data[propName] === undefined || data[propName] == "") {
				delete data[propName];
			}
		}

		$.ajax({
		  url: "api/edit_user_curl.php",
		  type: "POST",
		  data: data,
		  success: function(data) {	  
			
			if (data) {
			  var obj = $.parseJSON(data);
			  if (obj.status === "success") {
				updateuserlist(adminId);			
				window.location.href =
				  "user_details.php?id=" +
				  obj.result.userId +
				  "&email=" +
				  obj.result.email;

			  } else {
				alert(obj.result.message);
			  }
			}
		  },
		  error: function(argument) {
			console.log("error" , argument);
		  },
		  async: false
		});
	  }
	
}

// function guidGenerator() {
// var S4 = function() {
// return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
//     								};
//  return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
// 								}

// function nullChecker(){
// 	var value = $("#userid").val();
// 	if( value == 'NULL'){
// 		uuid = guidGenerator();
// 		$("#userid").val(uuid)
// 		}
// 		else{
// 			return false;
// 		}
// }

function editServices() {
  // $("#Modal5").modal();
  const serviceslist = localStorage.getItem("serviceslist");
  const servicesListData = $.parseJSON(serviceslist);
  console.log("service data from local storage");

  console.log(servicesListData);

  $.ajax({
    url: "api/edit_user_curl.php",
    type: "POST",
    data: { action: "getServices" },
    success: function(data) {
      console.log(data);
      data = JSON.parse(data);
      var insertnew = "";
      let serviceSelected = "";
      let serviceNotSelected = "";
      //   $.each(servicesListData, function(key, value) {
      $.each(data.services, function(index2, element) {
        if ($.inArray(element.id, servicesListData) != -1) {
          console.log("service matched");
          serviceSelected += `<tr>
        		<td>
        			<div class="checkbox">
        				<input type="checkbox" checked name="addsource" value="${element.id}" id="${element.id}" class="updateServiceCount">
        				<label for="${element.id}">${element.displayName}</label>
        			</div>
        		</td>
        				<tr>`;
          return;
        } else {
          serviceNotSelected += `<tr><td>
        	  		<div class="checkbox">
        				<input type="checkbox" name="addsource" value="${element.id}" id="${element.id}" class="updateServiceCount">
        				<label for="${element.id}" >${element.displayName}</label>
        			</div>
        				</td><tr>`;
        }
      });
      //   });
      $("#logos5").html("");
      $("#logos5").html(serviceSelected);
      $("#logos5").append(serviceNotSelected);
      //   $("#Modal1").modal("toggle");
      // $("#Modal5").modal();
    }
  });

  //checkResources();
}
// function checkResources(){
// 	var serviceslist = localStorage.getItem("serviceslist");
//       servicesListData = JSON.parse(serviceslist);
// 	  console.log(servicesListData);
// 	  $.each(servicesListData, function(key, value) {
// 	$("input:checkbox[name=addsource]").each(function() {
// 	if(($(this).val())==value){
// 	// $("input:checkbox[name=addsource]").attr('checked', true);
//   	console.log("true");
// 	}
//   });
// });
// }
function updateService() {
  var selected = new Array();
  $("input:checkbox[name=addsource]:checked").each(function() {
    selected.push($(this).val());
  });

  console.log(selected);
  // return;
  var adminData = JSON.parse(getuserData());
  var userid = $("#userid").val();

  $.ajax({
    type: "POST",
    url: "api/edit_user_curl.php",
    data: {
      action: "updateUser",
      adminId: adminData.admin_user_id,
      email: $("#email").val(),
      services: selected
    },
    dataType: "JSON",
    success: function(response) {
      console.log(response);
      if (response.status === "success") {
        localStorage["serviceslist"] = JSON.stringify(selected);
        alert("Settings updated successfully");
      }
    }
  });
}

/*$("#role").on("change", function() {
	if($(this).val() == 'admin') {
		$("#status").val('subscribed');		
	}
});*/