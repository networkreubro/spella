$(function(){
$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}

var productid=$.urlParam('id'); 

var productsdata = localStorage.getItem("productsdata");
productsdata = JSON.parse(productsdata)

 $.each(productsdata, function( index, value ) {
  console.log(value);
  if (value.id==productid) {
  		$('#productnameen').html(value['name-en']);
  		$('#productnamefr').html(value['name-fr']);
  		$('#duration').html(value.duration);
  		$('#descriptionen').html(value['description-en']);
  		$('#descriptionfr').html(value['description-fr']);
  		$('#pricedollar').html(value['price-USD']);
  		$('#priceeuro').html(value['price-EUR']);
  }
});

});