$(function() {
  $("#loading").fadeOut(600);
  var remember = $.cookie("remember");
  if (remember == "true") {
    var email = $.cookie("email");
    var password = $.cookie("password");
    // autofill the fields
    $("#email").val(email);
    $("#password").val(password);
    $(".remember").prop("checked", true);
  }
});

function forgotPop() {
  $("#forgotUp").modal();
}

function resetpassword() {
  var email = $("#forgotemail").val();
  if ($("#forgotemail").val() == "") {
    $("#forgot_error").show();
    $("#forgot_error").html(
      "<span style='color:red;'>Please enter a valid email!</span>"
    );
    return false;
  } else if (!isValidEmailAddress(email)) {
    $("#forgot_error").show();
    $("#forgot_error").html(
      "<span style='color:red;'>Please enter a valid email!</span>"
    );
    return false;
  } else {
    $("#forgot_error").hide();
    $("#forgot_error").html("");

    // alert(email);return false;
    $.ajax({
      url: "api/forgot_curl.php",
      type: "POST",
      data: { action: "resetPassword", email: email },

      success: function(data) {
        $("#femail").val(email);

        var obj = $.parseJSON(data);
        // alert(obj['status']);return  false;
        if (obj["status"] === "success") {
          $("#rp").html(" Enter your code  to reset the password.");
          $("#replace").html(
            '<input type="text" id="fcode" class="form-control" placeholder="Code"><div class="col-sm-12"> <span class="validation-info pull-right" id="forgot_error" style="display: none;"></span> </div>'
          );
          $("#rbutton").html(
            ' <button type="button" class="btn btn-block btn-purple margin-top-10" onclick="check_code();">PROCEED</button>'
          );
          $("#forgotUp").modal();
        } else {
          $("#forgot_error").show();
          $("#forgot_error").html(
            "<span style='color:red;'>Please enter  your registered  email!</span>"
          );
          return false;
        }
      }
    });
  }
}

var pass_val = false;
var email_val = false;

$("#password").keypress(function(e) {
  if (e.which == 13) {
    loginApi();
  }
});
$("#email").keypress(function(e) {
  if (e.which == 13) {
    loginApi();
  }
});
//Name validation
$("#password").focusout(function() {
  check_pass();
});
//Email validation
$("#email").focusout(function() {
  check_email();
});
//validations and login
function check_email() {
  email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
  var len = $("#email").val().length;
  var email = $("#email").val();
  if (len <= 0) {
    $("#em_error")
      .html("Enter email to login")
      .css("color", "red");
    email_val = true;
  } else if (!email_regex.test(email)) {
    $("#em_error")
      .html("Please provide correct Email")
      .css("color", "red");
    $("#email_up").show();
    email_val = true;
  } else {
    $("#em_error").hide();
    email_val = false;
  }
}
function check_pass() {
  var pas = $("#password").val().length;
  if (pas < 1) {
    $("#pass_up")
      .html("Please provide a password")
      .css("color", "red");
    // $("#password").attr("placeholder", "Please provide a password");
    $("#pass_up").show();
    pass_val = true;
    return false;
  }
  if (pas < 6) {
    $("#pass_up")
      .html("Password must atleast be 6 characters")
      .css("color", "red");
    $("#pass_up").show();
  } else {
    $("#pass_up").hide();
    pass_val = false;
  }
}
function loginApi() {
  check_pass();
  check_email();

  var email = $("#email").val();
  var password = $("#password").val();

  if ($(".remember").is(":checked")) {
    // set cookies to expire in 14 days
    $.cookie("email", email, { expires: 14 });
    $.cookie("password", password, { expires: 14 });
    $.cookie("remember", true, { expires: 14 });
  } else {
    // reset cookies
    $.cookie("email", null);
    $.cookie("password", null);
    $.cookie("remember", null);
  }
  if (pass_val == false && email_val == false) {
    $("#loading").fadeIn(600);
    $.ajax({
      url: "api/index_curl.php",
      type: "POST",
      data: {
        action: "login",
        email: email,
        password: password
      },
      success: function(data) {
        console.log(data);
        var obj = $.parseJSON(data);
        if (
          obj["status"] === "success" &&
          (obj.result.role === "admin" || obj.result.role === "developer")
        ) {
          $.ajax({
            url: "index.php",
            type: "POST",
            data: obj,
            success: function(data) {
              window.location.href = "home.php";
            },
            error: function(data) {
              alert("Server Error");
            }
          });
        } else {
          $("#pass_up")
            .html(obj.result.message)
            .css("color", "red");
          $("#pass_up").show();
          $("#loading").fadeOut(600);
        }
      },
      error: function() {
        alert("error");
      }
    });
  }
}
function isValidEmailAddress(emailAddress) {
  var pattern = new RegExp(
    /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
  );
  return pattern.test(emailAddress);
}

function check_code() {
  var code = $("#fcode").val();
  var femail = $("#femail").val();
  if (code == "") {
    $("#forgot_error").html('<span style="color:red;">Enter your code</span>');
    $("#forgot_error").show();
    return false;
  } else if (!code.match(/^\d+/)) {
    $("#forgot_error").html(
      '<span style="color:red;">Enter your valid code</span>'
    );
    $("#forgot_error").show();
    return false;
  } else {
    $("#forgot_error").hide();
    $.ajax({
      url: "api/forgot_curl.php",
      type: "POST",
      data: { action: "resetPasswordValidation", email: femail, code: code },

      success: function(data) {
        alert(data);
        return false;
      }
    });
  }
}
