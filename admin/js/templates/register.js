$(function(){
    $.ajax({
      url: 'Json/countries.json',
      dataType: 'json',    
      success: function(data){
        $.each(data, function(key, val){
          $("#country").append('<option value='+val.code+' data-dial_code='+val.dial_code+'>' + val.name +'</option>');

        });
      }
    });

  });

  function prefix_inser()
  {
    var value= $('#country').find(':selected').data('dial_code');
    $('#prefix').val(value);
  }





  var name_val=false;
  var email_val=false;
  var pass_val=false;
  var country_val=false;
  var pass_val=false;
  var cpass_val=false;
  var prefix_val=false;
  var phnumber_val=false;
  $(function(){

    //Name validation
    $("#name").focusout(function(){

      check_name();

    });

    //Email validation
    $("#email").focusout(function(){

     check_email();

   });
    // Password Validation      
    $("#password").focusout(function(){

      check_pas();        
      confirm_pass(); 
    });

   // confirm password validation        
   $("#cpassword").focusout(function(){

    confirm_pass();

  });
    //Confirm country selection
    $("#country").focusout(function(){

      confirm_country();

    });
    $("#prefix").focusout(function(){

      confirm_prefix();

    });

    //Confirm phone Number
    $("#phnumber").focusout(function(){

      confirm_phNumber();

    });
  });



  function check_name()
  {
    var user= $("#name").val().length;
    if (user<=0)
    {
      $("#name").attr("placeholder", "Please provide your Name");
      $("#name").show();
      name_val=true;
    } 
    else 
    {
      name_val=false;

    }             
  }

  function check_email()
  {

    var email=$("#email").val();
    var len=$("#email").val().length;
    email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
    if (len<=0)
    {
      $("#email").attr("placeholder", "Please provide your Email");
      $("#email").show();
      email_val=true;
    }  
    else{ 
     if(!email_regex.test(email))
     {
      $("#email_up").html("Please provide correct Email").css('color', 'red').css('font-size','12px');
      $("#email_up").show();
      email_val=true;
    } 
    else{
      $("#email_up").hide();
      email_val=false;

    }  
  }        
}



function check_pas()
{
  var pas= $("#password").val().length;
  if (pas<=0)
  {
    $("#pass_up").html( "Please provide a password").css('color', 'red').css('font-size','12px');
        //$("#pass_up").attr("placeholder", "Please provide a password").css('color', 'green');
        $("#pass_up").show();
        pass_val=true;
      }
      else if(pas <6)
      {
        $("#pass_up").html( "Password must atleast be 6 characters").css('color', 'red').css('font-size','12px');
      }  
      else{
        $("#pass_up").hide();
        pass_val=false;    
      }            
    }


    function confirm_pass()
    {
      var pas= $("#password").val();
      var cpas= $("#cpassword").val();
      var cpas_len=$("#cpassword").val().length;
      if(cpas_len<=0)
      {
        $("#cpass_up").html("Please re-enter your password").css('color', 'red').css('font-size','12px');
        $("#cpassword").show();
        cpass_val=true;     
      }
      if(pas!=cpas)
      {
       $("#cpass_up").html("Passwords don't match").css('color', 'red').css('font-size','12px');
       $("#cpass_up").show();
       cpass_val=true;        
     }
     else{
      $("#cpass_up").hide();
      cpass_val=false;        

    }



  }


  function confirm_country()
  {
    country=$("#country").val();
    if(country=="")
    {
      $("#country_up").html("select your country").css('color', 'red');
      $("#country_up").show();
      country_val=true;
    }
    else{
      $("#country_up").hide();
      country_val=false;

    }
  }

  function confirm_prefix()
  {
    var intRegex = /[+0-9]$/;
    prefix= $("#prefix").val();
    p_len= $("#prefix").val().length;
    
    if(p_len<=0)
    {
      $("#ph_up").html("Prefix Necessary").css('color', 'red');
      $("#ph_up").show();
      prefix_val=true;  
    }
    else if(!intRegex.test(prefix))
    {
      $("#ph_up").html("No alphabets allowed").css('color', 'red');
      $("#ph_up").show();
      prefix_val=true;  
    }
    else{
      $("#ph_up").hide();
      prefix_val=false;  

    }
  }

  function confirm_phNumber()
  {
    var intRegex = /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/;
    prefix= $("#phnumber").val();
    if(!intRegex.test(prefix))
    {
      $("#ph_up").html("Enter correct number").css('color', 'red');
      $("#ph_up").show();
      phnumber_val=true;  
    }
    else{
      $("#ph_up").hide();
      phnumber_val=false;  

    }
  }

  function validate_call(){

    check_name();
    check_email();
    check_pas();        
    confirm_pass();
    confirm_country();
    confirm_prefix();
    confirm_phNumber();
    var name=$("#name").val();
    var email=$("#email").val();
    var country=$("#country").val();
    var prefix= $("#prefix").val();
    var dial_code= $('#country').find(':selected').data('dial_code');
    var phnumber=$("#phnumber").val();
    var pas= $("#password").val();
    phnumber = prefix+''+phnumber;
    if(name_val==false&email_val==false&&pass_val==false&&country_val==false&&prefix_val==false&&phnumber_val==false&&pass_val==false&&cpass_val==false)  
    {
      $.ajax({

        url: "api/sign_up_curl.php",
        type: "POST",
        data : {
         "action": "createUser",
         "email": email,
         "password": pas,
         "name": name,
         "cellphone": phnumber,
         "countryCode": country,
         "status": "subscribed",
         "role": "admin",
         "pushActive": true

       },

       success:function(data)
       {
        console.log(data)
        var obj= $.parseJSON(data);
        if(obj['status']==='success')
        {
          window.location.href="user_validate.php?email="+email; 
        }
        $(".loader").addClass("hidden");

      }, error: function() 
      {
        $(".loader").addClass("hidden");
      }
    });

    }

  }
