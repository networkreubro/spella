//     $(document).ready(function($) {
//         // delegate calls to data-toggle="lightbox"
//         $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function(event) {
//             event.preventDefault();
//             return $(this).ekkoLightbox({
//                 onShown: function() {
//                     if (window.console) {
//                         return console.log('Checking our the events huh?');
//                     }
//                 },
//                 onNavigate: function(direction, itemIndex) {
//                     if (window.console) {
//                         return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
//                     }
//                 }
//             });
//         });
//         //Programatically call
//         $('#open-image').click(function(e) {
//             e.preventDefault();
//             $(this).ekkoLightbox();
//         });
//         $('#open-youtube').click(function(e) {
//             e.preventDefault();
//             $(this).ekkoLightbox();
//         });
//         // navigateTo
//         $(document).delegate('*[data-gallery="navigateTo"]', 'click', function(event) {
//             event.preventDefault();
//             var lb;
//             return $(this).ekkoLightbox({
//                 onShown: function() {
//                     lb = this;
//                     $(lb.modal_content).on('click', '.modal-footer a', function(e) {
//                         e.preventDefault();
//                         lb.navigateTo(2);
//                     });
//                 }
//             });
//         });
   
//         LoadGrafanaImages();

//     });



//     function LoadGrafanaImages()
//     {
//     	$("#gallery-content-center").html("");
//     	$.ajax({

//     		url:"api/admin_Grafana_Curl.php",
//     		type:"POST",
//     		data:{"action":"getGrafana"},
//     		success:function(blob)
//     		{

//     			var obj=$.parseJSON(blob)
//     			console.log(obj)
//     			$.each(obj,function(index,element){

//     			var a = `<a href="data:image/jpg;base64,`+element+`" data-toggle="lightbox" data-gallery="multiimages" 
// 				 data-title="">
// 				 <img id="images" src="data:image/jpg;base64,`+element+`" alt="gallery" class="all studio" /> </a>`
// 				 $("#gallery-content-center").append(a);

//     			});
				
//     		},async:false
//     	});

//     	$("#gallery-content-center").css("height","100%").css("width","100%");
//     }

// $("#SeeSite").click(function(){
// 	$("#SeeSite").fadeOut();
// $("#gallery-content-center").fadeOut();
// $("#frameSite").fadeIn();
// $("#revert").fadeIn();
// });

// $("#revert").click(function(){
// 	$("#revert").fadeOut();
// 	$("#SeeSite").fadeIn();
// 	$("#gallery-content-center").fadeIn();
// 	$("#frameSite").fadeOut();	

// });


   GrafanaEmbed = {
        grafanaUrl: 'http://138.197.12.135/dashboard/db',
        dashboard: 'spella',
        queryParams: {
            dashnav: 0,
            // this is a base64-encoded string of username:password
            // for example on a *NIX machine (and Mac OS X):
            // $ echo "kiosk1:supersecret" | base64
            // a2lvc2sxOnN1cGVyc2VjcmV0Cg==
            auth: 'Bearer eyJrIjoiZ3FwMjFzbUliWHozUGF0QzUyQWNOd1EyWE1ySmNEWmUiLCJuIjoiZGFzaGJvYXJkIiwiaWQiOjF9',
            theme: 'light'
        }
    };

    (function() {
        var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
        d.src = GrafanaEmbed.grafanaUrl + '/public/app/features/dashboard/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
    })();