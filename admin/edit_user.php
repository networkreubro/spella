<?php
require_once('smarty-2.6.31/libs/Smarty.class.php');
$smarty= new Smarty();
$smarty->template_dir='templates';
$smarty->compile_dir='tmp';
include("general.php");
require_once "api/edit_user_curl.php";

if (empty($_SESSION)) {
	header("location: index.php");
}

include("home_header.php");
include("home_body_header.php");

// pass total venues/competitors added to template
$user_id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_URL);
$totalVenuesAdded = getVenueCount($user_id);
$totalCompsAdded = getCompetitorCount($user_id);
$smarty->assign('totalVenuesAdded', $totalVenuesAdded);
$smarty->assign('totalCompsAdded', $totalCompsAdded);

$smarty->display('edit_user.tpl');