<?php /* Smarty version 2.6.31, created on 2020-06-22 10:07:16
         compiled from create_user.tpl */ ?>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Create User</h1>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<!-- <li class="breadcrumb-item"><a href="#">Examples</a></li> -->
					<li class="breadcrumb-item active">Dashboard</li>
				</ol>
			</section>
			
			<!-- Main content -->
			<section class="content">
				<div class="col-md-12 col-lg-12">
					<div class="box box-solid">
					
						<div class="box-header with-border">
							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
									<i class="fa fa-minus"></i>
								</button>
							</div>
						</div>
						
						<div class="box-body"> 
							<div class="col-md-8 user-edit">
								<table class="table table-hover table-responsive services m-t-25">
									<tbody>
									
										<tr>
											<th>Fields</th>
											<th>Value</th>
										</tr>
										
										<tr>
											<td>Name</td>
											<td scope="row">									
												<div class="form-group">
													<input type="text" class="form-control type-bg" id="name" aria-describedby="emailHelp" placeholder="">
													<span id="name_up"></span>
												</div>
										
											</td>
										</tr>
										
										<tr>
											<td>Email</td>
											<td scope="row">								
												<div class="form-group">
													<input type="text" class="form-control type-bg" id="email" aria-describedby="emailHelp" placeholder="">
													<span id="email_up"></span>
												</div>
										
											</td>
										</tr>
										
										<tr>
											<td>Password</td>
											<td scope="row">									
												<div class="form-group">
													<input type="text" class="form-control type-bg" id="password" aria-describedby="emailHelp" placeholder="">
													<span id="pass_up"></span>
												</div>									
											</td>
										</tr>
										
										<tr>
											<td>Confirm Password</td>
											<td scope="row">									
												<div class="form-group">
													<input type="text" class="form-control type-bg" id="cpassword" aria-describedby="emailHelp" placeholder="">
													<span id="cpass_up"></span>
												</div>									
											</td>
										</tr>
										
										<tr>
											<td>Country</td>
											<td scope="row">									
												<div class="form-group">
													<select onchange='prefix_inser()' id="country" class="form-control type-bg">
														<option value="">Select Country</option>
													</select>
													<span id="country_up"></span>
												</div>									
											</td>
										</tr>
										
										<tr>
											<td>Cell Phone</td>
											<td scope="row">									
												<div class="form-group">
													<input type="text" class="form-control type-bg" id="phnumber" aria-describedby="emailHelp" placeholder="">
													<span id="ph_up"></span>
												</div>									
											</td>
										</tr>
										
										<tr>
											<td>Push Active</td>
											<td scope="row">									
												<div class="form-group">
													<!-- <input type="text" class="form-control type-bg" id="push" aria-describedby="emailHelp" placeholder="Enter email"> -->
													<select id="push" class="form-control type-bg" >
														<option value="true">True</option>
														<option value="false">False</option>
													</select>
												</div>									
											</td>
										</tr>
										
										<tr>
											<td>Preferred Language</td>
											<td scope="row">									
												<div class="form-group">
													<select id="language" class="form-control type-bg">
														<option value="FR">French</option>
														<option value="EN">English</option>
													</select>
												</div>									
											</td>
										</tr>
										
										<tr>
											<td>Role</td>
											<td scope="row">									
												<div class="form-group">
													<!-- <input type="text" class="form-control type-bg" id="role" aria-describedby="emailHelp" placeholder="Enter email"> -->
													<select id="role" class="form-control type-bg">
														<option value="user" selected>User</option>
														<option value="admin">Admin</option>
														<option value="developer">Developer</option>														
													</select>
												</div>									
											</td>
										</tr>
										
										<tr>
											<td>Status</td>
											<td scope="text">									
												<div class="form-group">
													<select class="form-control type-bg" id="status" placeholder="">
														<option value="trial" selected>Trial</option>
														<option value="subscribed">Subscribed</option>
														<option value="unsubscribed">Un-subscribed</option>
														<option value="blocked">Blocked</option>														
													</select>
												</div>									
											</td>
										</tr>
										
										<tr>
											<td>Dynamic Group Access</td>
											<td scope="row">									
												<div class="form-group">
													<select id="dynGroupAccess" class="form-control type-bg">
														<option value="true">True</option>
														<option value="false">False</option>
													</select>
												</div>									
											</td>
										</tr>
										
										<tr>						
											<td>Expiry Date</td>
											<td scope="row">
												<div class="form-group">
													<input type="date" class="form-control type-bg" name="expiryDate" id="expiryDate">
													<!--<input type="text" class="form-control type-bg" name="expireNever" id="expireNever" style="display:none">
													<br/><input type="checkbox" name="expiry_never" id="expiry_never" class="form-control type-bg"/> 
													<label for="expiry_never">OR Set As Never </label>
													<input type="hidden" name="expiryDate" id="expiryDate" /> -->
												</div>
											</td>
										</tr>
										
										<tr>
											<td>Number of Venues</td>
											<td scope="row">
												<div class="form-group">
													<input type="number" class="form-control type-bg" name="numberOfVenues" id="numberOfVenues">
												</div>
												<div id="error_venue"></div>
											</td>
										</tr>											
										<tr>
											<td>Number of Competitors</td>
											<td scope="row">
												<div class="form-group">
													<input type="number" class="form-control type-bg" name="numberOfCompetitors" id="numberOfCompetitors">
												</div>
												<div id="error_comp"></div>
											</td>
										</tr>
										
									</tbody>
								</table>
							</div>
							
							<div class="user manual pull-right">
								<p><button type="button" onclick="CreateUser();" class="btn btn-default btn-flat margin">Create User</button></p>
							</div>
							
						</div>
					</div>
				</div>
				<!--</div>-->
			</section>
			<!-- /.content -->
			
		</div>
		<!-- /.content-wrapper -->

		<footer class="main-footer">
			<div class="pull-right d-none d-sm-inline-block"></div>Copyright &copy; 2018 <a href="https://www.datastitute.fr/">Spella Corp</a>. All Rights Reserved.
		</footer>
		<!-- ./wrapper -->

		<!-- jQuery 3 -->
		<script src="../assets/vendor_components/jquery/dist/jquery.min.js"></script>

		<!-- popper -->
		<script src="../assets/vendor_components/popper/dist/popper.min.js"></script>

		<!-- Bootstrap v4.0.0-beta -->
		<script src="../assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

		<!-- SlimScroll -->
		<script src="../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

		<!-- FastClick -->
		<script src="../assets/vendor_components/fastclick/lib/fastclick.js"></script>
		
		<!-- Multi select -->
		<script src="assets/vendor_components/multi-select/js/jquery.multi-select.js"></script>

		<!-- maximum_admin App -->
		<script src="js/template.js"></script>

		<!-- maximum_admin for demo purposes -->
		<script src="js/demo.js"></script>

		<!-- Dijo's scripts -->
		<script src="js/templates/create_user.js"></script>

	</body>
</html>