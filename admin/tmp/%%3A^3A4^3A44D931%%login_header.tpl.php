<?php /* Smarty version 2.6.31, created on 2020-02-18 16:13:31
         compiled from login_header.tpl */ ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../images/favicon.ico">
    <title>Spella Admin</title>
      <!-- Bootstrap v4.0.0-beta -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- font awesome -->
  <link rel="stylesheet" href="css/font-awesome.css">
    <!-- ionicons -->
  <link rel="stylesheet" href="css/ionicons.css">
    <!-- theme style -->
  <link rel="stylesheet" href="css/master_style.css">
  <link rel="stylesheet" href="css/style.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- google font -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
  </head>
