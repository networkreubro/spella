<?php /* Smarty version 2.6.31, created on 2020-03-09 18:17:19
         compiled from addproduct.tpl */ ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <!-- <li class="breadcrumb-item"><a href="#">Examples</a></li> -->
      <li class="breadcrumb-item active">Dashboard</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
   <div class="col-md-12 col-lg-12">
    <div class="box box-solid">
      <div class="box-header with-border">
       <h3 class="box-title">Add product</h3>
       <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body"> 
      <div class="product_management">
        <!-- <div class="box-title lead">Add Product</div> -->
        <form>
          <div class="form-group row pt-3">
           <label for="" class="col-sm-2 col-form-label">Product Name</label>
           <div class="col-sm-3">
            <input type="text" class="form-control" name="productnameen" id="productnameen" placeholder="English">
            <label id="valnameen">  </label>
          </div>
          <div class="col-sm-3">
            <input type="text" class="form-control" name="productnamefr" id="productnamefr" placeholder="French">
            <label id="valnamefr">  </label>
          </div>
        </div>
        <div class="form-group row">
         <label for="" class="col-sm-2 col-form-label">Plan Duration</label>
         <div class="col-sm-3">
          <input type="number" class="form-control" name="duration" id="duration">
          <label id="valduration">  </label>
        </div>
        <label for="" class="col-sm-2 col-form-label pl-0">(in month)</label>
      </div>
        <div class="form-group row">
         <label for="" class="col-sm-2 col-form-label">Description</label>
         <div class="col-sm-3">
         <textarea class="form-control" rows="4" name="descriptionen" id="descriptionen" placeholder="English"></textarea>
         <label id="valdescen">  </label>
        </div>
        <div class="col-sm-3">
         <textarea class="form-control" name="descriptionfr" id="descriptionfr" rows="4" placeholder="French"></textarea>
         <label id="valdescfr">  </label>
        </div>
      </div>
         <div class="form-group row">
           <label for="" class="col-sm-2 col-form-label">Price</label>
           <div class="col-sm-3">
            <input type="text" class="form-control" name="" name="pricedollar" id="pricedollar" placeholder="Dollar">
            <label id="valpricedollar">  </label>
          </div>
          <div class="col-sm-3">
            <input type="text" class="form-control" name="" name="priceeuro" id="priceeuro" placeholder="Euro">
            <label id="valproceeuro">  </label>
          </div>
        </div>
        <div class="form-group row">
        <div class="col-md-10 ml-md-auto">
          <button type="button" class="btn btn-green pl-4 pr-4" onclick="window.history.back()">Cancel</button>
          <button type="button" class="btn btn-green pl-4 pr-4" onclick="addProduct();">Save</button>
          </div>
        </div>
    </form>
  </div>

</div>
</div>
</div>




</div>


</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<footer class="main-footer">
  <div class="pull-right d-none d-sm-inline-block">
  </div>Copyright &copy; 2018 <a href="https://www.datastitute.fr/">Spella Corp</a>. All Rights Reserved.
</footer>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../assets/vendor_components/jquery/dist/jquery.min.js"></script>

<!-- popper -->
<script src="../assets/vendor_components/popper/dist/popper.min.js"></script>

<!-- Bootstrap v4.0.0-beta -->
<script src="../assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>


<!-- SlimScroll -->
<script src="../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->
<script src="../assets/vendor_components/fastclick/lib/fastclick.js"></script>

<!-- maximum_admin App -->
<script src="js/template.js"></script>

<!-- maximum_admin for demo purposes -->
<script src="js/demo.js"></script>
<!-- Dijo's scripts -->
<script src="js/templates/addProduct.js"></script>
</body>
</html>