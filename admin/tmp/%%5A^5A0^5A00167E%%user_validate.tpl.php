<?php /* Smarty version 2.6.31, created on 2020-06-22 10:27:47
         compiled from user_validate.tpl */ ?>

<body class="hold-transition register-page">
  <div class="register-box">
    <div class="register-logo">
      <!-- <a href="index.php"><b>Maximum</b>Admin</a> -->
      <img src="images/logo-spella.png" alt="" class="img-fluid" / >
    </div>
    <div class="register-box-body">
      <div class="validation-box">
      <div class="col-sm-12 text-center">
       <h1>Validation</h1>
     </div>
     <hr>
     <div class="col-sm-12 margin-top-40">
      <p class="text-center">
        Dear user, please enter the validation code sent to your phone for verifying your account
      </p>
    </div>
    <div class="form-group">
     <input type="password" id="valCode" class="form-control" value="9999">
      <div class="validation-info" id="valUp"></div>
   </div>
   <div class="margin-top-40 text-center">
     <button type="button" onclick="validate();" class="btn btn-info btn-block btn-flat margin-top-10">VALIDATE</button>
   </div>
 </div>
</div>
</div>
<!-- /.register-box -->


</body>
<?php echo '

<!-- jQuery 3 -->
<script src="assets/vendor_components/jquery/dist/jquery.min.js"></script>

<!-- popper -->
<script src="assets/vendor_components/popper/dist/popper.min.js"></script>

<!-- Bootstrap v4.0.0-beta -->
<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!--Custom Js for validation-->
<script src="js/templates/user_validate.js"></script>

'; ?>




</html>



