<?php /* Smarty version 2.6.31, created on 2020-02-18 14:24:29
         compiled from home_header.tpl */ ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../../images/favicon.ico">

  <title>Spella</title>
  
  <!-- Bootstrap v4.0.0-beta -->
  <link rel="stylesheet" href="css/bootstrap.min.css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Ionicons -->
  <link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="/assets/vendor_components/glyphicons/glyphicon.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="css/master_style.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="js/templates/logout.js"></script>

  <!-- gallery -->
  <link rel="stylesheet" type="text/css" href="assets/vendor_components/gallery/css/animated-masonry-gallery.css" />
   
    <!-- fancybox -->
    <link rel="stylesheet" type="text/css" href="assets/vendor_components/lightbox-master/dist/ekko-lightbox.css" />

<!-- google font -->
 <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">

</head>