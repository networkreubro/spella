<?php /* Smarty version 2.6.31, created on 2020-02-18 14:24:29
         compiled from home_body_header.tpl */ ?>

<body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <header class="main-header fixed-top">
      <!-- Logo -->
      <a href="home.php" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><img src="../images/logo-small.png" alt="" /></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img src="../images/Spella_logo.png" alt="" /></span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
      </nav>
    </header>

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar fixed-top">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
         <li onclick="redirect('home');" class="treeview">
          <a href="home.php">
            <i class="fa fa-home"></i><span >Dashboard</span>
          </a>
        </li>
             <li onclick="redirect('productlist')" class="treeview">
          <a href="#">
            <i class="fa fa-th"></i>
            <span >Products</span>
          </a>
        </li>
           <li onclick="redirect('scrap_venueService')" class="treeview">
          <a href="#">
            <i class="fa fa-th"></i>
            <span >Immediate Scrap</span>
          </a>
        </li>
           <li onclick="redirect('Scrap_log')" class="treeview">
          <a href="#">
            <i class="fa fa-link"></i>
            <span >Scrap Log</span>
          </a>
        </li>
          <li onclick="redirect('API_log')" class="treeview">
          <a href="#">
            <i class="fa fa-link"></i>
            <span >API Log</span>
          </a>
        </li>
          <li onclick="redirect('createUser')" class="treeview">
          <a href="#">
            <i class="fa fa-plus"></i>
            <span >Create User</span>
          </a>
        </li> 
        <li onclick="goTo();" class="treeview">
          <a target="_blank" href="http://138.197.12.135/login">
            <embed type="image/svg+xml" src="images/grafana.svg" />
            <span onclick="goTo();">Grafana</span>
          </a>
        </li> 
      </ul>
    </section>
    <!-- /.sidebar -->
    <div class="sidebar-footer">
      <!-- item-->
      <!-- <a href="" class="link" data-toggle="tooltip" title="" data-original-title="Settings"><i class="fa fa-cog fa-spin"></i></a> -->
      <!-- item-->
      <!-- <a href="" class="link" data-toggle="tooltip" title="" data-original-title="Email"><i class="fa fa-envelope"></i></a> -->
      <!-- item-->
              <a href="" class="link" data-toggle="tooltip" title=""  onclick="logout();" data-original-title="Logout"><i class="fa fa-power-off"></i></a>


    </div>
  </aside>
<?php echo '
<script>
	function goTo() {
	window.open(\'http://142.93.227.1/login \', \'_blank\');
	}
</script>
'; ?>