<?php

require_once('smarty-2.6.31/libs/Smarty.class.php');
$smarty= new Smarty();
$smarty->template_dir='templates';
$smarty->compile_dir='tmp';
include("general.php");

if(empty($_SESSION['admin_user_id']))
{
	header("location: index.php");
}

if($_POST['request']=='getdata')
{
	echo json_encode($_SESSION);
	exit;
}

include("home_header.php");
include("home_body_header.php");

$smarty->display('home.tpl');

?>
