
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <!-- <li class="breadcrumb-item"><a href="#">Examples</a></li> -->
      <li class="breadcrumb-item active">Dashboard</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
   <div class="col-md-12 col-lg-12">
    <div class="box box-solid">
      <div class="box-header with-border">
       <h3 class="box-title">View product</h3>
       <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body"> 
      <div class="add-new float-right">
          <a href="productlist.php" class="btn btn-default btn-flat margin" style="color: white;">Back</a>
        </div>

      <div class="product_management">
        <!-- <div class="box-title lead">Add Product</div> -->
        <p>
          <label>Name in English: <span id="productnameen"></span> </label><br>
          <label>Name in French: <span id="productnamefr"></span></label>
        </p>
        <p>
          <label>Description in English:<span id="descriptionen"></span> </label><br>
          <label>Description in French: <span id="descriptionfr"></span></label>
        </p>
        <p>
          <label>Duration (in Months):<span id="duration"></span> </label><br>
        </p>
        <p>
          <label>Amount in USD: <span id="pricedollar"></span></label><br>
          <label>Amount in EUR:<span id="priceeuro"></span> </label>
        </p>

  </div>

</div>
</div>
</div>




</div>


</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<footer class="main-footer">
  <div class="pull-right d-none d-sm-inline-block">
  </div>Copyright &copy; 2018 <a href="https://www.datastitute.fr/">Spella Corp</a>. All Rights Reserved.
</footer>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../assets/vendor_components/jquery/dist/jquery.min.js"></script>

<!-- popper -->
<script src="../assets/vendor_components/popper/dist/popper.min.js"></script>

<!-- Bootstrap v4.0.0-beta -->
<script src="../assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>


<!-- SlimScroll -->
<script src="../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->
<script src="../assets/vendor_components/fastclick/lib/fastclick.js"></script>

<!-- maximum_admin App -->
<script src="js/template.js"></script>

<!-- maximum_admin for demo purposes -->
<script src="js/demo.js"></script>
<!-- Dijo's scripts -->
<script src="js/templates/viewProduct.js"></script>
</body>
</html>
