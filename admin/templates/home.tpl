
<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>User List</h1>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <!-- <li class="breadcrumb-item"><a href="#">Examples</a></li> -->
      <li class="breadcrumb-item active">Dashboard</li>
    </ol>
    <div class="row">
    <div class="col-6">
<!--       <div class="content-form-section">
      <form class="form-inline">
        <div class="form-group mb-2">
          <input type="text" class="form-control" id="userName" placeholder="Name">
        </div>
        <div class="form-group mx-sm-3 mb-2">
          <input type="text" class="form-control" id="email" placeholder="Email">
        </div>
      <button class="btn btn-outline-success mx-sm-3 mb-2" type="button" onclick="userSearch();">Search</button>
      </form>
    </div> -->
  </div>
  <div class="col-6"><span><button id="Previous" style="display: none;" onclick="PreviousMenu();" class="btn btn-green pull-right">See Previous List</button></span></div>
</div>
</section>

<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="box" id="adminbox">
    <div class="box-header with-border">
      <h3 class="box-title">Admin List</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fa fa-minus"></i></button>
      <!--       <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button> -->
      </div>
    </div>
    <div class="box-body">
      <div class="box-body">
        <table id="adminListTable" class="table table-bordered table-striped table-responsive table-hover ">
          <thead>
            <tr>
               <th>Sl no</th>
               <th>Name</th>
               <th>Email</th>
               <th>Role</th>
               <th>Options</th>
            </tr>
          </thead>


          <tbody id="adminListTableBody">


          </tbody>
        </table>
      </div>

    </div>
    
  </div><!-- /.box-body -->



  <!-- <div class="box" id="devbox">
    <div class="box-header with-border">
      <h3 class="box-title">Title</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fa fa-minus"></i></button>
       <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button> -->
     <!--  </div>
    </div>
    <div class="box-body">
      <div class="box-body">
        <table id="developerListTable" class="table table-bordered table-striped table-responsive table-hover ">
          <thead>
            <tr>
              <th>Sl no</th>
              <th>Name</th>
              <th>Email</th>
              <th>Role</th>
              <th>Options</th>
            </tr>
          </thead>

          <tbody id="developerListTableBody">


          </tbody>
        </table>
      </div>

    </div>
    
  </div> --><!-- /.box-body -->





  <div class="box" id="userbox">
    <div class="box-header with-border">
      <h3 class="box-title">User List</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fa fa-minus"></i></button>
      <!--       <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button> -->
      </div>
    </div>
    <div class="box-body">
      <div class="box-body">
        <table id="userListTable" class="table table-bordered table-striped table-responsive table-hover ">
          <thead>
            <tr>
              <th>Sl no</th>
              <th>Name</th>
              <th>Email</th>
              <th>Role</th>
              <th>Options</th>
            </tr>
          </thead>
          <tbody id="userListTableBody">


          </tbody>
        </table>
      </div>

    </div>
    
  </div><!-- /.box-body -->


  <div class="box" id="customSearchbox" style="display: none;">
    <div class="box-header with-border">
      <h3 class="box-title">Title</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fa fa-minus"></i></button>
      <!--       <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button> -->
      </div>
    </div>
    <div class="box-body">
      <div class="box-body">
        <table id="customSearchTable" class="table table-bordered table-striped table-responsive table-hover ">
          <thead>
            <tr>
               <th>Sl no</th>
               <th>Name</th>
               <th>Email</th>
               <th>Role</th>
               <th>Options</th>
            </tr>
          </thead>


          <tbody id="customSearchTableBody">


          </tbody>
        </table>
      </div>

    </div>
    
  </div><!-- /.box-body -->


</div>
<!-- /.box -->

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<footer class="main-footer">
  <div class="pull-right d-none d-sm-inline-block">
  </div>Copyright &copy; 2018 <a href="https://www.datastitute.fr/">Spella Corp</a>. All Rights Reserved.
</footer>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="assets/vendor_components/jquery/dist/jquery.min.js"></script>

<!-- popper -->
<script src="assets/vendor_components/popper/dist/popper.min.js"></script>

<!-- Bootstrap v4.0.0-beta -->
<script src="assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>


<!-- SlimScroll -->
<script src="assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->
<script src="assets/vendor_components/fastclick/lib/fastclick.js"></script>

<!-- maximum_admin App -->
<script src="js/template.js"></script>

<!-- maximum_admin for demo purposes -->
<script src="js/demo.js"></script>
	<!--Datatable scripts-->
	<script src="assets/vendor_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>


<!--Dijo Script-->
<script src="js/templates/home.js"></script>

</body>
</html>
