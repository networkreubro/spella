
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Scrap Service Venue</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li class="breadcrumb-item"><a href="#">Examples</a></li> -->
        <li class="breadcrumb-item active">Dashboard</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Scrap Venue</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
            <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="box-body">
           <form>
            <div class="form-group row">
              <label for="staticEmail2" class="col-sm-2 col-form-label">link of service venue</label>
                <div class="col-sm-7  mx-sm-3 mb-2">
                <input type="text" id="serviceUrl" class="form-control type-bg" placeholder="">
              </div>
  <button type="button" onclick="scrap();" class="col-sm-1 btn btn-success mb-2 text-upper">scrap</button>
            </div>
         </form>
          <div class="row justify-content-md-center">
      <div class="col col-lg-5 text-center">
        <div class="info-box mt-25">
          <div style="display: none;" class="success">
  <img src="../images/success.svg" alt="" />
</div>
   
         <div class="panel panel-default">
            <div class="panel-heading">
                         </div>
            <div  class="adjust">
               <div style="display: none;" id="scrapperLoader" class="loader3"></div>
            </div>
         </div>
 
    </div>
    </div>
           </div>

     </div>
     <!-- /.box-body -->
   </div>
   <!-- /.box-footer-->

 <!-- /.box -->

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<footer class="main-footer">
  <div class="pull-right d-none d-sm-inline-block">
  </div>Copyright &copy; 2018 <a href="https://www.datastitute.fr/">Spella Corp</a>. All Rights Reserved.
</footer>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../assets/vendor_components/jquery/dist/jquery.min.js"></script>

<!-- popper -->
<script src="../assets/vendor_components/popper/dist/popper.min.js"></script>

<!-- Bootstrap v4.0.0-beta -->
<script src="../assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>


<!-- SlimScroll -->
<script src="../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->
<script src="../assets/vendor_components/fastclick/lib/fastclick.js"></script>

<!-- maximum_admin App -->
<script src="js/template.js"></script>

<!-- maximum_admin for demo purposes -->
<script src="js/demo.js"></script>

<!--Script by dijo-->
<script src="js/templates/scrap_venueService.js"></script>
</body>
</html>
