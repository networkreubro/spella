<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>User Information</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!-- <li class="breadcrumb-item"><a href="#">Examples</a></li> -->
        <li class="breadcrumb-item active">Dashboard</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">



         <div class="col-md-12 col-lg-12">
          <div class="box box-solid">
            <div class="box-header with-border">
               <h3 class="box-title">User Information</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
            <i class="fa fa-minus"></i></button>
            </div>
             </div>
             <div class="box-body"> 
               <div class="user manual pull-right">
                 <p>
                <button type="button" class="btn btn-default btn-flat margin" onclick="javascript:document.getElementById('asuser').submit();">As User</button>
               <button onclick="Invalidate();" type="button" class="btn btn-default btn-flat margin">Invalidate</button>
                <button onclick="editUser();" type="button" class="btn btn-default btn-flat margin">Edit User</button>
                  <button type="button" onclick="LogFinder();" id="userlogbu" class="btn btn-default btn-flat margin">Userlog</button>
                <button id="seeUsDet" onclick="openseeUsDet();" type="button" style="display: none;" class="btn btn-default btn-flat margin">See User Detail</button>

                </p>
               </div>
               <form id="asuser" action="asuser.php" method="POST" target="_blank">
                 <input type="hidden" name="userId" id="userId">
                 <input type="hidden" name="userName" id="userName">
                 <input type="hidden" name="userRole" id="userRole">
                 <input type="hidden" name="userCell" id="userCell">
                 <input type="hidden" name="usercntCode" id="usercntCode">
                 <input type="hidden" name="userpushActive" id="userpushActive">
                 <input type="hidden" name="userEmail" id="userEmail">
                 <input type="hidden" name="userPreflang" id="userPreflang">
               </form>
                     <div class="clearfix"></div>
              <dl id="userMainDetailBox" class="dl-horizontal">
                <dt>Name :</dt>
                <dd id="name"></dd>
                <dt>User Id :</dt>
                <dd id="userid"></dd>
                <dt>Email :</dt>
                <dd id="email"></dd>
                <dt >Phone :</dt>
                <dd id="phone"></dd>
                <dt>CountryCode :</dt>
                <dd id="country"></dd>
                <dt> Preferred Language :</dt>
                <dd id="language"></dd>
                <dt>User Role :</dt>
                <dd id="role"></dd>
                 <dt>Push Active :</dt>
                <dd id="push"></dd>
                 <dt>Status :</dt>
                <dd id="status"></dd>
                </dl>
          <div style="display: none;" id="logSearchBox">
      <form class="form-inline">
            
           <span>
             <input type="text" class="form-control" id="start" placeholder="Start Date">
           </span> 
           <span style="margin-left: 10px;">
             <input type="text" class="form-control" id="end" placeholder="End Date">
           </span>  
            <button onclick="logfetcher();" type="button" class="btn btn-default btn-flat margin">Fetch</button>   
      </form>
        
        <div id="logdata">       
         <div class="modal-inner mt-25">
         <div class="panel panel-default">
            <div class="panel-heading">
                         </div>
            <div  class="adjust">
               <div style="display: none;" id="scrapperLoader" class="loader3"></div>
               <dl id="loglist" class="dl-horizontal" style="overflow-y: auto; max-height: 300px;">
                 
               </dl>
            </div>
         </div>
 
    </div>
    </div>

          </div>

            </div>
          </div>


      <div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Venue Information</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
            <i class="fa fa-minus"></i></button>
            </div>
          </div>
          <div class="box-body">
                <div id="venueListComp" class="box-body">
            
                 </div>
            </div>
         </div>

        </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">

      <!--body starting-->

          <div style="" id="logSearchBox">
      <form class="form-inline">
            
           <span>
            <label>StartDate</label> <input type="date" class="form-control" id="start1" placeholder="yy-mm-dd">
           </span> 
           <span style="margin-left: 10px;">
           <label>EndDate</label> <input type="date" class="form-control" id="end1" placeholder="yy-mm-dd">
           </span>  
            <button onclick="logfetcher2();" type="button" class="btn btn-default btn-flat margin">Fetch</button>   
      </form>
        
        <div id="logdata2">       
         <div class="info-box mt-25">
         <div class="panel panel-default">
            <div class="panel-heading">
                         </div>
            <div  class="adjust">
               <div style="display: none;" id="scrapperLoader" class="loader3"></div>
               <dl id="loglist2" class="dl-horizontal" style="overflow-y: scroll; max-height: 300px;">
                 
               </dl>
            </div>
         </div>
 
    </div>
    </div>

          </div>


      <!-- body ending -->

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<footer class="main-footer">
  <div class="pull-right d-none d-sm-inline-block">
  </div>Copyright &copy; 2018 <a href="https://www.datastitute.fr/">Spella Corp</a>. All Rights Reserved.
</footer>
<!-- ./wrapper -->
{literal}
<!-- jQuery 3 -->
<script src="../assets/vendor_components/jquery/dist/jquery.min.js"></script>

<!-- popper -->
<script src="../assets/vendor_components/popper/dist/popper.min.js"></script>

<!-- Bootstrap v4.0.0-beta -->
<script src="../assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>


<!-- SlimScroll -->
<script src="../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->
<script src="../assets/vendor_components/fastclick/lib/fastclick.js"></script>

<!-- maximum_admin App -->
<script src="js/template.js"></script>

<!-- maximum_admin for demo purposes -->
<script src="js/demo.js"></script>
<!-- Dijo Scripts -->
<script src="js/templates/user_details.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script>
  $( function() {
    $( "#start" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );

    $( function() {
    $( "#end" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );



  </script>

{/literal}
</body>
</html>
