
<body class="hold-transition register-page">
  <div class="register-box">
    <div class="register-logo">
      <!-- <a href="index.php"><b>Maximum</b>Admin</a> -->

    <a href="index.php">  <img src="../images/logo-spella.png" alt="" class="img-fluid" / ></a>

    </div>

    <div class="register-box-body">
      <p class="login-box-msg bold- white">Register a new membership</p>
               <div class="form-group has-feedback">
              <div class="input-group">
                <input type="text" id="name" class="form-control" placeholder="Full name">
                <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
              </div>
            </div>

            <div class="form-group has-feedback">
              <div class="input-group">
                <input type="email" id="email" class="form-control" placeholder="Email">
                <span class="input-group-addon"><i class="ion ion-email" aria-hidden="true"></i></span>
              </div>
              <div class="validation-info pull-right" id="email_up"></div>
            </div>

            <div class="form-group has-feedback">
              <div class="input-group">
                <input type="password" id="password" class="form-control" placeholder="Password">
                <span class="input-group-addon"><i class="ion ion-locked" aria-hidden="true"></i></span>
              </div>
              <div class="validation-info pull-right" id="pass_up"></div>
            </div>

            <div class="form-group has-feedback">
              <div class="input-group">
                <input type="password" id="cpassword" class="form-control" placeholder="Retype password">
                <span class="input-group-addon"><i class="ion ion-locked" aria-hidden="true"></i></span>
              </div>
               <div class="validation-info pull-right" id="cpass_up"></div>
               </div>

            <div class="form-group has-feedback  ">
              <select class="form-control select2" id="country" onchange='prefix_inser()' style="width: 100%;">
                <option value="">Select Country</option>
              </select>
               <div class="validation-info pull-right" id="country_up"></div>
            </div>
           
            <div class="form-group has-feedback" style="padding-top: 5px;">
             <div class="input-group">
              <input type="text" id="prefix" class="form-control" placeholder="Prefix">
              <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
            </div>
          </div>
          <div class="form-group has-feedback" >
           <div class="input-group">
            <input type="text" id="phnumber"  class="form-control" placeholder="Phone Number">
            <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
          </div>
          <div class="validation-info pull-right" id="ph_up"></div>
        </div>


        <!-- /.col -->
        <div class="form-group text-center">
          <button type="submit" onclick='validate_call()' class="btn btn-block btn-green margin-top-10">SIGN UP</button>
        </div>
        <!-- /.col -->

        <div class="margin-top-20 text-center">
         <p class="white">Already have an account?<a href="index.php" class="text-info blue m-l-5"> Sign In</a></p>
       </div>

     </div>
     <!-- /.form-box -->
   </div>
   <!-- /.register-box -->


 </body>
 

 <!-- jQuery 3 -->
<!-- jQuery 3 -->
  <script src="js/jquery.min.js"></script>
   <!-- popper -->
  <script src="js/popper.min.js"></script>
  <!-- Bootstrap v4.0.0-beta -->
  <script src="js/bootstrap.min.js"></script>
  <script src="js/templates/register.js"></script>





</html>





