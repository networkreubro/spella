

<body class="hold-transition login-page">
<div class="login-box">
<div class="login-logo">
      <img src="../images/logo-spella.png" alt="" class="img-fluid">
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg bold white">Sign in to start your session</p>
      <div class="form-group has-feedback">
        <input type="text" id="email" class="form-control" placeholder="Email">
          <div class="col-sm-12" >
        <span class="validation-info pull-right" id="em_error"></span>
      </div>
      </div>
      <div class="form-group has-feedback">
        <input type="password"  id="password" class="form-control" placeholder="Password">
        <div class="col-sm-12" >
        <span class="pull-right validation-info" id="pass_up"></span>
        </div> 
      </div>   
      <div class="clearfix"></div>   
      <div class="row">
           <div class="col-6">
          <div class="checkbox">
            <input type="checkbox" class="remember" id="basic_checkbox_1" >
			<label class="white" for="basic_checkbox_1">Remember Me</label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-6">
         <div class="fog-pwd">
          	<a class="white" onclick="forgotPop();"><i class="ion ion-locked"></i> Forgot password?</a><br>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-12 text-center">
          <button type="button" class="btn btn-block btn-green margin-top-10" onclick="loginApi()">SIGN IN</button>
        </div>
        <!-- /.col -->
      </div>
    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <!-- <a href="#" class="btn btn-social-icon btn-circle btn-facebook"><i class="fa fa-facebook"></i></a>
      <a href="#" class="btn btn-social-icon btn-circle btn-google"><i class="fa fa-google-plus"></i></a> -->
    </div>
    <!-- /.social-auth-links -->
    <div class="margin-top-30 text-center">
    	<p class="white">Don't have an account? <a href="register.php" class="white text-info m-l-5">Sign Up</a></p>
    </div>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<div class="modal fade" id="forgotUp" tabindex="-1" role="dialog" aria-labelledby="compUp" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Forgot Password?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div  class="modal-body sevicelist text-center">


<div class="login-box-body">
    <p class="login-box-msg">Enter you registred email to reset the password.</p>
      <div class="form-group has-feedback">
        <input type="text" id="forgotemail" class="form-control" placeholder="Email">
          <div class="col-sm-12">
        <span class="validation-info pull-right" id="forgot_error" style="display: none;"></span>
      </div>
      </div>
      <div class="clearfix"></div>   
      <div class="row">

        <div class="col-12 text-center">
          <button type="button" class="btn btn-block btn-purple margin-top-10" onclick="resetpassword();">PROCEED</button>
        </div>
        <!-- /.col -->
      </div>
  </div>

      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> -->
      </div>
    </div>
  </div>
</div>


</body>

  <!-- jQuery 3 -->
  <script src="js/jquery.min.js"></script>
   <!-- popper -->
  <script src="js/popper.min.js"></script>
  <!-- Bootstrap v4.0.0-beta -->
  <script src="js/bootstrap.min.js"></script>
    <!-- jQuery cookie -->
  <script src="assets/vendor_components/jquery/dist/jquery.cookie.js"></script>
  <!--Dijo Script-->
  <script src="js/templates/index.js"></script>

</html>
