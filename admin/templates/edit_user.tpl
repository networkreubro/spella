		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Edit User</h1>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<!-- <li class="breadcrumb-item"><a href="#">Examples</a></li> -->
					<li class="breadcrumb-item active">Dashboard</li>
				</ol>
			</section>
			
			<!-- Main content -->
			<section class="content">
				<div class="col-md-12 col-lg-12">
					<div class="box box-solid">
					
						<div class="box-header with-border">
							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
									<i class="fa fa-minus"></i>
								</button>
							</div>
						</div>
						
						<div class="box-body"> 
							<div class="col-md-8 user-edit">
								<table class="table table-hover table-responsive services m-t-25">
									<tbody>
									
										<tr>
											<th>Fields</th>
											<th>Value</th>
										</tr>
										
										<tr>
											<td>Name</td>
											<td scope="row">									
												<div class="form-group">
													<input type="text" class="form-control type-bg" id="name" aria-describedby="emailHelp" placeholder="">
												</div>									
												<div id="error_name" style="color:red;"></div>
											</td>
										</tr>
										
										<tr>
											<td>Email</td>
											<td scope="row">									
												<div class="form-group">
													<input type="text" class="form-control type-bg" id="email" aria-describedby="emailHelp" placeholder="">
												</div>									
												<div id="error_email" style="color:red;"></div>
											</td>
										</tr>
										
										<tr>
											<td>User Id</td>
											<td scope="row">									
												<div class="form-group">
													<input type="text" class="form-control type-bg" id="userid" aria-describedby="emailHelp" placeholder="" readonly>
												</div>									
											</td>
										</tr>
										
										<tr>
											<td>Country</td>
											<td scope="row">									
												<div class="form-group sele">
													<input type="hidden" name="hide_country" id="hide_country" value="">
													<select onchange='prefix_inser()' id="country" class="form-control type-bg" name="country"></select>
													<span id="country_up"></span>
												</div>									
											</td>
										</tr>
										
										<tr>
											<td>Cell Phone</td>
											<td scope="row">									
												<div class="form-group">
													<input type="text" class="form-control type-bg" id="phone" aria-describedby="emailHelp" placeholder="">
												</div>									
												<div id="error_phone" style="color:red;"></div>
											</td>
										</tr>
										
										<tr>
											<td>Push Active</td>
											<td scope="row">									
												<div class="form-group">
													<!-- <input type="text" class="form-control type-bg" id="push" aria-describedby="emailHelp" placeholder="Enter email"> -->
													<select id="push" class="form-control type-bg" >
														<option value="true">True</option>
														<option value="false">False</option>
													</select>
												</div>									
											</td>
										</tr>
										
										<tr>
											<td>Preferred Language</td>
											<td scope="row">									
												<div class="form-group">
													<select id="language" class="form-control type-bg">
														<option value="FR">French</option>
														<option value="EN">English</option>
													</select>
												</div>									
											</td>
										</tr>
										
										<tr>
											<td>Role</td>
											<td scope="row">									
												<div class="form-group">
													<!-- <input type="text" class="form-control type-bg" id="role" aria-describedby="emailHelp" placeholder="Enter email"> -->
													<select id="role" class="form-control type-bg">
														<option value="admin">Admin</option>
														<option value="developer">Developer</option>
														<option value="user">User</option>
													</select>
												</div>									
											</td>
										</tr>
										
										<tr>
											<td>Status</td>
											<td scope="text">									
												<div class="form-group">
													<select  class="form-control type-bg" id="status" placeholder="">
														<option value="trial">Trial</option>
														<option value="subscribed">Subscribed</option>
														<option value="unsubscribed">Un-subscribed</option>
														<option value="blocked">Blocked</option>
													</select>
												</div>									
											</td>
										</tr>
										
										<tr>
											<td>Dynamic Access</td>
											<td scope="row">									
												<div class="form-group">
													<select id="dynaccess" class="form-control type-bg">
														<option value="true">True</option>
														<option value="false">False</option>
													</select>
												</div>								
											</td>
										</tr>
										
										<tr>
											<td>Auto Renew</td>
											<td scope="row">									
												<div class="form-group">
													<select id="autorenew" class="form-control type-bg">
														<option value="true">True</option>
														<option value="false">False</option>
													</select>
												</div>									
											</td>
										</tr>
										
									</tbody>
								</table>
							</div>
							
							<div class="user manual pull-right">
								<p>
									<button type="button" onclick="updateUser();" class="btn btn-default btn-flat margin">Save</button>
								</p>
							</div>
						</div>
					</div>
					
					<!-------------------------->
					{* Custom Services section *}

					<div class="box box-solid">
					
						<div class="box-header with-border">
							<h3>Custom Services</h3>
							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"	title="Collapse">
									<i class="fa fa-minus"></i>
								</button>
							</div>
						</div>
						
						<div class="box-body"> 
							<div class="col-md-8 user-edit">
								<table class="table table-hover table-responsive services m-t-25">
									<tbody id="logos5"></tbody>
								</table>
					
								<!----------------------------------------->
								{* <table class="table table-hover table-responsive services m-t-25">
								<tbody>
								<tr>
								<th>Fields</th>
								<th>Value</th>
								</tr>
								<tr>
								<td>Limitation Name</td>
								<td scope="row">
								<form>
								<div class="form-group">
								<textarea type="text" class="form-control type-bg" id="limName" aria-describedby="emailHelp" placeholder=""></textarea>
								</div>
								</form>
								</td>
								</tr>
								</tbody>
								</table> *}
								<!----------------------------------------->
								{* <table class="table table-hover table-responsive services m-t-25">
								<tbody>
								<tr>
								<th>Fields</th>
								<th>Value</th>
								</tr>
								<tr>
								<td>Limitation Name</td>
								<td scope="row">
								<form>
								<div class="form-group">
								<textarea type="text" class="form-control type-bg" id="limName" aria-describedby="emailHelp" placeholder=""></textarea>
								</div>
								</form>
								</td>
								</tr>
								<tr>
								<td>Limitation Adress</td>
								<td scope="row">
								<form>
								<div class="form-group">
								<textarea type="text" class="form-control type-bg" id="limAddress" aria-describedby="emailHelp" placeholder=""></textarea>
								</div>
								</form>
								</td>
								</tr>
								<tr>
								<td>Limitation Type</td>
								<td scope="row">
								<form>
								<div class="form-group">
								<textarea type="text" class="form-control type-bg" id="limType" aria-describedby="emailHelp" placeholder=""></textarea>
								</div>
								</form>
								</td>
								</tr>
								<tr>
								<td>Limitation Service</td>
								<td scope="row">
								<form>
								<div class="form-group">
								<textarea type="text" class="form-control type-bg" id="limSer" aria-describedby="emailHelp" placeholder=""></textarea>
								</div>
								</form>
								</td>
								</tr>
								<tr>
								<td>Limitation Area</td>
								<td scope="row">
								<form>
								<div class="form-group">
								<textarea type="text" class="form-control type-bg" id="limArea" aria-describedby="emailHelp" placeholder=""></textarea>
								</div>
								</form>
								</td>
								</tr>
								<tr>
								<td>Limitattion Count</td>
								<td scope="row">
								<form>
								<div class="form-group">
								<input class="form-control type-bg" aria-describedby="emailHelp" id="limCount" type="number" name="">
								</div>
								</form>
								</td>
								</tr>
								</tbody>
								</table> *}		
								<!-------------------------->		
			
							</div>
				
							<div class="user manual pull-right">
								<p>
									<button type="button" onclick="updateService();" class="btn btn-default btn-flat margin">Update</button>
								</p>
							</div>
							
						</div>
					</div>

					<div class="box box-solid">
					
						<div class="box-header with-border">
							<h3>Limitation List</h3>
							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"	title="Collapse">
									<i class="fa fa-minus"></i>
								</button>
							</div>
						</div>
						
						<div class="box-body">
							<form method="post" name="limForm" id="limForm">
								<div class="col-md-8 user-edit">
									<table class="table table-hover table-responsive services m-t-25">
										<tbody>
										
											<tr>
												<th>Fields</th>
												<th>Value</th>
											</tr>
											
											<tr>						
												<td>Expiry Date</td>
												<td scope="row">
													<div class="form-group">
														<input type="date" class="form-control type-bg" name="expirationDate" id="expirationDate">
													</div>
													<div id="error_date" style="color:red;"></div>
												</td>
											</tr>
											
											<tr>
												<td>Number of Venues</td>
												<td scope="row">
													<div class="form-group">
														<input type="number" class="form-control type-bg" name="numberOfVenues" id="numberOfVenues" data-added-venues="{$totalVenuesAdded}">
													</div>
													<div id="error_venue" style="color:red;"></div>
												</td>
											</tr>											
											<tr>
												<td>Number of Competitors</td>
												<td scope="row">
													<div class="form-group">
														<input type="number" class="form-control type-bg" name="numberOfCompetitors" id="numberOfCompetitors" data-added-comps="{$totalCompsAdded}">
													</div>
													<div id="error_comp" style="color:red;"></div>
												</td>
											</tr>
											{ *											
											<tr>
												<td>Number of Services</td>
												<td scope="row">
													<div class="form-group">
														<input type="number" class="form-control type-bg" name="numberOfServices" id="numberOfServices" readonly>
													</div>
												</td>
											</tr>
											
											<tr>
												<td>Venue Names</td>
												<td scope="row">
													<div class="form-group">
														<textarea class="form-control type-bg" name="limName[]" id="limName"></textarea>
													</div>
												</td>
											</tr>
											
											<tr>
												<td>Venue Types</td>
												<td scope="row">
													<div class="form-group">
														<select id="limType" name="limType[]" class="form-control type-bg" multiple="multiple"><option value="" disabled>Limited To</option></select>
													</div>
												</td>
											</tr>
											
											<tr>
												<td>Venue Address</td>
												<td scope="row">
													<div class="form-group">
														<textarea class="form-control type-bg" name="limAddress[]" id="limAddress" readonly placeholder="Not Available"></textarea>
													</div>
												</td>
											</tr>
											
											<tr>
												<td>Venue Areas</td>
												<td scope="row">
													<div class="form-group">
														<textarea class="form-control type-bg" name="limArea[]" id="limArea" readonly placeholder="Not Available"></textarea>
													</div>
												</td>
											</tr>
										*}
										</tbody>
									</table>
								</div>
							
								<div class="user manual pull-right">
									<p>
										<button type="button" onclick="updateLimitationToUser();" class="btn btn-default btn-flat margin">Apply Limit</button>
									</p>
								</div>
							
							</form>
						</div>
					</div>
				</div>
				<!--</div></div>-->
			</section>
			
			<!-------------->
			{* <div class="modal fade" id="Modal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
			<h5 class="modal-title tservices" id="exampleModalLabel"></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
			</div>
			<div  class="modal-body sevicelist text-center">
			<ul id="logos5">
			</ul>
			</div>
			<div class="modal-footer">
			<button type="button" class="btn btn-secondary tcancel" data-dismiss="modal">Cancel</button>
			<button type="button" class="btn btn-secondary" onclick="addSource();">Submit</button>
			</div>
			</div>
			</div>
			</div> *}
			<!-- /.content -->
			
		</div>
		<!-- /.content-wrapper -->

		<footer class="main-footer">
			<div class="pull-right d-none d-sm-inline-block"></div>Copyright &copy; 2018 <a href="https://www.datastitute.fr/">Spella Corp</a>.All Rights Reserved.
		</footer>
		<!-- ./wrapper -->

		<!-- jQuery 3 -->
		<script src="../assets/vendor_components/jquery/dist/jquery.min.js"></script>

		<!-- popper -->
		<script src="../assets/vendor_components/popper/dist/popper.min.js"></script>

		<!-- Bootstrap v4.0.0-beta -->
		<script src="../assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

		<!-- SlimScroll -->
		<script src="../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

		<!-- FastClick -->
		<script src="../assets/vendor_components/fastclick/lib/fastclick.js"></script>
		
		<!-- Multi select -->
		<script src="assets/vendor_components/multi-select/js/jquery.multi-select.js"></script>
		
		<!-- maximum_admin App -->
		<script src="js/template.js"></script>

		<!-- maximum_admin for demo purposes -->
		<script src="js/demo.js"></script>
		
		<!-- Dijo's scripts -->
		<script src="js/templates/edit_user.js"></script>
		
	</body>
</html>
