<?php
include('general_path.php');

include(ABS_PATH . 'classes/curl.php');
if ($_POST) {
	$action = $_POST['action'];

	switch ($action) {
		case 'updateUser':
			$curl = new curl();
			$data = $_POST;
			$output = $curl->curl_call($data);
			echo $output;
			break;

		case 'getServices':
			$output = getServices();
			echo $output;
			break;
			
		case 'listProducts':
			$output = get_products();
			echo $output;
			break;
			
		case 'poseAsUser' :
			$curl= new curl();
			$data= $_POST;
			$output= $curl->curl_call($data);
			echo $output;
			break;
			
		case 'default':
			echo "nothing of note";
			exit;
			break;		
	}
}


function getServices()
{
	$output = file_get_contents("https://api.spella.com/services/services.json");

	// echo "12525645";exit;
	if ($output) {
		return $output;
	} else {
		echo "no data";
	}
}

function getVenueCount($user_id)
{
	$curl = new curl();

	$params = [
		'action' => 'getVenues',
		'userId' => $user_id
	];

	$venues = json_decode($curl->curl_call($params), true);

	return count($venues['result']['graph']);
}

function getCompetitorCount($user_id)
{
	$curl = new curl();

	$params = [
		'action' => 'getVenues',
		'userId' => $user_id
	];

	$venues = json_decode($curl->curl_call($params), true);

	return ((count($venues['result']['venues']))-(count($venues['result']['graph'])));
}

function get_products()
{

	$data = [
		'action' => 'listProducts'
	];

	$url = 'https://api.spella.com/1/listProducts';
	$ch = curl_init();
	
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$return = curl_exec($ch);
	curl_close($ch);

	return $return;
}