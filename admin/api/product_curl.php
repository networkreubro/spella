<?php
include('general_path.php');

include(ABS_PATH . 'classes/curl.php');

if ($_POST) {
  $action = $_POST['action'];

  switch ($action) {

    case 'listProducts':
      $curl = new curl();
      $data = $_POST;
      $output = $curl->curl_call($data);
      echo $output;
      break;
    case 'updateProduct':
      $curl = new curl();
      $data = $_POST;
      $output = $curl->curl_call($data);
      echo $output;
      break;
    case 'deleteProduct':
      $curl = new curl();
      $data = $_POST;
      $output = $curl->curl_call($data);
      echo $output;
      break;
    case 'createProduct':
      $curl = new curl();
      $data = $_POST;
      $output = $curl->curl_call($data);
      echo $output;
      break;
    case 'default':
      echo "nothing of note";
      exit;
      break;
  }
}


function list_products()
{
  $curl = new curl();

  $data['action'] = "listProducts";
  $data['adminId'] = $_SESSION['admin_user_id'];

  return json_decode($curl->curl_call($data), true);
}
