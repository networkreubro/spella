<?php
require_once('smarty-2.6.31/libs/Smarty.class.php');
include("general.php");
include("login_header.php");

$smarty= new Smarty();
$smarty->template_dir='templates';
$smarty->compile_dir='tmp';


if(!isset($_COOKIE['admin_user'])||$_COOKIE['admin_user']=='bad') {

	header("location: validate.php");

} 

if(!empty($_SESSION['admin_user_id']))
{
	header("location: home.php");
}

if($_POST)
{

	if($_POST['status']=='success')
	{
		$_SESSION['admin_user_id']		=$_POST['result']['userId'];
		$_SESSION['admin_user_cellphone']	=$_POST['result']['cellphone'];		
		$_SESSION['admin_role']			=$_POST['result']['role'];
		$_SESSION['admin_name']			=$_POST['result']['name'];
		$_SESSION['admin_countryCode']	=$_POST['result']['countryCode'];
		$_SESSION['admin_pushActive']		=$_POST['result']['pushActive'];
		$_SESSION['admin_email']			=$_POST['result']['email'];
		exit;
	}
	else
	{
		$message="error empty data";
	}
}

$smarty->display('register.tpl');

?>





