<?php
require_once('smarty-2.6.31/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->template_dir = 'templates';
$smarty->compile_dir = 'tmp';
include("general.php");
require_once "api/getVenue_curl.php";

if (empty($_SESSION['user']['user_id'])) {
	header("location: index.php");
}


if ($_POST['request'] == 'getdata') {
	echo json_encode($_SESSION['user']);
	exit;
}
$productid =  $_SESSION['user']['productSubscribed'];
$products = json_decode(get_products(), true)['result'];

// set numberOfVenues = 1 and numberOfCompetitors = 0 for trial plan or unsubscribed users
$numberofcompet = 0;
$no_of_venues = 1;

foreach ($products as $x => $val) {
	if ($productid == $val['id']) {
		$numberofcompet = $val['numberOfCompetitors'];
		$no_of_venues = $val['numberOfVenues'];
	}
}

include("home_header.php");
include("home_body_header.php");

$days_remaining = strtotime($_SESSION['user']['productExpiry']) - strtotime(Date('Y-m-d'));
$days_remaining = $days_remaining / 60 / 60 / 24; // converting to days

$smarty->assign('product_status', $_SESSION['user']['product_status']);
$smarty->assign('plan_expiry_days', $days_remaining);

// if number of venues/competitors set from admin side take it, else take them according to plan
if($_SESSION['user']['numberOfCompetitors']) {
	$numberofcompet = $_SESSION['user']['numberOfCompetitors'];
}

if($_SESSION['user']['numberOfVenues']) {
	$no_of_venues = $_SESSION['user']['numberOfVenues'];
}
 
$smarty->assign('no_of_venues', $no_of_venues);
$smarty->assign('no_of_venues_added', getVenueCount($_SESSION['user']['user_id']));
$smarty->assign('no_of_competitors', $numberofcompet);
$smarty->assign('no_of_competitors_added', getCompetitorCount($_SESSION['user']['user_id']));
$smarty->assign('productid', $_SESSION['user']['productSubscribed']);
$smarty->display('home.tpl');
