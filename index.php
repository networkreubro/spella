<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('smarty-2.6.31/libs/Smarty.class.php');
include("general.php");
include("login_header.php");

$smarty = new Smarty();
$smarty->template_dir = 'templates';
$smarty->compile_dir = 'tmp';

// print_r($_SESSION);
// exit;

// if (!empty($_SESSION['user']['user_id'])) {
// 	if (Date('Y-m-d') <= Date($_SESSION['user']['productExpiry']) || $_SESSION['user']['product_status'] == 'trial') {
// 		header('location: pricing.php');
// 		exit;
// 	}
// 	header('location: home.php');
// 	exit;
// }
if ($_POST) {
	if ($_POST['status'] == 'success') {
		
		$_SESSION['user']['user_id']				= $_POST['result']['userId'];
		$_SESSION['user']['user_cellphone']			= $_POST['result']['cellphone'];
		$_SESSION['user']['role']					= $_POST['result']['role'];
		$_SESSION['user']['name']					= $_POST['result']['name'];
		$_SESSION['user']['countryCode']			= $_POST['result']['countryCode'];
		$_SESSION['user']['pushActive']				= $_POST['result']['pushActive'];
		$_SESSION['user']['email']					= $_POST['result']['email'];
		$_SESSION['user']['preferredLanguage']		= $_POST['result']['preferredLanguage'];
		$_SESSION['user']['reportThreshold']		= $_POST['result']['reportThreshold'];
		$_SESSION['user']['reportDaily']			= $_POST['result']['reportDaily'];
		$_SESSION['user']['reportWeekly']			= $_POST['result']['reportWeekly'];
		$_SESSION['user']['reportWeekDay']			= $_POST['result']['reportWeekDay'];
		$_SESSION['user']['reportRange']			= $_POST['result']['reportRange'];
		$_SESSION['user']['dynGroupAccess']			= $_POST['result']['dynGroupAccess'];
		$_SESSION['user']['product_status']			= $_POST['result']['status'];
		$_SESSION['user']['productSubscribed']	   	= $_POST['result']['productId'];
		$_SESSION['user']['productExpiry']	       	= $_POST['result']['expirationDate'];
		$_SESSION['user']['autorenew']	       		= $_POST['result']['autorenew'];
		$_SESSION['user']['services']	       		= $_POST['result']['services'];
		$_SESSION['user']['subscribedDate']	       	= $_POST['result']['subscribedDate'];
		$_SESSION['user']['numberOfVenues']	       	= $_POST['result']['numberOfVenues'];
		$_SESSION['user']['numberOfCompetitors']	= $_POST['result']['numberOfCompetitors'];

		// stripe details
		$_SESSION['stripe']['email']				= $_POST['result']['stripeEmail'];
		$_SESSION['stripe']['stripe_customer']		= $_POST['result']['stripeCustomerId'];

		// $_SESSION['stripe']['email']				= $_POST['result']['stripeEmail'];
		// $_SESSION['stripe']['stripe_customer']		= "cus_GtFrlUFY6Su04X"; //for testing.

		$_SESSION['service']['maxNumberOfServices']	= $_POST['result']['maxNumberOfServices'];
		$_SESSION['service']['allowedSerivces']		= $_POST['ressult']['allowedServices'];
		exit;
	} else {
		$message = "error empty data";
	}
}
$smarty->display('index.tpl');
