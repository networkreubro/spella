<?php

error_reporting(E_ALL);
ini_set("display_error", true);

include("general.php");
require_once "api/getVenue_curl.php";
// require_once "api/add_venue_curl.php";

if (empty($_SESSION['user']['user_id'])) {
	header("location: index.php");
}

if ($_POST['request'] == 'getdata') {
	echo json_encode($_SESSION['user']);
	exit;
}
$count = 0;
$productid = $_SESSION['user']['productSubscribed'];
$products = json_decode(get_products(), true)['result'];
$productidlist = array();
$productnamelist = array();

// set numberOfVenues = 1 and numberOfCompetitors = 0 for trial plan or unsubscribed users
$numberofcompet = 0;
$no_of_venues = 1;

// if product/plan is selected take counts accordingly and get current product
// 
foreach ($products as $x => $val) {
	if ($val['id'] == $productid) {
		$current_product = $val;
		$numberofcompet = $val['numberOfCompetitors'];
		$no_of_venues = $val['numberOfVenues'];
	}
}

foreach ($products as $x => $val) {
	array_push($productidlist, $val['id']);
	array_push($productnamelist, $val['name-en']);
	$count++;
}
// echo "<pre>";
// print_r($_SESSION['user']);
// exit;

// array key value without -  as smarty cannot print - keys
$current_product['name'] = $current_product['name-en'];

require_once('smarty-2.6.31/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->template_dir = 'templates';
$smarty->compile_dir = 'tmp';



$days_remaining = strtotime($_SESSION['user']['productExpiry']) - strtotime(Date('Y-m-d'));
$days_remaining = $days_remaining / 60 / 60 / 24; // converting to days

// echo  "<pre>";
// print_r($_SESSION);
// exit;



// include('templates/loader.html');
include("home_header.php");
include("home_body_header.php");

$smarty->assign('product_status', $_SESSION['user']['product_status']);
$smarty->assign('productid', $_SESSION['user']['productSubscribed']);
$smarty->assign('productidlist', $productidlist);
$smarty->assign('productnamelist', $productnamelist);
$smarty->assign('count', $count);

$smarty->assign('current_product', $current_product);
$smarty->assign('plan_expiry_days', $days_remaining);
$smarty->assign('services', $_SESSION['user']['services']);

// if number of venues/competitors set from admin side take it, else take them according to plan
if($_SESSION['user']['numberOfCompetitors']) {
	$numberofcompet = $_SESSION['user']['numberOfCompetitors'];
}

if($_SESSION['user']['numberOfVenues']) {
	$no_of_venues = $_SESSION['user']['numberOfVenues'];
}

$smarty->assign('no_of_venues', $no_of_venues);
$smarty->assign('no_of_venues_added', getVenueCount($_SESSION['user']['user_id']));
$smarty->assign('numberofcompet', $numberofcompet);
$smarty->assign('no_of_competitors', getCompetitorCount($_SESSION['user']['user_id']));

$smarty->display('add_venue.tpl');
