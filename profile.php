<?php
include("general.php");
session_start();

if ($_POST['request'] == 'getdata') {
	if (!empty($_SESSION['user'])) {
		echo json_encode($_SESSION['user']);
		exit;
	} else {
		echo "false";
		exit;
	}
}

if ($_POST['action'] == "getdata") {
	if (!empty($_SESSION['user'])) {
		echo "true";
		exit;
	}
	echo "false";
	exit;
}

if (empty($_SESSION['user']['user_id'])) {
	header("location: index.php");
}



require_once('smarty-2.6.31/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->template_dir = 'templates';
$smarty->compile_dir = 'tmp';

// include('templates/loader.html');
include("home_header.php");
include("home_body_header.php");
$smarty->display('profile.tpl');
