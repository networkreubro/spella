<?php
session_start();
include("general.php");


require_once('smarty-2.6.31/libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->template_dir = 'templates';
$smarty->compile_dir = 'tmp';

// include('templates/loader.html');
include("home_header.php");


// $smarty->assign('plan_expiry', "20-11-2019");

// plan details
if (isset($_SESSION['user'])) {
    // echo "<pre>";
    // print_r($_SESSION);
    // exit;

    $days_remaining = strtotime($_SESSION['user']['productExpiry']) - strtotime(Date('Y-m-d'));
    $days_remaining = $days_remaining / 60 / 60 / 24; 
    if($days_remaining <= 0 ){
    $days_remaining = 0;
    }// converting to days

    $smarty->assign('product_status', $_SESSION['user']['product_status']);
    $smarty->assign('name', $_SESSION['user']['name']);
    $smarty->assign('productid', $_SESSION['user']['productSubscribed']);
    $smarty->assign('plan_expiry_days', $days_remaining);
}

$smarty->display('pricing.tpl');
