<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
<link rel="stylesheet" type="text/css" media="all" href="style.css" />
<title>6. Notifications</title>

</head>

<body>

<p><img src="images/Spella_logo.png" alt="Spella"></p>

<h1 id="toc_0">6. Notifications</h1>

<p><strong>This section describes the notification mechanism provided by the server.</strong></p>
<ul>
<li><a href="README.html">Back to Main Menu</a></li>
</ul>

<h2 id="toc_1">General description</h2>

<p>Spella&#39;s purpose is to allow the user to monitor the data from its client. Whenever the data changes, the client must be notified appropriately by the server.</p>

<p>The notification method should be picked by the server according to varying parameters:</p>

<ul>
<li>The <strong>client type</strong>:

<ul>
<li>iOS</li>
<li>Android</li>
<li>web</li>
</ul></li>
<li>The <strong>notification type</strong>:

<ul>
<li>notification with a message,</li>
<li>silent notification.</li>
</ul></li>
</ul>

<h2 id="toc_2">Client type</h2>

<p>The client type is indicated to the server by the client itself, using the <code>clientInfo</code> API described in chapter 4. The contents of this info is very different among clients.</p>

<p>For example, a web client might need to establish a socket as notification channel, or a PHP session, or whatever technique is deemed appropriate on the server side.</p>

<p>An iOS client will need to use Apple&#39;s Push Notification Service (APNS), thus the client info will mainly contain the Apple token required to push toward the user&#39;s devices.</p>

<p>See the API specifications for more details on <code>clientInfo</code>.</p>

<blockquote>
<p>IMPORTANT: It is important to keep in mind that a user can be logged-in simultaneously with <strong>more than one client</strong>, and <strong>more than one client types</strong>. An extreme case could be a user owning an Android phone, an iPad, and also looking at the web client page. Any data update should be notified to all the clients.</p>
</blockquote>

<p>Certain types need to be handled in a completely asynchronous manner. For example, if the server has received a <code>clientInfo</code> for an <strong>iOS client</strong>, the APNS token is recorded in the database and can be used without regard of the user&#39;s actual current status of connection. Apple will deliver the notification to the user&#39;s device(s) in appropriate time, or not (the user may have turned off notifications entirely).</p>

<p>On the contrary, other types such as the <strong>web client</strong> might rely on an active session or socket, and only work in the user is synchronously connected on the page.</p>

<h2 id="toc_3">Notification type</h2>

<p>Considering that a notification can be very different if it is an Apple push notification or a web-socket based one, <strong>this document describes an abstract notification object</strong>. The server should create and set the concrete object required for each client type.</p>

<p>This abstract notification can have a <strong>message</strong> and a <strong>payload</strong>. The message is a UTF-8 string intended for the user. The payload is also a UTF-8 string containing JSON data.</p>

<p>The message is optional. A notification that has no message is called a <strong>silent notification</strong>. It is intended for the client app only and not directly visible by the user.</p>

<blockquote>
<p>Note: APNS has dedicated settings for silent notifications, which should be used for iOS clients. See <a href="https://developer.apple.com/library/content/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/APNSOverview.html">Apple&#39;s APNS documentation</a> for details.</p>

<p><strong>Important</strong>: make sure you do not confuse the <em>abstract payload</em> described here with the <em>APNS payload</em>, which actually also contains the message string. <strong>The JSON examples below do not include the message string</strong>, however in APNS case, it would be added using Apple&#39;s <code>alert</code> field, as well as other elements such as the <code>sound</code> name (always <code>Vib.m4a</code>, standard for Spella iOS), <code>badge</code> value (always identical to our abstract badge counter, see below), a default <code>category</code> (always <code>INVITE_CATEGORY</code>), the payload presence flag (<code>content_available</code> = true), and finally the actual JSON payload in the <code>custom_data</code> field. See Apple&#39;s doc mentioned above for details.</p>
</blockquote>

<p>Our abstract notification has a JSON payload that, at least, contains the <code>graphVersion</code> and <code>class</code> fields, <strong>both required</strong>.</p>

<h2 id="toc_4">Handling the Badge</h2>

<p>In mobile apps, there is a standard relationship between the received notifications and the <strong>numeric badge</strong> that appears laid over the application&#39;s icon. This is widely used to convey instant information to the user and show how many notifications occurred since the last time the application was activated.</p>

<p>Considering the multi-device, multi-platform architecture of Spella clients and server, <strong>it is the server that performs the counting</strong>: a <code>badgeCounter</code> field is globally attached to the user and is sent along with the non-silent notifications in the appropriate places.</p>

<p>This value is incremented by the server before sending any non-silent notification. Beginning at zero, it means that the first notification sent for a user will set the badge to 1. Every instance of <strong>that</strong> notification will share that same value: if the user owns both an iPhone and an Android tablet, he will receive the badge at 1 on both. The following event (the next venue update) will make the counter increment again to 2, etc.</p>

<p>This <code>badgeCounter</code> value is included into any non-silent notification&#39;s payload. If the target is iOS and the notification channel is APNS, the <code>badge</code> value in Apple&#39;s field is used instead, and the payload&#39;s <code>badgeCounter</code> can be omitted. (However it is acceptable to duplicate the value if keeping the same payload for all platforms is simpler.)</p>

<h3 id="toc_5">Resetting the badge</h3>

<p>The <code>badgeValue</code> is reset to zero anytime one of the user&#39;s clients calls any API function on the server. By definition, any API call means that the client is &quot;awake&quot;, so the badge must be reset on all of the user&#39;s devices. As soon as the server is called by any device pertaining to a user, all the notifications are considered &quot;seen&quot; and done.</p>

<p>This &quot;badge reset&quot; must be notified in turn to all the user&#39;s devices, by sending to each device a dedicated silent notification:</p>

<div><pre><code class="language-none">{
    &quot;class&quot;: &quot;badgeResetNotification&quot;,
    &quot;graphVersion&quot;: 42
}</code></pre></div>

<p>This special notification is silent, has no message and no particular payload besides the required fields. It must be sent to all the user&#39;s devices. The device that initiated the reset by calling the server can optionnally be skipped.</p>

<p>Upon receiving that silent notification, the client should reset the badge. As a result, a user action on any of his devices will instantly reset the badge on all his devices.</p>

<h2 id="toc_6">What to notify and how</h2>

<p>As a rule of thumb, the notifications should be sent to the client using the available mechanism with the best chances of reaching the user. <strong>For iOS devices, Apple Push Notification Service (APNS) should be used to send the notifications.</strong></p>

<h3 id="toc_7">Graph version change</h3>

<p>Any time the graph version is updated from some server-side cause, and <strong>as long as this server-side cause has not already triggered a more specific notification</strong>, this minimal notification should be sent. It should also be sent whenever a client adds or deletes a venue, so other devices using the same account can stay in sync.</p>

<p>This notification is silent: it has no message and no particular payload besides the required fields. The client might take a refresh action according to its particular situation and status, and optionally alert the user if deemed appropriate.</p>

<div><pre><code class="language-none">{
    &quot;class&quot;: &quot;graphVersionChangeNotification&quot;,
    &quot;graphVersion&quot;: 42
}</code></pre></div>

<h3 id="toc_8">Update in a venue</h3>

<p>If a level-1 venue (i.e. a first-level, not a competitor venue) has some new data, a notification should be sent with a payload describing the update.</p>

<p>This notification contains a localized message such as &quot;<em>venueName</em> changed.&quot; The language for localization is the user&#39;s language as described in the <code>clientInfo</code>API.</p>

<blockquote>
<p>Note: As various devices pertaining to the same user may be set to different languages, this implies that the message sent to each device should use the <code>clientInfo</code> language for that particular device and pick the appropriate localization.</p>
</blockquote>

<p>The payload contains the minimum data required to find out about the update:</p>

<div><pre><code class="language-none">{
    &quot;class&quot;: &quot;venueDataUpdateNotification&quot;,
    &quot;graphVersion&quot;: 42,
    &quot;venueId&quot;: &quot;7416DAAA-9107-4A29-A0D2-0EB4A8594BDF&quot;,
    &quot;venueVersion&quot;: 55
}</code></pre></div>

<p>Upon receiving this notification, the client is responsible for taking an appropriate action such as calling <code>venueData</code> to get the new data and update its display.</p>

<p>The <code>clientInfo</code> call allows each device to specify its desired notifications classes. Consequently, this level-1 notification should only be sent if the devices requests it. Each device may have a different setting for each class of notifications.</p>

<p>When this notification is sent, there is no need to send the <code>graphVersionChangeNotification</code> for the same data update. On the contrary, if the device <code>clientInfo</code> is set to <strong>not send</strong> this  <code>venueDataUpdateNotification</code>, the silent <code>graphVersionChangeNotification</code> should be sent instead.</p>

<h3 id="toc_9">Update in a competitor</h3>

<p>If a level-2 venue (i.e. a competitor of a level-1 venue) has some new data, a notification should be sent with appropriate message and payload. The overall mechanism is the same as level-1, with the following format:</p>

<div><pre><code class="language-none">{
    &quot;class&quot;: &quot;competitorDataUpdateNotification&quot;,
    &quot;graphVersion&quot;: 42,
    &quot;venueId&quot;: &quot;7416DAAA-9107-4A29-A0D2-0EB4A8594BDF&quot;,
    &quot;venueVersion&quot;: 55
}</code></pre></div>

<p>As for level-1, when this notification is sent, there is no need to send the <code>graphVersionChangeNotification</code> for the same data update. On the contrary, if the device <code>clientInfo</code> is set to <strong>not send</strong> this <code>competitorDataUpdateNotification</code>, the silent <code>graphVersionChangeNotification</code> should be sent instead.</p>

<blockquote>
<p>Important: The user can set each of his devices (and web client) to get whatever classes of notifications he desires: all, only venues and no competitors, nothing at all. These settings can be different among devices for the same user.</p>
</blockquote>

<h2 id="toc_10"></h2>




</body>

</html>
