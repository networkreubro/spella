<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
<link rel="stylesheet" type="text/css" media="all" href="style.css" />
<title>9. Venue editor</title>

</head>

<body>

<p><img src="images/Spella_logo.png" alt="Spella"></p>

<h1 id="toc_0">9. Venue editor</h1>

<p><strong>This section describes the venue editor.</strong></p>

<blockquote>
<p>IMPORTANT: <strong>using the venueSearch API is discouraged</strong>. It is not deprecated, however the clients are strongly encouraged to move to this new <strong>venueCreate API</strong>.</p>
</blockquote>

<p>Contents:</p>

<ul>
<li><a href="README.html">Back to Main Menu</a></li>
<li>Venue Editor

<ul>
<li><a href="#toc_2">Workflow</a></li>
<li><a href="#toc_6">Source services</a></li>
<li><a href="#toc_8">venueService</a></li>
</ul></li>
</ul>

<h2 id="toc_1">General description</h2>

<p>A venue is defined by the following elements:</p>

<ul>
<li>A <strong>name</strong>. The name is entered by the user and can be different from the actual venue&#39;s name on the sources sites. Its main purpose is to allow users to differentiate the venues in their list, by customizing the name with complete freedom, adding an area or city name, or whatever they like.</li>
<li>A <strong>type</strong>. The type is also entered by the user, and is used to specialize the dictionary of keywords used by topic analysis. For example, a restaurant has no use for the keyword <code>room</code>.</li>
<li>A list of zero or more <strong>source pages</strong>, expressed as an array of <code>venueService</code> structures.</li>
</ul>

<p><img src="images/venue-ed-1.png" alt="Venue"></p>

<h2 id="toc_2">Workflow of venue editor</h2>

<p>The venue creation (<code>createVenue</code>) and edition (<code>updateVenue</code>) is identical except for the action verb called in API at the end.</p>

<blockquote>
<p><strong>Important</strong> — The venue editor has no impact on the user&#39;s graph. In particular, creating a venue does not inserts it in the graph. It is the responsability of the client to call <code>updateGraph</code> appropriately after creating a venue, to insert it either as a first-level venue, or as another venue&#39;s competitor.</p>
</blockquote>

<p>The venue editor offers 4 steps in the workflow:</p>

<p>— The client presents a form where the user can edit the venue name, select the venue type and display the pages if any:</p>

<p><img src="images/venue-ed-2.png" alt="Venue"></p>

<h2 id="toc_3"></h2>

<p>— When clicking or tapping a <code>+</code> button (or <code>Add Page</code>), a list of available sources is presented and the user must pick one to continue (or cancel and return to form):</p>

<p><img src="images/venue-ed-3.png" alt="Venue"></p>

<h2 id="toc_4"></h2>

<p>— A query is performed to try and find the relevant pages in the source, according to the venue name. The results are presented as a list to the user, as well as a link to the source&#39;s home page:</p>

<p><img src="images/venue-ed-4.png" alt="Venue"></p>

<h2 id="toc_5"></h2>

<p>— When the user clicks/taps a page name, or the home page on top, the client presents a <strong>web view</strong> containing the page. From that point on, the user can navigate within the source&#39;s web site. <strong>HTTP requests toward other sites are blocked.</strong> Any page visited is tested with a call to <code>validatePage</code>, to see if the server recognizes the page as &quot;scrapable&quot;. If the page is valid, the client presents a confirmation button such as <code>Use This Page</code> to the user. Clicking that button creates the venueService (see below) and adds it to the list of pages of the venue, then returns to the form.</p>

<p><img src="images/venue-ed-5.png" alt="Venue"></p>

<blockquote>
<p><strong>Important</strong> — When the client calls <code>validatePage</code>, the server may return a different URL to propose another page for the venue in the service. In such a case, the client should display another button such as <code>See the proposed page</code>. Clicking this button would navigate to the page and display the <code>Use This Page</code> button immediately.</p>
</blockquote>

<p>Finally, from the initial form, the user can click a <code>Save</code> button to save the venue and close the editor. If the form is modal, covering the normal workflow of the app like in the iOS client, a <code>Cancel</code> button should be also provided.</p>

<h2 id="toc_6">Source services</h2>

<p>The list of available sources is available on the server in a JSON file located at <a href="">https://spella.com/services/services.json</a>. The client should always refer to that file and check its version within if its contents is cached.</p>

<p>The file describes each source (&quot;service&quot;), indicating for each source the following attributes:</p>

<table>
<thead>
<tr>
<th>Attribute</th>
<th>Description</th>
</tr>
</thead>

<tbody>
<tr>
<td>id</td>
<td>the UUID of the source</td>
</tr>
<tr>
<td>keywords</td>
<td>an array of keywords (see below)</td>
</tr>
<tr>
<td>shortName</td>
<td>a short name to be used as key or tag whenever needed</td>
</tr>
<tr>
<td>displayName</td>
<td>the name displayed to the user, properly capitalized</td>
</tr>
<tr>
<td>icon</td>
<td>the icon file name (see below)</td>
</tr>
<tr>
<td>homePage</td>
<td>the home page of the service</td>
</tr>
<tr>
<td>domainKey</td>
<td>the partial domain used to filter HTTP requests</td>
</tr>
<tr>
<td>searchKey</td>
<td>to designate the search domain in the initial query</td>
</tr>
</tbody>
</table>

<p>Example of a single service, extracted from the JSON file:</p>

<div><pre><code class="language-none">{
        &quot;id&quot;: &quot;3d4f5a85-5e9b-4d48-b981-645d41fa80b0&quot;,
        &quot;keywords&quot;: [&quot;hotel&quot;, &quot;restaurant&quot;, &quot;booking&quot;, &quot;travel&quot;, &quot;attractions&quot;, &quot;cars&quot;, &quot;business&quot;],
        &quot;shortName&quot;: &quot;facebook&quot;,
        &quot;displayName&quot;: &quot;Facebook&quot;,
        &quot;icon&quot;: &quot;facebook.png&quot;,
        &quot;homePage&quot;: &quot;https://facebook.com&quot;,
        &quot;domainKey&quot;: &quot;facebook&quot;,
        &quot;searchKey&quot;: &quot;facebook.com&quot;
}</code></pre></div>

<p>The <strong>icon</strong> of the service is also available from the server and should always be taken there. It is available in two forms:</p>

<ul>
<li>The fully-shaped logo: <code>https://spella.com/services/logos-large/(icon file name)</code></li>
<li>The squared-shaped icon: <code>https://spella.com/services/logos-small/(icon file name)</code></li>
<li>The discoloured, grey, smaller versions: <code>https://spella.com/services/logos-mini/(icon file name)</code></li>
</ul>

<h4 id="toc_7">For copyright-related reasons, it is recommended to use only the &#39;mini&#39; versions.</h4>

<blockquote>
<p>Note: do not use the images located within the /services directory itself. They are misplaced, deprecated and will be removed soon. Only use the images from the subdirectories indicated above.</p>
</blockquote>

<p>If the client stores the images in a local cache, it should invalidate the cache if the JSON file&#39;s version changes.</p>

<p>The <strong>keywords</strong> can help differenciate services. They are not used in the iOS app at this point, but might be used to sort services in a meaningful way for a category of users, or present only a partial list of services in certain contexts. The keywords are actually the keys in a dictionary where the values are also dictionaries, into which each key is a language key, and the value is the localized version of the keyword. Of course, the localized version should only be used for display purposes, and any use of the keyword as a key or a keyword should use the unlocalized version.</p>

<h2 id="toc_8">venueService</h2>

<p>A venueService is the result of the user selecting pages in a service. It is a structure with several fields:</p>

<table>
<thead>
<tr>
<th>Attribute</th>
<th>Description</th>
</tr>
</thead>

<tbody>
<tr>
<td>serviceId</td>
<td>the UUID of the source, as indicated in the <code>services.json</code> file</td>
</tr>
<tr>
<td>serviceVenueURL</td>
<td>the full URL of the page to scrap</td>
</tr>
<tr>
<td>serviceVenueUUID</td>
<td>the UUID of the venueService (see below)</td>
</tr>
<tr>
<td>serviceEnabled</td>
<td>boolean, defaults to true</td>
</tr>
</tbody>
</table>

<p>Example in JSON:</p>

<div><pre><code class="language-none">{
  &quot;serviceId&quot;: &quot;012db315-100e-459a-a716-9dc513346f68&quot;,
  &quot;serviceVenueURL&quot;: &quot;https://www.tripadvisor.com/Hotel_Review-g187147-d197650-Reviews-Ibis_Paris_Gare_du_Nord_Chateau_Landon-Paris_Ile_de_France.html&quot;,
  &quot;serviceVenueUUID&quot;: &quot;BB69E236-4543-4D75-93F1-3780016FB886&quot;,
  &quot;serviceEnabled&quot;: true
}
</code></pre></div>

<p>When a new page is added by the user, the venueService is created by the client. The <code>serviceVenueUUID</code> is <strong>generated by the client</strong> when the venueService is created.</p>

<p><code>serviceEnabled</code> is always <code>true</code> for the moment, however it is possible that in a future version, each page in a venue might be enabled or disabled by user, allowing to preserve a page parameters without actually using it. The server takes this flag into account to calculate the venue&#39;s statistics.</p>

<h2 id="toc_9">Search algorithm in client software</h2>

<p>In the process of building the list of proposed pages, the client software should take precautions to avoid overtaxing search engines and reaching their daily limit of queries.</p>

<p>To perform a Google search, the client can use <code>searchWeb</code> in the API. This search can be performed globally for all services, or targeting a particular service. See the API documentation of this call for details. These calls are in blue in the diagram below.</p>

<p>The DuckDuckGo search is efficient to perform a site-targeted search. It returns easily parsed HTML. The query can be built directly from the venue name and the service&#39;s <code>searchKey</code> attribute, for example:</p>

<p><code>https://duckduckgo.com/html/?q=hotel%20ibis%20paris+site%3afr.hotels.com</code></p>

<p>... in which spaces and punctuations are properly escaped. This call is in green in the diagram.</p>

<p>This algorithm describes the iOS app implementation. It is appropriate in that context because caching the initial Google search avoids repeating it for each service. A web-based client might prefer to do this in a different way. User actions are in yellow.</p>

<p><img src="images/venue-ed-6.png" alt="Venue"></p>




</body>

</html>
